import { createTerminus } from '@godaddy/terminus';
import mongoose from 'mongoose';
import { log } from '../logging/app-logger.js';

const options = {
    healthChecks: {
        '/readyz': readinessCheck,
        '/livez': livenessCheck
    },
    onSignal,
    beforeShutdown
}

function configureTerminus(server) {
    createTerminus(server, options);
}

function readinessCheck() {
    if (mongoose.connection.readyState == 1)
        return Promise.resolve('db connection ok');
    else
        return Promise.reject('db not connected');
}

function livenessCheck() {
    return Promise.resolve('living');
}

function onSignal() {
    log.info('server is starting cleanup');
    return mongoose.disconnect();
}

function beforeShutdown() {
    log.info('server shutting down');
    return new Promise(resolve => {
        setTimeout(resolve, 5000);
    });
}

export { configureTerminus }