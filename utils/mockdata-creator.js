import { Course } from '../models/course-model.js';
import cprConnection from '../models/cprConnection-model.js';
import Lesson from '../models/lesson-model.js';
import Period from '../models/period-model.js';
import Pupil from '../models/pupil-model.js';
import { RatingScheme } from '../models/ratingscheme-model.js';
import Record from '../models/record-model.js';
import User from '../models/user-model.js';

import readJson from './readJson.js';

const parentFilepath = 'misc/mock-data'
const userFilepath = `${parentFilepath}/users.json`
const periodFilepath = `${parentFilepath}/periods.json`
const coursesFilepath = `${parentFilepath}/courses.json`
const pupilFilepath = `${parentFilepath}/pupils.json`
const cprConnectionFilepath = `${parentFilepath}/cprconnections.json`
const ratingschemeFilepath = `${parentFilepath}/ratingschemes.json`
const lessonFilepath = `${parentFilepath}/lessons.json`
const recordFilepath = `${parentFilepath}/records.json`

const createMockData = async (req, res) => {
    // users
    let users = await readJson(userFilepath);
    for (let u of users) {
        let user = new User(u);
        await user.save();
    }

    // periods
    let periods = await readJson(periodFilepath);
    for (let p of periods) {
        let period = new Period(p);
        await period.save();
    }

    // courses
    let courses = await readJson(coursesFilepath);
    for (let c of courses) {
        let course = new Course(c);
        await course.save();
    }

    // pupils
    let pupils = await readJson(pupilFilepath);
    for (let p of pupils) {
        let pupil = new Pupil(p);
        await pupil.save();
    }

    // cprConnections
    let cprConnections = await readJson(cprConnectionFilepath);
    for (let cpr of cprConnections) {
        let cprConn = new cprConnection(cpr);
        await cprConn.save();
    }

    // ratingSchemes
    let ratingSchemes = await readJson(ratingschemeFilepath);
    for (let ratingscheme of ratingSchemes) {
        let rs = new RatingScheme(ratingscheme);
        await rs.save();
    }

    // lessons
    let lessons = await readJson(lessonFilepath);
    for (let lesson of lessons) {
        let ls = new Lesson(lesson);
        await ls.save();
    }

    // records
    let records = await readJson(recordFilepath);
    for (let record of records) {
        let r = new Record(record);
        await r.save();
    }

    res.sendStatus(204);
};

export default createMockData;