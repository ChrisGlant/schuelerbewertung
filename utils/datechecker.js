const dateRegexpressions = [
    (/\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/),   // full ISO-Date
    (/^\d{4}[/\-.](0?[1-9]|1[012])[/\-.](0?[1-9]|[12][0-9]|3[01])$/)                // international Date format (yyyy[.-/].mm[.-/].dd)
];

const checkDate = (date) => {
    return dateRegexpressions.some(regex => {
        return regex.test(date);
    });
}

export default checkDate;