import mongoose from 'mongoose';
import AppError from '../errorHandling/AppError.js';

const ObjectId = mongoose.Types.ObjectId;

export const checkId = (id) => {
  if (typeof(id) !== 'string')
    id = id.toString();

  if (!ObjectId.isValid(id))
    throw new AppError(400, `${id} is not an ObjectId`);
};

export const checkForCredentials = (data) => {
  if (typeof data != 'object')
    throw new AppError(401, 'checkForCredentials - data is not a object');
  if (Object.keys(data).length != 2)
    throw new AppError(401, 'checkForCredentials - data has invalid key length');
  if (data.username == undefined || data.password === undefined)
    throw new AppError(401, 'checkForCredentials - one or more keys are undefined')
}
