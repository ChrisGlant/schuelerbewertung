import fs from 'fs';

function readJson(path) {
    return new Promise((resolve, reject) => {
        try {
            let data = fs.readFileSync(path);
            resolve(JSON.parse(data));
        } catch (err) {
            reject('error: ' + err);
        }
    })
}

export default readJson;