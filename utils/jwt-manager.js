import dotenv from 'dotenv';
import jwt from 'jsonwebtoken';

import AppError from '../errorHandling/AppError.js'

const options = {
    expiresIn: '12h'
}

let blackList = [];

dotenv.config();

function getJWTToken(payload) {
    if (typeof payload != 'object' && typeof payload != 'string')
        throw new AppError(401, 'jwt manager - payload is not an object');

    if (typeof payload == 'string')
        payload = { usr: payload };


    payload.iat = Date.now();
    return jwt.sign(payload, process.env.TOKEN_SECRET, options)
}

function verifyJWTToken(token) {
    jwt.verify(token, process.env.TOKEN_SECRET);

    if (blackList.find(t => t === token))
        throw new AppError(401, 'jwt manager - token is blacklisted');
}

function decodeJWTToken(token) {
    return jwt.decode(token, process.env.TOKEN_SECRET);
}

function blackListToken(token) {
    if (!blackList.find(t => t === token))
        blackList.push(token);
}

function getTokenFromReq(req) {
    return req.headers.authorization?.replace(/^Bearer\s+/, "");
}

function getCurrentUserId(req) {
    return decodeJWTToken(getTokenFromReq(req)).usr;
}

export { getJWTToken, verifyJWTToken, decodeJWTToken, getTokenFromReq, blackListToken, getCurrentUserId };