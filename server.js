import express from 'express';
import dotenv from 'dotenv';
import http from 'http'

import { connectDB } from './dal/db-connector.js';
import { log } from './logging/app-logger.js';
import { configure, configureInErrorMode } from './app/app-loader.js';
import { convertToBoolean } from './utils/convert.js';
import { configureTerminus } from './utils/terminus.js';

dotenv.config();

const hostname = process.env.HOST;
const port = process.env.PORT;
const mongoConnectionString = process.env.MONGO_CONNECTION_STRING;
const recreateDatabase = convertToBoolean(process.env.RECREATE_DATABASE);
const dbConnectTimeout = process.env.DB_CONNECT_TIMEOUT;

const app = express();
const server = http.createServer(app);

app.hostname = hostname;

const main = async () => {
    log.info(`server started. starting webserver ...`);
    configureTerminus(server);
    server.listen(port, hostname, async () => {
        try {
            log.success(`Web Server up and running at: ${hostname}:${port}.`);
            log.info('Trying to connect to database ...');

            await connectDB(mongoConnectionString, dbConnectTimeout, recreateDatabase);

            if (recreateDatabase) {
                log.warning('Current Database dropped');
            }
            log.success(`DBConnect to ${mongoConnectionString} successful`);

            configure(app);
            log.info(`Gradeservice Web Application successfully configured`);
        }
        catch (err) {
            configureInErrorMode(app);
            log.error(
                `Chat Web Application running in error-mode.\n` +
                `There were startup-problems. App is not healthy!\n` +
                err,
            );
        }
    });
}

main();

export default app;