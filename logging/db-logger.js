import { DbLogEntry } from '../models/logEntry-model.js';
import User from '../models/user-model.js';

const defaultNumberOfLogsPerLogtype = 10;
const numberOfLogsPerLogtype = defaultNumberOfLogsPerLogtype; // to-do: add usage of env-variable

const dbLogTypes = {
    login: 'login',
    create: 'create',
    update: 'update',
    delete: 'delete'
}

const dbLogger = {
    log: logToDb,
    logTypes: dbLogTypes
}

async function logToDb(userId, logType, message, affectedEntity) {
    let newLogEntry = createLogEntryObject(logType, message, affectedEntity);

    await deleteOldestLogIfNecessary(userId, logType);
    await insertNewLog(userId, logType, newLogEntry);
}

async function insertNewLog(userId, logType, newLog) {
    let pathToColl = `logEntries.${dbLogTypes[logType]}`;
    await User.updateOne({ _id: userId },
        { $push: { [pathToColl]: newLog } }, { upsert: true });
}

async function deleteOldestLogIfNecessary(userId, logType) {
    let pathToColl = `logEntries.${dbLogTypes[logType]}`;
    let oldestLog = await getOldestLogIfNecessary(userId, logType);

    if (oldestLog) {
        await User.updateOne({ _id: userId },
            { $pull: { [pathToColl]: oldestLog } });
    }
}

async function getOldestLogIfNecessary(userId, logtype) {
    let oldestLog = null;
    let selectedUser = await User.findById(userId, 'logEntries');
    if (selectedUser.logEntries[dbLogTypes[logtype]].length >= numberOfLogsPerLogtype)
        oldestLog = getOldestDocument(selectedUser.logEntries[dbLogTypes[logtype]]);

    return oldestLog;
}

function getOldestDocument(collection) {
    return collection.sort((first, second) => {
        return new Date(first.logTimestamp) - new Date(second.logTimestamp);
    })[0];
}

function createLogEntryObject(logType, message, entity) {
    let timestamp = new Date();
    return new DbLogEntry({
        logType: logType,
        logTimestamp: timestamp,
        logMessage: message,
        affectedEntity: entity
    })
}

export { dbLogger }