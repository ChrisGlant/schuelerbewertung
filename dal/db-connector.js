import mongoose from 'mongoose';
import User from '../models/user-model.js';
import { Course } from '../models/course-model.js';
import Record from '../models/record-model.js';
import Pupil from '../models/pupil-model.js';
import { RatingScheme } from '../models/ratingscheme-model.js';
import cprConnection from '../models/cprConnection-model.js';
import { pswrdReset } from '../models/pswrdReset-model.js';

const initAllModels = async () => {
  await User.init();
  await Course.init();
  await Pupil.init();
  await cprConnection.init();
  await RatingScheme.init();
  await Record.init();
  await pswrdReset.init();
};

export const connectDB = async (
  connectionString,
  dbConnectTimeout,
  recreateDatabase,
) => {
  let dbConn = await mongoose.createConnection(connectionString, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    serverSelectionTimeoutMS: dbConnectTimeout,
  });

  if (recreateDatabase) {
    await dbConn.dropDatabase();
  }

  await mongoose.connect(connectionString, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: true,
    serverSelectionTimeoutMS: dbConnectTimeout,
  });

  if (recreateDatabase) {
    await initAllModels();
  }
};
