FROM node:14-alpine

# base dir for the application
WORKDIR /usr/src/app

# copy over the package information
COPY package*.json ./

# and install the dependencies
RUN npm ci --only=production

# copy over the rest of the app but ignore node_modules etc.
COPY . .

EXPOSE 8080

CMD [ "node", "server.js" ]
