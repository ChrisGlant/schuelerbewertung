import express from 'express';
import AppError from '../errorHandling/AppError.js';
import { handleAsyncError } from '../errorHandling/error-handler.js';
import { RatingScheme, Rating } from '../models/ratingscheme-model.js';

import { checkId } from '../utils/payload-checker.js';

const router = express.Router();

const selectionFields = '_id name ratings weight';
const validRatingSchemeKeys = Object.keys(RatingScheme.base.modelSchemas.RatingScheme.obj).filter(key => key != 'userId');
const validRatingKeys = Object.keys(Rating.base.modelSchemas.Rating.obj);
validRatingSchemeKeys.push('_id');
validRatingKeys.push('_id');

router.get('/', handleAsyncError(async (req, res, next) => {
    let schemes = await RatingScheme.find({ userId: req.currentUserId }, selectionFields);

    res.status(200).send(schemes);
}));

router.get('/:id', handleAsyncError(async (req, res, next) => {
    checkId(req.params.id);

    let ratingScheme = await select(req.params.id, req.currentUserId);

    res.status(200).send(ratingScheme);
}));

router.post('/', handleAsyncError(async (req, res, next) => {
    if (Object.getOwnPropertyNames(req.body).some(prop => !validRatingSchemeKeys.includes(prop) || prop == '_id')) {
        throw new AppError(400, 'Ratingscheme contains not allowed property!');
    }

    if (req.body.ratings) {
        checkRatings(req.body.ratings);
    }

    let schemeToAdd = new RatingScheme(req.body);
    schemeToAdd.userId = req.currentUserId;

    await schemeToAdd.save();

    delete schemeToAdd._doc.userId;
    res.status(201).send(schemeToAdd);
}));

router.patch('/:id', handleAsyncError(async (req, res, next) => {
    checkId(req.params.id);
    let updates = req.body;

    if (Object.keys(updates).length === 0)
        throw new AppError(400, 'nothing to update!');

    if (updates._id != undefined && updates._id != req.params.id)
        throw new AppError(400, 'updating of ratingscheme id not permitted!');

    let ratingschemeToUpdate = await select(req.params.id, req.currentUserId);

    if (updates.userId && ratingschemeToUpdate.userId != updates.userId)
        throw new AppError(400, 'updating of userId not permitted!');
    delete updates.userId;

    if (req.body.ratings) {
        checkRatings(req.body.ratings, ratingschemeToUpdate.ratings);
    }

    for (let key in updates) {
        if (!validRatingSchemeKeys.includes(key))
            throw new AppError(400, `updating of not existing field '${key}' not permitted!`);
        ratingschemeToUpdate[key] = updates[key];
    }

    await ratingschemeToUpdate.save();

    delete ratingschemeToUpdate._doc.userId;
    res.status(200).send(ratingschemeToUpdate);
}));

router.delete('/:id', handleAsyncError(async (req, res, next) => {
    checkId(req.params.id);
    let schemeToDelete = await select(req.params.id, req.currentUserId);
    await schemeToDelete.delete();

    res.status(204).send();
}));

function checkRatings(ratings, existingRatings) {
    if (!Array.isArray(ratings))
        throw new AppError(400, 'Ratings of ratingscheme must be provided in an array!');

    if (ratings.some(r => Object.getOwnPropertyNames(r).some(prop => !validRatingKeys.includes(prop))))
        throw new AppError(400, 'Rating contains not allowed property!');

    let ratingContainsId = ratings.some(r => {
        if (r._id) {
            if (existingRatings && existingRatings.some(er => er._id == r._id))
                return false;
            return true;
        }
        return false;
    })
    if (ratingContainsId)
        throw new AppError(400, 'Rating must not contain an id!');
}

const selectRatingById = async (ratingId, userId) => {
    let ratingsOfScheme = await RatingScheme.findOne({ userId: userId, 'ratings._id': ratingId }, 'ratings');
    let ratingToSearch = ratingsOfScheme && ratingsOfScheme.ratings.find(r => r._id == ratingId);
    if (ratingsOfScheme == null || ratingToSearch == null)
        throw new AppError(404, `Rating with id ${ratingId} not found!`);
    return ratingToSearch;
};

const select = async (id, currentUserId) => {
    let ratingscheme = await RatingScheme.findOne({ _id: id, userId: currentUserId }, selectionFields);
    if (ratingscheme == null) throw new AppError(404, `Ratingscheme with id: ${id} not found.`);

    return ratingscheme;
}

export { router as ratingschemeRouter, selectRatingById }