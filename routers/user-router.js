import express from 'express';
import dotenv from 'dotenv';

import User from '../models/user-model.js';
import AppError from '../errorHandling/AppError.js';
import { handleAsyncError } from '../errorHandling/error-handler.js';
import { checkId } from '../utils/payload-checker.js'
import { decodeJWTToken, getTokenFromReq, blackListToken } from '../utils/jwt-manager.js'
import { LogEntries } from '../models/logEntry-model.js';

const router = express.Router();

const selectionFields = '_id username email password lastname firstname state';
const validKeys = Object.keys(User.base.modelSchemas.User.obj);
validKeys.push('_id');

dotenv.config();

router.post('/', handleAsyncError(async (req, res, next) => {
    let body = req.body;
    if (Object.getOwnPropertyNames(body).some(prop => !validKeys.includes(prop))) {
        throw new AppError(400, 'User contains not allowed property!');
    }

    if (body._id !== undefined)
        throw new AppError(400, 'creating with id is not allowed');

    if(body.username == "")
        delete body.username;

    let userToAdd = new User(body);
    userToAdd.logEntries = new LogEntries();
    await userToAdd.save();

    delete userToAdd._doc.password;
    delete userToAdd._doc.logEntries;

    res.status(201).send(userToAdd);
}));

router.get('/:id', handleAsyncError(async (req, res, next) => {
    checkId(req.params.id);

    if (decodeJWTToken(getTokenFromReq(req)).usr !== req.params.id)
        throw new AppError(401, 'user-router - not authorized')

    let user = await selectById(req.params.id);
    delete user._doc.password;
    res.status(200).send(user);
}));

router.delete('/:id', handleAsyncError(async (req, res, next) => {
    checkId(req.params.id);

    let token = getTokenFromReq(req);
    if (decodeJWTToken(token).usr !== req.params.id)
        throw new AppError(401, 'user-router - not authorized')

    let userToDelete = await selectById(req.params.id);
    await userToDelete.delete();
    blackListToken(token);

    res.status(204).send();
}));

router.patch('/:id', handleAsyncError(async (req, res, next) => {
    checkId(req.params.id);
    let updates = req.body;

    if (decodeJWTToken(getTokenFromReq(req)).usr !== req.params.id)
        throw new AppError(401, 'user-router - not authorized')

    if (updates._id && updates._id != req.params.id)
        throw new AppError(400, 'updating of id not permitted!');

    if (Object.keys(updates).length === 0)
        throw new AppError(400, 'nothing to update');

    let userToUpdate = await selectById(req.params.id);

    for (let key in updates) {
        if (!validKeys.includes(key))
            throw new AppError(400, 'updating of not existing field not permitted!');
        userToUpdate[key] = updates[key];
    }
    await userToUpdate.save();

    delete userToUpdate._doc.password;

    res.status(200).send(userToUpdate);
}));

async function selectById(id) {
    checkId(id);

    let user = await User.findById(id, selectionFields);
    if (user == null)
        throw new AppError(404, `User with id: ${id} not found.`);

    return user;
}

async function selectByFilter(filter) {
    if (typeof filter != 'object')
        throw new AppError(400, 'userrouter - filter is not an object')

    let user = await User.findOne(filter, selectionFields);
    if (user == null)
        throw new AppError(404, `userrouter - user with filter: ${JSON.stringify(filter)} not found`);

    return user;
}

async function selectByToken(token) {
    const decodedToken = decodeJWTToken(token);
    return await selectById(decodedToken.usr);
}

export { router as userrouter, selectById, selectByFilter, selectByToken };