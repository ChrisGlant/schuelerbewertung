/* ************************************************************************* */
/*                            record-router.js                               */
/*  HTTP Endpoints for the record - REST API                                 */
/*                                                                           */
/*  Method  |  url                                                           */
/*  GET     |  /                                                             */
/*  GET     |  /:id                                                          */
/*  POST    |  /                                                             */
/*  PATCH   |  /:id                                                          */
/*  DELETE  |  /:id                                                          */
/*                                                                           */
/* ************************************************************************* */

import express from 'express';
import AppError from '../errorHandling/AppError.js';
import { handleAsyncError } from '../errorHandling/error-handler.js';
import { Rating } from '../models/ratingscheme-model.js';
import Record from '../models/record-model.js';
import checkDate from '../utils/datechecker.js';
import { checkId } from '../utils/payload-checker.js';
import { selectCPRConnection, selectCPRConnectionByPupilAndCourse } from './cprConnection-router.js';
import { checkPupilExists } from './pupil-router.js';
import { selectRatingById } from './ratingscheme-router.js';
import { selectCPRConnectionByFilter } from './cprConnection-router.js';

const router = express.Router();

const recordSelectionFields = '_id cprId rating weight validOn pupilNotPresent comment tags';
const validRecordKeys = Object.keys(Record.base.modelSchemas.Record.obj).filter(key => key != 'userId');
const validRatingKeys = Object.keys(Rating.base.modelSchemas.Rating.obj);
const validBulkAddKeys = ['courseId', 'validOn', 'weight', 'records', 'tags'];
const validBulkAddRecordKeys = ['pupilId', 'ratingId', 'comment', 'pupilNotPresent', 'tags'];
const validQueryStringsGetRecords = ['cprId', 'pupilId', 'courseId'];
const validQueryStringsGetGrade = ['pupilId', 'courseId', 'multiple'];

validRecordKeys.push('_id');
validRatingKeys.push('_id');

router.get('/grade', handleAsyncError(async (req, res, next) => {
    // check for query (min 1)
    if (Object.keys(req.query).length == 0)
        throw new AppError(400, 'Please provide a query-string!');

    if (Object.keys(req.query).some(q => !validQueryStringsGetGrade.includes(q)))
        throw new AppError(400, 'Invalid query-string! Valid queries are: ' + validQueryStringsGetGrade);

    if (Object.keys(req.query).some(q => (q == 'pupilId' || q == 'courseId') && checkId(req.query[q]) && q != 'multiple'))
        throw new AppError(400, 'Invalid Value for query!');

    // build filter object
    let { courseId, pupilId } = req.query;
    let filter = { courseId, pupilId }
    Object.keys(filter).map(key => {
        if (filter[key] == undefined)
            delete filter[key]
    });

    let cprs = (await selectCPRConnectionByFilter(filter)).map(cpr => cpr._doc);

    let grades = await getGradeSuccs(cprs, req.query.multiple === 'true');

    res.status(200).send(grades)
}))

async function getGradeSuccs(cprs, multiple = false) {
    // map over every cpr connection
    for (let cpr of cprs) {
        // delete records
        delete cpr.records;
        // get all records for cpr
        let records = await selectRecordsByCprId(cpr._id)
        delete cpr._id

        if (records.length == 0 || records.every(r => r.pupilNotPresent == true))
            cpr.notEnoughData = true
        else {
            // map over overy record and build grade
            let weightSum = 0
            let propGradeSum = 0;
            let tempGrades = [];
            for (let record of records) {
                if (record.pupilNotPresent)
                    continue
                weightSum += record.weight;
                propGradeSum += record.rating.value * record.weight;

                // if multipe query
                // set grade succ for every new record
                if (multiple)
                    tempGrades.push({ grade: propGradeSum / (weightSum == 0 ? 1 : weightSum), date: record.validOn })
            }

            // if multiple query
            // set every grade
            if (multiple) {
                cpr.grade = tempGrades;
            } else {
                // else set grade for every cpr connection
                cpr.grade = propGradeSum / (weightSum == 0 ? 1 : weightSum);
            }
        }
    }
    return cprs;
}

router.get('/', handleAsyncError(async (req, res, next) => {
    let filterObject = { userId: req.currentUserId }

    if (Object.keys(req.query).length == 0)
        throw new AppError(400, 'Please provide a query-string!');

    if (Object.keys(req.query).some(q => !validQueryStringsGetRecords.includes(q)))
        throw new AppError(400, 'invalid query key')

    Object.keys(req.query).forEach(key => checkId(req.query[key]));

    let cprs = await getCprIdsOfCourseOrPupil(req.query.courseId, req.query.pupilId)
    cprs = cprs.map(cpr => cpr._id)

    if (req.query.cprId != undefined)
        cprs.push(req.query.cprId)

    filterObject.cprId = { $in: cprs };

    let records = await Record.find(filterObject, recordSelectionFields);

    res.status(200).send(records);
}));

router.get('/:id', handleAsyncError(async (req, res, next) => {
    checkId(req.params.id);
    let record = await selectRecordById(req.params.id, req.currentUserId);

    res.status(200).send(record);
}));

router.post('/', handleAsyncError(async (req, res, next) => {
    if (Object.getOwnPropertyNames(req.body).some(prop => !validRecordKeys.includes(prop) || prop == '_id')) {
        throw new AppError(400, 'Record contains not allowed property! ' + JSON.stringify(req.body));
    }
    validateRecordDate(req.body.validOn);
    if (req.body.cprId == undefined || req.body.cprId == null)
        throw new AppError(400, 'missing property: cprId')
    checkId(req.body.cprId);

    if (req.body.tags == null || req.body.tags == undefined)
        delete req.body.tags;
    else
        checkTags(req.body.tags);

    let record = new Record(req.body);

    if (record._doc.pupilNotPresent) {
        if (record._doc.records || record._doc.weight) {
            throw new AppError(400, 'Cannot add rating or record weight when pupil is not present!');
        }
    } else {
        checkRating(record.rating);
        if (!record._doc.weight) {
            throw new AppError(400, 'Weight of record is missing!');
        }
    }

    let cprConn = await selectCPRConnection(record.cprId);
    record.userId = req.currentUserId;
    await record.save();
    await addRecordToCPRConnection(cprConn, record._id);

    delete record._doc.userId;
    res.status(201).send(record);
}));

router.post('/bulkadd', handleAsyncError(async (req, res, next) => {
    let response = [];
    if (Object.getOwnPropertyNames(req.body).some(prop => !validBulkAddKeys.includes(prop)))
        throw new AppError(400, 'Payload contains not allowed property!');

    if (!req.body.courseId)
        throw new AppError(400, 'missing property: courseId');
    checkId(req.body.courseId);
    validateRecordDate(req.body.validOn);

    if (!req.body.weight)
        throw new AppError(400, 'Weight of records is missing!');

    if (req.body.tags != null && req.body.tags != undefined)
        checkTags(req.body.tags);
    else
        req.body.tags = [];

    let preparedRecords = await validateRecords(req.body.records, req.body.courseId, req.currentUserId);

    for (let r of preparedRecords) {
        let tagsOfRecord = req.body.tags.concat(r.tags.filter(t => req.body.tags.indexOf(t) < 0));
        let recordObj = new Record({
            userId: req.currentUserId,
            cprId: r.cprConn._id,
            rating: r.rating,
            weight: req.body.weight,
            validOn: req.body.validOn,
            pupilNotPresent: r.pupilNotPresent || false,
            comment: r.comment,
            tags: tagsOfRecord
        });

        await recordObj.save();
        await addRecordToCPRConnection(r.cprConn, recordObj._id);
        delete recordObj._doc.userId;
        response.push(recordObj);
    }

    res.status(201).send(response);
}));

router.patch('/:id', handleAsyncError(async (req, res, next) => {
    checkId(req.params.id);
    let updates = req.body;

    let recordToUpdate = await selectRecordById(req.params.id, req.currentUserId);

    if (Object.keys(updates).length === 0)
        throw new AppError(400, 'nothing to update!');

    if (updates._id != undefined && updates._id != req.params.id)
        throw new AppError(400, 'updating of record id is not permitted!');

    if (updates.userId && updates.userId != recordToUpdate.userId)
        throw new AppError(400, 'updating of userid is not permitted!');
    delete updates.userId;

    if (updates.cprId && updates.cprId != recordToUpdate.cprId)
        throw new AppError(400, 'record cannot be moved to another course/pupil!');

    if (updates.validOn != undefined)
        validateRecordDate(updates.validOn);

    if (req.body.tags == null || req.body.tags == undefined)
        req.body.tags = [];
    else
        checkTags(req.body.tags);

    for (let key in updates) {
        if (!validRecordKeys.includes(key))
            throw new AppError(400, `non existing field ${key} cannot be updated!`);
        recordToUpdate[key] = updates[key];
    }

    await recordToUpdate.save();

    delete recordToUpdate._doc.userId;
    res.status(200).send(recordToUpdate);
}));

router.delete('/:id', handleAsyncError(async (req, res, next) => {
    checkId(req.params.id);

    let recordToDelete = await selectRecordById(req.params.id, req.currentUserId);

    let cprConn = await selectCPRConnection(recordToDelete.cprId);
    await recordToDelete.delete();
    await removeRecordFromCPRConnection(cprConn, recordToDelete._id);

    res.status(204).send();
}));

const selectRecordById = async (id, currentUserId) => {
    let record = await Record.findOne({ _id: id, userId: currentUserId }, recordSelectionFields);
    if (record == null) throw new AppError(404, `Record with id: ${id} not found.`);

    return record;
}

const selectRecordsByCprId = async (cprId) => {
    let records = await Record.find({ cprId }, recordSelectionFields);
    if (records == null) throw new AppError(404, `Records with cprId: ${cprId} not found.`);

    return records;
}

const checkRating = (rating) => {
    if (rating == undefined)
        throw new AppError(400, 'Rating is invalid')

    if (typeof rating._doc !== 'object') {
        throw new AppError(400, 'Rating of record needs to be of type rating!');
    }
    if (Object.getOwnPropertyNames(rating._doc).some(prop => !validRatingKeys.includes(prop))) {
        throw new AppError(400, 'Rating of record contains not allowed property!');
    }
}

const addRecordToCPRConnection = async (cprConn, recordId) => {
    cprConn.records.push(recordId);
    await cprConn.save();
}

const removeRecordFromCPRConnection = async (cprConn, recordId) => {
    cprConn.records.splice(cprConn.records.indexOf(recordId), 1);
    await cprConn.save();
}

const validateRecordDate = (date) => {
    if (!checkDate(date)) {
        throw new AppError(400, 'Date of record is not valid!');
    }
}

const validateRecords = async (recordsArray, courseId, userId) => {
    let adjustedRecords = [];
    if (!Array.isArray(recordsArray))
        throw new AppError(400, 'Records need to be provided in an array!');

    for (let r of recordsArray) {
        if (Object.getOwnPropertyNames(r).some(prop => !validBulkAddRecordKeys.includes(prop)))
            throw new AppError(400, 'Record of bulkadd contains not allowed property!');

        if (!r.pupilId)
            throw new AppError(400, 'missing property: pupilId');
        checkId(r.pupilId);
        checkPupilExists(r.pupilId, userId);

        r.cprConn = await selectCPRConnectionByPupilAndCourse(r.pupilId, courseId);

        if (r.tags != null && r.tags != undefined)
            checkTags(r.tags);
        else
            r.tags = [];

        if (r.pupilNotPresent) {
            if (r.ratingId)
                throw new AppError(400, 'No rating can be given if pupil is not present!');
        }
        else {
            if (!r.ratingId)
                throw new AppError(400, 'missing property: ratingId');
            checkId(r.ratingId);

            r.rating = await selectRatingById(r.ratingId, userId);
        }
        adjustedRecords.push(r);
    }
    return adjustedRecords;
}

const getCprIdsOfCourseOrPupil = async (courseId, pupilId) => {
    if (courseId == undefined && pupilId == undefined)
        return []

    // TODO: check resposne if one is undefined
    let filter = { $or: [{ 'courseId': courseId }, { 'pupilId': pupilId }] };

    return await selectCPRConnectionByFilter(filter);
}

const checkTags = (tagsArray) => {
    if (!Array.isArray(tagsArray) || tagsArray.some(t => typeof t != 'string'))
        throw new AppError(400, 'invalid tags array');
}


export { router as recordRouter }