/* ************************************************************************* */
/*                             period-router.js                              */
/*  HTTP Endpoints for the period - REST API                                 */
/*                                                                           */
/*  Method  |  url                                                           */
/*  GET     |  /                                                             */
/*  GET     |  /:id                                                          */
/*  POST    |  /                                                             */
/*  PATCH   |  /:id                                                          */
/*  DELETE  |  /:id                                                          */
/*                                                                           */
/* ************************************************************************* */

import express from 'express';

import Period from '../models/period-model.js';
import AppError from '../errorHandling/AppError.js';
import { handleAsyncError } from '../errorHandling/error-handler.js';
import { checkId } from '../utils/payload-checker.js';
import checkDate from '../utils/datechecker.js';
import { dbLogger } from '../logging/db-logger.js';

const router = express.Router();

const states = {
    active: 'active',
    inactive: 'inactive'
};

const selectionFields = '_id label startdate enddate state';
const validKeys = Object.keys(Period.base.modelSchemas.Period.obj).filter(key => key != 'userId');
validKeys.push('_id');

router.get('/', handleAsyncError(async (req, res, next) => {
    let periods = await Period.find({ userId: req.currentUserId }, selectionFields);

    res.status(200).send(periods);
}));

router.get('/:id', handleAsyncError(async (req, res, next) => {
    checkId(req.params.id);

    let period = await select(req.params.id, req.currentUserId);

    res.status(200).send(period);
}));

router.post('/', handleAsyncError(async (req, res, next) => {
    if (Object.getOwnPropertyNames(req.body).some(prop => !validKeys.includes(prop) || prop == '_id')) {
        throw new AppError(400, 'Period contains not allowed property!');
    }

    validateDates(req.body);

    let periodToAdd = new Period(req.body);
    periodToAdd.userId = req.currentUserId;

    await periodToAdd.save();
    dbLogger.log(req.currentUserId, dbLogger.logTypes.create, `Period with label '${periodToAdd.label}' was created`, 'Period');

    if (periodToAdd.state === states.active) {
        await deactivateActivePeriod(req.currentUserId, periodToAdd._id);
    }

    delete periodToAdd._doc.userId;

    res.status(201).send(periodToAdd);
}));

router.patch('/:id', handleAsyncError(async (req, res, next) => {
    checkId(req.params.id);
    let updates = req.body;

    if (Object.keys(updates) == 0)
        throw new AppError(400, 'nothing to update!');

    if (updates._id && updates._id != req.params.id)
        throw new AppError(400, 'updating of id not permitted!');

    let periodToUpdate = await select(req.params.id, req.currentUserId);

    if (updates.userId && periodToUpdate.userId != updates.userId)
        throw new AppError(400, 'updating of period userId not permitted!');
    delete updates.userId;

    validateDates(updates);

    for (let key in updates) {
        if (!validKeys.includes(key))
            throw new AppError(400, 'updating of not existing field not permitted!');
        periodToUpdate[key] = updates[key];
    }

    await periodToUpdate.save();
    dbLogger.log(req.currentUserId, dbLogger.logTypes.update, `Period with label '${periodToUpdate.label}' was updated`, 'Period');

    if (updates.state === states.active) {
        await deactivateActivePeriod(req.currentUserId, periodToUpdate._id);
    }

    delete periodToUpdate._doc.userId;

    res.status(200).send(periodToUpdate);
}));

router.delete('/:id', handleAsyncError(async (req, res, next) => {
    checkId(req.params.id);

    let periodToDelete = await select(req.params.id, req.currentUserId);
    await periodToDelete.delete();
    dbLogger.log(req.currentUserId, dbLogger.logTypes.delete, `Period with label '${periodToDelete.label}' was deleted`, 'Period');

    res.status(204).send();
}));

const select = async (id, currentUserId) => {
    checkId(id);
    checkId(currentUserId);
    let period = await Period.findOne({ _id: id, userId: currentUserId }, selectionFields);
    if (period == null) throw new AppError(404, `Period with id: ${id} not found.`);

    return period;
}

const selectActivePeriod = async (currentUserId) => {
    checkId(currentUserId);
    let period = await Period.findOne({ userId: currentUserId, state: 'active' }, selectionFields);
    if (period == null) throw new AppError(404, `Active Period for user ${currentUserId} not found.`);

    return period;
}

const deactivateActivePeriod = async (currentUserId, periodId) => {
    checkId(currentUserId);
    checkId(periodId);
    let period = await Period.findOne({ _id: { $ne: periodId }, userId: currentUserId, state: states.active }, selectionFields);
    if (period) {
        period.state = states.inactive;
        await period.save();
    }
}

const validateDates = (period) => {
    if (period.startdate && !checkDate(period.startdate))
        throw new AppError(400, `startdate "${period.startdate}" is not valid!`);

    if (period.enddate && !checkDate(period.enddate))
        throw new AppError(400, `enddate "${period.enddate}" is not valid!`);
}

export { router as periodRouter, select as selectPeriodByIdAndUser, selectActivePeriod }