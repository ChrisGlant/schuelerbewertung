import express from 'express';
import { authenticate } from '../middleware/authenticate.js';
import { metadatasetter } from '../middleware/metadatasetter.js';
import { authrouter } from './auth-router.js';
import { courseRouter } from './course-router.js';
import { cprConnectionRouter } from './cprConnection-router.js';
import { dbLogEntryRouter } from './dbLogEntry-router.js';
import { importRouter } from './import-router.js';
import { lessonRouter } from './lesson-router.js';
import { periodRouter } from './period-router.js';
import { pupilRouter } from './pupil-router.js';
import { ratingschemeRouter } from './ratingscheme-router.js';
import { recordRouter } from './record-router.js';
import { userrouter } from './user-router.js';
import createMockData from '../utils/mockdata-creator.js';
import { handleAsyncError } from '../errorHandling/error-handler.js';

const router = express.Router();

router.use(authenticate);
router.use(metadatasetter)
router.use('/', authrouter);
router.use('/users', userrouter);
router.use('/periods', periodRouter);
router.use('/courses', courseRouter);
router.use('/pupils', pupilRouter);
router.use('/cprconnections', cprConnectionRouter);
router.use('/lessons', lessonRouter);
router.use('/ratingschemes', ratingschemeRouter);
router.use('/records', recordRouter);
router.use('/logentries', dbLogEntryRouter);
router.use('/import', importRouter);

router.post('/mockdata', handleAsyncError(createMockData));

export { router as apiV1Router }