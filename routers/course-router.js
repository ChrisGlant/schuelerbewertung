/* ************************************************************************* */
/*                             course-router.js                              */
/*  HTTP Endpoints for the course - REST API                                 */
/*                                                                           */
/*  Method  |  url                                                           */
/*  GET     |  /                                                             */
/*  GET     |  /:id                                                          */
/*  POST    |  /                                                             */
/*  PATCH   |  /:id                                                          */
/*  DELETE  |  /:id                                                          */
/*                                                                           */
/* ************************************************************************* */

import express from 'express';

import { Course } from '../models/course-model.js'
import cprConnection from '../models/cprConnection-model.js'
import AppError from '../errorHandling/AppError.js';
import { handleAsyncError } from '../errorHandling/error-handler.js';
import { selectPeriodByIdAndUser } from './period-router.js'
import { checkId } from '../utils/payload-checker.js'
import { dbLogger } from '../logging/db-logger.js';

const router = express.Router();

const selectionFields = '_id label subjectLabel pupilGroupLabel notes pupils periodId';
const validKeys = Object.keys(Course.base.modelSchemas.Course.obj).filter(key => key != 'userId');
validKeys.push('pupils');
validKeys.push('_id');

router.get('/', handleAsyncError(async (req, res, next) => {
    let courses = await select(req.currentUserId, req.activePeriodId);

    res.status(200).send(courses);
}));

router.get('/:id', handleAsyncError(async (req, res, next) => {
    checkId(req.params.id);
    let course = await selectById(req.currentUserId, req.activePeriodId, req.params.id);

    res.status(200).send(course);
}));

router.post('/', handleAsyncError(async (req, res, next) => {
    let body = req.body;
    if (Object.getOwnPropertyNames(body).some(prop => !validKeys.includes(prop) || prop == 'pupils')) {
        throw new AppError(400, 'Course contains not allowed property!');
    }

    if (body._id !== undefined)
        throw new AppError(400, 'creating with id is not allowed');

    if (body.periodId !== undefined && body.periodId !== null) {
        let p = await selectPeriodByIdAndUser(body.periodId, req.currentUserId);
        if (p.state !== 'active')
            throw new AppError(400, 'course with inactive period is not allowed');
    } else if (req.activePeriodId !== undefined && req.activePeriodId !== null)
        body.periodId = req.activePeriodId;
    else
        throw new AppError(400, 'no period existing')

    body.userId = req.currentUserId;
    let courseToAdd = new Course(body);
    await courseToAdd.save();
    dbLogger.log(req.currentUserId, dbLogger.logTypes.create, `Course with label '${courseToAdd.label}' was created`, 'Course');
    res.status(201).send(courseToAdd);
}));

router.patch('/:id', handleAsyncError(async (req, res, next) => {
    checkId(req.params.id);
    let updates = req.body;

    if (Object.keys(updates) == 0)
        throw new AppError(400, 'nothing to update!');

    if (updates._id && updates._id != req.params.id)
        throw new AppError(400, 'updating of id not permitted!');

    let courseToUpdate = await selectById(req.currentUserId, req.activePeriodId || updates.periodId, req.params.id);

    if (updates.userId && courseToUpdate.userId != updates.userId)
        throw new AppError(400, 'updating of course userId not permitted!');
    delete updates.userId;

    if (updates.pupils) {
        let sendPupils = updates.pupils.filter(p => p !== null);
        let dbPupils = await cprConnection.find({ courseId: req.params.id });

        //  für jeden neuen Schüler wird eine cpr erstellt
        for (let sp in sendPupils) {
            if (!dbPupils.find(db => db.pupilId == sendPupils[sp])) {
                let newCpr = new cprConnection({ courseId: req.params.id, pupilId: sendPupils[sp] });
                await newCpr.save();
            }
        }

        // für jeden schüler der nicht mehr im sendPupils array ist wird die cpr gelöscht
        for (let dp in dbPupils) {
            if (!sendPupils.find(sp => sp == dbPupils[dp].pupilId.toString()))
                await dbPupils[dp].delete();
        }
    }

    for (let key in updates) {
        if (!validKeys.includes(key))
            throw new AppError(400, 'updating of not existing field not permitted!');
        courseToUpdate[key] = updates[key];
    }

    await courseToUpdate.save();
    dbLogger.log(req.currentUserId, dbLogger.logTypes.update, `Course with label '${courseToUpdate.label}' was updated`, 'Course');

    res.status(200).send(courseToUpdate);
}));

router.delete('/:id', handleAsyncError(async (req, res, next) => {
    checkId(req.params.id);

    let courseToDelete = await selectById(req.currentUserId, req.activePeriodId, req.params.id);
    await courseToDelete.delete();
    dbLogger.log(req.currentUserId, dbLogger.logTypes.delete, `Course with label '${courseToDelete.label}' was deleted`, 'Course');

    res.status(204).send();
}));

const selectById = async (currentUserId, activePeriodId, courseId) => {
    let course = await Course.findOne({ userId: currentUserId, periodId: activePeriodId, _id: courseId }, selectionFields);
    if (course == null) {
        course = await Course.findOne({ userId: currentUserId, periodId: null, _id: courseId })
        if (course == null)
            throw new AppError(404, `Course with userId: ${currentUserId}, periodId: ${activePeriodId}, _id: ${courseId} not found.`);
    }
    return course;
}

const selectCourseIdByLabel = async (currentUserId, activePeriodId, label) => {
    let id = await Course.findOne({ userId: currentUserId, periodId: activePeriodId, label: label }, '_id');
    return id;
}

const checkCourseExists = async (courseId, userId) => {
    let courseExists = await Course.exists({ _id: courseId, userId: userId });

    if (!courseExists)
        throw new AppError(404, `Course with id '${courseId}' not found!`);
}

const select = async (currentUserId, activePeriodId) => {
    let courses = await Course.find({ userId: currentUserId, periodId: activePeriodId }, selectionFields);
    let courseWithoutPeriod = await Course.find({ userId: currentUserId, periodId: null }, selectionFields);
    if (courses == null && courseWithoutPeriod == null) throw new AppError(404, `Courses with userId: ${currentUserId}, periodId: ${activePeriodId} not found.`);

    return courses.concat(courseWithoutPeriod);
}

export { router as courseRouter, selectById as selectCourseById, checkCourseExists, selectCourseIdByLabel }