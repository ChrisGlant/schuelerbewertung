import express from 'express';

import { handleAsyncError } from '../errorHandling/error-handler.js';
import Pupil from '../models/pupil-model.js';
import { Course } from '../models/course-model.js';
import { checkPupilKeys, selectPupilByMail } from './pupil-router.js';
import AppError from '../errorHandling/AppError.js';
import { selectCourseIdByLabel } from './course-router.js';
import cprConnection from '../models/cprConnection-model.js';
import { checkCprConnectionExists } from './cprConnection-router.js';

const router = express.Router();

const validKeys = Object.keys(Pupil.base.modelSchemas.Pupil.obj).filter(key => key != 'userId');
validKeys.push('courses');
validKeys.push('_id');
validKeys.push('courses');

const pendingStates = {
    toImport: "to-import",
    actionsNecessary: "actions-necessary"
}

router.put('/pupils', handleAsyncError(async (req, res, next) => {
    let dataToConfirm = req.body;
    let response = [];

    checkPayload(dataToConfirm);

    for (let obj of dataToConfirm) {
        let responseProp = {
            state: pendingStates.toImport,
            object: obj
        };
        let actions = await getActions(obj, req.currentUserId, req.activePeriodId);
        if (actions != null) {
            responseProp.state = pendingStates.actionsNecessary;
            responseProp.actions = actions;
        }
        response.push(responseProp);
    }

    res.status(200).send(response);
}));

router.post('/pupils/confirm', handleAsyncError(async (req, res, next) => {
    let dataToImport = req.body;
    let response = [];

    checkPayload(dataToImport);
    await validatePupils(dataToImport);

    for (let obj of dataToImport) {
        let pupil = await createOrUpdatePupil(obj, req.currentUserId);
        if (obj.courses)
            await assignPupilToCoursesIfNecessary(req.currentUserId, req.activePeriodId, obj.courses, pupil._id);
        response.push(pupil);
    }
    res.status(201).send(response);
}));

async function getActions(pupilObj, userId, activePeriodId) {
    let courseActions;
    let propActions = await getErrorAndUpdateProps(pupilObj, userId);
    if (pupilObj.courses)
        courseActions = await getCoursesToCreate(pupilObj.courses, userId, activePeriodId);

    return getActionsObject(propActions.errorActions, propActions.updateActions, courseActions);
}

async function getErrorAndUpdateProps(pupilObj, userId) {
    let props = {};
    let errorProps = {};
    let updateProps = [];

    let pupil = new Pupil(pupilObj);
    pupil.validate()
        .catch(err => {
            Object.getOwnPropertyNames(err.errors).forEach(errorKey => {
                errorProps[err.errors[errorKey].path] = err.errors[errorKey].message;
            });
        });

    let existingPupil = await selectPupilByMail(pupil.mail, userId);

    Object.getOwnPropertyNames(pupilObj).forEach(prop => {
        if (!validKeys.includes(prop) || prop == '_id') {
            errorProps[prop] = 'property not allowed!';
        }
        if (existingPupil) {
            if (existingPupil[prop] != pupil[prop])
                updateProps.push(prop);
        }
    });

    if (Object.keys(errorProps).length != 0)
        props.errorActions = errorProps;
    if (updateProps.length != 0)
        props.updateActions = updateProps;

    return props;
}

async function getCoursesToCreate(courseLabels, userId, activePeriodId) {
    let courseLabelsToCreate = [];

    for (let courseLabel of courseLabels) {
        let foundCourse = await Course.findOne({ label: courseLabel, userId: userId, periodId: activePeriodId });
        if (!foundCourse)
            courseLabelsToCreate.push(courseLabel);
    }

    return courseLabelsToCreate.length == 0 ? null : courseLabelsToCreate;
}

function getActionsObject(errorActions, updateActions, courseActions) {
    let actions = {};
    if (errorActions != null)
        actions.error = errorActions;

    if (updateActions != null)
        actions.update = updateActions;

    if (courseActions != null)
        actions.coursesToCreate = courseActions;

    return Object.getOwnPropertyNames(actions).length == 0 ? null : actions;
}

async function assignPupilToCoursesIfNecessary(userId, activePeriodId, courseLabels, pupilId) {
    let coursesToCreate = await getCoursesToCreate(courseLabels, userId, activePeriodId);
    let courseId;
    let alreadyAssigned = false;

    for (let courseLabel of courseLabels) {
        if (coursesToCreate && coursesToCreate.includes(courseLabel)) {
            courseId = await createCourse(courseLabel, userId, activePeriodId);
            alreadyAssigned = false;
        }
        else {
            courseId = await selectCourseIdByLabel(userId, activePeriodId, courseLabel);
            alreadyAssigned = await checkCprConnectionExists(courseId, pupilId);
        }

        if (!alreadyAssigned)
            await createCPRConnection(pupilId, courseId);
    }
}

async function createCourse(label, userId, activePeriodId) {
    let course = new Course({
        userId: userId,
        label: label,
        periodId: activePeriodId
    });
    await course.save();
    return course._id;
}

async function createCPRConnection(pupilId, courseId) {
    let newCpr = new cprConnection({ courseId: courseId, pupilId: pupilId });
    await newCpr.save();
}

async function createOrUpdatePupil(pupilObj, userId) {
    pupilObj.userId = userId;
    let pupil = await selectPupilByMail(pupilObj.mail, userId);
    if (!pupil) {
        pupil = new Pupil(pupilObj);
    } else {
        Object.getOwnPropertyNames(pupilObj).forEach(key => {
            pupil[key] = pupilObj[key];
        });
    }
    await pupil.save();
    delete pupil._doc.userId;
    return pupil;
}

async function validatePupils(pupils) {
    try {
        for (let pupil of pupils) {
            checkPupilKeys(validKeys, pupil);
            let pupilToValidate = new Pupil(pupil);
            await pupilToValidate.validate();
        }
    } catch (ex) {
        throw new AppError(400, 'Pupil import failed due to invalid pupil fields: ' + ex);
    }
}

function checkPayload(payload) {
    if (!Array.isArray(payload))
        throw new AppError(400, 'Payload needs to be an array!');

    if (payload.length < 1)
        throw new AppError(400, 'Payload must not be an empty array!');

    if (payload.some(elem => !isObject(elem)))
        throw new AppError(400, 'Elements of payload need to be objects!');

    if (payload.some(elem => elem.courses && !Array.isArray(elem.courses)))
        throw new AppError(400, 'Courses need to be provided in an array!');
}

function isObject(obj) {
    return typeof obj === 'object' && !Array.isArray(obj) && obj !== null;
}

export { router as importRouter }