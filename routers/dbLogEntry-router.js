import express from 'express';
import AppError from '../errorHandling/AppError.js';
import { handleAsyncError } from '../errorHandling/error-handler.js';
import { dbLogger } from '../logging/db-logger.js';
import User from '../models/user-model.js';
import { getCurrentUserId } from '../utils/jwt-manager.js';

const router = express.Router();

router.get('/', handleAsyncError(async (req, res, next) => {
    let currentUserId = getCurrentUserId(req);
    let selectionFields = '';

    if (Object.keys(req.query).some(q => !Object.keys(dbLogger.logTypes).includes(q.toLowerCase())))
        throw new AppError(400, 'Not allowed querystring provided!');

    if (Object.keys(req.query).some(q => Array.isArray(req.query[q])))
        throw new AppError(400, 'Only one value allowed per query-parameter!');

    Object.keys(req.query).forEach(q => {
        let queryLowercase = q.toLowerCase();
        let queryValue = req.query[q];
        if (queryValue != '' && queryValue.toLowerCase() != 'true' && queryValue.toLowerCase != 'false')
            throw new AppError(400, 'Queries can only be booleans!');
        selectionFields += `logEntries.${dbLogger.logTypes[queryLowercase]} `;
    });

    let logs = await getLogEntriesAsArray(currentUserId, selectionFields);

    res.status(200).send(logs);
}));

async function getLogEntriesAsArray(userId, selectionFields) {
    let allLogs = [];
    let selectedArrays = await User.findById(userId, selectionFields);
    Object.keys(selectedArrays.logEntries._doc).forEach(type => {
        allLogs = [...allLogs, ...selectedArrays.logEntries[type]];
    });
    return allLogs;
}

export { router as dbLogEntryRouter }