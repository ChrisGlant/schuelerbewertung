/* ************************************************************************* */
/*                             pupil-router.js                               */
/*  HTTP Endpoints for the pupil - REST API                                  */
/*                                                                           */
/*  Method  |  url                                                           */
/*  GET     |  /                                                             */
/*  GET     |  /:id                                                          */
/*  POST    |  /                                                             */
/*  PATCH   |  /:id                                                          */
/*  DELETE  |  /:id                                                          */
/*                                                                           */
/* ************************************************************************* */

import express from 'express';
import AppError from '../errorHandling/AppError.js';

import { handleAsyncError } from '../errorHandling/error-handler.js';
import Lesson from '../models/lesson-model.js';
import { checkId } from '../utils/payload-checker.js';
import { selectCourseById } from './course-router.js';
import moment from 'moment';

const router = express.Router();

const selectionFields = '_id courseId courseLabel starttime endtime roomNr weekday colorCode';
const validKeys = Object.keys(Lesson.base.modelSchemas.Lesson.obj).filter(key => key != 'userId');
validKeys.push('_id');

const format = 'hh:mm'

router.get('/', handleAsyncError(async (req, res, next) => {
    let lessons = await Lesson.find({ userId: req.currentUserId }, selectionFields);

    res.status(200).send(lessons);
}));

router.get('/:id', handleAsyncError(async (req, res, next) => {
    checkId(req.params.id);

    let lesson = await select(req.params.id, req.currentUserId);

    res.status(200).send(lesson);
}));

router.post('/', handleAsyncError(async (req, res, next) => {
    if (Object.getOwnPropertyNames(req.body).some(prop => !validKeys.includes(prop) || prop == '_id')) {
        throw new AppError(400, 'lesson contains not allowed property!');
    }

    await checkCourseId(req.body.courseId, req.currentUserId, req.activePeriodId);
    checkTime(req.body.starttime, req.body.endtime)

    if (!req.body.colorCode)
        delete req.body.colorCode;

    let lesson = new Lesson(req.body);
    if (await lessonDuringOtherLesson(lesson, req.currentUserId)) {
        throw new AppError(400, 'lesson cannot be during other lesson');
    }

    lesson.userId = req.currentUserId;
    await lesson.save();

    delete lesson._doc.userId;
    res.status(201).send(lesson);
}));

router.patch('/:id', handleAsyncError(async (req, res, next) => {
    checkId(req.params.id);
    let updates = req.body;

    if (Object.keys(updates).length === 0)
        throw new AppError(400, 'nothing to update!');

    if (updates._id != undefined && updates._id != req.params.id)
        throw new AppError(400, 'updating of lesson id not permitted!');

    if (!req.body.colorCode)
        delete req.body.colorCode;

    let lessonToUpdate = await select(req.params.id, req.currentUserId);

    if (updates.userId && lessonToUpdate.userId != updates.userId)
        throw new AppError(400, 'updating of lesson userId not permitted!');
    delete updates.userId;

    if (updates.courseId)
        await checkCourseId(req.body.courseId, req.currentUserId, req.activePeriodId);

    checkTime((updates.starttime != undefined) ? updates.starttime : lessonToUpdate.starttime, (updates.endtime != undefined) ? updates.endtime : lessonToUpdate.endtime);

    for (let key in updates) {
        if (!validKeys.includes(key))
            throw new AppError(400, `updating of not existing field '${key}' not permitted! (lesson)`);
        lessonToUpdate[key] = updates[key];
    }

    if (await lessonDuringOtherLesson(lessonToUpdate, req.currentUserId)) {
        throw new AppError(400, 'lesson cannot be during other lesson');
    }

    await lessonToUpdate.save();

    delete lessonToUpdate._doc.userId;
    res.status(200).send(lessonToUpdate);
}));

router.delete('/:id', handleAsyncError(async (req, res, next) => {
    checkId(req.params.id);

    let lessonToDelete = await select(req.params.id, req.currentUserId);
    await lessonToDelete.delete();

    res.status(204).send();
}));

const select = async (id, currentUserId) => {
    let timetable = await Lesson.findOne({ _id: id, userId: currentUserId }, selectionFields);
    if (timetable == null) throw new AppError(404, `Lesson with id: ${id} not found.`);

    return timetable;
}

const lessonDuringOtherLesson = async (lesson, currentUserId) => {
    let lessons = await Lesson.find({ userId: currentUserId, weekday: lesson.weekday }, selectionFields);
    if (lessons.length === 0)
        return false;
    return lessons.some((_lesson) => compareLessons(lesson, _lesson));
}

const compareLessons = (lessonToCreate, lesson) => {
    if (lessonToCreate._id.toString() === lesson._id.toString())
        return false;

    let time = moment(lessonToCreate.starttime, format),
        endtime = moment(lessonToCreate.endtime, format),
        beforeTimeCompare = moment(lesson.starttime, format),
        afterTimeCompare = moment(lesson.endtime, format);

    if (lessonToCreate.starttime === lesson.starttime || lessonToCreate.endtime === lesson.endtime)
        return true;

    if (time.isBefore(beforeTimeCompare) && endtime.isAfter(afterTimeCompare))
        return true;

    return (time.isBetween(beforeTimeCompare, afterTimeCompare) || endtime.isBetween(beforeTimeCompare, afterTimeCompare));
}

const checkCourseId = async function (courseId, currentUserId, activePeriodId) {
    if (courseId == undefined || courseId == null)
        throw new AppError(400, 'lesson contains invalid courseId!');
    checkId(courseId)
    await selectCourseById(currentUserId, activePeriodId, courseId)
}

function checkTime(starttimeraw, endtimeraw) {
    let starttime = moment(starttimeraw, format);
    let endtime = moment(endtimeraw, format);

    if (starttime.isAfter(endtime))
        throw new AppError(400, 'lesson contains invalid times!');
}

export { router as lessonRouter }