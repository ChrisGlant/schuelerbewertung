import express from 'express';
import nodemailer from 'nodemailer';
import dotenv from 'dotenv';
import crypto from 'crypto';
import fs from 'fs';

import { handleAsyncError } from '../errorHandling/error-handler.js';
import { checkForCredentials } from '../utils/payload-checker.js'
import { getJWTToken, getTokenFromReq, blackListToken } from '../utils/jwt-manager.js'
import { selectByFilter, selectById } from './user-router.js';
import AppError from '../errorHandling/AppError.js';
import { dbLogger } from '../logging/db-logger.js';
import { pswrdReset } from '../models/pswrdReset-model.js';
import { convertToBoolean } from '../utils/convert.js';

dotenv.config();

const router = express.Router();

const emailPath = './misc/email-inline.html';
const selectionFieldsReset = '_id createdAt userId recoveryToken';
const expirationSeconds = process.env.EXPIRATION_SECONDS;
const recoveryTokenLength = 20;

router.post('/login', handleAsyncError(async (req, res, next) => {
    let credentials = req.body;
    checkForCredentials(credentials);

    let user = await selectByFilter({ "$or": [{ username: credentials.username }, { email: credentials.username }] });

    if (user.password !== credentials.password)
        throw new AppError(401, 'user-router - invalid credentials');

    user.state = 'active';
    await user.save();

    const token = getJWTToken(user._id.toString());
    dbLogger.log(user._id, dbLogger.logTypes.login);
    res.status(200).json({ token });
}))

router.post('/logout', handleAsyncError(async (req, res, next) => {
    let user = await selectById(req.currentUserId)
    user.state = 'inactive';
    await user.save();
    blackListToken(getTokenFromReq(req));

    res.status(200).send('logout completed');
}))

router.get('/whoIAm', handleAsyncError(async (req, res, next) => {
    let user = await selectById(req.currentUserId);

    res.status(200).send(user);
}))

router.put('/password-reset', handleAsyncError(async (req, res, next) => {
    if (typeof req.body != 'object' || Object.keys(req.body).length != 1)
        throw new AppError(400, 'invalid request body')

    if (typeof req.body.email != 'string' || req.body.email.length < 0)
        throw new AppError(400, 'invalid email')

    // checking if user exits
    // if not ==> catch with AppError and 404
    // 202 is still send so no user existance is exposed
    let user;
    try {
        user = await selectByFilter({ email: req.body.email });
    } catch (err) {
        if (err instanceof AppError && err.statusCode == 404)
            res.status(202).send();
        else
            throw err;
    }

    // if no user exists
    if (user != null) {
        let randomHash = crypto.randomBytes(recoveryTokenLength).toString('hex');

        let newEntry = new pswrdReset({ userId: user._id, recoveryToken: randomHash });
        await newEntry.save();

        let sendEmails = convertToBoolean(process.env.SEND_EMAILS);
        if (sendEmails === true) {
            let transporter = nodemailer.createTransport({
                service: process.env.NODEMAILER_SERVICE,
                auth: {
                    user: process.env.NODEMAILER_USER,
                    pass: process.env.NODEMAILER_PASS
                }
            });

            let email = fs.readFileSync(emailPath).toString()
            email = email.replace('placeholder', process.env.FRONTEND_URL + ':' + process.env.PORT + '/ChangePW/' + newEntry.recoveryToken);

            let mailOptions = {
                from: process.env.NODEMAILER_USER,
                to: user.email,
                subject: 'Password Recovery',
                text: '',
                html: email
            };

            transporter.sendMail(mailOptions, (error, info) => console.log(error, 'email sent' + info));
        }
        res.status(202).send();
    }
}))

router.post('/password-reset', handleAsyncError(async (req, res, next) => {
    if (typeof req.body != 'object' || Object.keys(req.body).length != 2)
        throw new AppError(400, 'invalid request body')

    if (typeof req.body.new != 'string' || req.body.new.length < 0)
        throw new AppError(400, 'invalid new password')

    if (typeof req.body.recoverytoken != 'string' || req.body.recoverytoken.length / 2 != recoveryTokenLength)
        throw new AppError(400, 'invalid recoveryToken')

    let { recoverytoken, new: newPassword } = req.body;

    let reset;
    try {
        reset = await selectResetByFilter({ recoveryToken: recoverytoken })
    } catch (err) {
        if (err instanceof AppError && err.statusCode == 404)
            res.status(204).send();
        else
            throw err;
    }

    if (reset != null) {
        let created = new Date(reset.createdAt)
        let secondsBetween = (Date.now() - created.getTime()) / 1000;

        if (secondsBetween > expirationSeconds) {
            await reset.delete();
            throw new AppError(400, 'link is expired, please request a new recovery link')

        }
        let user;
        try {
            user = await selectById(reset.userId)
        } catch (err) {
            if (err instanceof AppError && err.statusCode == 404)
                res.status(204).send();
            else
                throw err;
        }

        if (user != null) {
            await reset.delete();
            user.password = newPassword;
            await user.save();
            res.status(204).send();
        }
    }
}))


async function selectResetByFilter(filter) {
    if (typeof filter != 'object')
        throw new AppError(400, 'authrouter - filter is not an object')

    let reset = await pswrdReset.findOne(filter, selectionFieldsReset);
    if (reset == null)
        throw new AppError(404, `authrouter - reset with filter: ${JSON.stringify(filter)} not found`);

    return reset;
}

export { router as authrouter };