
/* ************************************************************************* */
/*                      cprConnection-router.js                              */
/*  HTTP Endpoints for the course - REST API                                 */
/*                                                                           */
/*  Method  |  url                                                           */
/*  GET     |  /                                                             */
/*  GET     |  /:id                                                          */
/*  POST    |  /                                                             */
/*  PATCH   |  /:id                                                          */
/*  DELETE  |  /:id                                                          */
/*                                                                           */
/* ************************************************************************* */

import express from 'express';

import cprConnection from '../models/cprConnection-model.js'
import { handleAsyncError } from '../errorHandling/error-handler.js';
import { checkId } from '../utils/payload-checker.js'
import AppError from '../errorHandling/AppError.js';
import { checkCourseExists } from './course-router.js';
import { checkPupilExists } from './pupil-router.js';

const router = express.Router();

const selectionFields = '_id courseId pupilId records';
const validQueryStrings = ['pupilId', 'courseId'];

router.get('/', handleAsyncError(async (req, res, next) => {
    let cprConnections;
    let selectionObj = {};

    if (Object.keys(req.query).length == 0)
        throw new AppError(400, 'Please provide a query-string!');

    if (Object.keys(req.query).some(q => !validQueryStrings.includes(q)))
        throw new AppError(400, 'Invalid query-string! Valid queries are: ' + validQueryStrings);

    if (Object.keys(req.query).some(q => Array.isArray(req.query[q])))
        throw new AppError(400, 'Only one value allowed per query-parameter!');

    if (req.query.pupilId) {
        checkId(req.query.pupilId);
        await checkPupilExists(req.query.pupilId, req.currentUserId);
        selectionObj['pupilId'] = req.query.pupilId;
    }

    if (req.query.courseId) {
        checkId(req.query.courseId);
        await checkCourseExists(req.query.courseId, req.currentUserId);
        selectionObj['courseId'] = req.query.courseId;
    }

    cprConnections = await cprConnection.find(selectionObj, selectionFields);
    res.status(200).send(cprConnections);
}));

router.get('/:id', handleAsyncError(async (req, res, next) => {
    let _id = req.params.id;

    checkId(req.params.id);

    let cpr = await selectCPRConnectionById(_id);

    res.status(200).send(cpr);
}));

const selectCPRConnectionById = async (cprId) => {
    let cprConn = await cprConnection.findOne({ _id: cprId }, selectionFields);
    if (cprConn == null) throw new AppError(404, `CPR-Connection with id ${cprId} does not exist!`);

    return cprConn;
}

const selectCPRConnectionByPupilAndCourse = async (pupilId, courseId) => {
    let cprConn = await cprConnection.findOne({ pupilId: pupilId, courseId: courseId }, selectionFields);
    if (cprConn == null) throw new AppError(404, `Pupil ${pupilId} does not attend course ${courseId}`);

    return cprConn;
};

const selectCPRConnectionByFilter = async (filter) => {
    let cprConn = await cprConnection.find(filter, selectionFields);
    if (cprConn == null) throw new AppError(404, `Couldnt find cpr`);
    return cprConn;
}

const checkCprConnectionExists = async (courseId, pupilId) => {
    let exists = await cprConnection.exists({ pupilId: pupilId, courseId: courseId });
    return exists;
}

export { router as cprConnectionRouter, selectCPRConnectionById as selectCPRConnection, selectCPRConnectionByFilter, checkCprConnectionExists, selectCPRConnectionByPupilAndCourse }
