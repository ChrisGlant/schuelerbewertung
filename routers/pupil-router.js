/* ************************************************************************* */
/*                             pupil-router.js                               */
/*  HTTP Endpoints for the pupil - REST API                                  */
/*                                                                           */
/*  Method  |  url                                                           */
/*  GET     |  /                                                             */
/*  GET     |  /:id                                                          */
/*  POST    |  /                                                             */
/*  PATCH   |  /:id                                                          */
/*  DELETE  |  /:id                                                          */
/*                                                                           */
/* ************************************************************************* */

import express from 'express';
import AppError from '../errorHandling/AppError.js';

import { handleAsyncError } from '../errorHandling/error-handler.js';
import { dbLogger } from '../logging/db-logger.js';
import Pupil from '../models/pupil-model.js';
import { checkId } from '../utils/payload-checker.js';

const router = express.Router();

const selectionFields = '_id firstname lastname mail state notes tags';
const validKeys = Object.keys(Pupil.base.modelSchemas.Pupil.obj).filter(key => key != 'userId');
validKeys.push('_id');

router.get('/', handleAsyncError(async (req, res, next) => {
    let pupils = await Pupil.find({ userId: req.currentUserId }, selectionFields);

    res.status(200).send(pupils);
}));

router.get('/:id', handleAsyncError(async (req, res, next) => {
    checkId(req.params.id);

    let pupil = await select(req.params.id, req.currentUserId);

    res.status(200).send(pupil);
}));

router.post('/', handleAsyncError(async (req, res, next) => {
    checkPupilKeys(validKeys, req.body);
    let pupil = new Pupil(req.body);

    pupil.userId = req.currentUserId;
    await pupil.save();
    dbLogger.log(req.currentUserId, dbLogger.logTypes.create, `Pupil with mail '${pupil.mail}' was created`, 'Pupil');
    delete pupil._doc.userId;
    res.status(201).send(pupil);
}));

router.patch('/:id', handleAsyncError(async (req, res, next) => {
    checkId(req.params.id);
    let updates = req.body;

    if (Object.keys(updates).length === 0)
        throw new AppError(400, 'nothing to update!');

    if (updates._id != undefined && updates._id != req.params.id)
        throw new AppError(400, 'updating of pupil id not permitted!');

    let pupilToUpdate = await select(req.params.id, req.currentUserId);

    if (updates.userId && pupilToUpdate.userId != updates.userId)
        throw new AppError(400, 'updating of pupil userId not permitted!');
    delete updates.userId;

    for (let key in updates) {
        if (!validKeys.includes(key))
            throw new AppError(400, `updating of not existing field '${key}' not permitted!`);
        pupilToUpdate[key] = updates[key];
    }

    await pupilToUpdate.save();
    dbLogger.log(req.currentUserId, dbLogger.logTypes.update, `Pupil with mail '${pupilToUpdate.mail}' was updated`, 'Pupil');

    delete pupilToUpdate._doc.userId;
    res.status(200).send(pupilToUpdate);
}));

router.delete('/:id', handleAsyncError(async (req, res, next) => {
    checkId(req.params.id);

    let pupilToDelete = await select(req.params.id, req.currentUserId);
    await pupilToDelete.delete();
    dbLogger.log(req.currentUserId, dbLogger.logTypes.delete, `Pupil with mail '${pupilToDelete.mail}' was deleted`, 'Pupil');

    res.status(204).send();
}));

const select = async (id, currentUserId) => {
    let pupil = await Pupil.findOne({ _id: id, userId: currentUserId }, selectionFields);
    if (pupil == null) throw new AppError(404, `Pupil with id: ${id} not found.`);

    return pupil;
}

const selectByMail = async (mailToFind, currentUserId) => {
    return await Pupil.findOne({ mail: mailToFind, userId: currentUserId }, selectionFields);
}

const checkPupilExists = async (pupilId, userId) => {
    let pupilExists = await Pupil.exists({ _id: pupilId, userId: userId });

    if (!pupilExists)
        throw new AppError(404, `Pupil with id '${pupilId}' not found!`);
}

const checkPupilKeys = (allowedKeys, pupil) => {
    if (Object.getOwnPropertyNames(pupil).some(prop => !allowedKeys.includes(prop) || prop == '_id')) {
        throw new AppError(400, 'Pupil contains not allowed property!');
    }
}

export { router as pupilRouter, checkPupilExists, selectByMail as selectPupilByMail, checkPupilKeys }