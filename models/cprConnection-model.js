import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const cprConnectionSchema = new Schema({
    courseId: {
        type: mongoose.Schema.Types.ObjectId,
        required: [true, 'courseId is missing!']
    },
    pupilId: {
        type: mongoose.Schema.Types.ObjectId,
        required: [true, 'pupilId is missing!']
    },
    records: {
        type: [mongoose.Schema.Types.ObjectId],
        "default": []
    }
},
    { versionKey: false });

cprConnectionSchema.index({ courseId: 1, pupilId: 1 }, { unique: true });

let cprConnection = mongoose.model('cprConnection', cprConnectionSchema);

export default cprConnection;