import mongoose from 'mongoose';
import { SchemaRating } from './ratingscheme-model.js';

const Schema = mongoose.Schema;

const recordSchema = new Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId
    },
    cprId: {
        type: mongoose.Schema.Types.ObjectId,
        required: [true, 'CPR-ID is missing!']
    },
    rating: {
        type: SchemaRating
    },
    weight: {
        type: Number
    },
    validOn: {
        type: Date,
        required: [true, 'Date of record is missing!']
    },
    pupilNotPresent: {
        type: Boolean
    },
    comment: {
        type: String
    }, tags: {
        type: [String],
        default: []
    }
},
    { versionKey: false });

let Record = mongoose.model('Record', recordSchema);
export default Record;