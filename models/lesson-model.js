import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const lessonSchema = new Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId
    },
    courseId: {
        type: mongoose.Schema.Types.ObjectId,
        required: [true, 'courseId is missing']
    },
    courseLabel: {
        type: String,
        required: [true, 'CourseLabel is missing!']
    },
    starttime: {
        type: String,
        required: [true, 'Starttime is missing!'],
        validate: {
            validator: (val) => {
                return (val.match(/^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/) || []).length >= 1;
            },
            msg: 'starttime is not valid!'
        }
    },
    endtime: {
        type: String,
        required: [true, 'Endtime is missing!'],
        validate: {
            validator: (val) => {
                return (val.match(/^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/) || []).length >= 1;
            },
            msg: 'endtime is not valid!'
        }
    },
    roomNr: {
        type: String,
        required: [true, 'roomnumber is missing'],

    },
    weekday: {
        type: String,
        enum: {
            values: ['monday', 'tuesday', 'wednesday', 'thursday', 'friday'],
            message: 'Weekday not in [monday, tuesday, wednesday, thursday, friday]!'
        },
        required: [true, 'Weekday is missing!']
    },
    colorCode: {
        type: String,
        default: '#023047',
        validate: {
            validator: (val) => {
                return (val.match(/^#([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$/) || []).length >= 1;
            }
        }
    }
},
    { versionKey: false });

let Lesson = mongoose.model('Lesson', lessonSchema);

export default Lesson;