import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const pupilSchema = new Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId
    },
    firstname: {
        type: String,
        required: [true, 'Firstname of pupil is missing!']
    },
    lastname: {
        type: String,
        required: [true, 'Lastname of pupil is missing!']
    },
    mail: {
        type: String,
        unique: true,
        required: [true, 'E-Mail of pupil is missing!'],
        validate: {
            validator: (val) => {
                return (val.match(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)+$/) || []).length == 1;
            },
            msg: 'email is not valid!'
        }
    },
    state: {
        type: String,
        enum: {
            values: ['active', 'inactive'],
            message: 'State of pupil can only be active, inactive!'
        },
        default: 'active'
    },
    notes: {
        type: String,
        default: ''
    }, tags: {
        type: [String],
        default: []
    }
},
    { versionKey: false });

let Pupil = mongoose.model('Pupil', pupilSchema);
export default Pupil;