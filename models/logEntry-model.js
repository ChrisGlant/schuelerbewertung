import mongoose from 'mongoose';
import { dbLogger } from '../logging/db-logger.js';
const logTypes = dbLogger.logTypes;

const Schema = mongoose.Schema;

const logEntrySchema = new Schema({
    logType: {
        type: String,
        enum: {
            values: Object.values(logTypes),
            message: 'Logtype invalid! Valid logtypes are ' + Object.keys(logTypes)
        },
        required: [true, 'Logtype is missing!']
    },
    logTimestamp: {
        type: Date,
        required: [true, 'LogTimestamp is missing!']
    },
    logMessage: {
        type: String
    },
    affectedEntity: {
        type: String
    }
}, {
    _id: false,
    versionKey: false
});

const logEntriesSchema = new Schema(generateLogEntriesSchema(),
    {
        _id: false,
        versionKey: false
    }
);

function generateLogEntriesSchema() {
    let schema = {};

    Object.keys(logTypes).forEach(type => {
        schema[logTypes[type]] = {
            type: [logEntrySchema],
            default: []
        }
    });
    return schema;
}

let DbLogEntry = mongoose.model('DbLogEntry', logEntrySchema);
let LogEntries = mongoose.model('LogEntries', logEntriesSchema);
export { DbLogEntry, LogEntries, logEntriesSchema };