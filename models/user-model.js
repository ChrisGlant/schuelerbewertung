import mongoose from 'mongoose';
import validator from 'validator';
import { logEntriesSchema } from './logEntry-model.js';

const Schema = mongoose.Schema;

const userSchema = new Schema({
    username: {
        type: String,
        unique: true,
        index: true,
        sparse: true
    },
    email: {
        type: String,
        unique: true,
        validate: [validator.isEmail, 'invalid email'],
        required: [true, 'email is missing']
    },
    password: {
        type: String,
        required: [true, 'password is missing'],
        writeOnly: true
    },
    lastname: {
        type: String,
        required: [true, 'lastname is missing']
    },
    firstname: {
        type: String,
        required: [true, 'firstname is missing']
    },
    state: {
        type: String,
        enum: ['active', 'inactive'],
        default: 'inactive'
    },
    logEntries: {
        type: logEntriesSchema
    }
}, { versionKey: false });

let User = mongoose.model('User', userSchema);

export default User;
