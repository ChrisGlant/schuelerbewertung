import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const courseSchema = new Schema({
    label: {
        type: String,
        required: [true, 'Label is missing!']
    },
    subjectLabel: {
        type: String,
        default: ""
    },
    pupilGroupLabel: {
        type: String,
        default: ""
    },
    notes: {
        type: String
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        required: [true, 'UserId is missing!']
    },
    periodId: {
        type: mongoose.Schema.Types.ObjectId
    }
},
    { versionKey: false });

courseSchema.index({ label: 1, userId: 1, periodId: 1 }, { unique: true });

let Course = mongoose.model('Course', courseSchema);

export { Course, courseSchema };