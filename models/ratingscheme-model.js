import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const schemaRating = new Schema({
    label: {
        type: String,
        required: [true, 'Label of rating is missing!']
    },
    symbol: {
        type: String,
        required: [true, 'Symbol of rating is missing!']
    },
    value: {
        type: Number,
        required: [true, 'Value of rating is missing!'],
        min: 0,
        max: 100
    }
},
    { versionKey: false });

let Rating = mongoose.model('Rating', schemaRating);

const schemaRatingscheme = new Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId
    },
    name: {
        type: String,
        required: [true, 'Name of ratingscheme is missing!']
    },
    ratings: [schemaRating],
    weight: {
        type: Number,
        required: [true, 'Weight is missing!']
    }
},
    { versionKey: false });

let RatingScheme = mongoose.model('RatingScheme', schemaRatingscheme);

export { RatingScheme, Rating, schemaRating as SchemaRating }