import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const periodSchema = new Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId
    },
    label: {
        type: String,
        required: [true, 'Label is missing!']
    },
    startdate: {
        type: Date,
        required: [true, 'Startdate is missing!']
    },
    enddate: {
        type: Date,
        required: [true, 'Enddate is missing!'],
        validate: [dateValidator, 'Startdate mustn`t be after enddate!']
    },
    state: {
        type: String,
        enum: {
            values: ['active', 'inactive', 'archived'],
            message: 'State can only be active, inactive or archived!'
        },
        required: [true, 'State flag is missing!']
    },
},
    { versionKey: false });

function dateValidator(value) {
    return value > this.startdate;
}

periodSchema.index({ label: 1, userId: 1 }, { unique: true });

let Period = mongoose.model('Period', periodSchema);

export default Period;