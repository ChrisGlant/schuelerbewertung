import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const pswrdResetSchema = new Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        required: [true, 'userId is missing!']
    },
    recoveryToken: {
        type: String,
        required: [true, 'recoveryToken is missing!']
    }
},
    {
        versionKey: false,
        timestamps: true
    });

let pswrdReset = mongoose.model('pswrdReset', pswrdResetSchema);

export { pswrdReset, pswrdResetSchema };