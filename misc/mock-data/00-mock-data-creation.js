'use strict'
import chai from 'chai';
import chaiHttp from 'chai-http';
import app from '../../server.js';

import User from '../../models/user-model.js';
import Period from '../../models/period-model.js';
import { Course } from '../../models/course-model.js';
import Pupil from '../../models/pupil-model.js';
import cprConnection from '../../models/cprConnection-model.js';
import Lesson from '../../models/lesson-model.js';
import { RatingScheme } from '../../models/ratingscheme-model.js';
import Record from '../../models/record-model.js';

import { healthCheck } from '../../tests/00-startup-hook.js';
import readJson from '../../utils/readJson.js';

const { expect } = chai;

// regex for replacing ids and other stuff
// strg + h
// enable regex
// \{ *\n *\t*"\$oid": (".*")\n *\t*\}
// $1

// endpoints
const loginEndpoint = '/api/v1/login';
const userEndpoint = '/api/v1/users';
const periodEndpoint = '/api/v1/periods'
const courseEndpoint = '/api/v1/courses'
const pupilEndpoint = '/api/v1/pupils'
const cprConnectionEndpoint = '/api/v1/cprconnections'
const ratingschemeEndpoint = '/api/v1/ratingschemes'
const lessonEndpoint = '/api/v1/lessons'
const recordEndpoint = '/api/v1/records'

// filenames
const parentFilepath = 'misc/mock-data'
const userFilepath = `${parentFilepath}/users.json`
const periodFilepath = `${parentFilepath}/periods.json`
const coursesFilepath = `${parentFilepath}/courses.json`
const pupilFilepath = `${parentFilepath}/pupils.json`
const cprConnectionFilepath = `${parentFilepath}/cprconnections.json`
const ratingschemeFilepath = `${parentFilepath}/ratingschemes.json`
const lessonFilepath = `${parentFilepath}/lessons.json`
const recordFilepath = `${parentFilepath}/records.json`

// setup

chai.use(chaiHttp);
chai.should();

// data injection

// users
let users = await readJson(userFilepath);
for (let u of users) {
    let user = new User(u);
    await user.save();
}

// periods
let periods = await readJson(periodFilepath);
for (let p of periods) {
    let period = new Period(p);
    await period.save();
}

// courses
let courses = await readJson(coursesFilepath);
for (let c of courses) {
    let course = new Course(c);
    await course.save();
}

// pupils
let pupils = await readJson(pupilFilepath);
for (let p of pupils) {
    let pupil = new Pupil(p);
    await pupil.save();
}

// cprConnections
let cprConnections = await readJson(cprConnectionFilepath);
for (let cpr of cprConnections) {
    let cprConn = new cprConnection(cpr);
    await cprConn.save();
}

// ratingSchemes
let ratingSchemes = await readJson(ratingschemeFilepath);
for (let ratingscheme of ratingSchemes) {
    let rs = new RatingScheme(ratingscheme);
    await rs.save();
}

// lessons
let lessons = await readJson(lessonFilepath);
for (let lesson of lessons) {
    let ls = new Lesson(lesson);
    await ls.save();
}

// records
let records = await readJson(recordFilepath);
for (let record of records) {
    let r = new Record(record);
    await r.save();
}

// hook - done() fires, if healthcheck point answers with 200
before(() => {
    return healthCheck(app);
});

describe(`MOCK-DATA CREATION`, () => {
    let testGroupName;
    let testNumber = 0;

    testGroupName = 'Basic user stuff'
    describe(testGroupName, () => {
        it(`${testNumber++} ${testGroupName} - Login first user - 200`, (done) => {
            chai
                .request(app)
                .post(loginEndpoint)
                .send({ 'username': users[0]['username'], 'password': users[0]['password'] })
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(1);
                    res.body.should.have.property('token');
                    users[0].token = res.body.token;
                    done();
                });
        });
    })

    testGroupName = 'CHECK MOCK-DATA'
    describe(testGroupName, () => {
        describe('users', () => {
            users.forEach(u => {
                it(`${testNumber++} ${testGroupName} - check users - 200`, (done) => {
                    chai
                        .request(app)
                        .get('/api/v1/whoIAm')
                        .set('Authorization', u.token)
                        .send()
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(200);
                            res.body.should.be.a('object');
                            res.body.email.should.equal(u.email)
                            done();
                        });
                });
            })
        })

        describe('periods', () => {
            periods.forEach(p => {
                it(`${testNumber++} ${testGroupName} - check periods - 200`, (done) => {
                    chai
                        .request(app)
                        .get(periodEndpoint + '/' + p._id)
                        .set('Authorization', users[0].token)
                        .send()
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(200);
                            res.body.should.be.a('object');
                            res.body._id.should.equal(p._id)
                            done();
                        });
                });
            })
        })

        describe('courses', () => {
            courses.forEach(c => {
                it(`${testNumber++} ${testGroupName} - check courses - 200`, (done) => {
                    chai
                        .request(app)
                        .get(courseEndpoint + '/' + c._id)
                        .set('Authorization', users[0].token)
                        .send()
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(200);
                            res.body.should.be.a('object');
                            res.body._id.should.equal(c._id)
                            done();
                        });
                });
            })
        })

        describe('pupils', () => {
            pupils.forEach(p => {
                it(`${testNumber++} ${testGroupName} - check pupils - 200`, (done) => {
                    chai
                        .request(app)
                        .get(pupilEndpoint + '/' + p._id)
                        .set('Authorization', users[0].token)
                        .send()
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(200);
                            res.body.should.be.a('object');
                            res.body._id.should.equal(p._id)
                            done();
                        });
                });
            })
        })

        describe('cpr-connections', () => {
            cprConnections.forEach(cpr => {
                it(`${testNumber++} ${testGroupName} - check cpr-connections - 200`, (done) => {
                    chai
                        .request(app)
                        .get(cprConnectionEndpoint + '/' + cpr._id)
                        .set('Authorization', users[0].token)
                        .send()
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(200);
                            res.body.should.be.a('object');
                            res.body._id.should.equal(cpr._id)
                            done();
                        });
                });
            })
        })

        describe('ratingschemes', () => {
            ratingSchemes.forEach(rs => {
                it(`${testNumber++} ${testGroupName} - check ratingschemes - 200`, (done) => {
                    chai
                        .request(app)
                        .get(ratingschemeEndpoint + '/' + rs._id)
                        .set('Authorization', users[0].token)
                        .send()
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(200);
                            res.body.should.be.a('object');
                            res.body._id.should.equal(rs._id)
                            done();
                        });
                });
            })
        })

        describe('lessons', () => {
            lessons.forEach(ls => {
                it(`${testNumber++} ${testGroupName} - check lessons - 200`, (done) => {
                    chai
                        .request(app)
                        .get(lessonEndpoint + '/' + ls._id)
                        .set('Authorization', users[0].token)
                        .send()
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(200);
                            res.body.should.be.a('object');
                            res.body._id.should.equal(ls._id)
                            done();
                        });
                });
            })
        })

        describe('records', () => {
            records.forEach(r => {
                it(`${testNumber++} ${testGroupName} - check records - 200`, (done) => {
                    chai
                        .request(app)
                        .get(recordEndpoint + '/' + r._id)
                        .set('Authorization', users[0].token)
                        .send()
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(200);
                            res.body.should.be.a('object');
                            res.body._id.should.equal(r._id)
                            done();
                        });
                });
            })
        })
    });
});