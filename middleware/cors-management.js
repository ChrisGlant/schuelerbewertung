import dotenv from 'dotenv';
dotenv.config();

function cors() {
    return async (req, res, next) => {
        res.setHeader('Access-Control-Allow-Origin', process.env.FRONTEND_URL);
        res.setHeader('Access-Control-Allow-Credentials', 'true');
        res.setHeader('Access-Control-Allow-Headers', 'content-type, authorization');
        res.setHeader('Access-Control-Allow-Methods', 'DELETE, PATCH, PUT');

        if (req.method.toUpperCase() === 'OPTIONS')
            res.status(200).send('PREFLIGHT OK');
        else
            next();
    }
}

export { cors };