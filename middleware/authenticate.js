import { verifyJWTToken, getTokenFromReq } from '../utils/jwt-manager.js'
import { selectByToken } from '../routers/user-router.js';
import AppError from '../errorHandling/AppError.js';
import { handleAsyncError } from '../errorHandling/error-handler.js';

const whitelistedUrls = [
    { method: 'POST', url: '/api/v1/users' },
    { method: 'POST', url: '/api/v1/login' },
    { method: 'PUT', url: '/api/v1/password-reset' },
    { method: 'POST', url: '/api/v1/password-reset' },
    { method: 'POST', url: '/api/v1/mockdata' }
];

const authenticate = handleAsyncError(async (req, res, next) => {
    if (whitelistedUrls.find(e => e.url == req.originalUrl && e.method == req.method)) {
        next();
        return;
    }

    let token = getTokenFromReq(req);
    verifyJWTToken(token);
    try {
        await selectByToken(token)
        next();
    } catch (err) {
        throw new AppError(401, 'Contact Adminstrator - User not found')
    }
});


export { authenticate, whitelistedUrls }
