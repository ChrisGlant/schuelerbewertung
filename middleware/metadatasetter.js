import { handleAsyncError } from '../errorHandling/error-handler.js';
import AppError from '../errorHandling/AppError.js';
import { whitelistedUrls } from './authenticate.js'
import { getCurrentUserId } from '../utils/jwt-manager.js'
import { selectActivePeriod } from '../routers/period-router.js'

const metadatasetter = handleAsyncError(async (req, res, next) => {
    if (whitelistedUrls.find(e => e.url == req.originalUrl && e.method == req.method)) {
        next();
        return;
    }

    try {
        // set currentUserId
        req.currentUserId = getCurrentUserId(req)

        // set activePeriodId
        try {
            let period = await selectActivePeriod(req.currentUserId)
            req.activePeriodId = period._id;
        } catch (err) {
            if (err.statusCode != 404)
                throw err;
        }
        next();
    } catch (err) {
        throw new AppError(401, 'Contact Adminstrator - Metadata error')
    }
});

export { metadatasetter }