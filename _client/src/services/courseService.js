import { authService } from './authenticationService';

const baseUrl = process.env.REACT_APP_BACKEND_URL + `/courses`;

export const courseService = {
    getAll,
    getById,
    create,
    update,
    delete: _delete,
    //setDefaultUsers,
};

function getAll() {
    return authService.get(baseUrl);
}

function getById(id) {
    return authService.get(`${baseUrl}/${id}`);
}

function create(params) {
    return authService.post(baseUrl, params);
}

function update(id, params) {
    return authService.patch(`${baseUrl}/${id}`, params);
}

// prefixed with underscored because delete is a reserved word in javascript
function _delete(id) {
    return authService.delete(`${baseUrl}/${id}`);
}

