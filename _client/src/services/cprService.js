
import { authService } from './authenticationService';

const baseUrl = process.env.REACT_APP_BACKEND_URL + `/cprconnections`;

export const cprService = {
    getCoursesById,
    getPupilsById,
    getById
};


function getCoursesById(id) {
    return authService.get(`${baseUrl}?pupilId=${id}`);
}

function getPupilsById(id) {
    return authService.get(`${baseUrl}?courseId=${id}`);
}

function getById(id) {
    return authService.get(`${baseUrl}/${id}`);
}
