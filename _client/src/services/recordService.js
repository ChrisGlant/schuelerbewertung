import { authService } from './authenticationService';

const baseUrl = process.env.REACT_APP_BACKEND_URL + `/records`;

export const recordService = {
    getAllOfCPR,
    getAllOfPupil,
    getById,
    create,
    update,
    delete: _delete,
    bulkAdd,
    //setDefaultUsers,
    getAllOfCourse,
    getGradeSuccOfPupilInClass,
    getGradeSuccOfCourse,
    getGradeSuccsOfPupil,
    getGradeSuccHistoryOfPupilInClass
};

function getGradeSuccHistoryOfPupilInClass(courseId, pupilId) {
    return authService.get(baseUrl + '/grade?courseId=' + courseId + '&pupilId=' + pupilId + '&multiple=true');
}

function getGradeSuccOfPupilInClass(courseId, pupilId) {
    return authService.get(baseUrl + '/grade?courseId=' + courseId + '&pupilId=' + pupilId);
}

function getGradeSuccOfCourse(courseId) {
    return authService.get(baseUrl + '/grade?courseId=' + courseId);
}

function getGradeSuccsOfPupil(pupilId) {
    return authService.get(baseUrl + '/grade?pupilId=' + pupilId);
}

function getAllOfCourse(courseId) {
    return authService.get(baseUrl + '?courseId=' + courseId);
}

function getAllOfCPR(cprid) {
    return authService.get(baseUrl + '?cprId=' + cprid);
}
function getAllOfPupil(pupilId) {
    return authService.get(baseUrl + '?pupilId=' + pupilId);
}

function getById(id) {
    return authService.get(`${baseUrl}/${id}`);
}

function create(params) {
    return authService.post(baseUrl, params);
}

function bulkAdd(params) {
    return authService.post(baseUrl + '/bulkadd', params);
}

function update(id, params) {
    return authService.patch(`${baseUrl}/${id}`, params);
}

// prefixed with underscored because delete is a reserved word in javascript
function _delete(id) {
    return authService.delete(`${baseUrl}/${id}`);
}

