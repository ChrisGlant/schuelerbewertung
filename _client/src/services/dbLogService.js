import { authService } from './authenticationService';

const baseUrl = process.env.REACT_APP_BACKEND_URL + `/logentries`;

export const dbLogService = {
    getAll
};

function getAll() {
    return authService.get(baseUrl);
}


