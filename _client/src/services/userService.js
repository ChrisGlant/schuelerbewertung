import { dataService } from './dataService';
import { authService } from './authenticationService';

const baseUrl = process.env.REACT_APP_BACKEND_URL + `/users`;

export const userService = {
    getAll,
    getById,
    getCurrent,
    create,
    update,
    delete: _delete,
    //setDefaultUsers,
};

function getCurrent() {
    return authService.getCurrentUser();
}

function getAll() {
    return dataService.get(baseUrl);
}

function getById(id) {
    return authService.get(`${baseUrl}/${id}`);
}

function create(params) {
    return dataService.post(baseUrl, params);
}

function update(id, params) {
    return authService.patch(`${baseUrl}/${id}`, params);
}

// prefixed with underscored because delete is a reserved word in javascript
function _delete(id) {
    return authService.delete(`${baseUrl}/${id}`);
}
/*
function setDefaultUsers() {
  const requestOptions = {
    method: 'PATCH',
  };
  return fetch(baseUrl + 'init', requestOptions);
}*/
