import { authService } from './authenticationService';

const baseUrl = process.env.REACT_APP_BACKEND_URL + `/import`;

export const importService = {
    putPupils,
    confirmImport
};

function putPupils(pupils) {
    return authService.put(baseUrl + '/pupils', pupils);
}

function confirmImport(pupils) {
    return authService.post(baseUrl + '/pupils/confirm', pupils);
}
