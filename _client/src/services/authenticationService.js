import { dataService } from './dataService';

// Auth Info stored in Memory and in Storage - Provider (sessionstorage localstorage)
//let _authInfo = null;
const baseUrl = process.env.REACT_APP_BACKEND_URL;

export const authService = {
    login,
    logout,
    getCurrentUser,
    get,
    post,
    patch,
    put,
    delete: _delete,
    startPasswordResetProcess,
    confirmPasswordReset
};

function confirmPasswordReset(obj) {
    return dataService.post(baseUrl + '/password-reset', obj);
}

function startPasswordResetProcess(email) {
    return dataService.put(baseUrl + '/password-reset', email);
}

function getCurrentUser(headers) {
    return dataService.get(baseUrl + '/whoIAm', combineHeadersWithAuthInfo(headers));
}

function login(userobj) {
    return dataService.post(baseUrl + '/login', userobj).then((res) => {
        sessionStorage.setItem(process.env.REACT_APP_USER_DATA_TOKEN, res.token);
    })
}

function logout(params) {
    return dataService.post(baseUrl + '/logout', combineHeadersWithAuthInfo(params)).then(() => {
        sessionStorage.removeItem(process.env.REACT_APP_USER_DATA_TOKEN);
    })
}

function put(url, body, headers) {
    return dataService.put(url, body, combineHeadersWithAuthInfo(headers));
}

function get(url, headers) {
    return dataService.get(url, combineHeadersWithAuthInfo(headers));
}

function post(url, body, headers) {
    return dataService.post(url, body, combineHeadersWithAuthInfo(headers));
}

function patch(url, body, headers) {
    return dataService.patch(url, body, combineHeadersWithAuthInfo(headers));
}

function _delete(url, headers) {
    return dataService.delete(url, combineHeadersWithAuthInfo(headers));
}

function combineHeadersWithAuthInfo(headers) {
    return { Authorization: getAuthInfo(), ...headers };
}

function getAuthInfo() {
    return 'Bearer ' + sessionStorage.getItem(process.env.REACT_APP_USER_DATA_TOKEN);
}
