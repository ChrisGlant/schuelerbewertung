import './App.css';
import React from 'react';
import './index.css';
import 'bootstrap/dist/css/bootstrap.css';
import SideBar from './components/utils/SideBar.jsx';
import SidebarItems from './components/utils/SideBarItems.jsx';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function App() {
    //const location = useLocation();
    return (
        <div>
            <SideBar items={SidebarItems} />
            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
        </div>
    );
}

export default App;