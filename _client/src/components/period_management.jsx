import { toast } from 'react-toastify';
import React, { useState, useEffect } from 'react';
import 'react-toastify/dist/ReactToastify.css';
import { periodService } from '../services/periodService.js';
import { courseService } from '../services/courseService.js';
import '../css/Period.css';
import '../App.css';
import { errorHandler, getAndShowAllMsgs, formOK } from './utils/utils';
import ModalWindow from './utils/modalWindow.jsx';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


const PeriodManagement = () => {
    //---States, UseEffects---//
    //#region 
    const [stateOfModal, setStateOfModal] = useState({
        isOpen: false
    });
    const openModal = () => setStateOfModal({ isOpen: true });
    const closeModal = () => setStateOfModal({ isOpen: false });
    const [stateOfModalRemoveCourse, setStateOfModalRemoveCourse] = useState({
        isOpen: false
    });
    const openRemoveCourseModal = () => setStateOfModalRemoveCourse({ isOpen: true });
    const closeRemoveCourseModal = () => setStateOfModalRemoveCourse({ isOpen: false });
    const [stateOfModalArchivePeriod, setStateOfModalArchivePeriod] = useState({
        isOpen: false
    });
    const openArchivePeriodModal = () => setStateOfModalArchivePeriod({ isOpen: true });
    const closeArchivePeriodModal = () => setStateOfModalArchivePeriod({ isOpen: false });
    const [coursesOfCurrPeriod, setCoursesOfCurrPeriod] = useState([]);
    const [remainingCourses, setRemainingCourses] = useState([]);
    const [currCourse, setCurrCourse] = useState({});
    const [eventOfState, setEventObjectOfState] = useState({});
    const [unfilteredPeriods, setUnfilteredPeriods] = useState([]);
    const [periods, setPeriods] = useState([]);
    useEffect(() => {
        init();
    }, []);

    const init = () => {
        periodService.getAll().then(_periods => {
            sortAndSetPeriods(_periods.map((element) => {
                element.startdate = element.startdate.split('T')[0];
                element.enddate = element.enddate.split('T')[0];
                return element;
            }));
        }).catch((reason) => {
            errorHandler(reason, toast);
        })
    }

    const flagSwitchAddUpdate = {
        ADD: 0,
        UPDATE: 1
    };
    const [flagAddUpdate, setFlagAddUpdate] = useState(flagSwitchAddUpdate.UPDATE);

    const emptyPeriodObj = {
        'label': '',
        'startdate': '',
        'enddate': '',
        'state': 'active'
    }
    const [period, setPeriod] = useState(emptyPeriodObj);
    const [idOfCurrentPeriod, setIdOfCurrentPeriod] = useState(null);

    const initialFormValidationInfo = {
        'label': {
            valid: false,
            msg: 'No input for label'
        },
        'startdate': {
            valid: false,
            msg: 'No input for date'
        },
        'enddate': {
            valid: false,
            msg: 'No input for date'
        }
    };
    const [formValidationInfo, setFormValidationInfo] = useState(initialFormValidationInfo);

    useEffect(() => {
        checkWholePeriodObj();
    }, [period]);

    useEffect(() => {
        fillCourseLists();
    }, [idOfCurrentPeriod]);

    //#endregion
    //--------Methods--------//
    //#region 

    const fillCourseLists = (searchKritCoursesAdd, searchKritCoursesRemaining) => {
        if (idOfCurrentPeriod) {
            courseService.getAll().then((_courses) => {
                let _remainingCs = [];
                let _currentCs = [];
                _courses.forEach((_course) => {
                    if (_course.periodId === null || _course.periodId === undefined || _course.periodId === 'null') {
                        if (searchKritCoursesRemaining) {
                            if (_course.label.toUpperCase().includes(searchKritCoursesRemaining.toUpperCase()))
                                _remainingCs.push(_course);
                        } else {
                            _remainingCs.push(_course);
                        }
                    } else if (_course.periodId === idOfCurrentPeriod) {
                        if (searchKritCoursesAdd) {
                            if (_course.label.toUpperCase().includes(searchKritCoursesAdd.toUpperCase()))
                                _currentCs.push(_course);
                        } else {
                            _currentCs.push(_course);
                        }
                    }

                });
                setCoursesOfCurrPeriod([..._currentCs]);
                setRemainingCourses([..._remainingCs]);
            }).catch((reason) => {
                errorHandler(reason, toast);
            })
        } else {
            courseService.getAll().then((_courses) => {

                setCoursesOfCurrPeriod([]);
                setRemainingCourses(_courses);
            }).catch((reason) => {
                errorHandler(reason, toast);
            })
        }
    }

    const sortAndSetPeriods = (_periods) => {
        if (_periods.length > 0) {
            _periods.sort((a, b) => { return a.label.localeCompare(b.label) });
            setPeriods([..._periods]);
        }
    }

    const onBtnPeriodAddClick = () => {
        setFlagAddUpdate(flagSwitchAddUpdate.ADD);
        setPeriod(emptyPeriodObj);
        setIdOfCurrentPeriod(null);
    }

    const onChange = (event) => {
        if (event) {
            event = event.target;
        } else {
            event = eventOfState;
        }
        const { name, value } = event;
        closeArchivePeriodModal();

        setPeriod({ ...period, [name]: value });
    }

    const onChangeState = (event) => {
        const { name, value } = event.target;
        if (value === 'archived') {
            setEventObjectOfState({
                name: name,
                value: value
            });
            openArchivePeriodModal();
        } else {
            setPeriod({ ...period, [name]: value });
        }
    }

    function checkInput(name, value) {
        let tempObj = {
            valid: false,
            msg: ''
        }
        if (!value) {
            tempObj.msg = 'no value for ' + name;
            return tempObj;
        }
        switch (name) {
            case 'label':
                if (value.length > 40 || value.length < 3) {
                    tempObj = {
                        valid: false,
                        msg: 'Label cannot be shorter than 3 chars or longer than 40'
                    };
                } else {
                    tempObj = {
                        valid: true,
                        msg: ''
                    };
                }
                break;
            case 'startdate':
                tempObj.valid = true;
                if (!isDateFormatValid(value)) {
                    tempObj = {
                        valid: false,
                        msg: 'input for startdate is not a date'
                    };
                } else if (!areDatesValid(value, period.enddate)) {
                    tempObj = {
                        valid: false,
                        msg: 'startdate cannot be past enddate'
                    };
                } else {
                    tempObj = {
                        valid: true,
                        msg: ''
                    };
                }
                break;
            case 'enddate':
                tempObj.valid = true;
                if (!isDateFormatValid(value)) {
                    tempObj = {
                        valid: false,
                        msg: 'input for enddate is not a date'
                    };
                } else if (!areDatesValid(period.startdate, value)) {
                    tempObj = {
                        valid: false,
                        msg: 'startdate cannot be past enddate'
                    };

                } else {
                    tempObj = {
                        valid: true,
                        msg: ''
                    };
                }
                break;

            default:
                return;
        }
        return tempObj;
    }

    function isDateFormatValid(input) {
        if ((/^(0?[1-9]|[12][0-9]|3[01])[/\-.](0?[1-9]|1[012])[/\-.]\d{4}$/).test(input) || //01.01.2000 (.-/)
            (/^\d{4}[/\-.](0?[1-9]|1[012])[/\-.](0?[1-9]|[12][0-9]|3[01])$/).test(input)) { //2000.01.01 (.-/)
            return true;
        }
        return false;
    }

    function checkWholePeriodObj() {
        let tempobj = {};
        let newFormValidationInfo = {};
        Object.keys(formValidationInfo).forEach((element) => {
            tempobj = checkInput(element, period[element]);
            if (tempobj)
                newFormValidationInfo[element] = tempobj;
            else
                newFormValidationInfo[element] = initialFormValidationInfo[element];
        })
        setFormValidationInfo({ ...newFormValidationInfo });
    }

    function areDatesValid(startdate, enddate) {
        return startdate < enddate;
    }


    const onBtnSaveClick = () => {
        if (!formOK(formValidationInfo)) {
            getAndShowAllMsgs(formValidationInfo, toast);
            return;
        }
        let tempPeriodObj = period;
        tempPeriodObj.startdate = new Date(tempPeriodObj.startdate);
        tempPeriodObj.enddate = new Date(tempPeriodObj.enddate);

        switch (flagAddUpdate) {
            case flagSwitchAddUpdate.ADD:
                if (periods.length === 0 && tempPeriodObj.state === 'inactive') {
                    toast.error('first created period must be active');
                    return;
                }
                periodService.create(tempPeriodObj).then(() => {
                    init();
                    toast.success('period created');
                }).catch((reason) => {
                    errorHandler(reason, toast);
                })
                checkAndUpdateCourses();
                break;
            case flagSwitchAddUpdate.UPDATE:
                if (!idOfCurrentPeriod) {
                    toast.error('error: probably no period selected to delete');
                    break;
                } else if (periods.find((element) => element._id === idOfCurrentPeriod).state === 'active' && tempPeriodObj.state !== 'active') {
                    if (tempPeriodObj.state === 'inactive') {
                        toast.error('activate another period instead of deactivating this one');
                    } else {
                        toast.error('before archiving this period, deactivate it by activating another period');
                    }
                    break;
                }
                checkAndUpdateCourses();
                periodService.update(idOfCurrentPeriod, tempPeriodObj).then(() => {
                    init();
                    toast.success('period updated');
                }).catch((reason) => {
                    errorHandler(reason, toast);
                })
                break;
            default:
                break;
        }
    }

    const checkAndUpdateCourses = () => {
        try {
            let coursesArr = [coursesOfCurrPeriod, remainingCourses];
            for (let arrIdx = 0; arrIdx < coursesArr.length; arrIdx++) {
                arrIdx = parseInt(arrIdx);
                for (let _course of coursesArr[arrIdx]) {
                    // _course = (coursesArr[arrIdx])[parseInt(_course)];
                    if (arrIdx === 0) {
                        if (_course.periodId === null || _course.periodId === undefined || _course.periodId === 'null') {
                            _course.periodId = idOfCurrentPeriod;
                            courseService.update(_course._id, _course).catch((reason) => {
                                errorHandler(reason, toast);
                            })
                        }
                    } else if (arrIdx === 1) {
                        if (_course.periodId) {
                            _course.periodId = null;
                            courseService.update(_course._id, _course).catch((reason) => {
                                errorHandler(reason, toast);
                            })
                        }
                    }
                }
            }
        } catch (err) {
            errorHandler(err, toast);
        }
    }

    const onLiBtnClick = (_period) => {
        setFlagAddUpdate(flagSwitchAddUpdate.UPDATE);
        setIdOfCurrentPeriod(_period._id);
        setPeriod(_period)
    }

    const deleteCurrPeriod = () => {
        closeModal();

        periodService.delete(idOfCurrentPeriod).then(() => {
            setIdOfCurrentPeriod(null);
            init();
            toast.success('period deleted');
        }).catch((reason) => {
            errorHandler(reason, toast);
        })
    }


    const getPeriodLabel = () => {
        if (idOfCurrentPeriod) {
            let periodFound = periods.find((element) => element._id === idOfCurrentPeriod);
            if (periodFound)
                return periodFound.label;
        }
        return '';
    }

    const onSearchFieldChange = (event) => {
        const { value } = event.target;
        let tempPeriods = unfilteredPeriods;
        if (unfilteredPeriods.length < periods.length) {
            setUnfilteredPeriods(periods);
            tempPeriods = periods;
        }
        setPeriods(tempPeriods.filter((element) => {
            return element.label.toUpperCase().includes(value.toUpperCase());
        }).map((element) => {
            element.startdate = element.startdate.split('T')[0];
            element.enddate = element.enddate.split('T')[0];
            return element;
        }));

    }

    const openDeleteModal = () => {
        try {
            if (idOfCurrentPeriod === null) {
                toast.error('no period selected')
            } else if (periods.find((element) => element._id === idOfCurrentPeriod).state === 'active') {
                toast.error('Active Periods cannot be deleted; activate another Period before deleting this one');
            } else {
                openModal();
            }
        } catch (err) {
            errorHandler(err, toast);
        }
    }

    const onRemainingCourseClick = (obj) => {
        coursesOfCurrPeriod.push(obj);
        let tempCourses = remainingCourses;
        let idx = tempCourses.indexOf(obj);
        tempCourses.splice(idx, 1);
        setRemainingCourses([...tempCourses]);
    }

    const onAddedCourseClick = () => {
        closeRemoveCourseModal();
        remainingCourses.push(currCourse);
        let tempCourses = coursesOfCurrPeriod;
        let idx = tempCourses.indexOf(currCourse);
        tempCourses.splice(idx, 1);
        setCoursesOfCurrPeriod([...tempCourses]);
    }

    const onSearchFieldRemainingCoursesChange = (event) => {
        const { value } = event.target;
        fillCourseLists(null, value);

    }

    const onSearchFieldAddedCoursesChange = (event) => {
        const { value } = event.target;
        fillCourseLists(value, null);

    }

    const getCourseLabel = () => {
        if (currCourse.label)
            return currCourse.label;
        return ' ';
    }

    //#endregion
    //---------HTML----------//
    //#region 
    return (
        <div>
            <h1>Periods</h1>
            <div className="grid-container">
                <div style={{ margin: '10px' }} className="grid-row-sm">
                    <label className="header">Periods</label>
                    <input
                        type='text'
                        autoComplete="off"
                        className="search inputFieldPeriod inputField"
                        //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                        id='inputCtrl_Search'
                        name='label'
                        placeholder='Search...'
                        //value={period.label}
                        onChange={onSearchFieldChange}
                    ></input>
                    <button type="button" className="bttnSearch bttn" disabled >
                        <FontAwesomeIcon icon="search" size="sm" />
                    </button>
                    <div className="block">
                    <ul className="list-group">
                        {
                            periods.map((item) =>
                                <li className="list-group-item d-flex justify-content-between align-items-center" style={{ border: '2px solid #023047', color: (idOfCurrentPeriod === item._id) ? 'white' : '#023047', backgroundColor: (idOfCurrentPeriod === item._id) ? '#023047' : 'white' }} key={item._id} onClick={() => onLiBtnClick(item)}>
                                    <div class="ms-4 me-auto">
                                        <div class="fw-bold">{item.label}</div>
                                    </div>
                                    <div class="ms-2 me-auto">
                                        <span class={'state ' + item.state + ' badge rounded-pill'}>{item.state}</span>
                                    </div>
                                </li>
                            )
                        }
                    </ul>
                    </div>
                    <button type="button" className="bttnAdd bttn" onClick={onBtnPeriodAddClick} >Add Period</button>
                </div>
                <div style={{ margin: '10px', visibility: (flagSwitchAddUpdate.UPDATE === flagAddUpdate && idOfCurrentPeriod === null) ? 'hidden' : 'visible' }} className="grid-row-sm">
                    <label className="header">Period-Info</label>
                    <form style={{ width: '100%' }}>
                        <label className="fat" htmlFor={`inputCtrl_Status`}>Label:</label>
                        <input
                            type='text'
                            autoComplete="off"
                            className="inputFieldPeriod inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            id='inputCtrl_Bezeichnung'
                            name='label'
                            placeholder='Bezeichnung'
                            value={period.label}
                            onChange={onChange}
                        >
                        </input>
                        <br />
                        <label className="fat" htmlFor={`inputCtrl_Status`}>Startdate:</label>
                        <input
                            type='date'
                            autoComplete="off"
                            className="picker inputFieldPeriod inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            id='inputCtrl_Startdatum'
                            name='startdate'
                            placeholder='Startdatum'
                            value={period.startdate}
                            onChange={onChange}
                        >
                        </input>
                        <br />
                        <label className="fat" htmlFor={`inputCtrl_Status`}>Enddate:</label>
                        <input
                            type='date'
                            autoComplete="off"
                            className="inputFieldPeriod inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            id='inputCtrl_Enddatum'
                            name='enddate'
                            placeholder='Enddatum'
                            value={period.enddate}
                            onChange={onChange}
                        >
                        </input>
                        <br />
                        <label className="fat" htmlFor={`inputCtrl_Status`}>State:</label>
                        <select id={`inputCtrl_Status`} name='state' onChange={onChangeState} value={period.state}>
                            <option>active</option>
                            <option>inactive</option>
                            <option>archived</option>
                        </select>
                        <br />
                        <button type="button" className="bttnUpAdd bttn" onClick={onBtnSaveClick}>{(flagAddUpdate === flagSwitchAddUpdate.ADD) ? 'Add' : 'Update'}</button>
                        <button type="button" className="bttnDelete bttn" onClick={openDeleteModal}>Delete</button>
                    </form>
                    <br></br>
                    <div style={{ margin: '10px', visibility: (flagSwitchAddUpdate.ADD === flagAddUpdate || idOfCurrentPeriod === null) ? 'hidden' : 'visible' }} className="grid-row-sm">

                        <label className="header">Courses in selected Period:</label>
                        {/*
                    <button type="button" className="bttnFilter bttn" onClick={openSortModal}>
                        <FontAwesomeIcon icon="filter" size="sm" />
                    </button>
*/}
                        <input
                            type='text'
                            autoComplete="off"
                            className="search inputFieldPeriod inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            id='inputCtrl_Search'
                            name='label'
                            placeholder='Search...'
                            //value={period.label}
                            onChange={onSearchFieldAddedCoursesChange}
                        ></input>
                        <button type="button" className="bttnSearch bttn" disabled >
                            <FontAwesomeIcon icon="search" size="sm" />
                        </button>
                    <div className="block">
                        <ul className="list-group">
                            {
                                coursesOfCurrPeriod.map((item) =>
                                    <li className="list-group-item d-flex justify-content-center align-items-center" style={{ border: '2px solid #023047', color: '#023047', backgroundColor: 'white'}} key={'curr' + item._id} onClick={() => {
                                        setCurrCourse(item);
                                        openRemoveCourseModal();
                                    }}>
                                        {item.label}
                                    </li>
                                )
                            }
                        </ul>
                    </div>
                    </div>
                </div>
                <div style={{ margin: '10px', visibility: (flagSwitchAddUpdate.ADD === flagAddUpdate || idOfCurrentPeriod === null) ? 'hidden' : 'visible' }} className="grid-row-sm">
                    <label className="header">Select Course to add:</label>
                    {/*}
                    <button type="button" className="bttnFilter bttn" onClick={openSortModal}>
                        <FontAwesomeIcon icon="filter" size="sm" />
                    </button>
                */}
                    <input
                        type='text'
                        autoComplete="off"
                        className="search inputFieldPeriod inputField"
                        //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                        id='inputCtrl_Search'
                        name='label'
                        placeholder='Search...'
                        //value={period.label}
                        onChange={onSearchFieldRemainingCoursesChange}
                    ></input>
                    <button type="button" className="bttnSearch bttn" disabled >
                        <FontAwesomeIcon icon="search" size="sm" />
                    </button>
                    <div className="block">
                    <ul className="list-group">
                        {
                            remainingCourses.map((item) =>
                                <li className="list-group-item d-flex justify-content-center align-items-center" style={{ border: '2px solid #023047', color: '#023047', backgroundColor: 'white'}} key={'remaining' + item._id} onClick={() => onRemainingCourseClick(item)}>
                                    {item.label}
                                </li>
                            )
                        }
                    </ul>
                </div>
                </div>
            </div>

            <ModalWindow
                stateOfModal={stateOfModal}
                modalTitle={'Confirm Delete'}
                modalText={'Do you really want to delete period \'' + getPeriodLabel() + '\' ?'}
                btnOkText={'Delete'}
                closeModal={closeModal}
                methodOkConfirmClick={deleteCurrPeriod}
            ></ModalWindow>
            <ModalWindow
                stateOfModal={stateOfModalRemoveCourse}
                modalTitle={'Confirm Remove'}
                modalText={'Do you really want to remove course \'' + getCourseLabel() + '\' ?'}
                btnOkText={'Remove'}
                closeModal={closeRemoveCourseModal}
                methodOkConfirmClick={onAddedCourseClick}
            ></ModalWindow>
            <ModalWindow
                stateOfModal={stateOfModalArchivePeriod}
                modalTitle={'Archivate this period?'}
                modalText={'Do you really want to archive \'' + getPeriodLabel() + '\' ?'}
                btnOkText={'Archive'}
                closeModal={closeArchivePeriodModal}
                methodOkConfirmClick={() => onChange()}
            ></ModalWindow>
        </div >
    );
    //#endregion
};
export default PeriodManagement;

