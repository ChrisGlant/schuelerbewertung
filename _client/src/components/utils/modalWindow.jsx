import React from 'react';
import { Modal, Button } from "react-bootstrap";
import '../../css/Period.css';
import '../../App.css';
import compatibility from '../../data/compatibility.PNG';

const ModalWindow = (props) => {
    const { stateOfModal, modalTitle, modalText, btnOkText, closeModal, methodOkConfirmClick, useImg } = props;

    return (
        <Modal
            show={stateOfModal.isOpen}
            backdrop="static"
            keyboard={false}>
            <Modal.Body className="modal">
                <Modal.Title>{modalTitle}</Modal.Title>
                {modalText}
                {
                    (useImg) ? <div><br></br><img alt="unknown" src={compatibility} height={'400px'} width={'450px'}></img></div> : ' '
                }
            </Modal.Body>
            <Modal.Footer>
                {
                    (closeModal) ? <Button className="bttnClose bttn" onClick={closeModal}>
                        Close
                    </Button> : ' '

                }
                <Button className="bttnOK bttn" onClick={methodOkConfirmClick}>{btnOkText}</Button>
            </Modal.Footer>
        </Modal>
    );
}

export default ModalWindow;