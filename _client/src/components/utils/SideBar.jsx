import React, { useState } from 'react';
import SideNav, { NavItem, NavIcon, NavText } from '@trendmicro/react-sidenav';
import { authService } from '../../services/authenticationService.js';
import '@trendmicro/react-sidenav/dist/react-sidenav.css';
import ClickOutside from './ClickOutside.jsx';
import {
    BrowserRouter as Router,
    Route,
    Redirect,
    Switch
} from "react-router-dom";
import '../../css/SideBar.css';

function SideBar(props) {
    const { items } = props;

    const [expanded, setExpanded] = useState(false)
    const [token] = useState(sessionStorage.getItem(process.env.REACT_APP_USER_DATA_TOKEN))
    const routesToHide = ['Login', 'Ratingscheme', 'RecordsOfPupil/:pupilId', 'GradesOfCourse/:courseId', 'Register', 'User', 'Logs', 'ChangePW/:id', 'ImportStudents', 'BulkAddRecords/:courseId'];
    const routesWithoutSB = ['Login', 'ChangePW/:id'];
    let cnt = 0;

    if (token === undefined || token === null) {
        return (
            <Router>
                <Switch>
                    {
                        items.filter(i => routesWithoutSB.includes(i.route)).map((i) => {
                            return (
                                <Route key={cnt++} path={'/' + i.route} exact component={i.component} >
                                </Route>
                            );
                        })
                    }
                    <Redirect exact from="/" to="/Login"></Redirect>
                </Switch>
            </Router>
        );
    }

    return (
        <Router>
            <Route render={({ location, history }) => (
                <React.Fragment>
                    <ClickOutside
                        onClickOutside={() => {
                            setExpanded(false);
                        }}
                    >
                        <SideNav
                            expanded={expanded}
                            onToggle={(ex) => {
                                setExpanded(ex);
                            }}
                            className="mynavbar"
                            onSelect={
                                (selected) => {
                                    items.map((i) => {
                                        if (i.route !== selected)
                                            i.selected = false;
                                        else
                                            i.selected = true;
                                        return true;
                                    })
                                    if (selected === 'Logout') {
                                        authService.logout();
                                        sessionStorage.removeItem(process.env.REACT_APP_USER_DATA_TOKEN);
                                        history.push('/Login');
                                        window.location.reload();
                                    } else {
                                        const to = '/' + selected;
                                        if (location.pathname !== to) {
                                            history.push(to);
                                        }
                                    }
                                }
                            }
                        >
                            <SideNav.Toggle />
                            <SideNav.Nav defaultSelected="" className="relative">

                                {items.filter(i => !routesToHide.some((_r) => _r === i.route)).map((i) => {
                                    return (<NavItem
                                        className="myNavitem"
                                        eventKey={i.route}
                                        key={i.route}
                                        style={i.route === 'Logout' ? {
                                            position: 'absolute',
                                            bottom: 0,
                                            margin: '0px',
                                            width: '100%',
                                            borderBottom: i.selected ? '5px solid #FB8500' : 'none'
                                        } : i.route === 'Settings' ? {
                                            position: 'absolute',
                                            bottom: 50,
                                            margin: '0px',
                                            width: '100%',
                                            borderBottom: i.selected ? '5px solid #FB8500' : 'none'
                                        } : { borderBottom: i.selected ? '5px solid #FB8500' : 'none' }}>
                                        <NavIcon>
                                            {i.icon}
                                        </NavIcon>
                                        <NavText>
                                            {i.name}
                                        </NavText>
                                    </NavItem>);
                                })}
                            </SideNav.Nav>
                        </SideNav>
                    </ClickOutside>
                    <main style={{ marginLeft: expanded ? '240px' : '64px' }}>
                        {
                            items.filter(i => !routesWithoutSB.includes(i.route)).map((i) => {
                                return (
                                    <Route key={cnt++} path={'/' + i.route} exact component={i.component} />
                                );
                            })
                        }
                        {/* <Redirect exact from="/Login" to="/"></Redirect> 57-bugfix*/}
                    </main>
                </React.Fragment>
            )}
            />
        </Router>
    );
}

export default SideBar;