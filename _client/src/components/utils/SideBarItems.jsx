import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'

import Home from '../home.jsx';
import Bewertung from '../bewertung.jsx';
import CourseManagement from '../course_management.jsx';
import PupilManagement from '../pupil_management.jsx';
import Timetable from '../timetable.jsx';
import PeriodManagement from '../period_management.jsx';
import loginPage from '../loginPage.jsx';
import SettingsPage from '../settingsPage.jsx';
import Bewertungsschema from '../bewertungsschema.jsx';
import RecordsOfPupil from '../recordsOfPupil.jsx';
import DBLogs from '../dbLogs.jsx';
import UserManagement from '../user_management.jsx';
import ImportStudents from '../importStudents.jsx';
import PWFGchangePW from '../PWFG_changePW.jsx';
import BulkAddRecords from '../bulkAddRecords.jsx';
import GradesOfCourse from '../gradesOfCourse.jsx';

library.add(fas)

const SidebarItems = [
    {
        name: "Home",
        route: 'Home',
        selected: true,
        icon: (<FontAwesomeIcon icon="home" size="lg" />),
        component: Home
    },
    {
        name: "GradesOfCourse",
        route: 'GradesOfCourse/:courseId',
        icon: (<FontAwesomeIcon icon="home" size="lg" />),
        component: GradesOfCourse
    },
    {
        name: "BulkAddRecords",
        route: 'BulkAddRecords/:courseId',
        icon: (<FontAwesomeIcon icon="home" size="lg" />),
        component: BulkAddRecords
    },
    {

        name: "ImportStudents",
        route: 'ImportStudents',
        icon: (<FontAwesomeIcon icon="home" size="lg" />),
        component: ImportStudents
    }, {
        name: "ChangePW",
        route: 'ChangePW/:id',
        icon: (<FontAwesomeIcon icon="home" size="lg" />),
        component: PWFGchangePW
    },
    {
        name: "Records",
        route: 'Records',
        icon: (<FontAwesomeIcon icon="pencil-alt" size="lg" />),
        component: Bewertung
    },
    {
        name: "Courses",
        route: 'Courses',
        icon: (<FontAwesomeIcon icon="book" size="lg" />),
        component: CourseManagement
    },
    {
        name: "Pupils",
        route: 'Pupils',
        icon: (<FontAwesomeIcon icon="user-graduate" size="lg" />),
        component: PupilManagement
    },
    {
        name: "Periods",
        route: 'Periods',
        icon: (<FontAwesomeIcon icon="calendar" size="lg" />),
        component: PeriodManagement
    },
    {
        name: "Timetable",
        route: 'Timetable',
        icon: (<FontAwesomeIcon icon="clock" size="lg" />),
        component: Timetable
    },
    {
        name: "Settings",
        route: 'Settings',
        icon: (<FontAwesomeIcon icon="cog" size="lg" />),
        component: SettingsPage
    },
    {
        name: "Ratingscheme",
        route: 'Ratingscheme',
        icon: (<FontAwesomeIcon icon="sign-in-alt" size="lg" />),
        component: Bewertungsschema
    },
    {
        name: "RecordsOfPupil",
        route: 'RecordsOfPupil/:pupilId',
        icon: (<FontAwesomeIcon icon="sign-in-alt" size="lg" />),
        component: RecordsOfPupil
    },
    {
        name: "LogIn",
        route: 'Login',
        icon: (<FontAwesomeIcon icon="sign-in-alt" size="lg" />),
        component: loginPage
    },
    {
        name: "Logs",
        route: 'Logs',
        icon: (<FontAwesomeIcon icon="sign-in-alt" size="lg" />),
        component: DBLogs
    },
    {
        name: "LogOut",
        route: 'Logout',
        icon: (<FontAwesomeIcon icon="sign-out-alt" size="lg" />),
        component: undefined
    }, {
        name: "User",
        route: 'User',
        icon: (<FontAwesomeIcon icon="home" size="lg" />),
        component: UserManagement
    }
];

export default SidebarItems;