import AppError from '../../AppError';

function errorHandler(reason, toast) {
    let msg = reason;
    if (reason instanceof AppError) {
        msg = reason.statusCode + ' ' + reason.message;
    }
    console.log(msg);
    if (toast)
        toast.error(msg);
}

function sortAndSetPupils(_pupils, setPupils) {
    if (_pupils.length > 0) {
        _pupils.sort((a, b) => { return (a.lastname + a.firstname).localeCompare(b.lastname + b.firstname) });
        //_pupils.sort((a, b) => { return (b.lastname + b.firstname).localeCompare(a.lastname + a.firstname) });
        setPupils([..._pupils]);
    }
}

function getDateTimeString(datestring) {
    let d = new Date(datestring);
    return d.getDate() + '.' + (d.getMonth() + 1) + '.' + d.getFullYear() + ' ' + d.getHours() + ':' + d.getMinutes();
}

function compareRatingSchemes(a, b) {
    if (a.value < b.value) {
        return -1;
    }
    if (a.value > b.value) {
        return 1;
    }
    // a muss gleich b sein
    return 0;
}

function parseDateIntoValidFormat(date) {
    return date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + (date.getDate() + 1)).slice(-2) + 'T' + ('0' + (date.getHours() + 1)).slice(-2) + ':' + ('0' + (date.getMinutes() + 1)).slice(-2);
}

function getAndShowAllMsgs(formValidationInfo, toast) {
    Object.keys(formValidationInfo).forEach((element) => {
        if (!formValidationInfo[element].valid)
            toast.error(formValidationInfo[element].msg);
    })
}

function formOK(fvi) {
    // if (!fvi)
    //     fvi = formValidationInfo;
    return Object.keys(fvi).every((element) => fvi[element].valid);
}


function onSchemeSelectChanged(event, setIdOfSelectedScheme) {
    const { value } = event.target; //id
    setIdOfSelectedScheme(value);
}

function checkWholeRecordObj(formValidationInfo, setFormValidationInfo, record, setRecord, parseDateIntoValidFormat) {
    let newFormValidationInfo = {};
    Object.keys(formValidationInfo).forEach((element) => {
        newFormValidationInfo[element] = checkRatingInput(element, record[element], record, setRecord, parseDateIntoValidFormat);
    })
    setFormValidationInfo({ ...newFormValidationInfo });
    return newFormValidationInfo;
}

function checkRatingInput(name, value, record, setRecord, parseDateIntoValidFormat) {
    let tempObj = {
        valid: false,
        msg: ''
    }
    switch (name) {
        case 'validOn':
            if (value === null || value === undefined) {
                setRecord({ ...record, 'validOn': parseDateIntoValidFormat(new Date(new Date(Date.now()).toISOString())) })
                tempObj.valid = true;
            } else {
                tempObj.valid = true;
            }
            break;
        case 'rating':
            if (value === null || value === undefined) {
                tempObj.msg = 'no rating selected';
            } else {
                tempObj.valid = true;
            }
            break;
        default:
            return;
    }
    return tempObj;
}

function onSignSelectChanged(event, signs, setRecord, record) {
    const { value } = event.target;
    let rating = signs.find((el) => el._id === value);
    setRecord({ ...record, 'rating': rating, 'weight': rating.value });
}
export { onSignSelectChanged, errorHandler, sortAndSetPupils, getDateTimeString, checkRatingInput, compareRatingSchemes, onSchemeSelectChanged, parseDateIntoValidFormat, getAndShowAllMsgs, formOK, checkWholeRecordObj }