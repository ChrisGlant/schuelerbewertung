import React from 'react';
import '../../css/LogIn.css';
import '../../App.css';

export default function TextInputWithValidation(props) {

    const { objectKey, placeholder, formValidationInfo, onChange, type } = props;
    let inputType = type || "text";
    let key = objectKey;
    return (
        <div>
            <input
                type={inputType}
                autoComplete="off"
                className={!formValidationInfo[key]?.valid ? "inputFields is-invalid" : "inputFields is-valid"}
                id={`inputCtrl_${key}`}
                name={key}
                placeholder={placeholder}
                onChange={onChange}>
            </input>
            <div className="feedback valid-feedback">{formValidationInfo[key]?.msg}</div>
            <div className="feedback invalid-feedback">{formValidationInfo[key]?.msg}</div>
        </div>
    )

}
