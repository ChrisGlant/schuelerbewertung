import { toast } from 'react-toastify';
import React, { useState, useEffect } from 'react';
import 'react-toastify/dist/ReactToastify.css';
import '../css/Timetable.css';
import '../App.css';
import { errorHandler, getAndShowAllMsgs, formOK } from './utils/utils';
import { lessonService } from '../services/lessonService.js';
import { courseService } from '../services/courseService';
import { HexColorPicker } from "react-colorful";
import ModalWindow from './utils/modalWindow.jsx';


const Timetable = () => {
    const _weekdays = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday'];

    const flagSwitchAddUpdate = {
        ADD: 0,
        UPDATE: 1
    };
    const [flagAddUpdate, setFlagAddUpdate] = useState(flagSwitchAddUpdate.UPDATE);

    const [stateOfModal, setStateOfModal] = useState({
        isOpen: false
    });
    const openModal = () => setStateOfModal({ isOpen: true });
    const closeModal = () => setStateOfModal({ isOpen: false });
    const [idOfSelectedCourse, setIdOfSelectedCourse] = useState(null);
    const [courses, setCourses] = useState([]);

    const [lesson, setLesson] = useState({});
    const [idOfCurrentLesson, setIdOfCurrentLesson] = useState(null);
    const emptyLessonObj = {
        courseLabel: '',
        roomNr: '',
        weekday: 'monday',
        starttime: '00:00',
        endtime: '00:00',
        colorCode: '#023047'
    }

    const [timetable, setTimetable] = useState({});

    useEffect(() => {
        init();
    }, []);

    const init = () => {
        lessonService.getAll().then((_lessons) => {
            let tempTimetable = {};
            _weekdays.forEach((_weekday) => {
                tempTimetable[_weekday] = _lessons.filter(_lesson => _lesson.weekday === _weekday);
                if (!tempTimetable[_weekday] || tempTimetable[_weekday].length === 0)
                    tempTimetable[_weekday] = [];
            })
            setTimetable({ ...tempTimetable });
        }).catch((reason) => {
            errorHandler(reason, toast);
        })
    }

    useEffect(() => {
        courseService.getAll().then((_courses) => {
            setCourses(_courses);
        }).catch((reason) => {
            errorHandler(reason, toast);
        })
    }, [idOfCurrentLesson]);

    useEffect(() => {
        checkWholeLessonObj();
    }, [lesson]);

    let old, min, max;


    const initialFormValidationInfo = {
        'courseLabel': {
            valid: false,
            msg: 'No input for courseLabel'
        },
        'roomNr': {
            valid: false,
            msg: 'No input for roomNr'
        },
        'starttime': {
            valid: false,
            msg: 'No input for starttime'
        },
        'endtime': {
            valid: false,
            msg: 'No input for endtime'
        },
        'courseId': {
            valid: false,
            msg: 'No course selected'
        },
        'colorCode': {
            valid: false,
            msg: 'No input for colorCode'
        }
    };
    const [formValidationInfo, setFormValidationInfo] = useState(initialFormValidationInfo);

    const onChange = (event) => {
        const { name, value } = event.target;
        setLesson({ ...lesson, [name]: value });
    }

    const setColor = (color) => {
        lesson.colorCode = color;
        setLesson(lesson);
    }

    function checkWholeLessonObj() {
        let newFormValidationInfo = {};
        Object.keys(formValidationInfo).forEach((element) => {
            newFormValidationInfo[element] = checkInput(element, lesson[element]);
        })
        setFormValidationInfo({ ...newFormValidationInfo });
    }

    function checkInput(name, value) {
        let tempObj = {
            valid: false,
            msg: ''
        }
        switch (name) {
            case 'courseLabel':
                if (!value) {
                    tempObj = {
                        valid: false,
                        msg: 'No input for courselabel'
                    };
                } else {
                    tempObj = {
                        valid: true,
                        msg: ''
                    };
                }
                break;
            case 'roomNr':
                if (!value) {
                    tempObj = {
                        valid: false,
                        msg: 'No input for roomNr'
                    };
                } else {
                    tempObj = {
                        valid: true,
                        msg: ''
                    };
                }
                break;
            case 'starttime':
                if (!value) {
                    tempObj = {
                        valid: false,
                        msg: 'No input for starttime'
                    };
                } else {
                    tempObj = {
                        valid: true,
                        msg: ''
                    };
                }
                break;
            case 'endtime':
                if (!value) {
                    tempObj = {
                        valid: false,
                        msg: 'No input for endtime'
                    };
                } else if (diff_minutes(lesson.starttime, value) <= 0) {
                    tempObj = {
                        valid: false,
                        msg: 'Endtime cannot be before than or the same as starttime'
                    };
                } else {
                    tempObj = {
                        valid: true,
                        msg: ''
                    };
                }
                break;
            case 'courseId':
                if (!value) {
                    tempObj = {
                        valid: false,
                        msg: 'No course selected'
                    };
                } else {
                    tempObj = {
                        valid: true,
                        msg: ''
                    };
                }
                break;
            case 'colorCode':
                if (!value) {
                    tempObj = {
                        valid: false,
                        msg: 'No color selected'
                    };
                } else {
                    tempObj = {
                        valid: true,
                        msg: ''
                    };
                }
                break;
            default:
                break;
        }
        return tempObj;
    }

    const onBtnSaveClick = () => {
        if (!formOK(formValidationInfo)) {
            getAndShowAllMsgs(formValidationInfo, toast);
            return;
        }

        switch (flagAddUpdate) {
            case flagSwitchAddUpdate.ADD:
                lessonService.create(lesson).then(() => {
                    init();
                    toast.success('lesson created');
                }).catch((reason) => {
                    errorHandler(reason, toast);
                })
                break;
            case flagSwitchAddUpdate.UPDATE:
                if (!idOfCurrentLesson) {
                    toast.error('error: probably no pupil selected to delete');
                    break;
                }
                lessonService.update(idOfCurrentLesson, lesson).then(() => {
                    init();
                    toast.success('lesson updated');
                }).catch((reason) => {
                    errorHandler(reason, toast);
                })
                break;
            default:
                break;
        }
    }
    const openDeleteModal = () => {
        try {
            if (lesson === null) {
                toast.error('no course selected')
            } else {
                openModal();
            }
        } catch (err) {
            errorHandler(err, toast);
        }
    }

    const onLessonClick = (item) => {
        setLesson(item);
        setIdOfCurrentLesson(item._id);
        setFlagAddUpdate(flagSwitchAddUpdate.UPDATE);
    }

    const onBtnLessonAddClick = () => {
        setFlagAddUpdate(flagSwitchAddUpdate.ADD);
        setLesson(emptyLessonObj);
        setIdOfCurrentLesson(null);
        setIdOfSelectedCourse(null);
    }

    const onCourseClick = (_course) => {
        let tempL = lesson;
        tempL.courseId = _course._id;
        setLesson({ ...tempL });
        setIdOfSelectedCourse(_course._id);
    }

    const deleteCurrLesson = () => {
        closeModal();

        lessonService.delete(idOfCurrentLesson).then(() => {
            setIdOfCurrentLesson(null);
            init();
            toast.success('lesson deleted');
        }).catch((reason) => {
            errorHandler(reason, toast);
        })
    }

    const getLessonLabel = () => {
        return lesson.courseLabel;
    }

    const getLessonWeekday = () => {
        return lesson.weekday;
    }

    let arrHours = [];
    Object.keys(timetable).forEach((tag) => {
        timetable[tag].forEach((item) => {

            if (!max) {
                max = item.endtime;
            }
            else
                if (get_time(item.endtime) > get_time(max))
                    max = item.endtime;

            if (!min) {
                min = item.starttime;
            }
            else
                if (get_time(item.starttime) < get_time(min))
                    min = item.starttime;

            let last = 'monday';
            for (let tag in timetable) {
                if (timetable[tag].length > 0)
                    last = tag;
            }

            if (timetable[tag][timetable[tag].length - 1] === item && tag === last) {
                let grenze = parseInt(max.split(':')[1], 10) === 0 ? parseInt(max.split(':')[0], 10) - 1 : parseInt(max.split(':')[0], 10);
                for (let i = parseInt(min.split(':')[0], 10); i <= grenze; i++) {
                    arrHours.push(
                        (<li className="myLi" style={{ margin: '0px', borderRadius: '0px', borderBottom: 'none', borderTop: '1px solid white', color: 'white', height: 60 + 'px' }} key={i}>
                            <p style={{ marginBottom: '0px', marginRight: '0px', alignSelf: 'flex-start' }}>{i + ':00'}</p>
                        </li>))
                }
            }
        })
    })

    return (
        <div>
            <h1>Timetable</h1>

            <div className="cont" style={{ margin: '15px' }}>
                <div style={{ width: '70%', display: 'inline-block' }}>
                    {
                        <div>
                            <div style={{ padding: '2px', paddingTop: '0px', width: '10%', display: 'inline-block' }}>
                                <label className="header" style={{ visibility: 'hidden', marginBottom: "5px" }}>hours</label>
                                <ul className="myUl">
                                    {
                                        arrHours
                                    }
                                </ul>
                            </div>
                            {Object.keys(timetable).map((tag) => {
                                old = '00:00';
                                return (
                                    <div key={tag} style={{ padding: '2px', paddingTop: '0px', width: '18%', display: 'inline-block' }}>
                                        <label className="header" style={{ marginBottom: "5px" }}>{tag}</label>
                                        <ul className="myUl">
                                            {
                                                timetable[tag].map((item) => {
                                                    let erg = diff_minutes(item.starttime, item.endtime), mar = 0;
                                                    if (!min) {
                                                        min = item.starttime;
                                                    }
                                                    else
                                                        if (get_time(item.starttime) < get_time(min))
                                                            min = item.starttime;

                                                    if (old !== '00:00')
                                                        if (get_time(item.starttime) < get_time(old))
                                                            mar = diff_minutes(old, item.starttime);
                                                        else
                                                            mar = diff_minutes(old, item.starttime); // +1 in order to get at least a slim margin
                                                    else
                                                        mar = diff_minutes(min, item.starttime);

                                                    old = item.endtime;

                                                    return (<li className="myLi" style={{ positon: 'fixed', borderTop: '1px solid white', borderBottom: '1px solid white', border: (idOfCurrentLesson && idOfCurrentLesson === item._id) ? '3px solid red' : 'none', background: item.colorCode, height: erg + 'px', marginTop: mar + 'px' }} key={item.courseLabel} onClick={() => onLessonClick(item)
                                                    }>
                                                        <p style={{ fontWeight: 'bold', marginTop: '0px', marginLeft: '0px', alignSelf: 'flex-start' }}>{item.courseLabel} ({item.roomNr})</p>
                                                        <p style={{ marginBottom: '0px', marginRight: '0px', alignSelf: 'flex-end' }}>{item.starttime} - {item.endtime}</p>
                                                    </li>);
                                                })
                                            }
                                        </ul>
                                    </div>
                                );
                            })}
                        </div>
                    }
                    <button type="button" className="bttnAdd bttn" style={{ marginLeft: '2px', marginTop: '5px', borderRadius: "25px" }} onClick={onBtnLessonAddClick} >Add Lesson</button>
                </div>
                <div style={{ marginLeft: '5px', marginRight: '5px', width: '30%', display: 'inline-block', visibility: (flagSwitchAddUpdate.UPDATE === flagAddUpdate && idOfCurrentLesson === null) ? 'hidden' : 'visible' }}>
                    <label className="header">Lesson-Info</label>
                    <form style={{ width: '100%' }}>
                        <label style={{ marginTop: '5px' }} className="fat" htmlFor={`inputCtrl_label`}>courselabel:</label>
                        <input
                            type='text'
                            autoComplete="off"
                            className="inputFieldPeriod inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            _id='inputCtrl_label'
                            placeholder='courseLabel'
                            name='courseLabel'
                            value={lesson.courseLabel}
                            onChange={onChange}
                        >
                        </input>
                        <br />
                        <label className="fat" htmlFor={`inputCtrl_roomNr`}>roomNr:</label>
                        <input
                            type='text'
                            autoComplete="off"
                            className="inputFieldPeriod inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            _id='inputCtrl_roomNr'
                            placeholder='roomNr'
                            name='roomNr'
                            value={lesson.roomNr}
                            onChange={onChange}
                        >
                        </input>
                        <br />
                        <label className="fat" htmlFor={`inputCtrl_Email`}>weekday:</label>
                        <select _id={`inputCtrl_Weekday`} courseLabel='weekdays' name='weekday' onChange={onChange}>
                            <option selected={(lesson.weekday === "monday") ? true : false}  >monday</option>
                            <option selected={(lesson.weekday === "tuesday") ? true : false} >tuesday</option>
                            <option selected={(lesson.weekday === "wednesday") ? true : false} >wednesday</option>
                            <option selected={(lesson.weekday === "thursday") ? true : false} >thursday</option>
                            <option selected={(lesson.weekday === "friday") ? true : false} >friday</option>
                        </select>
                        <br />
                        <label className="fat" htmlFor={`inputCtrl_starttime`}>starttime:</label>
                        <input
                            type='time'
                            autoComplete="off"
                            className="inputFieldPeriod inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            _id='inputCtrl_starttime'
                            placeholder='starttime'
                            name='starttime'
                            value={lesson.starttime}
                            onChange={onChange}
                        >
                        </input>
                        <br />
                        <label className="fat" htmlFor={`inputCtrl_endtime`}>endtime:</label>
                        <input
                            type='time'
                            autoComplete="off"
                            className="inputFieldPeriod inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            _id='inputCtrl_endtime'
                            placeholder='endtime'
                            name='endtime'
                            value={lesson.endtime}
                            onChange={onChange}
                        >
                        </input>
                        <HexColorPicker
                            name='colorCode'
                            color={lesson.colorCode}
                            onChange={setColor} />
                        <br></br>
                        <button type="button" className="bttnUpAdd bttn" onClick={onBtnSaveClick}>{(flagAddUpdate === flagSwitchAddUpdate.ADD) ? 'Add' : 'Update'}</button>
                        <button type="button" className="bttnDelete bttn" onClick={() => { (idOfCurrentLesson) ? openDeleteModal() : toast.error('no lesson selected') }}>Delete</button>

                    </form>
                    <br></br>
                    <label className="header">Select Course:</label>
                    <ul className="list-group">
                        {
                            courses.map((item) =>
                                <li className="list-group-item d-flex justify-content-center align-items-center" style={{ border: '2px solid #023047', color: '#023047', backgroundColor: (idOfSelectedCourse === item._id) ? '#FB8500' : 'white' }} key={'c' + item._id} onClick={() => onCourseClick(item)}>
                                    {item.label}
                                </li>
                            )
                        }
                    </ul>
                </div>
            </div>
            <ModalWindow
                stateOfModal={stateOfModal}
                modalTitle={'Confirm Delete'}
                modalText={'Do you really want to delete lesson \'' + getLessonLabel() + ' on ' + getLessonWeekday() + '\' ?'}
                btnOkText={'Delete'}
                closeModal={closeModal}
                methodOkConfirmClick={deleteCurrLesson}
            ></ModalWindow>
        </div >

    );
};

function diff_minutes(dt2, dt1) {
    dt2 = get_time(dt2);
    dt1 = get_time(dt1);
    var diff = dt1 - dt2;
    return diff;
}

function get_time(dt2) {
    return parseInt(dt2.split(':')[0]) * 60 + parseInt(dt2.split(':')[1]);
}

export default Timetable;