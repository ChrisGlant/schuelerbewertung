import { toast } from 'react-toastify';
import React, { useState, useEffect } from 'react';
import { userService } from '../services/userService.js';
import '../css/Period.css';
import '../css/User.css';
import '../App.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import AppError from '../AppError';

const UserManagement = () => {
    //---States, UseEffects---//
    //#region 
    const [isShown1, setIsShown1] = useState(true);
    const [isShown2, setIsShown2] = useState(true);

    let defaultUser = {
        'firstname': '',
        'lastname': '',
        'username': '',
        'password': '',
        'state': 'active'
    };

    const [passwords, setPasswords] = useState({
        'password1': '',
        'password2': '',
    });

    const [user, setUser] = useState(defaultUser);
    const [formValidationInfo, setFormValidationInfo] = useState(
        {
            'firstname': {
                valid: true,
                msg: ''
            },
            'lastname': {
                valid: true,
                msg: ''
            },
            'username': {
                valid: true,
                msg: ''
            },
            'password1': {
                valid: true,
                msg: ' '
            },
            'password2': {
                valid: true,
                msg: ' '
            },
            'form': {
                valid: false,
                msg: 'fields cannot be empty!'
            }
        });

    useEffect(() => {
        init();
    }, []);

    const init = () => {
        userService.getCurrent().then((_curruser) => {
            setUser(_curruser);
            setPasswords({
                'password1': _curruser.password,
                'password2': _curruser.password,
            })
        }).catch((reason) => {
            let msg = reason;
            if (reason instanceof AppError) {
                if (reason.statusCode !== 404) {
                    msg = reason.statusCode + ' ' + reason.message;
                    console.log(msg);
                    toast.error(msg);
                }
            } else {
                console.log(msg);
                toast.error(msg);
            }
        })
    }

    const onChange = (event) => {
        const { name, value } = event.target;
        if (name !== 'password1' && name !== 'password2')
            setUser({ ...user, [name]: value });
        if (name !== 'username')
            validateField(name, value);
    }

    const checkFirstname = (value) => {
        let msg = 'Firstname is okay';
        let valid = true;
        if (!value) {
            msg = 'provide a first name';
            valid = false;
        } else {
            if (value.length < 4) {
                msg = 'first name is too short';
                valid = false;
            } else if (value.length > 20) {
                msg = 'first name is too long';
                valid = false;
            }
        }
        return { valid, msg };

    }

    const checkLastname = (value) => {
        let msg = 'Lastname is okay';
        let valid = true;
        if (!value) {
            msg = 'provide a last name';
            valid = false;
        }
        return { valid, msg };
    }

    const checkPW = (value, nameOfField) => {
        setPasswords({ ...passwords, [nameOfField]: value });
        let msg = 'Password is okay';
        let valid = true;
        if (!value) {
            msg = 'provide a Password';
            valid = false;
        } else {
            if (value.length < 4) {
                msg = 'Password is too short';
                valid = false;
            } else if (value.length > 20) {
                msg = 'Password is too long';
                valid = false;
            } else if (nameOfField === 'password1' && value !== passwords.password2) {
                msg = 'Passwords do not match';
                valid = false;
            } else if (nameOfField === 'password2' && value !== passwords.password1) {
                msg = 'Passwords do not match';
                valid = false;
            } else {
                return {
                    'password1': {
                        'valid': valid,
                        'msg': msg
                    },
                    'password2': {
                        'valid': valid,
                        'msg': msg
                    }
                };
            }
        }
        return { valid, msg };
    }

    const checkEmail = (value) => {
        let msg = 'email is okay';
        let valid = true;
        if (!value) {
            msg = 'provide an email';
            valid = false;
        } else {
            if (!validateEmail(value)) {
                msg = 'invalid email format';
                valid = false;
            }
        }
        return { valid, msg };
    }

    const validateEmail = (email) => {
        return String(email)
            .toLowerCase()
            .match(
                /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            );
    };


    const validateField = (name, value) => {

        let validationInfo;
        switch (name) {
            case 'firstname':
                validationInfo = checkFirstname(value);
                break;
            case 'lastname':
                validationInfo = checkLastname(value);
                break;
            case 'email':
                validationInfo = checkEmail(value);
                break;
            case 'password1':
            case 'password2':
                validationInfo = checkPW(value, name);
                break;
            default:
                break;
        }

        let newFormValidationInfo;

        if (!['password1', 'password2'].includes(name) || (!validationInfo.password1 && !validationInfo.password2)) {
            newFormValidationInfo = {
                ...formValidationInfo,
                [name]: validationInfo
            };
        } else {
            newFormValidationInfo = {
                ...formValidationInfo,
                'password1': validationInfo.password1,
                'password2': validationInfo.password2
            };
            setUser({ ...user, 'password': value });
        }

        if (areSomeFieldsEmpty(newFormValidationInfo)) {
            newFormValidationInfo.form.valid = false;
            newFormValidationInfo.form.msg = 'fields cannot be empty!';
        } else if (isFormValidationObjectValid(newFormValidationInfo)) {
            newFormValidationInfo.form.valid = true;
            newFormValidationInfo.form.msg = '';
        } else {
            newFormValidationInfo.form.valid = false;
            newFormValidationInfo.form.msg = 'Some data is invalid';
        }
        setFormValidationInfo(newFormValidationInfo);
    }

    const isFormValidationObjectValid = (obj) => {
        let keys = Object.keys(obj);
        return !keys.some((element) => (element !== 'form' && !obj[element].valid));
    }

    const areSomeFieldsEmpty = () => {
        let keys = Object.keys(user);
        return keys.some((element) => user[element] === '');
    }

    function getAndShowAllMsgs() {
        Object.keys(formValidationInfo).forEach((element) => {
            if (!formValidationInfo[element].valid)
                toast.error(formValidationInfo[element].msg);
        })
    }


    const onBtnUpdateClick = () => {
        if (!isFormValidationObjectValid(formValidationInfo)) {
            getAndShowAllMsgs();
            return;
        }
        userService.update(user._id, user).then(() => {
            toast.success('user updated');
        }).catch((reason) => {
            let msg = reason;
            if (reason instanceof AppError) {
                if (reason.statusCode !== 404) {
                    msg = reason.statusCode + ' ' + reason.message;
                    console.log(msg);
                    toast.error(msg);
                }
            } else {
                console.log(msg);
                toast.error(msg);
            }
        })
    }

    //#endregion
    //---------HTML----------//
    //#region 
    return (
        <div>
            <h1>User</h1>
            <div className="grid-container">
                <div style={{ margin: '10px' }} className="grid-row-sm">
                    <label className="header">User-Profile</label>
                    <form className="inline form-group col">
                        <label className="fat" htmlFor={`inputCtrl_Label`}>Firstname:</label>
                        <input
                            type='text'
                            autoComplete="off"
                            className="inputFieldPeriod inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            id='inputCtrl_Label'
                            name='firstname'
                            placeholder='firstname'
                            value={user.firstname}
                            onChange={onChange}
                        >
                        </input>
                        <br />
                        <label className="fat" htmlFor={`inputCtrl_Label`}>Lastname:</label>
                        <input
                            type='text'
                            autoComplete="off"
                            className="inputFieldPeriod inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            id='inputCtrl_Label'
                            name='lastname'
                            placeholder='lastname'
                            value={user.lastname}
                            onChange={onChange}
                        >
                        </input>
                        <br />
                        <label className="fat" htmlFor={`inputCtrl_Label`}>Username:</label>
                        <input
                            type='text'
                            autoComplete="off"
                            className="inputFieldPeriod inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            id='inputCtrl_Label'
                            name='username'
                            placeholder='username'
                            value={user.username}
                            onChange={onChange}
                        >
                        </input><br />
                        <label className="fat" htmlFor={`inputCtrl_Label`}>Email:</label>
                        <input
                            type='text'
                            autoComplete="off"
                            className="inputFieldPeriod inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            id='inputCtrl_Label'
                            name='email'
                            placeholder='email'
                            value={user.email}
                            onChange={onChange}
                        >
                        </input>
                        <br />
                        <label className="fat" htmlFor={`inputCtrl_Label`}>Password:</label>
                        <input
                            type={isShown1 ? 'password' : 'text'}
                            autoComplete="off"
                            className="inputFieldPasword inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            id='inputCtrl_Label'
                            name='password1'
                            placeholder='password'
                            value={passwords.password1}
                            onChange={onChange}
                        >
                        </input>
                        <button type="button" className="bttnEyeUser bttnEye bttn"
                            onMouseEnter={() => setIsShown1(false)}
                            onMouseLeave={() => setIsShown1(true)}>
                            <FontAwesomeIcon icon="eye" size="lg" />
                        </button>
                        <br />
                        <label className="fat" htmlFor={`inputCtrl_Label`}>Password repeat:</label>
                        <input
                            type={isShown2 ? 'password' : 'text'}
                            autoComplete="off"
                            className="inputFieldPaswordRP inputFieldPasword inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            id='inputCtrl_Label'
                            name='password2'
                            placeholder='password'
                            value={passwords.password2}
                            onChange={onChange}
                        >
                        </input>
                        <button type="button" className="bttnEyeUser bttnEye bttn"
                            onMouseEnter={() => setIsShown2(false)}
                            onMouseLeave={() => setIsShown2(true)}>
                            <FontAwesomeIcon icon="eye" size="lg" />
                        </button>
                        <br />
                    </form>
                    <button type="button" className="bttnUpAdd bttn" onClick={onBtnUpdateClick}>Update</button>

                </div>

            </div>
        </div >
    );
    //#endregion
};
export default UserManagement;