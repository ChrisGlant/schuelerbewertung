import React, { useState, useEffect } from 'react';
import { courseService } from '../services/courseService.js';
import { errorHandler } from './utils/utils';
import { toast } from 'react-toastify';
import { cprService } from '../services/cprService.js';
import { recordService } from '../services/recordService.js';
import { pupilService } from '../services/pupilService.js';
import { useParams } from 'react-router-dom';
import { Line } from 'react-chartjs-2';


import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';

ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
);

const options = {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
        y: {
            max: 100,
            min: 0,
            ticks: {
                stepSize: 1
            }
        }
    }
}

const GradesOfCourse = () => {

    let { courseId } = useParams();
    const [courseLbl, setCourseLbl] = useState('');
    const [data, setData] = useState([]);
    const [labels, setLabels] = useState([]);
    const [pupilGradeObjs, setPupilGradeObjs] = useState([]);

    useEffect(() => {
        // init();
        courseService.getById(courseId).then((_course) => {
            setCourseLbl(_course.label);
        }).catch((reason) => {
            errorHandler(reason, toast);
        })
        cprService.getPupilsById(courseId).then((_cprs) => {
            recordService.getGradeSuccOfCourse(courseId).then((_gso) => {
                let promises = [];
                _cprs.forEach((_cprsObj) => {
                    promises.push(pupilService.getById(_cprsObj.pupilId));
                })
                Promise.all(promises).then((values) => setPupilGradeObjs(values.map((_pupil) => { return ({ pupil: _pupil, grade: _gso.find((_el) => _el.pupilId === _pupil._id).grade }); })))
            }).catch((reason) => {
                errorHandler(reason, toast);
            })
        }).catch((reason) => {
            errorHandler(reason, toast);
        })

    }, []);

    useEffect(() => {
        setData([...pupilGradeObjs.map((_el) => {
            return ((Math.round(_el.grade * 10) / 10));
        })]);

        setLabels([...pupilGradeObjs.map((_el) => {
            return (_el.pupil.lastname.toUpperCase() + ' ' + _el.pupil.firstname)
        })]);
    }, [pupilGradeObjs])


    function getAvg(data) {
        const sum = data.reduce((a, b) => a + b, 0);
        let avg = (sum / data.length);
        let arr = [];
        for (let i = 0; i < data.length; i++)
            arr.push(avg);
        return arr;
    }

    return (
        <div>
            <h1>Grades of Course {courseLbl}</h1>
            {/* <div className="block" style={{ height: '700px' }}> */}
            <div style={{ height: "500px", width: "100%" }}>
                <Line options={options} data={{
                    labels,
                    datasets: [
                        {
                            label: 'Grades',
                            data,
                            borderColor: 'rgb(255, 99, 132)'
                        },
                        {
                            label: 'Average',
                            data: getAvg(data),
                            borderColor: 'rgb(0, 255, 132)'
                        }
                    ],
                }} />
            </div>
            <div className="block" style={{ height: "300px", width: "100%" }}>
                <ul className="list-group">
                    {
                        (pupilGradeObjs.length === 0) ? <h6>no pupil in this course has a record</h6> :
                            pupilGradeObjs.map((_el) => {
                                return (
                                    <li className="list-group-item d-flex justify-content-between align-items-center" style={{ border: '2px solid #023047', color: '#023047', backgroundColor: 'white' }}>
                                        <div class="ms-4 me-auto">
                                            <div class="fw-bold">{_el.pupil.lastname.toUpperCase() + ' ' + _el.pupil.firstname}</div>
                                        </div>
                                        <div class="ms-2 me-auto" style={{ color: 'red' }}>
                                            <div class="fw-bold">{(isNaN(_el.grade) || _el.grade === 0) ? 'has no records' : (Math.round(_el.grade * 10) / 10) + '%'}</div>
                                        </div>
                                    </li>
                                );
                            })
                    }
                </ul>
            </div>
        </div>
    );
};

export default GradesOfCourse;