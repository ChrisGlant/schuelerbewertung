import React, { useState } from 'react';
import { authService } from '../services/authenticationService.js';
import '../css/LogIn.css';
import '../App.css';
import { toast } from 'react-toastify';
import logo from '../data/logo.png';
import { errorHandler } from './utils/utils';
import { useHistory, useParams } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const PWFGchangePW = () => {
    const history = useHistory();
    let { id } = useParams();
    const [isShown1, setIsShown1] = useState(true);
    const [isShown2, setIsShown2] = useState(true);
    const [passwords, setPasswords] = useState({
        'password1': '',
        'password2': '',
    });

    const [formValidationInfo, setFormValidationInfo] = useState(
        {

            'password1': {
                valid: true,
                msg: ' '
            },
            'password2': {
                valid: true,
                msg: ' '
            },
            'form': {
                valid: false,
                msg: 'fields cannot be empty!'
            }
        });


    const onChange = (event) => {
        const { name, value } = event.target;
        validateField(name, value);
    }

    const validateField = (name, value) => {

        let validationInfo;
        switch (name) {
            case 'password1':
            case 'password2':
                validationInfo = checkPW(value, name);
                break;
            default:
                break;
        }

        let newFormValidationInfo;

        if (!['password1', 'password2'].includes(name) || (!validationInfo.password1 && !validationInfo.password2)) {
            newFormValidationInfo = {
                ...formValidationInfo,
                [name]: validationInfo
            };
        } else {
            newFormValidationInfo = {
                ...formValidationInfo,
                'password1': validationInfo.password1,
                'password2': validationInfo.password2
            };
        }

        if (areSomeFieldsEmpty(newFormValidationInfo)) {
            newFormValidationInfo.form.valid = false;
            newFormValidationInfo.form.msg = 'fields cannot be empty!';
        } else if (isFormValidationObjectValid(newFormValidationInfo)) {
            newFormValidationInfo.form.valid = true;
            newFormValidationInfo.form.msg = '';
        } else {
            newFormValidationInfo.form.valid = false;
            newFormValidationInfo.form.msg = 'Some data is invalid';
        }
        setFormValidationInfo(newFormValidationInfo);
    }

    const isFormValidationObjectValid = (obj) => {
        let keys = Object.keys(obj);
        return !keys.some((element) => (element !== 'form' && !obj[element].valid));
    }

    const areSomeFieldsEmpty = () => {
        let keys = Object.keys(passwords);
        return keys.some((element) => passwords[element] === '');
    }

    const checkPW = (value, nameOfField) => {
        setPasswords({ ...passwords, [nameOfField]: value });
        let msg = 'Password is okay';
        let valid = true;
        if (!value) {
            msg = 'provide a Password';
            valid = false;
        } else {
            if (value.length < 4) {
                msg = 'Password is too short';
                valid = false;
            } else if (value.length > 20) {
                msg = 'Password is too long';
                valid = false;
            } else if (nameOfField === 'password1' && value !== passwords.password2) {
                msg = 'Passwords do not match';
                valid = false;
            } else if (nameOfField === 'password2' && value !== passwords.password1) {
                msg = 'Passwords do not match';
                valid = false;
            } else {
                return {
                    'password1': {
                        'valid': valid,
                        'msg': msg
                    },
                    'password2': {
                        'valid': valid,
                        'msg': msg
                    }
                };
            }
        }
        return { valid, msg };
    }

    const onBtnBackToLogin = () => {
        history.push("/Login");
    }

    // {
    //     "recoverytoken": "string",
    //     "new": "string"
    //   }
    const onBtnSubmit = () => {
        let tempEl = Object.keys(formValidationInfo).find((el) => !formValidationInfo[el].valid && el !== 'form');
        if (tempEl) {
            toast.error(formValidationInfo[tempEl].msg);
            return;
        }
        if (areSomeFieldsEmpty()) {
            toast.error(formValidationInfo.form.msg);
            return;
        }
        authService.confirmPasswordReset({ 'recoverytoken': id, 'new': passwords.password1 }).then(() => {
            history.push('/Login');
            toast.success('success - password has been changed!');
        }).catch((reason) => {
            errorHandler(reason, toast);
        })
    }

    return (
        <div className="login">
            <form className="loginForm">
                <div className="form-group row">
                    <div className="logoEnv form-group col">
                        <img className="logo" alt="logo" src={logo}></img>
                    </div>
                    <div className="inline form-group col">
                        <h3 className="loginheader headers">Set new password:</h3>
                        <input
                            type={isShown1 ? 'password' : 'text'}
                            autoComplete="off"
                            className="password inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            id='inputCtrl_PW1'
                            name='password1'
                            style={{marginBottom: '5px'}}
                            placeholder='password'
                            value={passwords.password1}
                            onChange={onChange}
                        >
                        </input>
                        <button type="button" className="bttnEye bttn"
                            onMouseEnter={() => setIsShown1(false)}
                            onMouseLeave={() => setIsShown1(true)}>
                            <FontAwesomeIcon icon="eye" size="lg" />
                        </button>
                        <br />
                        <input
                            type={isShown2 ? 'password' : 'text'}
                            autoComplete="off"
                            className=/*"inputFieldPaswordRP inputFieldPasword inputField"*/"password inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            id='inputCtrl_PW2'
                            name='password2'
                            placeholder='password'
                            value={passwords.password2}
                            onChange={onChange}
                        >
                        </input>
                        <button type="button" className="bttnEye bttn"
                            onMouseEnter={() => setIsShown2(false)}
                            onMouseLeave={() => setIsShown2(true)}>
                            <FontAwesomeIcon icon="eye" size="lg" />
                        </button>
                        <br />
                        <div style={{ display: "flex" }}>
                            <button type="button" className="bttnRegister" onClick={onBtnBackToLogin} >Back to Login</button>
                            <button type="button" style={{color: 'white', backgroundColor: '#FB8500'}} className="bttnRegister" onClick={onBtnSubmit} >Submit</button>
                        </div>
                    </div>
                </div>
            </form>

        </div >

    );
}


export default PWFGchangePW;