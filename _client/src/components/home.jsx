import { toast } from 'react-toastify';
import React, { useState, useEffect } from 'react';
import 'react-toastify/dist/ReactToastify.css';
import '../css/Home.css';
import '../App.css';
import { errorHandler, sortAndSetPupils, compareRatingSchemes, checkRatingInput, checkWholeRecordObj, parseDateIntoValidFormat, getAndShowAllMsgs, formOK } from './utils/utils';
import { lessonService } from '../services/lessonService.js';
import ModalWindow from './utils/modalWindow.jsx';
import { cprService } from '../services/cprService.js';
import { pupilService } from '../services/pupilService.js';
import { recordService } from '../services/recordService';
import { schemeService } from '../services/schemeService';
import moment from 'moment';

const Home = () => {
    const [stateOfModalTags, setStateOfModalTags] = useState({
        isOpen: false
    });
    const openModalTags = () => setStateOfModalTags({ isOpen: true });
    const closeModalTags = () => setStateOfModalTags({ isOpen: false });

    const _weekdays = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday'];

    const emptyRecordObj = {
        'rating': /*{ value: 1 }*/null,
        'weight': 0,
        'validOn': null,
        'pupilNotPresent': false,
        'comment': '',
        'tags': []
    }
    const [newTag, setNewTag] = useState('');
    const [tagsOfCourse, setTagsOfCourse] = useState([]);
    const [modifiedTags, setModifiedTags] = useState([]);
    const [record, setRecord] = useState(emptyRecordObj);
    const [idOfCurrPupil, setIdOfCurrPupil] = useState(null);
    const [schemes, setSchemes] = useState([]);
    const [idOfSelectedScheme, setIdOfSelectedScheme] = useState(null);
    const [signs, setSigns] = useState([]);
    const [timetable, setTimetable] = useState([]);
    const [currentLesson, setCurrentLesson] = useState(null);
    const [pupils, setPupils] = useState([]);
    const [cprObjects, setCprObjects] = useState([]);
    const [selectedLesson, setSelectedLesson] = useState(null);
    const initialFormValidationInfo = {
        'validOn': {
            valid: false,
            msg: 'No input for date'
        },
        'rating': {
            valid: false,
            msg: 'No symbol selected'
        }
    };
    const [formValidationInfo, setFormValidationInfo] = useState(initialFormValidationInfo);

    function onSignSelectChanged(event, signs) {
        const { value } = event.target;
        let rating = signs.find((el) => el._id === value);
        setRecord({ ...record, 'rating': { ...rating } });
    }

    function onSchemeSelectChanged(event) {
        const { value } = event.target; //id
        setIdOfSelectedScheme(value);
    }

    useEffect(() => {
        let _tt = [];
        lessonService.getAll().then((_lessons) => {
            let tempTimetable = [];
            let idx = (new Date()).getDay() - 1;
            let _weekday = _weekdays[idx];
            if (_weekday) {
                tempTimetable = _lessons.filter(_lesson => _lesson.weekday === _weekday);
                if (!tempTimetable || tempTimetable.length === 0)
                    tempTimetable = [];
            }

            setTimetable([...tempTimetable]);
            _tt = tempTimetable;
            selectCurrentLesson(_tt);
        }).catch((reason) => {
            errorHandler(reason, toast);
        })
        schemeService.getAll().then((_schemes) => {
            setSchemes(_schemes);
        }).catch((reason) => {
            errorHandler(reason, toast);
        })


        const interval = setInterval(() => {
            selectCurrentLesson(_tt);
        }, process.env.REACT_APP_INTERVAL_GET_CURRENT_LESSON);


        return () => { clearInterval(interval); };

    }, []);

    useEffect(() => {
        setAllTagsOfCourse();
    }, [currentLesson])

    useEffect(() => {
        let scheme = schemes.find((el) => el._id === idOfSelectedScheme);
        if (scheme) {
            setSigns([...scheme.ratings].sort(compareRatingSchemes));
            setRecord({ ...record, 'weight': scheme.weight });
        }
    }, [idOfSelectedScheme])

    let old = '00:00', max, min;

    const onChange = (event) => {
        let { name, value } = event.target;
        if (name === 'pupilNotPresent') {
            if (value === 'on') {
                value = true;
            } else {
                value = false;
            }
        }
        setRecord({ ...record, [name]: value });
        if (name === 'validOn' || name === 'sign' || name === 'schemaId')
            setFormValidationInfo({ ...formValidationInfo, [name]: checkRatingInput(name, value, record, setRecord, parseDateIntoValidFormat) });
    }


    const onBtnSaveClick = () => {
        if (!record.pupilNotPresent) {
            if (!formOK(checkWholeRecordObj(formValidationInfo, setFormValidationInfo, record, setRecord, parseDateIntoValidFormat))) {
                getAndShowAllMsgs(formValidationInfo, toast);
                return;
            }
        } else {
            record.rating = null;
        }


        recordService.create({ ...record, 'validOn': new Date(record.validOn).toISOString() }).then(() => {
            toast.success('record created');
            setAllTagsOfCourse();
        }).catch((reason) => {
            errorHandler(reason, toast);
        })


    }


    Number.prototype.addZero = function (b, c) {
        var l = (String(b || 10).length - String(this).length) + 1;
        return l > 0 ? new Array(l).join(c || '0') + this : this;
    }

    const setAllTagsOfCourse = () => {
        if (currentLesson)
            recordService.getAllOfCourse(currentLesson.courseId).then((_recsOfC) => {
                let mappedArr = _recsOfC.map((_el) => _el.tags);
                let temparr = [...tagsOfCourse];
                for (let i of mappedArr) {
                    temparr = temparr.concat(i);
                }
                setTagsOfCourse([...new Set(temparr)]);
            }).catch((reason) => {
                errorHandler(reason, toast);
            })
    }

    const selectCurrentLesson = (tempTimeTable) => {
        let d = new Date();
        if (!tempTimeTable) {
            tempTimeTable = timetable;
        }

        if (tempTimeTable) {
            if (tempTimeTable.length === 0) {
                setCurrentLesson(null);
            } else {
                let format = 'hh:mm';
                let currTime = moment(updateTimeFormat(d.getHours()) + ':' + updateTimeFormat(d.getMinutes()), format);
                let result = tempTimeTable.filter((_elem) => checkTimes(currTime, _elem));
                if (result && result.length !== 0) {
                    if (!(currentLesson && (result[0]._id === currentLesson._id))) {
                        setCurrentLesson(result[0]);
                        setPupilsOfCourse(result[0].courseId);
                    }
                } else {
                    setCurrentLesson(null);
                }
            }

        }
    }

    const setPupilsOfCourse = (cid) => {
        cprService.getPupilsById(cid).then((_pupils) => {
            setCprObjects(_pupils);
            let temparr = [];
            for (let _ppl in _pupils) {
                _ppl = _pupils[_ppl];
                pupilService.getById(_ppl.pupilId).then((_pupil) => {
                    temparr.push(_pupil);
                    sortAndSetPupils([...temparr], setPupils);
                }).catch((reason) => {
                    errorHandler(reason, toast);
                })
            }

        }).catch((reason) => {
            errorHandler(reason, toast);
        })
    }

    const checkTimes = (currtime, less) => {
        let format = 'hh:mm';
        let starttime = moment(less.starttime, format);
        let endtime = moment(less.endtime, format);

        return currtime.isBetween(starttime, endtime);
    }

    function updateTimeFormat(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }

    const onPupilSelect = (pupilId) => {
        setIdOfCurrPupil(pupilId);

        let _record = cprObjects.filter((elem) => elem.pupilId === pupilId);
        if (_record.length === 0) {
            toast.error('something went wrong: no record found');
        } else {
            setRecord({ ...emptyRecordObj, 'cprId': _record[0]._id, 'validOn': parseDateIntoValidFormat(new Date(new Date(Date.now()).toISOString())) });
            // setIdOfCurrCPR(_record[0]._id);
        }
    }

    let arrHours = [];
    Object.keys(timetable).forEach((item) => {
        if (!max) {
            max = timetable[item].endtime;
        }
        else
            if (get_time(timetable[item].endtime) > get_time(max))
                max = timetable[item].endtime;

        if (!min) {
            min = timetable[item].starttime;
        }
        else
            if (get_time(timetable[item].starttime) < get_time(min))
                min = timetable[item].starttime;

        if (timetable[timetable.length - 1] === timetable[item]) {
            let grenze = max.split(':')[1] === '00' ? parseInt(max.split(':')[0], 10) - 1 : parseInt(max.split(':')[0], 10);
            for (let i = parseInt(min.split(':')[0], 10); i <= grenze; i++) {
                arrHours.push(
                    (<li className="myLi" style={{ borderRadius: '0px', borderBottom: 'none', borderTop: '2px solid white', color: 'white', height: 60 + 'px' }} key={i}>
                        <p style={{ marginBottom: '0px', marginRight: '0px', alignSelf: 'flex-start' }}>{i + ':00'}</p>
                    </li>))
            }
        }
    })

    const closeModalAndApplyChanges = () => {
        closeModalTags();
        setRecord({ ...record, tags: [...modifiedTags] })
    }

    const onChangeTag = (event) => {
        let { value } = event.target;
        setNewTag(value);
    }

    const onBtnModifyTagsClick = () => {
        setNewTag('');
        setModifiedTags([...record.tags]);
        openModalTags();
    }

    const onBtnAddTagClick = () => {
        if (modifiedTags.includes(newTag)) {
            toast.error('tag already exists');
            return;
        }
        if (newTag === "") {
            toast.error('provide a tagname or select an existing tag');
            return;
        }
        modifiedTags.push(newTag);
        //added auch zu record.tags
        setNewTag('');
    }

    const onBtnCloseModalTagsClick = () => {
        closeModalTags();
        setModifiedTags([]);
    }

    const removeTag = (tagname) => {
        let temptags = modifiedTags;
        temptags.splice(modifiedTags.indexOf(tagname), 1);
        setModifiedTags([...temptags]);
    }

    function onTagSelectChanged(event) {
        const { value } = event.target;
        let temptags = modifiedTags;
        if (temptags.includes(value)) {
            toast.error('tag already exists');
            return;
        }
        if (value === "") {
            toast.error('provide a tagname or select an existing tag');
            return;
        }
        temptags.push(value);
        setModifiedTags([...temptags]);

        setNewTag('');
    }


    var stringToColour = function (str) {
        var hash = 0;
        for (let i = 0; i < str.length; i++) {
            hash = str.charCodeAt(i) + ((hash << 5) - hash);
        }
        var colour = '#';
        for (let i = 0; i < 3; i++) {
            var value = (hash >> (i * 8)) & 0xFF;
            colour += ('00' + value.toString(16)).substr(-2);
        }
        return colour;
    }

    function invertColor(hex) {
        if (hex.indexOf('#') === 0) {
            hex = hex.slice(1);
        }
        // convert 3-digit hex to 6-digits.
        if (hex.length === 3) {
            hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
        }
        if (hex.length !== 6) {
            throw new Error('Invalid HEX color.');
        }
        // invert color components
        var r = (255 - parseInt(hex.slice(0, 2), 16)).toString(16),
            g = (255 - parseInt(hex.slice(2, 4), 16)).toString(16),
            b = (255 - parseInt(hex.slice(4, 6), 16)).toString(16);
        // pad each with zeros and return
        return '#' + padZero(r) + padZero(g) + padZero(b);
    }

    function padZero(str, len) {
        len = len || 2;
        var zeros = new Array(len).join('0');
        return (zeros + str).slice(-len);
    }

    const onValueChange = (event) => {
        let { value } = event.target;
        if (value < 0) {
            value = 0;
        } else if (!(/^\d+$/.test(value))) {
            return;
        }
        let rating = record.rating;
        rating.value = value;
        setRecord({ ...record, "rating": rating });
    }

    const onLessonClick = (lesson) => {
        setSelectedLesson(lesson);
        setPupilsOfCourse(lesson.courseId);
    }

    return (
        <div>
            <h1>Home</h1>
            <div className="home">
                <div className="inline70">
                    <div className="flexContainer">
                        <div className="inline50">
                            <div className="inline100">
                                <label className="header noMargin">{(currentLesson) ? 'Selected lesson: ' + currentLesson.courseLabel : 'no lesson atm'}</label>
                                {(currentLesson) ?
                                    <table className="homeTable">
                                        <tbody>
                                            <tr>
                                                <td><p>{'from: ' + currentLesson.starttime}</p></td>
                                                <td><p>{'until: ' + currentLesson.endtime}</p></td>
                                            </tr>
                                            <tr>
                                                <td><p>{'room: ' + currentLesson.roomNr}</p></td>
                                                <td><p>{ }</p></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    : ''}
                            </div>
                            <label className="header" style={{ visibility: (currentLesson) ? 'visible' : 'hidden' }}>Pupils In Current Lesson</label>
                            <div className="block">
                                <ul className="list-group">
                                    {
                                        pupils.map((item) => {
                                            return (
                                                <li className="list-group-item d-flex justify-content-center align-items-center" style={{ border: '2px solid #023047', color: (idOfCurrPupil === item._id) ? 'white' : '#023047', backgroundColor: (idOfCurrPupil === item._id) ? '#023047' : 'white' }} key={item._id} onClick={() => onPupilSelect(item._id)}>
                                                    {item.lastname.toUpperCase() + ' ' + item.firstname}
                                                </li>
                                            );
                                        })
                                    }
                                </ul>
                            </div>
                        </div>
                        <div className="inline50 marginLR5" style={{ visibility: (idOfCurrPupil) ? 'visible' : 'hidden' }}>
                            <label className="header">Record-Info</label>
                            <form>
                                <label className="fat" htmlFor={`inputCtrl_date`}>Date:</label>
                                <input
                                    type='datetime-local'
                                    autoComplete="off"
                                    className="inputFieldPeriod inputField"
                                    id='inputCtrl_date'
                                    name='validOn'
                                    placeholder='validOn'
                                    value={record.validOn}
                                    onChange={onChange}
                                >
                                </input>
                                <br />
                                <label className="fat" htmlFor={`inputCtrl_Status`}>Select Scheme:</label>
                                <select id={`inputCtrl_Status`} name='schemaId' onChange={(event) => onSchemeSelectChanged(event)} >
                                    <option value="88" disabled selected={(idOfSelectedScheme === null || idOfSelectedScheme === undefined || idOfSelectedScheme === 'null') ? true : false}>Select your scheme</option>
                                    {
                                        schemes.map((element) => {
                                            return (
                                                <option key={element._id} value={element._id} selected={(idOfSelectedScheme === element._id) ? true : false}>{element.name}</option>
                                            );
                                        })
                                    }
                                </select>
                                <br></br>
                                <div style={{ opacity: ((idOfSelectedScheme === null) || idOfCurrPupil === null) ? '0.2' : '1' }}>
                                    <label className="fat" htmlFor={`inputCtrl_Sign`}>Select Symbol:</label>
                                    <select id={`inputCtrl_Sign`} name='sign' onChange={(event) => onSignSelectChanged(event, signs)} >
                                        <option value="" disabled selected={(record.rating === null || record.rating === "null") ? true : false}>Select your symbol</option>
                                        {
                                            signs.map((element) => {
                                                return (
                                                    <option key={element._id} value={element._id} selected={(record.rating !== null && record.rating._id === element._id) ? true : false}>{element.label} {element.symbol} {element.value}</option>
                                                );
                                            })
                                        }
                                    </select>
                                </div>
                                <label className="fat" htmlFor="weight">Weight:</label>
                                <input type="number" id="weight" name="weight" className="inputFieldPeriod inputField"
                                    min="0" max="100" value={record.weight} onChange={onChange}></input>
                                <br />
                                <label className="fat" htmlFor="value">Value</label>
                                <input type="number" id="value" name="value" className="inputFieldPeriod inputField"
                                    min="1" value={(record.rating) ? record.rating.value : 1} onChange={onValueChange}></input>

                                <br />
                                <label className="fat" htmlFor="pupilNotPresent">Pupil not Present:</label>
                                <input type="checkbox" id="pupilNotPresent" name="pupilNotPresent"
                                    checked={record.pupilNotPresent} onChange={onChange}></input>
                                <br></br>
                                <label className="fat" htmlFor={`inputCtrl_Notes`} onChange={onChange}>Comment: </label>
                                <input
                                    type='text'
                                    autoComplete="off"
                                    className="inputFieldPeriod inputField"
                                    id='inputCtrl_Notes'
                                    name='comment'
                                    placeholder='comment'
                                    value={record.comment}
                                    onChange={onChange}
                                >
                                </input>
                                <button type="button" className="bttnTags bttn" onClick={onBtnModifyTagsClick}>Manage Tags</button>
                                <br></br>
                                <button type="button" style={{ width: '100%' }} className="bttnUpAdd bttn" onClick={onBtnSaveClick}>Create Record</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div className="inline30">
                    <div className="inline100">
                        <div className="times">
                            <label className="header headerLessons">Time</label>
                            <ul>
                                {
                                    arrHours
                                }
                            </ul>
                        </div>
                        <div className="lessons">
                            <label className="header headerLessons">Lesson</label>
                            <ul>
                                {
                                    timetable.map((item) => {
                                        if (item) {
                                            let erg = diff_minutes(item.starttime, item.endtime), mar = 0;
                                            if (!min)
                                                min = item.starttime;
                                            else
                                                if (get_time(item.starttime) < get_time(min))
                                                    min = item.starttime;
                                            if (old !== '00:00')
                                                mar = diff_minutes(old, item.starttime); // +1 in order to get at least a slim margin
                                            else
                                                mar = diff_minutes(min, item.starttime);
                                            old = item.endtime;
                                            return (<li className="myLi" style={{ height: erg + 'px', backgroundColor: item.colorCode, marginTop: mar + 'px', border: (selectedLesson && selectedLesson._id === item._id) ? '3px solid red' : 'none' }} key={item.courseLabel} onClick={() => onLessonClick(item)}>
                                                <p style={{ fontWeight: 'bold', marginTop: '0px', marginLeft: '0px', alignSelf: 'flex-start' }}>{item.courseLabel} ({item.roomNr})</p><p style={{ color: 'red', backgroundColor: 'white', visibility: (currentLesson && currentLesson._id === item._id) ? 'visible' : 'hidden' }}>current lesson</p>
                                                <p style={{ marginBottom: '0px', marginRight: '0px', alignSelf: 'flex-end' }}>{item.starttime} - {item.endtime}</p>
                                            </li>);
                                        } else return <p key="ptagifnolesson" />;
                                    })
                                }
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <ModalWindow
                stateOfModal={stateOfModalTags}
                modalTitle={'Manage Tags'}
                modalText={
                    <div>
                        <h6>New tag:</h6>
                        <input style={{ borderRadius: '25px 0px 0px 25px', width: '70%', display: 'inline-block' }}
                            type='text'
                            autoComplete="off"
                            className="inline50 inputFieldPeriod inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            id='inputCtrl_Notes'
                            name='comment'
                            placeholder='comment'
                            //value={newTag}
                            onChange={onChangeTag}
                        >
                        </input>
                        <button type="button" style={{ borderRadius: '0px 25px 25px 0px', border: '2px solid #FB8500', width: '25%', display: 'inline-block' }} className="inline50 bttnUpAdd bttn" onClick={onBtnAddTagClick}>Add</button>

                        <br></br>
                        <br></br>
                        <h6>Existing tags:</h6>
                        <select onChange={onTagSelectChanged}>
                            <option value="" disabled selected={(newTag === "") ? true : false}>Select a tag</option>
                            {
                                tagsOfCourse.map((_tag) => {
                                    return (
                                        <option value={_tag}>
                                            {_tag}
                                        </option>
                                    );
                                })
                            }
                        </select>
                        <br></br>
                        <br></br>
                        <h6>Added tags:</h6>
                        <ul className="list-group list-group-horizontal">
                            {
                                (modifiedTags) ?
                                    modifiedTags.map((_el) => {
                                        return (
                                            <li style={{ color: invertColor(stringToColour(_el)), backgroundColor: stringToColour(_el) }} className="list-group-item d-flex justify-content-between align-items-center" onClick={() => removeTag(_el)} key={"btn" + _el}>
                                                {_el}
                                            </li>
                                        );
                                    })
                                    : ''
                            }
                        </ul>
                    </div>
                }
                btnOkText={'Apply'}
                closeModal={onBtnCloseModalTagsClick}
                methodOkConfirmClick={closeModalAndApplyChanges}
            ></ModalWindow>
        </div >
    );
};

function diff_minutes(dt2, dt1) {
    dt2 = get_time(dt2);
    dt1 = get_time(dt1);
    var diff = dt1 - dt2;
    return diff;
}

function get_time(dt2) {
    return parseInt(dt2.split(':')[0]) * 60 + parseInt(dt2.split(':')[1]);
}

export default Home;