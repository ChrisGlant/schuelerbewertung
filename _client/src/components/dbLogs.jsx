import React, { useState, useEffect } from 'react';
import { dbLogService } from '../services/dbLogService.js';
import { errorHandler } from './utils/utils';
import { toast } from 'react-toastify';

const DBLogs = () => {
    const colors = ['#4d8c99', '#8b74b8', '#fcba03', '#fc4103', '#31fc03', '#03fcca', '#03a9fc', '#9803fc', '#fc03f4', '#fc0303'];
    const [logs, setLogs] = useState([]);
    const [filteredLogs, setFilteredLogs] = useState([]);
    const [filterObjects, setFilterObjects] = useState([]);

    useEffect(() => {
        dbLogService.getAll().then((_logs) => {

            _logs = assignColors(_logs.sort(function (x, y) {
                return new Date(y.logTimestamp) - new Date(x.logTimestamp);
            }));
            setLogs(_logs);

        }).catch((reason) => {
            errorHandler(reason, toast);
        })
    }, []);

    useEffect(() => {
        setFilteredLogs(logs);
    }, [logs]);


    function assignColors(_logs) {
        let arrOfCombinedLogObjs = [];
        for (let _l of _logs) {
            let tempKey = 'login';
            if (_l.logType !== "login") {
                tempKey = _l.affectedEntity;
            }
            if (!arrOfCombinedLogObjs.some(_el => _el.key === tempKey)) {
                //color setten und obj pushen
                let c = '#d0ff00';
                if (arrOfCombinedLogObjs.length <= colors.length) {
                    c = colors[arrOfCombinedLogObjs.length];
                }
                _l.color = c;
                arrOfCombinedLogObjs.push({ 'key': tempKey, 'color': c });

            } else {
                let _obj = arrOfCombinedLogObjs.find((_el) => _el.key === tempKey);
                _l.color = _obj.color;
            }
        }
        setFilterObjects([...arrOfCombinedLogObjs]);
        return _logs;
    }

    function onBtnShowAllClick() {
        setFilteredLogs(logs);
    }

    function filter(key) {
        setFilteredLogs(logs.filter((_el) => (key === 'login') ? _el.logType === 'login' : _el.affectedEntity === key))
    }

    return (
        <div>
            <h1>Logs</h1>
            <div style={{ width: "70%", margin: "20px" }}>
                <button className="bttn bttnTab" onClick={onBtnShowAllClick}>Show all</button>
                <br />
                {
                    filterObjects.map((_obj) =>
                        <button className="bttn bttnTab" style={{ width: (100 / filterObjects.length) + '%' }} onClick={() => filter(_obj.key)}>{_obj.key}</button>
                    )
                }
                <br />
                <br />
                <div className="block">
                    <ul>
                        {
                            filteredLogs.map((_el) =>
                                <li style={{ backgroundColor: 'transparent', padding: '20px' }} key={_el.logTimestamp}>
                                    <div className="round" style={{ padding: '15px', paddingBottom: '10px', margin: '2px' }}>
                                        <h6 style={{ color: 'grey', margin: '0px', padding: '0px' }}>on: {new Date(_el.logTimestamp).getDate() + "." + new Date(_el.logTimestamp).getMonth() + 1 + '.' + new Date(_el.logTimestamp).getFullYear() + ' at: ' + new Date(_el.logTimestamp).getHours() + ':' + new Date(_el.logTimestamp).getMinutes()}</h6>
                                        <h3 style={{ color: _el.color, margin: '5px', padding: '0px' }}>{(_el.logType === "login") ? _el.logType : _el.affectedEntity + ' ' + _el.logType}</h3>
                                        <h6 style={{ margin: '0px', padding: '0px' }}>{(_el.logMessage !== undefined) ? _el.logMessage : ''}</h6>
                                    </div>
                                </li>
                            )
                        }
                    </ul>
                </div>
            </div>
        </div>
    );
};

export default DBLogs;