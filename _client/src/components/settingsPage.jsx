import React from 'react';
import { useHistory } from 'react-router-dom';

const SettingsPage = () => {
    const history = useHistory();

    return (
        <div>
            <h1>Settings</h1>
            <div style={{ margin: '10px' }}>
                <h4 style={{ marginTop: '20px', fontWeight: 'bold' }}>Record:</h4>
                <button type="button" style={{ width: '260px' }} className="bttnUpAdd bttn" onClick={() => { history.push('/Ratingscheme') }}>Ratingscheme</button>

                <br></br>
                <h4 style={{ marginTop: '20px', fontWeight: 'bold' }}>User:</h4>
                <button type="button" style={{ width: '260px' }} className="bttnUpAdd bttn" onClick={() => { history.push('/User') }}>User-Profile</button>

                <br></br>
                <h4 style={{ marginTop: '20px', fontWeight: 'bold' }}>Other features:</h4>
                <button type="button" style={{ width: '260px' }} className="bttnUpAdd bttn" onClick={() => { history.push('/Logs') }}>Logs</button>
                <br></br>

            </div>
        </div>
    );
};

export default SettingsPage;