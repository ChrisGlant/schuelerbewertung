import { toast } from 'react-toastify';
import React, { useState, useEffect } from 'react';
import { pupilService } from '../services/pupilService.js';
import { courseService } from '../services/courseService.js';
import '../css/Bewertung.css';
import '../App.css';
import ModalWindow from './utils/modalWindow.jsx';
import { cprService } from '../services/cprService.js';
import { schemeService } from '../services/schemeService.js';
import { recordService } from '../services/recordService.js';
import { errorHandler, getDateTimeString, compareRatingSchemes, checkRatingInput, checkWholeRecordObj, parseDateIntoValidFormat, getAndShowAllMsgs, formOK } from './utils/utils';
import { useHistory } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Bewertung = () => {
    const history = useHistory();
    const [stateOfModal, setStateOfModal] = useState({
        isOpen: false
    });
    const openModal = () => setStateOfModal({ isOpen: true });
    const closeModal = () => setStateOfModal({ isOpen: false });
    const [stateOfModalTags, setStateOfModalTags] = useState({
        isOpen: false
    });
    const openModalTags = () => setStateOfModalTags({ isOpen: true });
    const closeModalTags = () => setStateOfModalTags({ isOpen: false });
    const [unfilteredCourses, setUnfilteredCourses] = useState([]);
    const [unfilteredPupils, setUnfilteredPupils] = useState([]);
    const [courses, setCourses] = useState([]);
    const [schemes, setSchemes] = useState([]);
    const [idOfSelectedScheme, setIdOfSelectedScheme] = useState(null);
    const [signs, setSigns] = useState([]);
    const [pupilsOfCourse, setPupilsOfCourse] = useState([]);
    const emptyRecordObj = {
        'rating': /*{ value: 0 }*/null,
        'weight': 0,
        'validOn': null,
        'pupilNotPresent': false,
        'tags': [],
        'comment': ''
    }
    const [newTag, setNewTag] = useState('');
    const [tagsOfCourse, setTagsOfCourse] = useState([]);
    const [modifiedTags, setModifiedTags] = useState([]);
    const [record, setRecord] = useState(emptyRecordObj);
    const [recordsOfPupilInCourse, setRecordsOfPupilInCourse] = useState([]);
    const [unfilteredRecords, setUnfilteredRecords] = useState([]);
    const [idOfCurrCourse, setIdOfCurrentCourse] = useState(null);
    const [idOfCurrPupil, setIdOfCurrPupil] = useState(null);
    const [idOfCurrRecord, setIdOfCurrRecord] = useState(null);
    const [cprObjects, setCprObjects] = useState([]);
    const [idOfCurrCPR, setIdOfCurrCPR] = useState(null);
    const flagSwitchAddUpdate = {
        ADD: 0,
        UPDATE: 1
    };
    const [flagAddUpdate, setFlagAddUpdate] = useState(flagSwitchAddUpdate.UPDATE);
    const initialFormValidationInfo = {
        'validOn': {
            valid: false,
            msg: 'No input for date'
        },
        'rating': {
            valid: false,
            msg: 'No symbol selected'
        }
    };
    const [formValidationInfo, setFormValidationInfo] = useState(initialFormValidationInfo);

    useEffect(() => {
        //setSignsOfScheme();
        let scheme = schemes.find((el) => el._id === idOfSelectedScheme);
        if (scheme) {
            setSigns([...scheme.ratings].sort(compareRatingSchemes));
            setRecord({ ...record, 'weight': scheme.weight });
        }
    }, [idOfSelectedScheme])

    useEffect(() => {
        init();
    }, []);

    useEffect(() => {
        setPupilsOfCourse([]);
        if (idOfCurrCourse !== null && idOfCurrCourse !== undefined && idOfCurrCourse !== 'null')
            cprService.getPupilsById(idOfCurrCourse).then((_cprstuff) => {
                setCprObjects(_cprstuff);
            }).catch((reason) => {
                errorHandler(reason, toast);
            })
        setAllTagsOfCourse();

    }, [idOfCurrCourse]);

    useEffect(() => {

        for (let _obj in cprObjects) {
            _obj = cprObjects[_obj];
            if (_obj.pupilId !== null && _obj.pupilId !== undefined && Object.pupilId !== 'null')
                pupilService.getById(_obj.pupilId).then((_pupil) => {
                    let _arr = pupilsOfCourse;
                    _arr.push(_pupil);
                    setPupilsOfCourse([..._arr]);
                }).catch((reason) => {
                    errorHandler(reason, toast);
                })
        }

    }, [cprObjects]);

    useEffect(() => {
        checkWholeRecordObj(formValidationInfo, setFormValidationInfo, record, setRecord, parseDateIntoValidFormat);
        setModifiedTags((record.tags) ? [...record.tags] : []);
    }, [record]);

    const onCourseClick = (_course) => {
        setIdOfCurrentCourse(_course._id);
        setIdOfCurrPupil(null);
    }

    const onSearchFieldChange = (event) => {
        const { value } = event.target;
        let tempCourses = unfilteredCourses;
        if (unfilteredCourses.length < courses.length) {
            setUnfilteredCourses(courses);
            tempCourses = courses;
        }
        setCourses(tempCourses.filter((element) => {
            return element.label.toUpperCase().includes(value.toUpperCase());
        }).map((element) => {
            return element;
        }));

    }

    const setAllTagsOfCourse = () => {
        if (idOfCurrCourse)
            recordService.getAllOfCourse(idOfCurrCourse).then((_recsOfC) => {
                let mappedArr = _recsOfC.map((_el) => _el.tags);
                let temparr = [...tagsOfCourse];
                for (let i of mappedArr) {
                    temparr = temparr.concat(i);
                }
                setTagsOfCourse([...new Set(temparr)]);
            }).catch((reason) => {
                errorHandler(reason, toast);
            })
    }

    const onPupilClick = (_pupil) => {
        setIdOfCurrPupil(_pupil._id);
        setFlagAddUpdate(flagSwitchAddUpdate.ADD);

        let _record = cprObjects.filter((elem) => elem.pupilId === _pupil._id);
        if (_record.length === 0) {
            toast.error('something went wrong: no record found');
        } else {
            setRecord({ ...emptyRecordObj, 'cprId': _record[0]._id, 'validOn': parseDateIntoValidFormat(new Date(new Date(Date.now()).toISOString())) });
            setIdOfCurrCPR(_record[0]._id);
            setAllRecordsOfPupil(_record[0]._id);
            setIdOfCurrRecord(null);
            setIdOfSelectedScheme(null)
        }
    }

    const onSearchFieldAddedRecordsChange = (event) => {
        const { value } = event.target;
        let tempRecords = unfilteredRecords;
        if (unfilteredRecords.length < recordsOfPupilInCourse.length) {
            setUnfilteredRecords(recordsOfPupilInCourse);
            tempRecords = recordsOfPupilInCourse;
        }
        setRecordsOfPupilInCourse(tempRecords.filter((element) => {
            return element.validOn.includes(value);
        }).map((element) => {
            return element;
        }));
    }

    const onSearchFieldPupilChange = (event) => {
        const { value } = event.target;
        let tempPupils = unfilteredPupils;
        if (unfilteredPupils.length < pupilsOfCourse.length) {
            setUnfilteredPupils(pupilsOfCourse);
            tempPupils = pupilsOfCourse;
        }
        setPupilsOfCourse(tempPupils.filter((element) => {
            return (element.lastname + ' ' + element.firstname).toUpperCase().includes(value.toUpperCase());
        }).map((element) => {
            return element;
        }));
    }

    const openDeleteModal = () => {
        try {
            if (idOfCurrRecord === null) {
                toast.error('no record selected')
            } else {
                openModal();
            }
        } catch (err) {
            errorHandler(err, toast);
        }
    }

    const deleteCurrRecord = () => {
        closeModal();
        recordService.delete(idOfCurrRecord).then(() => {
            toast.success('record removed');
            setFlagAddUpdate(flagSwitchAddUpdate.ADD);

            init();
        }).catch((reason) => {
            errorHandler(reason, toast);
        })
    }

    const init = () => {
        courseService.getAll().then((_courses) => {
            setCourses(_courses);
        }).catch((reason) => {
            errorHandler(reason, toast);
        })
        schemeService.getAll().then((_schemes) => {
            setSchemes(_schemes);
        }).catch((reason) => {
            errorHandler(reason, toast);
        })
        setAllRecordsOfPupil();

    }

    const setAllRecordsOfPupil = (tempId, filterQuery) => {
        if (!tempId) {
            if (idOfCurrCPR) {
                tempId = idOfCurrCPR;
            }
        }
        if (tempId)
            recordService.getAllOfCPR(tempId).then((rec) => {
                if (filterQuery) {
                    let tempArr = [];
                    for (let _rec in rec) {
                        _rec = rec[_rec];
                        if (new Date(_rec.validOn).includes(filterQuery))
                            tempArr.push(_rec);
                    }
                    setRecordsOfPupilInCourse([...tempArr]);
                } else {
                    setRecordsOfPupilInCourse(rec);
                }
            }).catch((reason) => {
                errorHandler(reason, toast);
            })
    }

    const onChange = (event) => {
        let { name, value } = event.target;
        if (name === 'pupilNotPresent') {
            let { checked } = event.target;
            value = checked;
        } else if (name === 'weight') {
            if (value > 100) {
                value = 100;
            } else if (value < 0) {
                value = 0;
            } else if (!(/^\d+$/.test(value))) {
                return;
            }
        }
        setRecord({ ...record, [name]: value });
        if (name === 'validOn' || name === 'sign' || name === 'schemaId')
            setFormValidationInfo({ ...formValidationInfo, [name]: checkRatingInput(name, value, record, setRecord, parseDateIntoValidFormat) });
    }

    const onValueChange = (event) => {
        let { value } = event.target;
        if (value < 0) {
            value = 0;
        } else if (!(/^\d+$/.test(value))) {
            return;
        }
        let rating = record.rating;
        rating.value = value;
        setRecord({ ...record, "rating": rating });
    }

    const onBtnSaveClick = () => {
        if (!record.pupilNotPresent) {
            if (!formOK(checkWholeRecordObj(formValidationInfo, setFormValidationInfo, record, setRecord, parseDateIntoValidFormat))) {
                getAndShowAllMsgs(formValidationInfo, toast);
                return;
            }
        } else {
            record.rating = null;
        }


        switch (flagAddUpdate) {
            case flagSwitchAddUpdate.ADD:
                recordService.create({ ...record, 'validOn': new Date(record.validOn).toISOString() }).then(() => {
                    toast.success('record created');
                    init();
                    setAllTagsOfCourse();
                }).catch((reason) => {
                    errorHandler(reason, toast);
                })
                break;
            case flagSwitchAddUpdate.UPDATE:
                if (!idOfCurrRecord) {
                    toast.error('error: probably no record selected to delete');
                    break;
                }
                recordService.update(idOfCurrRecord, { ...record, 'validOn': new Date(record.validOn).toISOString() }).then(() => {
                    toast.success('record updated');
                    init();
                    setAllTagsOfCourse();
                }).catch((reason) => {
                    errorHandler(reason, toast);
                })
                break;
            default:
                break;
        }
    }





    const onRecordSelect = (_record) => {
        setFlagAddUpdate(flagSwitchAddUpdate.UPDATE);
        setIdOfCurrRecord(_record._id);
        setRecord({ ..._record, 'validOn': parseDateIntoValidFormat(new Date(new Date(_record.validOn).toISOString())) });
        if (!_record.pupilNotPresent)
            setIdOfSelectedScheme(schemes.find((el) => el.ratings.some((rel) => rel._id === _record.rating._id))._id);
    }


    Number.prototype.addZero = function (b, c) {
        var l = (String(b || 10).length - String(this).length) + 1;
        return l > 0 ? new Array(l).join(c || '0') + this : this;
    }

    const openBulkAddRecords = () => {
        history.push('/BulkAddRecords/' + idOfCurrCourse);
    }

    const closeModalAndApplyChanges = () => {
        closeModalTags();
        setRecord({ ...record, tags: [...modifiedTags] })
        if (flagAddUpdate === flagSwitchAddUpdate.UPDATE)
            toast.success('tags modified; dont forget to click \'update\'');
    }

    const onChangeTag = (event) => {
        let { value } = event.target;
        setNewTag(value);
    }

    const onBtnModifyTagsClick = () => {
        setNewTag('');
        setModifiedTags([...record.tags]);
        openModalTags();
    }

    const onBtnAddTagClick = () => {
        if (modifiedTags.includes(newTag)) {
            toast.error('tag already exists');
            return;
        }
        if (newTag === "") {
            toast.error('provide a tagname or select an existing tag');
            return;
        }
        modifiedTags.push(newTag);

        setNewTag('');
    }

    const onBtnCloseModalTagsClick = () => {
        closeModalTags();
        setModifiedTags([]);
    }

    const removeTag = (tagname) => {
        let temptags = modifiedTags;
        temptags.splice(modifiedTags.indexOf(tagname), 1);
        setModifiedTags([...temptags]);
    }

    function onTagSelectChanged(event) {
        const { value } = event.target;
        let temptags = modifiedTags;
        if (temptags.includes(value)) {
            toast.error('tag already exists');
            return;
        }
        if (value === "") {
            toast.error('provide a tagname or select an existing tag');
            return;
        }
        temptags.push(value);
        setModifiedTags([...temptags]);

        setNewTag('');
    }

    var stringToColour = function (str) {
        var hash = 0;
        for (let i = 0; i < str.length; i++) {
            hash = str.charCodeAt(i) + ((hash << 5) - hash);
        }
        var colour = '#';
        for (let i = 0; i < 3; i++) {
            var value = (hash >> (i * 8)) & 0xFF;
            colour += ('00' + value.toString(16)).substr(-2);
        }
        return colour;
    }

    function invertColor(hex) {
        if (hex.indexOf('#') === 0) {
            hex = hex.slice(1);
        }
        // convert 3-digit hex to 6-digits.
        if (hex.length === 3) {
            hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
        }
        if (hex.length !== 6) {
            throw new Error('Invalid HEX color.');
        }
        // invert color components
        var r = (255 - parseInt(hex.slice(0, 2), 16)).toString(16),
            g = (255 - parseInt(hex.slice(2, 4), 16)).toString(16),
            b = (255 - parseInt(hex.slice(4, 6), 16)).toString(16);
        // pad each with zeros and return
        return '#' + padZero(r) + padZero(g) + padZero(b);
    }

    function padZero(str, len) {
        len = len || 2;
        var zeros = new Array(len).join('0');
        return (zeros + str).slice(-len);
    }

    function onSignSelectChanged(event, signs) {
        const { value } = event.target;
        let rating = signs.find((el) => el._id === value);
        setRecord({ ...record, 'rating': { ...rating } });
    }

    function onSchemeSelectChanged(event) {
        const { value } = event.target; //id
        setIdOfSelectedScheme(value);
    }


    return (
        <div>
            <h1>Records  </h1>
            <div className="grid-container">
                <div style={{ margin: '10px' }} className="grid-row-sm">
                    <label className="header">Courses</label>
                    <input
                        type='text'
                        autoComplete="off"
                        className="search inputFieldPeriod inputField"
                        id='inputCtrl_Search'
                        name='label'
                        placeholder='Search...'
                        onChange={onSearchFieldChange}
                    ></input>
                    <button type="button" className="bttnSearch bttn" disabled >
                        <FontAwesomeIcon icon="search" size="sm" />
                    </button>
                    <div className="block">
                        <ul className="list-group">
                            {
                                courses.map((item) =>
                                    <li className="list-group-item d-flex justify-content-center align-items-center" style={{ border: '2px solid #023047', color: (idOfCurrCourse === item._id) ? 'white' : '#023047', backgroundColor: (idOfCurrCourse === item._id) ? '#023047' : 'white' }} key={item._id} onClick={() => onCourseClick(item)}>
                                        {item.label}
                                    </li>
                                )
                            }
                        </ul>
                    </div>
                </div>
                <div style={{ margin: '10px', visibility: (idOfCurrCourse === null) ? 'hidden' : 'visible' }} className="grid-row-sm">

                    <button type="button" className="importt bttn" onClick={openBulkAddRecords}>Bulk Add Records</button>
                    <br></br>
                    <label className="half header">Pupils In Course</label>
                    <button type="button" className="bttnFilter bttn">
                        <FontAwesomeIcon icon="filter" size="sm" />
                    </button>
                    <input
                        type='text'
                        autoComplete="off"
                        className="search inputFieldPeriod inputField"
                        //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                        id='inputCtrl_Search'
                        name='label'
                        placeholder='Search...'
                        //value={period.label}
                        onChange={onSearchFieldPupilChange}
                    ></input>
                    <button type="button" className="bttnSearch bttn" disabled >
                        <FontAwesomeIcon icon="search" size="sm" />
                    </button>
                    <div className="block">
                        <ul className="list-group">
                            {
                                pupilsOfCourse.map((item) =>
                                    <li className="list-group-item d-flex justify-content-center align-items-center" style={{ border: '2px solid #023047', color: (idOfCurrPupil === item._id) ? 'white' : '#023047', backgroundColor: (idOfCurrPupil === item._id) ? '#023047' : 'white' }} key={item._id} onClick={() => onPupilClick(item)}>
                                        {item.lastname.toUpperCase() + ' ' + item.firstname}
                                    </li>

                                )
                            }
                        </ul>
                    </div>
                </div>
                <div style={{ margin: '10px', visibility: (idOfCurrPupil === null) ? 'hidden' : 'visible' }} className="grid-row-sm">
                    <label className="header">Record-Info</label>
                    <form style={{ width: '100%' }}>

                        <label className="fat" htmlFor={`inputCtrl_date`}>Date:</label>
                        <input
                            type='datetime-local'
                            autoComplete="off"
                            className="inputFieldPeriod inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            id='inputCtrl_date'
                            name='validOn'
                            placeholder='validOn'
                            value={record.validOn}
                            onChange={onChange}
                        >
                        </input>
                        <br />
                        <label className="fat" htmlFor={`inputCtrl_Status`}>Select Scheme:</label>
                        <select id={`inputCtrl_Status`} name='schemaId' onChange={(event) => onSchemeSelectChanged(event, setIdOfSelectedScheme)} >
                            <option value="88" disabled selected={(idOfSelectedScheme === null || idOfSelectedScheme === undefined || idOfSelectedScheme === 'null') ? true : false}>Select your scheme</option>
                            {
                                schemes.map((element) => {
                                    return (
                                        <option key={element._id} value={element._id} selected={(idOfSelectedScheme === element._id) ? true : false}>{element.name}</option>
                                    );
                                })
                            }
                        </select>
                        <br></br>
                        <div style={{ opacity: ((idOfSelectedScheme === null && flagAddUpdate === flagSwitchAddUpdate.ADD) || idOfCurrPupil === null) ? '0.2' : '1' }}>
                            <label className="fat" htmlFor={`inputCtrl_Sign`}>Select Symbol:</label>
                            <select id={`inputCtrl_Sign`} name='sign' onChange={(event) => onSignSelectChanged(event, signs)} >
                                <option value="" disabled selected={(record.rating === null || record.rating === "null") ? true : false}>Select your symbol</option>
                                {
                                    signs.map((element) => {
                                        return (
                                            <option key={element._id} value={element._id} selected={(record.rating && record.rating._id === element._id) ? true : false}>{element.label} ({element.symbol})</option>
                                        );
                                    })
                                }
                            </select>
                        </div>
                        <label className="fat" htmlFor="weight">Weight</label>
                        <input type="number" id="weight" name="weight" className="inputFieldPeriod inputField"
                            min="0" max="100" value={record.weight} onChange={onChange}></input>

                        <br />
                        <label className="fat" htmlFor="value">Value</label>
                        <input type="number" id="value" name="value" className="inputFieldPeriod inputField"
                            min="1" value={(record.rating) ? record.rating.value : 1} onChange={onValueChange}></input>

                        <br />
                        <label className="fat" htmlFor="pupilNotPresent">Pupil not Present</label>
                        <input type="checkbox" id="pupilNotPresent" name="pupilNotPresent" style={{ marginLeft: '10px' }}
                            checked={record.pupilNotPresent} onChange={onChange}></input>
                        <br></br>
                        <label className="fat" htmlFor={`inputCtrl_Notes`} onChange={onChange}>Comment: </label>
                        <input
                            type='text'
                            autoComplete="off"
                            className="inputFieldPeriod inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            id='inputCtrl_Notes'
                            name='comment'
                            placeholder='comment'
                            value={record.comment}
                            onChange={onChange}
                        >
                        </input>
                        <button type="button" className="bttnTags bttn" onClick={onBtnModifyTagsClick}>Manage Tags</button>
                        <br></br>
                        <button type="button" className="bttnUpAdd bttn" onClick={onBtnSaveClick}>{(flagAddUpdate === flagSwitchAddUpdate.ADD) ? 'Add' : 'Update'}</button>
                        <button type="button" className="bttnDelete bttn" onClick={openDeleteModal}>Delete</button>

                    </form>
                    <br></br>
                    <br></br>
                    <label className="header">Records of selected Pupil:</label>

                    <input
                        type='text'
                        autoComplete="off"
                        className="search inputFieldPeriod inputField"
                        //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                        id='inputCtrl_Search'
                        name='label'
                        placeholder='Search...'
                        //value={period.label}
                        onChange={onSearchFieldAddedRecordsChange}
                    ></input>
                    <button type="button" className="bttnSearch bttn" disabled >
                        <FontAwesomeIcon icon="search" size="sm" />
                    </button>
                    <div className="block" style={{ maxHeight: '30%' }}>
                        <ul className="myUl">
                            {
                                recordsOfPupilInCourse.map((item) =>
                                    <li className="list-group-item d-flex justify-content-between align-items-center" style={{ border: '2px solid #023047', color: (idOfCurrRecord === item._id) ? 'white' : '#023047', backgroundColor: (idOfCurrRecord === item._id) ? '#023047' : 'white' }} key={'curr' + item._id} onClick={() => {
                                        onRecordSelect(item);
                                    }}>
                                        <div class="ms-2 me-auto">
                                            <div class="fw-bold" >{(!item.pupilNotPresent) ? item.rating.label : ''}</div>
                                        </div>
                                        <div class="ms-2 me-auto">
                                            <div class="fw-bold" style={{ color: (!item.pupilNotPresent) ? (idOfCurrRecord === item._id) ? 'white' : '#023047' : 'red' }}>{(!item.pupilNotPresent) ? 'Symbol: ' + item.rating.symbol : 'pupil not present'}</div>
                                        </div>
                                        <div class="ms-4 me-auto">
                                            <div class="fw-bold">{getDateTimeString(item.validOn)}</div>
                                        </div>
                                    </li>
                                )
                            }
                        </ul>
                    </div>
                </div>
            </div>

            <ModalWindow
                stateOfModal={stateOfModal}
                modalTitle={'Confirm Delete'}
                modalText={'Do you really want to delete record ?'}
                btnOkText={'Delete'}
                closeModal={closeModal}
                methodOkConfirmClick={deleteCurrRecord}
            ></ModalWindow>
            <ModalWindow
                stateOfModal={stateOfModalTags}
                modalTitle={'Manage Tags'}
                modalText={
                    <div>
                        <h6>Create New Tag:</h6>
                        <input style={{ borderRadius: '25px 0px 0px 25px', width: '70%', display: 'inline-block' }}
                            type='text'
                            autoComplete="off"
                            className="inline50 inputFieldPeriod inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            id='inputCtrl_Notes'
                            name='comment'
                            placeholder='comment'
                            //value={newTag}
                            onChange={onChangeTag}
                        >
                        </input>
                        <button type="button" style={{ borderRadius: '0px 25px 25px 0px', border: '2px solid #FB8500', width: '25%', display: 'inline-block' }} className="inline50 bttnUpAdd bttn" onClick={onBtnAddTagClick}>Add</button>

                        <br></br>
                        <br></br>
                        <h6>or Select Existing Tags:</h6>
                        <select onChange={onTagSelectChanged}>
                            <option value="" disabled selected={(newTag === "") ? true : false}>Select a tag</option>
                            {
                                tagsOfCourse.map((_tag) => {
                                    return (
                                        <option value={_tag}>
                                            {_tag}
                                        </option>
                                    );
                                })
                            }
                        </select>
                        <br></br>
                        <br></br>
                        <h6>Added tags:</h6>
                        <ul className="list-group list-group-horizontal">
                            {
                                (modifiedTags) ?
                                    modifiedTags.map((_el) => {
                                        return (
                                            <li style={{ color: invertColor(stringToColour(_el)), backgroundColor: stringToColour(_el) }} className="list-group-item d-flex justify-content-between align-items-center" onClick={() => removeTag(_el)} key={"btn" + _el}>
                                                {_el}
                                            </li>
                                        );
                                    })
                                    : ''
                            }
                        </ul>
                    </div>
                }
                btnOkText={'Apply'}
                closeModal={onBtnCloseModalTagsClick}
                methodOkConfirmClick={closeModalAndApplyChanges}
            ></ModalWindow>
        </div >
    );
};
export default Bewertung;