
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import React, { useState, useEffect } from 'react';
import { courseService } from '../services/courseService.js';
import { pupilService } from '../services/pupilService.js';
import { periodService } from '../services/periodService.js';
import { recordService } from '../services/recordService.js';
import '../css/Period.css';
import '../App.css';
import ModalWindow from './utils/modalWindow.jsx';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { cprService } from '../services/cprService.js';
import { errorHandler, getAndShowAllMsgs, formOK } from './utils/utils';
import { deprecationHandler } from 'moment';
import { Line } from 'react-chartjs-2';
import { useHistory } from 'react-router-dom';

import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';

ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
);

const options = {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
        y: {
            max: 100,
            min: 0,
            ticks: {
                stepSize: 1
            }
        }
    }
}

const CourseManagement = () => {
    //---States, UseEffects---//
    const history = useHistory();
    const [stateOfModalGrades, setStateOfModalGrades] = useState({
        isOpen: false
    });
    const openModalGrades = () => setStateOfModalGrades({ isOpen: true });
    const closeModalGrades = () => setStateOfModalGrades({ isOpen: false });

    //#region 
    const [stateOfModal, setStateOfModal] = useState({
        isOpen: false
    });
    const openModal = () => setStateOfModal({ isOpen: true });
    const closeModal = () => setStateOfModal({ isOpen: false });
    const [stateOfModalRemovePupil, setStateOfModalRemovePupil] = useState({
        isOpen: false
    });
    const openRemovePupilModal = () => setStateOfModalRemovePupil({ isOpen: true });
    const closeRemovePupilModal = () => setStateOfModalRemovePupil({ isOpen: false });
    const [pupilGradeObjs, setPupilGradeObjs] = useState([]);
    const [checkBoxPeriodsChecked, setcheckBoxPeriodsChecked] = useState(false);
    const [unfilteredCourses, setUnfilteredCourses] = useState([]);
    const [courses, setCourses] = useState([]);
    const flagSwitchAddUpdate = {
        ADD: 0,
        UPDATE: 1
    };
    const [flagAddUpdate, setFlagAddUpdate] = useState(flagSwitchAddUpdate.UPDATE);
    const emptyCourseObj = {
        'label': '',
        'subjectLabel': '',
        'pupilGroupLabel': '',
        'notes': '',
        'periodId': null
    }
    const [course, setCourse] = useState(emptyCourseObj);
    const [idOfCurrentCourse, setIdOfCurrentCourse] = useState(null);
    const [pupilsOfCurrCourse, setPupilsOfCurrCourse] = useState([]);
    const [idsPupilsOfCurrCourse, setIdsOfPupilsOfCurrCourse] = useState([]);
    const [remainingPupils, setRemainingPupils] = useState([]);
    const [currPupil, setCurrPupil] = useState({});
    const [periods, setPeriods] = useState([]);
    const [data, setData] = useState([]);
    const [labels, setLabels] = useState([]);
    const initialFormValidationInfo = {
        'label': {
            valid: false,
            msg: 'No input for label'
        }
    };
    const [formValidationInfo, setFormValidationInfo] = useState(initialFormValidationInfo);

    useEffect(() => {
        init();

        setData([...pupilGradeObjs.map((_el) => {
            return ((Math.round(_el.grade * 10) / 10));
        })]);

        setLabels([...pupilGradeObjs.map((_el) => {
            return (_el.pupil.lastname.toUpperCase() + ' ' + _el.pupil.firstname)
        })]);
    }, []);

    const init = () => {
        courseService.getAll().then(_courses => {
            sortAndSetCourses(_courses);
        }).catch((reason) => {
            errorHandler(reason, toast);
        })

        periodService.getAll().then(_periods => {
            setPeriods(_periods);
        }).catch((reason) => {
            errorHandler(reason, toast);
        })
    }

    useEffect(() => {
        checkWholePupilObj();
    }, [course]);


    useEffect(() => {
        let temp = [];

        if (idOfCurrentCourse !== null && idOfCurrentCourse !== undefined && idOfCurrentCourse !== 'null') {
            cprService.getPupilsById(idOfCurrentCourse).then((_pupilsOfCourse) => {
                _pupilsOfCourse.forEach((element) => temp.push(element.pupilId));
                setIdsOfPupilsOfCurrCourse([...temp]);
                fillPupilsLists(null, null, temp);
            }).catch((reason) => {
                errorHandler(reason, toast);
            })
        } else {
            pupilService.getAll().then((_pupils) => {
                setPupilsOfCurrCourse([]);
                setRemainingPupils(_pupils);
            }).catch((reason) => {
                errorHandler(reason, toast);
            })
        }


    }, [idOfCurrentCourse]);

    //#endregion
    //--------Methods--------//
    //#region  

    const fillPupilsLists = (searchKritCoursesAdd, searchKritCoursesRemaining, tempArrIdsOfPupilsOfCurrCourse) => {
        if (!tempArrIdsOfPupilsOfCurrCourse)
            tempArrIdsOfPupilsOfCurrCourse = idsPupilsOfCurrCourse;
        if (idOfCurrentCourse) {
            pupilService.getAll().then((_pupils) => {
                let _remainingPs = [];
                let _currentPs = [];
                _pupils.forEach((_pupil) => {
                    if (tempArrIdsOfPupilsOfCurrCourse.includes(_pupil._id)) {
                        if (searchKritCoursesAdd) {
                            if ((_pupil.lastname.toUpperCase() + ' ' + _pupil.firstname.toUpperCase).includes(searchKritCoursesAdd.toUpperCase()))
                                _currentPs.push(_pupil);
                        } else {
                            _currentPs.push(_pupil);
                        }
                    } else {
                        if (searchKritCoursesRemaining) {
                            if ((_pupil.lastname.toUpperCase() + ' ' + _pupil.firstname.toUpperCase).includes(searchKritCoursesRemaining.toUpperCase()))
                                _remainingPs.push(_pupil);
                        } else {
                            _remainingPs.push(_pupil);
                        }
                    }
                })
                setPupilsOfCurrCourse([..._currentPs]);
                setRemainingPupils([..._remainingPs]);
            }).catch((reason) => {
                errorHandler(reason, toast);
            })

        } else {
            /*courseService.getAll().then((_courses) => {
                setRemainingCourses(JSON.parse(_courses));
            });*/
            setRemainingPupils([]);
            setPupilsOfCurrCourse([]);
        }
    }

    const sortAndSetCourses = (_courses) => {
        if (_courses.length > 0) {
            _courses.sort((a, b) => { return a.label.localeCompare(b.label) });
            setCourses([..._courses]);
        }
    }

    const onBtnPupilsAddClick = () => {
        setFlagAddUpdate(flagSwitchAddUpdate.ADD);
        setCourse(emptyCourseObj);
        setIdOfCurrentCourse(null);
    }

    const onChange = (event) => {
        const { name, value } = event.target;
        setCourse({ ...course, [name]: value });
    }

    function checkInput(name, value) {
        let tempObj = {
            valid: false,
            msg: ''
        }
        switch (name) {
            case 'label':
                if (value.length === 0 || value === ' ') {
                    tempObj = {
                        valid: false,
                        msg: 'label cannot be empty'
                    };
                } else {
                    tempObj = {
                        valid: true,
                        msg: ''
                    };
                }
                break;

            default:
                break;
        }
        return tempObj;
    }


    function checkWholePupilObj() {
        let newFormValidationInfo = {};
        Object.keys(formValidationInfo).forEach((element) => {
            newFormValidationInfo[element] = checkInput(element, course[element]);
        })
        setFormValidationInfo({ ...newFormValidationInfo });
    }

    const onBtnSaveClick = () => {
        try {
            if (!formOK(formValidationInfo)) {
                getAndShowAllMsgs(formValidationInfo, toast);
                return;
            }

        } catch (err) {
            deprecationHandler(err, toast);
        }

        switch (flagAddUpdate) {
            case flagSwitchAddUpdate.ADD:
                //course.pupils = idsPupilsOfCurrCourse;
                delete course.pupils;
                courseService.create(course).then(() => {
                    init();
                    toast.success('course created');
                }).catch((reason) => {
                    errorHandler(reason, toast);
                })
                break;
            case flagSwitchAddUpdate.UPDATE:
                if (!idOfCurrentCourse) {
                    toast.error('error: probably no course selected to delete');
                    break;
                }

                course.pupils = idsPupilsOfCurrCourse;
                console.log(idsPupilsOfCurrCourse)
                courseService.update(idOfCurrentCourse, course).then(() => {
                    init();
                    toast.success('course updated');
                }).catch((reason) => {
                    errorHandler(reason, toast);
                })
                break;
            default:
                break;
        }
    }

    const onLiBtnClick = (_course) => {
        setFlagAddUpdate(flagSwitchAddUpdate.UPDATE);
        setCourse(_course);
        setIdOfCurrentCourse(_course._id);
        setcheckBoxPeriodsChecked(false);
        cprService.getPupilsById(_course._id).then((_cprs) => {
            recordService.getGradeSuccOfCourse(_course._id).then((_gso) => {
                let promises = [];
                _cprs.forEach((_cprsObj) => {
                    promises.push(pupilService.getById(_cprsObj.pupilId));
                })
                Promise.all(promises).then((values) => setPupilGradeObjs(values.map((_pupil) => { return ({ pupil: _pupil, grade: _gso.find((_el) => _el.pupilId === _pupil._id).grade }); })))
            }).catch((reason) => {
                errorHandler(reason, toast);
            })
        }).catch((reason) => {
            errorHandler(reason, toast);
        })
    }

    const deleteCurrCourse = () => {
        closeModal();

        courseService.delete(idOfCurrentCourse).then(() => {
            setIdOfCurrentCourse(null);
            setcheckBoxPeriodsChecked(false);
            init();
            toast.success('course deleted');
        }).catch((reason) => {
            errorHandler(reason, toast);
        })
    }


    const getCourseLabel = () => {
        if (idOfCurrentCourse) {
            let courseFound = courses.find((element) => element._id === idOfCurrentCourse);
            if (courseFound)
                return courseFound.label;
        }
        return '';
    }

    const onSearchFieldChange = (event) => {
        const { value } = event.target;
        let tempCourses = unfilteredCourses;
        if (unfilteredCourses.length < courses.length) {
            setUnfilteredCourses(courses);
            tempCourses = courses;
        }
        setCourses(tempCourses.filter((element) => {
            return element.label.toUpperCase().includes(value.toUpperCase());
        }).map((element) => {
            return element;
        }));
    }

    const openDeleteModal = () => {
        try {
            if (idOfCurrentCourse === null) {
                toast.error('no course selected')
            } else {
                openModal();
            }
        } catch (err) {
            errorHandler(err, toast);
        }
    }


    const onRemainingPupilsClick = (obj) => {
        if (!idsPupilsOfCurrCourse.includes(obj._id)) {
            idsPupilsOfCurrCourse.push(obj._id);
        } else {
            setIdsOfPupilsOfCurrCourse([obj._id]);
        }
        fillPupilsLists();
    }

    const onAddedPupilsClick = () => {
        closeRemovePupilModal();
        if (idsPupilsOfCurrCourse.includes(currPupil._id)) {
            let temp = idsPupilsOfCurrCourse;
            temp.splice(idsPupilsOfCurrCourse.indexOf(currPupil._id), 1);
            setIdsOfPupilsOfCurrCourse([...temp]);
            fillPupilsLists();
        }
    }

    const onSearchFieldRemainingPupilsChange = (event) => {
        const { value } = event.target;
        fillPupilsLists(null, value);

    }

    const onSearchFieldAddedPupilsChange = (event) => {
        const { value } = event.target;
        fillPupilsLists(value, null);

    }

    const onPeriodSelectChanged = (event) => {
        const { value } = event.target;
        setCourse({ ...course, 'periodId': getPeriodIdByLabel(value) });
    }

    const getPeriodIdByLabel = (label) => {
        return periods.find((obj) => obj.label === label)._id;
    }

    const checkBoxSelected = () => {
        setcheckBoxPeriodsChecked(!checkBoxPeriodsChecked);

    }

    const getPupilName = () => {
        if (currPupil.firstname && currPupil.lastname)
            return currPupil.lastname.toUpperCase() + ' ' + currPupil.firstname;
        return ' ';
    }

    const openShowAllGrades = () => {
        if (idOfCurrentCourse)
            history.push('/GradesOfCourse/' + idOfCurrentCourse);
        // declared();
        // openModalGrades();
    }

    function getAvg(data) {
        const sum = data.reduce((a, b) => a + b, 0);
        let avg = (sum / data.length);
        let arr = [];
        for (let i = 0; i < data.length; i++)
            arr.push(avg);
        return arr;
    }

    //#endregion
    //---------HTML----------//
    //#region 
    return (
        <div>
            <h1>Courses</h1>
            <div className="grid-container">
                <div style={{ margin: '10px' }} className="grid-row-sm">
                    <label className="header">Courses</label>
                    <input
                        type='text'
                        autoComplete="off"
                        className="search inputFieldPeriod inputField"
                        //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                        id='inputCtrl_Search'
                        name='label'
                        placeholder='Search...'
                        //value={period.label}
                        onChange={onSearchFieldChange}
                    ></input>
                    <button type="button" className="bttnSearch bttn" disabled >
                        <FontAwesomeIcon icon="search" size="sm" />
                    </button>
                    <div className="block">
                        <ul className="list-group">
                            {
                                courses.map((item) =>
                                    <li className="list-group-item d-flex justify-content-center align-items-center" style={{ border: '2px solid #023047', color: (idOfCurrentCourse === item._id) ? 'white' : '#023047', backgroundColor: (idOfCurrentCourse === item._id) ? '#023047' : 'white' }} key={item._id} onClick={() => onLiBtnClick(item)}>
                                        {item.label}
                                    </li>

                                )
                            }
                        </ul>
                    </div>
                    <button type="button" className="bttnAdd bttn" onClick={onBtnPupilsAddClick} >Add Course</button>
                </div>
                <div style={{ margin: '10px', visibility: (flagSwitchAddUpdate.UPDATE === flagAddUpdate && idOfCurrentCourse === null) ? 'hidden' : 'visible' }} className="grid-row-sm">
                    <button type="button" className="importt bttn" onClick={openShowAllGrades}>Show Grade Suggestions</button>
                    <br></br>
                    <label className="header">Course-Info</label>
                    <form style={{ width: '100%' }}>
                        <label className="fat" htmlFor={`inputCtrl_Label`}>Label:</label>
                        <input
                            type='text'
                            autoComplete="off"
                            className="inputFieldPeriod inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            id='inputCtrl_Label'
                            name='label'
                            placeholder='label'
                            value={course.label}
                            onChange={onChange}
                        >
                        </input>
                        <br />
                        <label className="fat" htmlFor={`inputCtrl_subject-label`}>Subject-label:</label>
                        <input
                            type='text'
                            autoComplete="off"
                            className="inputFieldPeriod inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            id='inputCtrl_subject-label'
                            name='subjectLabel'
                            placeholder='subjectLabel'
                            value={course.subjectLabel}
                            onChange={onChange}
                        >
                        </input>
                        <br />
                        <label className="fat" htmlFor={`inputCtrl_pupil-group-label`}>Pupil-group-label:</label>
                        <input
                            type='text'
                            autoComplete="off"
                            className="inputFieldPeriod inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            id='inputCtrl_pupil-group-label'
                            name='pupilGroupLabel'
                            placeholder='pupilGroupLabel'
                            value={course.pupilGroupLabel}
                            onChange={onChange}
                        >
                        </input>
                        <br />
                        <label className="fat" htmlFor={`inputCtrl_Notes`}>Notes:</label>
                        <input
                            type='text'
                            autoComplete="off"
                            className="inputFieldPeriod inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            id='inputCtrl_Notes'
                            name='notes'
                            placeholder='notes'
                            value={course.notes}
                            onChange={onChange}
                        >
                        </input>
                        <div className="checkB">
                            <input type="checkbox" id="selectperiods" name="periodsSelect" checked={checkBoxPeriodsChecked} onClick={() => checkBoxSelected()}></input>
                            <label className="inputC" htmlFor="periodsSelect">Add to Period?</label>
                        </div>
                        <div style={{ visibility: (checkBoxPeriodsChecked) ? 'visible' : 'hidden' }}>
                            <label className="fat" htmlFor={`inputCtrl_Status`}>Select Period:</label>
                            <select id={`inputCtrl_Status`} name='periods' onChange={onPeriodSelectChanged} >
                                <option disabled selected={(course.periodId === null) ? true : false}>Choose a period</option>
                                {
                                    periods.map((element) => {
                                        return (
                                            <option key={element._id} selected={(element._id === course.periodId) ? true : false}>{element.label}</option>
                                        );
                                    })
                                }
                            </select>
                        </div>
                        <br />
                        <button type="button" className="bttnUpAdd bttn" onClick={onBtnSaveClick}>{(flagAddUpdate === flagSwitchAddUpdate.ADD) ? 'Add' : 'Update'}</button>
                        <button type="button" className="bttnDelete bttn" onClick={openDeleteModal}>Delete</button>
                    </form>
                    <br></br>
                    <div style={{ visibility: (flagSwitchAddUpdate.ADD === flagAddUpdate || idOfCurrentCourse === null) ? 'hidden' : 'visible' }}>
                        <label className="header">Pupils in selected Course:</label>

                        <input
                            type='text'
                            autoComplete="off"
                            className="search inputFieldPeriod inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            id='inputCtrl_Search'
                            name='label'
                            placeholder='Search...'
                            //value={period.label}
                            onChange={onSearchFieldAddedPupilsChange}
                        ></input>
                        <button type="button" className="bttnSearch bttn" disabled >
                            <FontAwesomeIcon icon="search" size="sm" />
                        </button>
                        <div className="block">
                            <ul className="myUl">
                                {
                                    pupilsOfCurrCourse.map((item) =>
                                        <li className="list-group-item d-flex justify-content-center align-items-center" style={{ border: '2px solid #023047', color: '#023047', backgroundColor: 'white' }} key={'curr' + item._id} onClick={() => {
                                            setCurrPupil(item);
                                            openRemovePupilModal();
                                        }}>
                                            {item.lastname.toUpperCase() + ' ' + item.firstname}
                                        </li>
                                    )
                                }
                            </ul>
                        </div>

                    </div>
                </div>
                <div style={{ margin: '10px', visibility: (flagSwitchAddUpdate.ADD === flagAddUpdate || idOfCurrentCourse === null) ? 'hidden' : 'visible' }} className="grid-row-sm">
                    <label className="header">Select Pupils to add:</label>
                    {/*}
                    <button type="button" className="bttnFilter bttn" onClick={openSortModal}>
                        <FontAwesomeIcon icon="filter" size="sm" />
                    </button>
                */}
                    <input
                        type='text'
                        autoComplete="off"
                        className="search inputFieldPeriod inputField"
                        //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                        id='inputCtrl_Search'
                        name='label'
                        placeholder='Search...'
                        //value={period.label}
                        onChange={onSearchFieldRemainingPupilsChange}
                    ></input>
                    <button type="button" className="bttnSearch bttn" disabled >
                        <FontAwesomeIcon icon="search" size="sm" />
                    </button>
                    <div className="block">
                        <ul className="myUl">
                            {
                                remainingPupils.map((item) =>
                                    <li className="list-group-item d-flex justify-content-center align-items-center" style={{ border: '2px solid #023047', color: '#023047', backgroundColor: 'white' }} key={'remaining' + item._id} onClick={() => onRemainingPupilsClick(item)}>
                                        {item.lastname.toUpperCase() + ' ' + item.firstname}
                                    </li>
                                )
                            }
                        </ul>
                    </div>
                </div>
            </div>
            <ModalWindow
                stateOfModal={stateOfModal}
                modalTitle={'Confirm Delete'}
                modalText={'Do you really want to delete course \'' + getCourseLabel() + '\' ?'}
                btnOkText={'Delete'}
                closeModal={closeModal}
                methodOkConfirmClick={deleteCurrCourse}
            ></ModalWindow>
            <ModalWindow
                stateOfModal={stateOfModalRemovePupil}
                modalTitle={'Confirm Remove'}
                modalText={'Do you really want to remove pupil \'' + getPupilName() + '\' ?'}
                btnOkText={'Remove'}
                closeModal={closeRemovePupilModal}
                methodOkConfirmClick={onAddedPupilsClick}
            ></ModalWindow>
            <ModalWindow
                stateOfModal={stateOfModalGrades}
                modalTitle={'Suggested Grades Of Course'}
                modalText={
                    <div style={{ height: '100%' }}>
                        <div style={{ height: "500px", width: "100%" }}>
                            <Line options={options} data={{
                                labels,
                                datasets: [
                                    {
                                        label: 'Grades',
                                        data,
                                        borderColor: 'rgb(255, 99, 132)'
                                    },
                                    {
                                        label: 'Average',
                                        data: getAvg(data),
                                        borderColor: 'rgb(0, 255, 132)'
                                    }
                                ],
                            }} />
                        </div>
                        <div className="block" style={{ height: '40%' }}>
                        <ul className="list-group">
                            {
                                (pupilGradeObjs.length === 0) ? <h6>no pupil in this course has a record</h6> :
                                    pupilGradeObjs.map((_el) => {
                                        return (
                                            <li className="list-group-item d-flex justify-content-between align-items-center" style={{ border: '2px solid #023047', color: '#023047', backgroundColor: 'white' }}>
                                                <div class="ms-4 me-auto">
                                                    <div class="fw-bold">{_el.pupil.lastname.toUpperCase() + ' ' + _el.pupil.firstname}</div>
                                                </div>
                                                <div class="ms-2 me-auto" style={{ color: 'red' }}>
                                                    <div class="fw-bold">{(isNaN(_el.grade) || _el.grade === 0) ? 'has no records' : (Math.round(_el.grade * 10) / 10) + '%'}</div>
                                                </div>
                                            </li>
                                        );
                                    })
                            }
                        </ul>
                        </div>
                    </div>
                }
                btnOkText={'OK'}
                methodOkConfirmClick={closeModalGrades}
            ></ModalWindow>
        </div >
    );
    //#endregion
    function declared() {

        setData([...pupilGradeObjs.map((_el) => {
            return ((Math.round(_el.grade * 10) / 10));
        })]);

        setLabels([...pupilGradeObjs.map((_el) => {
            return (_el.pupil.lastname.toUpperCase() + ' ' + _el.pupil.firstname)
        })]);
    }
};
export default CourseManagement;