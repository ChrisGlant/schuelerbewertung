import React, { useState } from 'react';
import { toast } from 'react-toastify';
import { CSVReader } from 'react-papaparse';
import Collapsible from 'react-collapsible';
import { useEffect } from 'react';
import '../css/Import.css';
import { importService } from '../services/importService';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import ModalWindow from './utils/modalWindow.jsx';
import { errorHandler } from './utils/utils.js';
//https://www.npmjs.com/package/react-collapsible


const ImportStudents = () => {
    const [stateOfModal, setStateOfModal] = useState({
        isOpen: false
    });
    const openModal = () => setStateOfModal({ isOpen: true });
    const closeModal = () => setStateOfModal({ isOpen: false });
    const [pupilObjsFromServer, setPupilObjsFromServer] = useState([]);
    const [pupilsCommitedUpdates] = useState([]);
    const [pupilObjs, setPupilObjs] = useState([]);
    /*
    pupilObjs:
    [
        "state": [
            "to-import",
            "actions-necessary",
            "imported",
            "imported-with-actions"
        ],
        actions:{
            "update": [
                "fields that need to be updated"
            ],
            "coursesToCreate": [
                "courses that need to be created"
            ],
            "error": {
                "error_field": "error-msg"
            }
        }
        object:{}
    ]

    */

    const [linesOfParsedFile, setLinesOfParsedFile] = useState(null);
    const emptyPupilObj = {
        'lastname': '',
        'firstname': '',
        'state': '',
        'mail': '',
        'notes': '', //nm
        'courses': null //nm
    }
    const [pupil, setPupil] = useState(emptyPupilObj);
    const [mandatoryFields, setMandatoryFields] = useState([{ 'lastname': null }, { 'firstname': null }, { 'state': null }, { 'mail': null }]);
    const [notMandatoryFields, setNotMandatoryFields] = useState([{ 'notes': null }, { 'courses': null }]);

    useEffect(() => {
        if (linesOfParsedFile)
            if (linesOfParsedFile.length < 2) {
                toast.error('select a file with a heading and at least one record! (number of lines has to be at least 2)');
            } else {
                let header = linesOfParsedFile[0].data; //arr of strings
                for (let _key of header) {
                    if (mandatoryFields.some((_el) => Object.keys(_el)[0] === _key)) {
                        setIndexOfFields(_key, header, mandatoryFields, setMandatoryFields);
                    } else if (notMandatoryFields.some((_el) => Object.keys(_el)[0] === _key)) {
                        setIndexOfFields(_key, header, notMandatoryFields, setNotMandatoryFields);
                    }
                }
                //checken, ob irgend ein mandatroy field nicht gesetzt ist
                let forgottenField = mandatoryFields.find((_el) => _el[Object.keys(_el)[0]] === null);
                if (forgottenField) {
                    toast.error('File must contain at least mandatory fields [' + mandatoryFields.map((_el) => { return Object.keys(_el)[0] }) + ']');
                    return;
                }
                let _pupils = [];
                for (let idx = 1; idx < linesOfParsedFile.length; idx++) {
                    //line für line ein obj zu _pupils hinzufügen
                    let _obj = {};
                    if (linesOfParsedFile[idx].data.length >= mandatoryFields.length) {
                        for (let _fieldObj of mandatoryFields.filter((_el) => _el[Object.keys(_el)[0]] !== null)) {
                            _obj[Object.keys(_fieldObj)[0]] = linesOfParsedFile[idx].data[_fieldObj[Object.keys(_fieldObj)[0]]];
                        }
                        for (let _fieldObj of notMandatoryFields.filter((_el) => _el[Object.keys(_el)[0]] !== null)) {
                            if (Object.keys(_fieldObj)[0] === 'courses') {
                                let line = (linesOfParsedFile[idx].data[_fieldObj[Object.keys(_fieldObj)[0]]]);
                                if (line === undefined) {
                                    toast.error('invalid csv format; some data appears to be missing in line ' + (idx + 1));
                                    return;
                                }
                                let arr = line.split(',');
                                if (!(arr.length === 1 && arr[0] === ''))
                                    _obj[Object.keys(_fieldObj)[0]] = arr;
                            } else {
                                _obj[Object.keys(_fieldObj)[0]] = linesOfParsedFile[idx].data[_fieldObj[Object.keys(_fieldObj)[0]]];
                            }
                        }
                        _pupils.push({ 'object': _obj });
                    }
                }
                checkAndSetPupilObjs(_pupils);
            }
    }, [linesOfParsedFile]);

    useEffect(() => {
        let tempPupils = pupilObjsFromServer.map((_el) => {
            if (_el.actions && (_el.actions.update || _el.actions.coursesToCreate)) {
                if (pupilsCommitedUpdates.some((_el2) => _el2.mail === _el.object.mail)) {
                    if (!_el.actions.error) {//no other actions
                        _el.state = 'to-import';
                    }
                    delete _el.actions.update;
                    delete _el.actions.coursesToCreate;
                } else if (pupilsCommitedUpdates.some((_el2) => _el.object.courses.every(v => _el2.courses.includes(v)))) {
                    //checken, ob courses bereits in anderem pupil commited
                    if (!_el.actions.error && !_el.actions.update) {//no other actions
                        _el.state = 'to-import';
                    }
                    delete _el.actions.coursesToCreate;
                }
            }
            return _el;
        })
        setPupilObjs([...tempPupils]);
    }, [pupilObjsFromServer])


    function setIndexOfFields(_key, header, fields, setFields) {
        let idx = fields.findIndex((_el) => Object.keys(_el)[0] === _key);
        if (!(fields[idx])[_key]) {
            let temparr = fields;
            temparr[idx] = { [_key]: header.indexOf(_key) };
            setFields([...temparr]);
        }
    }

    const checkAndSetPupilObjs = (objs, successmsg) => {
        importService.putPupils(objs.map((_el) => { return _el.object })).then((_objs) => {
            setPupilObjsFromServer([..._objs]);
            setPupilObjs([..._objs]);
            if (successmsg)
                toast.success(successmsg);
        }).catch((reason) => {
            errorHandler(reason, toast);
        })
    }

    const handleOnDrop = (data) => {
        setLinesOfParsedFile(data);
    }

    const handleOnError = (err, file, inputElem, reason) => {
        toast.error(err);
    }

    const handleOnRemoveFile = (data) => {
        toast.success('file removed');
        setLinesOfParsedFile(null);
        setPupilObjs([]);
        setPupilObjsFromServer([]);
    }

    const onBtnImportClick = () => {
        importService.confirmImport(pupilObjsFromServer.map((_el) => { return _el.object })).then((_objs) => {
            toast.success('pupils imported');
            clear();
        }).catch((reason) => {
            errorHandler(reason, toast);
        })
    }

    const clear = () => {
        setLinesOfParsedFile(null);
        setPupilObjs([]);
        setPupilObjsFromServer([]);
    }

    const onChange = (event, pupil) => {
        const { name, value } = event.target;
        let temparr = JSON.parse(JSON.stringify(pupilObjs));
        let idx = temparr.findIndex((_el) => _el.object.mail === pupil.mail);
        temparr[idx].object[name] = value;
        setPupilObjs([...temparr]);
    }

    const removeCourseFromPupil = (ppl, course) => {
        let temparr = [...pupilObjs];
        let idx = temparr.findIndex((_el) => _el.object.mail === ppl.mail);
        temparr[idx].object.courses.splice(temparr[idx].object.courses.indexOf(course), 1);
        setPupilObjs([...temparr]);
    }

    const onBtnCommitClick = (_pupil) => {
        let temparr = JSON.parse(JSON.stringify(pupilObjsFromServer));
        let pupilObjInList = pupilObjs.find((_el) => _el.object.mail === _pupil.mail);
        temparr[pupilObjs.findIndex((_el) => _el.object.mail === _pupil.mail)] = pupilObjInList;
        if (pupilObjInList.actions && (pupilObjInList.actions.update || pupilObjInList.actions.coursesToCreate) && !pupilsCommitedUpdates.includes(pupilObjsFromServer.find((_el) => _el.object.mail === _pupil.mail)).object) {
            pupilsCommitedUpdates.push(_pupil);
        }

        checkAndSetPupilObjs(temparr, 'changes committed');
    }

    const onBtnDeclineClick = () => {
        toast.success('procedure aborted');
        clear();
    }

    const openDeletePupilModal = (elem) => {
        setPupil(elem);
        openModal();
    }

    const deleteCurrPupil = () => {
        let temparr = pupilObjsFromServer;
        temparr.splice(temparr.findIndex((_el) => _el.object.mail === pupil.mail), 1);
        checkAndSetPupilObjs(temparr, 'pupil removed');
        closeModal();
    }

    return (
        <div>
            <h1>Import Students</h1>
            <div style={{ margin: "20px 0px 0px 20px" }}> {/*top right bottom left*/}
                <h3>Select file:</h3>
                <div className="reader">
                    <CSVReader
                        onDrop={handleOnDrop}
                        onError={handleOnError}
                        addRemoveButton
                        removeButtonColor='#659cef'
                        onRemoveFile={handleOnRemoveFile}>
                        <span>Drop a file here or click to upload.</span>
                    </CSVReader>
                </div>
                <br></br>
                <h3>Pupils of file:</h3>
                <div className="sm-block">
                    <ul className='import'>
                        {
                            pupilObjs.map((item) =>
                                <li className='import' style={{ margin: '3px', border: (item.state === 'actions-necessary') ? "3px solid red" : "none" }}>
                                    <div className="inline10 verticalMid">
                                        {(item.state === 'actions-necessary') ? <FontAwesomeIcon icon="exclamation-triangle" size="lg" color={"red"} /> : <FontAwesomeIcon color={"green"} icon="check" size="lg" />}
                                    </div>
                                    <div className="inline70">
                                        <Collapsible triggerTagName="div" triggerStyle={{ marginTop: '10px', marginBottom: '0px' }} trigger={item.object.lastname + ' ' + item.object.firstname + ' ' + item.object.mail} transitionTime={200} >
                                            <br></br>
                                            <div style={{ marginTop: '10px' }}>
                                                {
                                                    Object.keys(item.object).map((key) => {
                                                        return (
                                                            (key !== 'courses') ?
                                                                (item.actions && ((item.actions.error && Object.keys(item.actions.error).includes(key)) || (item.actions.update && item.actions.update.includes(key)))) ?
                                                                    (item.actions.update && item.actions.update.includes(key)) ?
                                                                        <div>
                                                                            <p>{key + ' will be updated; commit to update or remove pupil'}</p><br></br></div>
                                                                        :

                                                                        <div className="form-group row">
                                                                            <div className="nomargin form-group col-2">
                                                                                <label className="labels" htmlFor={`inputCtrl_Lastname`}>{key}:</label>
                                                                            </div>
                                                                            <div className="nomargin form-group col-10">
                                                                                <input
                                                                                    type='text'
                                                                                    autoComplete="off"
                                                                                    className="inputPP inputFieldPeriod inputField"
                                                                                    //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                                                                                    id='inputCtrl_Lastname'
                                                                                    name={key}
                                                                                    placeholder={key}
                                                                                    value={item.object[key]}
                                                                                    onChange={(event) => onChange(event, item.object)}
                                                                                >
                                                                                </input>
                                                                                <p style={{ color: "red" }}>
                                                                                    {
                                                                                        (item.actions.error && Object.keys(item.actions.error).includes(key)) ?
                                                                                            'error: ' + item.actions.error[key] + ' click \'commit changes\' after fixxing this issue'
                                                                                            :
                                                                                            ''
                                                                                    }
                                                                                </p>
                                                                            </div>
                                                                            <br />
                                                                        </div>
                                                                    : ''
                                                                :
                                                                <div>
                                                                    {(item.actions && item.actions.coursesToCreate) ?
                                                                        <div>
                                                                            <h4>Courses of pupil:</h4>
                                                                            <ul className="list-group">
                                                                                {
                                                                                    item.object.courses.map((c) => {
                                                                                        if (item.actions.coursesToCreate.includes(c) && !pupilsCommitedUpdates.some((_pcu) => _pcu.courses.includes(c))) {
                                                                                            if (c === '')
                                                                                                return ('');
                                                                                            return (
                                                                                                <li className="list-group-item d-flex justify-content-between align-items-center" style={{ border: '2px solid #023047', color: '#023047', backgroundColor: 'white' }}>
                                                                                                    <div className="course">{c}</div>
                                                                                                    <div className="middle">{(item.actions.coursesToCreate.includes(c)) ? 'Course doesn\'t exist; remove course or commit to create it' : ''}</div>
                                                                                                    <div>
                                                                                                        <button type="button" key={"btn"} className="muell trans bttnUpAdd bttn" onClick={() => removeCourseFromPupil(item.object, c)} >
                                                                                                            <FontAwesomeIcon icon="trash" size="lg" />
                                                                                                        </button>
                                                                                                    </div>
                                                                                                </li>
                                                                                            );
                                                                                        } else return '';
                                                                                    })
                                                                                }
                                                                            </ul>
                                                                        </div>
                                                                        : ''
                                                                    }
                                                                </div>

                                                        );
                                                    })
                                                }
                                                <br></br>
                                                <button className="green bttnUpAdd bttn" onClick={() => onBtnCommitClick(item.object)} style={{ visibility: (item.state === 'actions-necessary') ? 'visible' : 'hidden' }}>Commit Changes</button>
                                            </div>

                                        </Collapsible>
                                    </div>
                                    <button type="button" key={"btn"} className="inline10 trans bttnUpAdd bttn" onClick={() => openDeletePupilModal(item.object)}>
                                        <FontAwesomeIcon icon="trash" size="lg" />
                                    </button>
                                </li>
                            )
                        }
                    </ul>
                </div >
                <div >
                    <button className="inline40 bttn bttnDecline" onClick={onBtnDeclineClick} style={{ visibility: (pupilObjsFromServer.length > 0) ? 'visible' : 'hidden' }}>Decline</button>
                    <button className="inline40 bttn bttnImport" onClick={onBtnImportClick} style={{ visibility: ((pupilObjsFromServer.some((_el) => _el.state === 'actions-necessary') || pupilObjsFromServer.length === 0) ? 'hidden' : 'visible') }}>Import</button>
                </div>
                {/* <div style={{ width: "100%" }}>
                    <Collapsible tabIndex={0} trigger="Start here" style={{ width: "100%" }}>
                        <p>
                            This is the collapsible content. It can be any element or React
                            component you like.
                        </p>
                        <p>
                            It can even be another Collapsible component. Check out the next
                            section!
                        </p>
                    </Collapsible>

                    <Collapsible transitionTime={400} trigger="Then try this one">
                        <p>Would you look at that!</p>
                        <p>See; you can nest as many Collapsible components as you like.</p>

                        <Collapsible trigger="Mmmmm, it's all cosy nested here">
                            <p>
                                And there's no limit to how many levels deep you go. Or how many you
                                have on the same level.
                            </p>

                            <Collapsible trigger="This is just another Collapsible">
                                <p>
                                    It just keeps going and going! Well, actually we've stopped here.
                                    But that's only because I'm running out of things to type.
                                </p>
                            </Collapsible>
                            <Collapsible trigger="But this one is open by default!" open={true}>
                                <p>
                                    And would you look at that! This one is open by default. Sexy
                                    huh!?
                                </p>
                                <p>
                                    You can pass the prop of open=&#123;true&#125; which will make the
                                    Collapsible open by default.
                                </p>
                            </Collapsible>
                            <Collapsible
                                trigger="That's not all. Check out the speed of this one"
                                transitionTime={100}
                            >
                                <p>Whoosh! That was fast right?</p>
                                <p>
                                    You can control the time it takes to animate (transition) by
                                    passing the prop transitionTime a value in milliseconds. This one
                                    was set to transitionTime=&#123;100&#125;
                                </p>
                            </Collapsible>
                        </Collapsible>
                    </Collapsible>

                    <Collapsible
                        transitionTime={400}
                        trigger="This one will blow your mind."
                        easing={'cubic-bezier(0.175, 0.885, 0.32, 2.275)'}
                    >
                        <p>
                            Well maybe not. But did you see that little wiggle at the end. That is
                            using a CSS cubic-beizer for the easing!
                        </p>
                        <p>
                            You can pass any string into the prop easing that you would declare in
                            a CSS transition-timing-function. This means you have complete control
                            over how that Collapsible appears.
                        </p>
                    </Collapsible>

                    <Collapsible
                        transitionTime={400}
                        trigger="Oh and did I mention that I'm responsive?"
                        triggerWhenOpen="Plus you can change the trigger text when I'm open too"
                    >
                        <p>
                            That's correct. This collapsible section will animate to the height it
                            needs to and then set it's height back to auto.
                        </p>
                        <p>
                            This means that no matter what width you stretch that viewport to, the
                            Collapsible it will respond to it.
                        </p>
                        <p>
                            And no matter what height the content within it is, it will change
                            height too.
                        </p>
                        <h2>CSS Styles</h2>
                        <p>
                            All of the style of the Collapsible (apart from the overflow and
                            transition) are controlled by your own CSS too.
                        </p>
                        <p>
                            By default the top-level CSS class is Collapsible, but you have
                            control over this too so you can easily add it into your own project.
                            Neato!
                        </p>
                        <p>
                            So by setting the prop of
                            classParentString=&#123;"MyNamespacedClass"&#125; then the top-level
                            class will become MyNamespacedClass.
                        </p>
                    </Collapsible>

                    <Collapsible
                        lazyRender
                        transitionTime={600}
                        trigger="What happens if there's a shed-load of content?"
                        easing={'cubic-bezier(0.175, 0.885, 0.32, 2.275)'}
                        overflowWhenOpen="visible"
                    >
                        <p>
                            Add the prop of{' '}
                            <strong style={{ fontWeight: 'bold' }}>lazyRender</strong> and the
                            content will only be rendered when the trigger is pressed
                        </p>
                        <img src="https://lorempixel.com/320/240?random=1" />
                        <img src="https://lorempixel.com/320/240?random=2" />
                        <img src="https://lorempixel.com/320/240?random=3" />
                        <img src="https://lorempixel.com/320/240?random=4" />
                        <img src="https://lorempixel.com/320/240?random=5" />
                        <img src="https://lorempixel.com/320/240?random=6" />
                    </Collapsible>

                    <Collapsible
                        trigger="You can set a custom trigger tag name."
                        triggerTagName="div"
                    >
                        <p>
                            Use the <code>`triggerTagName`</code> prop to set the trigger wrapping
                            element.
                        </p>
                        <p>
                            Defaults to <code>span</code>.
                        </p>
                    </Collapsible>

                    <Collapsible
                        trigger="You can customise the CSS a bit more too"
                        triggerClassName="CustomTriggerCSS"
                        triggerOpenedClassName="CustomTriggerCSS--open"
                        contentOuterClassName="CustomOuterContentCSS"
                        contentInnerClassName="CustomInnerContentCSS"
                    >
                        <p>
                            This is the collapsible content. It can be any element or React
                            component you like.
                        </p>
                    </Collapsible>

                    <Collapsible
                        trigger="You can disable them programmatically too"
                        open
                        triggerDisabled
                    >
                        <p>This one has it's trigger disabled in the open position. Nifty.</p>
                        <p>
                            You also get the <strong>is-disabled</strong> CSS class so you can
                            style it.
                        </p>
                    </Collapsible>


                    <Collapsible
                        trigger={'Add a triggerStyle Prop to add style directly to the trigger'}
                        triggerStyle={{ background: '#2196f3' }}
                    >
                        <p>
                            Adds a <code>style</code> attribute to the <code>span</code> trigger.
                        </p>
                    </Collapsible>
                    <Collapsible
                        trigger="Pass properties to top element"
                        containerElementProps={{ id: 'my-cool-identifier', lang: 'en' }}
                        triggerStyle={{ background: '#6821f3' }}
                    >
                        <p>
                            Some element attributes (<strong>id & lang</strong>) have been passed
                            as properties using <strong>containerElementProps</strong>.
                        </p>
                        <div style={{ display: 'grid' }}>
                            <img src="https://lorempixel.com/320/240?random=1" />
                            <img src="https://lorempixel.com/320/240?random=2" />
                            <img src="https://lorempixel.com/320/240?random=3" />
                            <img src="https://lorempixel.com/320/240?random=4" />
                            <img src="https://lorempixel.com/320/240?random=5" />
                            <img src="https://lorempixel.com/320/240?random=6" />
                        </div>
                        <div style={{ padding: '50px' }}>
                            <button
                                onClick={() =>
                                    document.getElementById('my-cool-identifier').scrollIntoView()
                                }
                            >
                                Scroll to my id
                            </button>
                        </div>
                    </Collapsible>

                </div> */}
                <ModalWindow
                    stateOfModal={stateOfModal}
                    modalTitle={'Confirm Removal'}
                    modalText={'Do you really want to remove pupil \'' + pupil.lastname.toUpperCase() + ' ' + pupil.firstname + '\' ?'}
                    btnOkText={'Confirm'}
                    closeModal={closeModal}
                    methodOkConfirmClick={deleteCurrPupil}
                ></ModalWindow>
            </div >
        </div >
    );
};



export default ImportStudents;