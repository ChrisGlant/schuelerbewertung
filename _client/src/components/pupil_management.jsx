import { toast } from 'react-toastify';
import React, { useState, useEffect } from 'react';
import { pupilService } from '../services/pupilService.js';
import { courseService } from '../services/courseService.js';
import '../css/Period.css';
import '../css/Pupil.css';
import '../App.css';
import { errorHandler, sortAndSetPupils, getAndShowAllMsgs, formOK } from './utils/utils';
import ModalWindow from './utils/modalWindow.jsx';
import { cprService } from '../services/cprService.js';

import { useHistory } from 'react-router-dom';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const PupilManagement = () => {
    //---States, UseEffects---//
    //#region 
    const history = useHistory();
    const [stateOfModal, setStateOfModal] = useState({
        isOpen: false
    });
    const openModal = () => setStateOfModal({ isOpen: true });
    const closeModal = () => setStateOfModal({ isOpen: false });
    const [stateOfModalTags, setStateOfModalTags] = useState({
        isOpen: false
    });
    const openModalTags = () => setStateOfModalTags({ isOpen: true });
    const closeModalTags = () => setStateOfModalTags({ isOpen: false });

    const [stateOfModalRemoveCourse, setStateOfModalRemoveCourse] = useState({
        isOpen: false
    });
    const openRemoveCourseModal = () => setStateOfModalRemoveCourse({ isOpen: true });
    const closeRemoveCourseModal = () => setStateOfModalRemoveCourse({ isOpen: false });
    const [coursesOfCurrPupil, setCoursesOfCurrPupil] = useState([]);
    const [remainingCourses, setRemainingCourses] = useState([]);
    const [idsOfCoursesOfCurrPupil, setIdsOfCoursesOfCurrPupil] = useState([]);
    const [currCourse, setCurrCourse] = useState({});
    const [unfilteredPupils, setUnfilteredPupils] = useState([]);
    const [newTag, setNewTag] = useState('');
    // const [tagsOfCourse, setTagsOfCourse] = useState([]);
    const [modifiedTags, setModifiedTags] = useState([]);
    const [pupils, setPupils] = useState([]);
    useEffect(() => {
        init();
    }, []);

    const init = () => {
        pupilService.getAll().then(_pupils => {
            sortAndSetPupils(_pupils, setPupils);
        }).catch((reason) => {
            errorHandler(reason, toast);
        })
    }

    const flagSwitchAddUpdate = {
        ADD: 0,
        UPDATE: 1
    };
    const [flagAddUpdate, setFlagAddUpdate] = useState(flagSwitchAddUpdate.UPDATE);

    const emptyPupilObj = {
        'lastname': '',
        'firstname': '',
        'mail': '',
        'notes': '',
        'state': 'active',
        'tags': []
    }
    const [pupil, setPupil] = useState(emptyPupilObj);
    const [idOfCurrentPupil, setIdOfCurrentPupil] = useState(null);

    const initialFormValidationInfo = {
        'lastname': {
            valid: false,
            msg: 'No input for lastname'
        },
        'firstname': {
            valid: false,
            msg: 'No input for firstname'
        },
        'mail': {
            valid: false,
            msg: 'No input for mail'
        }
    };
    const [formValidationInfo, setFormValidationInfo] = useState(initialFormValidationInfo);

    useEffect(() => {
        checkWholePupilObj();
    }, [pupil]);

    useEffect(() => {
        let temp = [];
        if (idOfCurrentPupil !== null && idOfCurrentPupil !== undefined && idOfCurrentPupil !== 'null') {
            cprService.getCoursesById(idOfCurrentPupil).then((_courses) => {
                _courses.forEach((element) => temp.push(element.courseId));
                setIdsOfCoursesOfCurrPupil([...temp]);
                fillCourseLists(null, null, temp);
            }).catch((reason) => {
                errorHandler(reason, toast);
            })
        } else {
            courseService.getAll().then((_courses) => {
                setCoursesOfCurrPupil([]);
                setRemainingCourses(_courses);
            }).catch((reason) => {
                errorHandler(reason, toast);
            })
        }
    }, [idOfCurrentPupil]);

    //#endregion
    //--------Methods--------//
    //#region 



    const fillCourseLists = (searchKritCoursesAdd, searchKritCoursesRemaining, tempArrIdsOfCoursesOfCurrPupil) => {
        if (!tempArrIdsOfCoursesOfCurrPupil)
            tempArrIdsOfCoursesOfCurrPupil = idsOfCoursesOfCurrPupil;
        if (idOfCurrentPupil) {

            courseService.getAll().then((_courses) => {
                //_courses = JSON.parse(_courses);
                let _remainingCs = [];
                let _currentCs = [];
                _courses.forEach((_course) => {
                    if (!tempArrIdsOfCoursesOfCurrPupil.includes(_course._id)) {
                        if (searchKritCoursesRemaining) {
                            if (_course.label.toUpperCase().includes(searchKritCoursesRemaining.toUpperCase()))
                                _remainingCs.push(_course);
                        } else {
                            _remainingCs.push(_course);
                        }
                    } else {
                        if (searchKritCoursesAdd) {
                            if (_course.label.toUpperCase().includes(searchKritCoursesAdd.toUpperCase()))
                                _currentCs.push(_course);
                        } else {
                            _currentCs.push(_course);
                        }
                    }

                });
                setCoursesOfCurrPupil([..._currentCs]);
                setRemainingCourses([..._remainingCs]);
            }).catch((reason) => {
                errorHandler(reason, toast);
            })
        } else {
            /*courseService.getAll().then((_courses) => {
                setRemainingCourses(JSON.parse(_courses));
            });*/
            setRemainingCourses([]);
            setCoursesOfCurrPupil([]);
        }
    }

    const onBtnPupilsAddClick = () => {
        setFlagAddUpdate(flagSwitchAddUpdate.ADD);
        setPupil(emptyPupilObj);
        setIdOfCurrentPupil(null);
    }

    const onChange = (event) => {
        const { name, value } = event.target;
        setPupil({ ...pupil, [name]: value });
    }

    function checkInput(name, value) {
        let tempObj = {
            valid: false,
            msg: ''
        }
        switch (name) {
            case 'firstname':
                if (value.length > 40 || value.length < 3) {
                    tempObj = {
                        valid: false,
                        msg: 'firstname cannot be shorter than 3 chars or longer than 40'
                    };
                } else {
                    tempObj = {
                        valid: true,
                        msg: ''
                    };
                }
                break;
            case 'lastname':
                if (value.length > 40 || value.length < 3) {
                    tempObj = {
                        valid: false,
                        msg: 'lastname cannot be shorter than 3 chars or longer than 40'
                    };
                } else {
                    tempObj = {
                        valid: true,
                        msg: ''
                    };
                }
                break;
            case 'mail':
                if (!value) {
                    tempObj = {
                        msg: 'provide an email',
                        valid: false
                    }
                } else {
                    if (value.length < 4) {
                        tempObj = {
                            msg: 'email is too short',
                            valid: false
                        }
                    } else if (value.length > 40) {
                        tempObj = {
                            msg: 'email is too long',
                            valid: false
                        }
                    } else if ((value.split('@').length - 1) !== 1) {
                        tempObj = {
                            msg: 'email must have ONE @',
                            valid: false
                        }
                    } else {
                        tempObj = {
                            valid: true,
                            msg: ''
                        };
                    }
                }
                break;
            default:
                break;
        }
        return tempObj;
    }


    function checkWholePupilObj() {
        let newFormValidationInfo = {};
        Object.keys(formValidationInfo).forEach((element) => {
            newFormValidationInfo[element] = checkInput(element, pupil[element]);
        })
        setFormValidationInfo({ ...newFormValidationInfo });
    }

    const onBtnSaveClick = () => {
        if (!formOK(formValidationInfo)) {
            getAndShowAllMsgs(formValidationInfo, toast);
            return;
        }

        switch (flagAddUpdate) {
            case flagSwitchAddUpdate.ADD:
                checkAndUpdateCourses();

                pupilService.create(pupil).then(() => {
                    init();
                    toast.success('pupil created');
                }).catch((reason) => {
                    errorHandler(reason, toast);
                })
                break;
            case flagSwitchAddUpdate.UPDATE:
                if (!idOfCurrentPupil) {
                    toast.error('error: probably no pupil selected to delete');
                    break;
                }
                checkAndUpdateCourses();
                pupilService.update(idOfCurrentPupil, pupil).then(() => {
                    init();
                    toast.success('pupil updated');
                }).catch((reason) => {
                    errorHandler(reason, toast);
                })
                break;
            default:
                break;
        }
    }

    const checkAndUpdateCourses = () => {
        // let _pupils = [];
        let coursesArr = [coursesOfCurrPupil, remainingCourses];
        for (let arrIdx = 0; arrIdx < coursesArr.length; arrIdx++) {
            arrIdx = parseInt(arrIdx);
            for (let _course of coursesArr[arrIdx]) {
                // _course = (coursesArr[arrIdx])[parseInt(_course)];
                cprService.getPupilsById(_course._id).then((_pupilsInner) => {
                    let _pupils = [];
                    _pupilsInner.forEach((elem) => _pupils.push(elem.pupilId));

                    if (arrIdx === 0) { //courses, in denen der schüler ist
                        if (!_pupils.includes(idOfCurrentPupil)) {
                            _pupils.push(idOfCurrentPupil);
                            _course.pupils = _pupils;
                            courseService.update(_course._id, _course).catch((reason) => {
                                errorHandler(reason, toast);
                            })

                        }
                    } else if (arrIdx === 1) { //courses, in denen der schüler nicht
                        if (_pupils.includes(idOfCurrentPupil)) {
                            _pupils.splice(_pupils.indexOf(idOfCurrentPupil), 1);
                            _course.pupils = _pupils;
                            courseService.update(_course._id, _course).catch((reason) => {
                                errorHandler(reason, toast);
                            })
                        }
                    }

                }).catch((reason) => {
                    errorHandler(reason, toast);
                })
            }
        }
    }

    const onLiBtnClick = (_pupil) => {
        setFlagAddUpdate(flagSwitchAddUpdate.UPDATE);
        setIdOfCurrentPupil(_pupil._id);
        setPupil(_pupil);
    }

    const deleteCurrPupil = () => {
        closeModal();

        pupilService.delete(idOfCurrentPupil).then(() => {
            setIdOfCurrentPupil(null);
            init();
            toast.success('pupil deleted');
        }).catch((reason) => {
            errorHandler(reason, toast);
        })
    }


    const getPupilsName = () => {
        if (idOfCurrentPupil) {
            let pupilFound = pupils.find((element) => element._id === idOfCurrentPupil);
            if (pupilFound)
                return pupilFound.lastname + ' ' + pupilFound.firstname;
        }
        return '';
    }

    const onSearchFieldChange = (event) => {
        const { value } = event.target;
        let tempPupils = unfilteredPupils;
        if (unfilteredPupils.length < pupils.length) {
            setUnfilteredPupils(pupils);
            tempPupils = pupils;
        }
        setPupils(tempPupils.filter((element) => {
            return (element.lastname + ' ' + element.firstname).toUpperCase().includes(value.toUpperCase());
        }).map((element) => {
            return element;
        }));
    }

    const openDeleteModal = () => {
        try {
            if (idOfCurrentPupil === null) {
                toast.error('no pupil selected')
            } else {
                openModal();
            }
        } catch (err) {
            errorHandler(err, toast);
        }
    }

    const onRemainingCourseClick = (obj) => {
        idsOfCoursesOfCurrPupil.push(obj._id);
        coursesOfCurrPupil.push(obj);
        let tempCourses = remainingCourses;
        let idx = tempCourses.indexOf(obj);
        tempCourses.splice(idx, 1);
        setRemainingCourses([...tempCourses]);

    }

    const onAddedCourseClick = () => {
        closeRemoveCourseModal();
        remainingCourses.push(currCourse);
        let tempCourses = coursesOfCurrPupil;
        let idx = tempCourses.indexOf(currCourse);
        tempCourses.splice(idx, 1);
        setCoursesOfCurrPupil([...tempCourses]);
        let tempCourseIds = idsOfCoursesOfCurrPupil;
        idx = tempCourseIds.indexOf(currCourse._id);
        tempCourseIds.splice(idx, 1);
        setIdsOfCoursesOfCurrPupil([...tempCourseIds]);

    }

    const onSearchFieldRemainingCoursesChange = (event) => {
        const { value } = event.target;
        fillCourseLists(null, value);
    }

    const onSearchFieldAddedCoursesChange = (event) => {
        const { value } = event.target;
        fillCourseLists(value, null);
    }

    const getCourseLabel = () => {
        if (currCourse.label)
            return currCourse.label;
        return ' ';
    }

    const openRecordsOfPupil = () => {
        history.push('/RecordsOfPupil/' + idOfCurrentPupil);
    }

    const onStateSelectChanged = (event) => {
        const { value } = event.target;
        setPupil({ ...pupil, 'state': value });
    }


    const closeModalAndApplyChanges = () => {
        closeModalTags();
        setPupil({ ...pupil, tags: [...modifiedTags] })
        if (flagAddUpdate === flagSwitchAddUpdate.UPDATE)
            toast.success('tags modified; dont forget to click \'update\'');
    }

    const onChangeTag = (event) => {
        let { value } = event.target;
        setNewTag(value);
    }

    const onBtnModifyTagsClick = () => {
        setNewTag('');
        setModifiedTags([...pupil.tags]);
        openModalTags();
    }

    const onBtnAddTagClick = () => {
        if (modifiedTags.includes(newTag)) {
            toast.error('tag already exists');
            return;
        }
        if (newTag === "") {
            toast.error('provide a tagname or select an existing tag');
            return;
        }
        modifiedTags.push(newTag);

        setNewTag('');
    }

    const onBtnCloseModalTagsClick = () => {
        closeModalTags();
        setModifiedTags([]);
    }

    const removeTag = (tagname) => {
        let temptags = modifiedTags;
        temptags.splice(modifiedTags.indexOf(tagname), 1);
        setModifiedTags([...temptags]);
    }


    var stringToColour = function (str) {
        var hash = 0;
        for (let i = 0; i < str.length; i++) {
            hash = str.charCodeAt(i) + ((hash << 5) - hash);
        }
        var colour = '#';
        for (let i = 0; i < 3; i++) {
            var value = (hash >> (i * 8)) & 0xFF;
            colour += ('00' + value.toString(16)).substr(-2);
        }
        return colour;
    }

    function invertColor(hex) {
        if (hex.indexOf('#') === 0) {
            hex = hex.slice(1);
        }
        // convert 3-digit hex to 6-digits.
        if (hex.length === 3) {
            hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
        }
        if (hex.length !== 6) {
            throw new Error('Invalid HEX color.');
        }
        // invert color components
        var r = (255 - parseInt(hex.slice(0, 2), 16)).toString(16),
            g = (255 - parseInt(hex.slice(2, 4), 16)).toString(16),
            b = (255 - parseInt(hex.slice(4, 6), 16)).toString(16);
        // pad each with zeros and return
        return '#' + padZero(r) + padZero(g) + padZero(b);
    }

    function padZero(str, len) {
        len = len || 2;
        var zeros = new Array(len).join('0');
        return (zeros + str).slice(-len);
    }


    //#endregion
    //---------HTML----------//
    //#region 
    return (
        <div>
            <h1>Pupils</h1>
            <div className="grid-container">
                <div style={{ margin: '10px' }} className="grid-row-sm">
                    <button type="button" className="importt bttn" onClick={() => history.push('/ImportStudents')}>Import Pupils</button>
                    <label className="header">Pupils</label>
                    <input
                        type='text'
                        autoComplete="off"
                        className="search inputFieldPeriod inputField"
                        //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                        id='inputCtrl_Search'
                        name='label'
                        placeholder='Search...'
                        //value={period.label}
                        onChange={onSearchFieldChange}
                    ></input>
                    <button type="button" className="bttnSearch bttn" disabled >
                        <FontAwesomeIcon icon="search" size="sm" />
                    </button>
                    <div className="block">
                        <ul className="list-group">
                            {
                                pupils.map((item) =>
                                    <li className="list-group-item d-flex justify-content-center align-items-center" style={{ border: '2px solid #023047', color: (idOfCurrentPupil === item._id) ? 'white' : '#023047', backgroundColor: (idOfCurrentPupil === item._id) ? '#023047' : 'white' }} key={item._id} onClick={() => onLiBtnClick(item)}>
                                        {item.lastname.toUpperCase() + ' ' + item.firstname}
                                    </li>

                                )
                            }
                        </ul>
                    </div>

                    <button type="button" className="bttnAdd bttn" onClick={onBtnPupilsAddClick} >Add Pupil</button>
                </div>
                <div style={{ margin: '10px', visibility: (flagSwitchAddUpdate.UPDATE === flagAddUpdate && idOfCurrentPupil === null) ? 'hidden' : 'visible' }} className="grid-row-sm">
                    <label className="header">Pupil-Info</label>
                    <form style={{ width: '100%' }}>
                        <label className="fat" htmlFor={`inputCtrl_Lastname`}>Lastname:</label>
                        <input
                            type='text'
                            autoComplete="off"
                            className="inputFieldPeriod inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            id='inputCtrl_Lastname'
                            name='lastname'
                            placeholder='lastname'
                            value={pupil.lastname}
                            onChange={onChange}
                        >
                        </input>
                        <br />
                        <label className="fat" htmlFor={`inputCtrl_Firstname`}>Firstname:</label>
                        <input
                            type='text'
                            autoComplete="off"
                            className="inputFieldPeriod inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            id='inputCtrl_Firstname'
                            name='firstname'
                            placeholder='firstname'
                            value={pupil.firstname}
                            onChange={onChange}
                        >
                        </input>
                        <br />
                        <label className="fat" htmlFor={`inputCtrl_Email`}>Email:</label>
                        <input
                            type='text'
                            autoComplete="off"
                            className="inputFieldPeriod inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            id='inputCtrl_Email'
                            name='mail'
                            placeholder='mail'
                            value={pupil.mail}
                            onChange={onChange}
                        >
                        </input>
                        <br></br>
                        <label className="fat" htmlFor={`inputCtrl_Status`}>State:</label>
                        <select id={`inputCtrl_Status`} name='state' onChange={onStateSelectChanged} >
                            <option key={'stateactive'} selected={(pupil.state === 'active') ? true : false}>active</option>
                            <option key={'stateinactive'} selected={(pupil.state === 'inactive') ? true : false}>inactive</option>
                        </select>
                        <br />
                        <label className="fat" htmlFor={`inputCtrl_Notes`}>Notes:</label>
                        <input
                            type='text'
                            autoComplete="off"
                            className="inputFieldPeriod inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            id='inputCtrl_Notes'
                            name='notes'
                            placeholder='notes'
                            value={pupil.notes}
                            onChange={onChange}
                        >
                        </input>
                        <br />
                        <button type="button" className="bttnTags bttn" onClick={onBtnModifyTagsClick}>Manage Tags</button>
                        <br></br>
                        <button type="button" className="bttnUpAdd bttn" onClick={onBtnSaveClick}>{(flagAddUpdate === flagSwitchAddUpdate.ADD) ? 'Add' : 'Update'}</button>
                        <button type="button" className="bttnDelete bttn" onClick={openDeleteModal}>Delete</button>
                        <button type="button" className="bttnRecords bttn" onClick={openRecordsOfPupil} style={{ visibility: (flagSwitchAddUpdate.UPDATE === flagAddUpdate && idOfCurrentPupil !== null) ? 'visible' : 'hidden' }}>Records of Pupil</button>

                    </form>
                    <br></br>
                    <br></br>
                    <div style={{ visibility: (flagSwitchAddUpdate.ADD === flagAddUpdate || idOfCurrentPupil === null) ? 'hidden' : 'visible' }}>
                        <label className="header">Courses of selected Pupil:</label>

                        <input
                            type='text'
                            autoComplete="off"
                            className="search inputFieldPeriod inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            id='inputCtrl_Search'
                            name='label'
                            placeholder='Search...'
                            //value={period.label}
                            onChange={onSearchFieldAddedCoursesChange}
                        ></input>
                        <button type="button" className="bttnSearch bttn" disabled >
                            <FontAwesomeIcon icon="search" size="sm" />
                        </button>
                        <div className="block">
                            <ul className="list-group">
                                {
                                    coursesOfCurrPupil.map((item) =>
                                        <li className="list-group-item d-flex justify-content-center align-items-center" style={{ border: '2px solid #023047', color: '#023047', backgroundColor: 'white' }} key={'curr' + item._id} onClick={() => {
                                            setCurrCourse(item);
                                            openRemoveCourseModal();
                                        }}>
                                            {item.label}
                                        </li>
                                    )
                                }
                            </ul>
                        </div>

                    </div>
                </div>
                <div style={{ margin: '10px', visibility: (flagSwitchAddUpdate.ADD === flagAddUpdate || idOfCurrentPupil === null) ? 'hidden' : 'visible' }} className="grid-row-sm">
                    <label className="header">Select Course to add pupil:</label>
                    {/*}
                    <button type="button" className="bttnFilter bttn" onClick={openSortModal}>
                        <FontAwesomeIcon icon="filter" size="sm" />
                    </button>
                */}
                    <input
                        type='text'
                        autoComplete="off"
                        className="search inputFieldPeriod inputField"
                        //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                        id='inputCtrl_Search'
                        name='label'
                        placeholder='Search...'
                        //value={period.label}
                        onChange={onSearchFieldRemainingCoursesChange}
                    ></input>
                    <button type="button" className="bttnSearch bttn" disabled >
                        <FontAwesomeIcon icon="search" size="sm" />
                    </button>
                    <div className="block">
                        <ul className="list-group">
                            {
                                remainingCourses.map((item) =>
                                    <li className="list-group-item d-flex justify-content-center align-items-center" style={{ border: '2px solid #023047', color: '#023047', backgroundColor: 'white' }} key={'remaining' + item._id} onClick={() => onRemainingCourseClick(item)}>
                                        {item.label}
                                    </li>
                                )
                            }
                        </ul>
                    </div>

                </div>
            </div>

            <ModalWindow
                stateOfModal={stateOfModal}
                modalTitle={'Confirm Delete'}
                modalText={'Do you really want to delete pupil \'' + getPupilsName() + '\' ?'}
                btnOkText={'Delete'}
                closeModal={closeModal}
                methodOkConfirmClick={deleteCurrPupil}
            ></ModalWindow>
            <ModalWindow
                stateOfModal={stateOfModalRemoveCourse}
                modalTitle={'Confirm Remove'}
                modalText={'Do you really want to remove pupil from course \'' + getCourseLabel() + '\' ?'}
                btnOkText={'Remove'}
                closeModal={closeRemoveCourseModal}
                methodOkConfirmClick={onAddedCourseClick}
            ></ModalWindow>
            <ModalWindow
                stateOfModal={stateOfModalTags}
                modalTitle={'Manage Tags'}
                modalText={
                    <div>
                        <h6>Create New Tag:</h6>
                        <input style={{ borderRadius: '25px 0px 0px 25px', width: '70%', display: 'inline-block' }}
                            type='text'
                            autoComplete="off"
                            className="inline50 inputFieldPeriod inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            id='inputCtrl_Notes'
                            name='comment'
                            placeholder='comment'
                            //value={newTag}
                            onChange={onChangeTag}
                        >
                        </input>
                        <button type="button" style={{ borderRadius: '0px 25px 25px 0px', border: '2px solid #FB8500', width: '25%', display: 'inline-block' }} className="inline50 bttnUpAdd bttn" onClick={onBtnAddTagClick}>Add</button>

                        <br></br>
                        <br></br>
                        <h6>Added tags:</h6>
                        <ul className="list-group list-group-horizontal">
                            {
                                (modifiedTags) ?
                                    modifiedTags.map((_el) => {
                                        return (
                                            <li style={{ color: invertColor(stringToColour(_el)), backgroundColor: stringToColour(_el) }} className="list-group-item d-flex justify-content-between align-items-center" onClick={() => removeTag(_el)} key={"btn" + _el}>
                                                {_el}
                                            </li>
                                        );
                                    })
                                    : ''
                            }
                        </ul>
                    </div>
                }
                btnOkText={'Apply'}
                closeModal={onBtnCloseModalTagsClick}
                methodOkConfirmClick={closeModalAndApplyChanges}
            ></ModalWindow>
        </div >
    );
    //#endregion
};
export default PupilManagement;