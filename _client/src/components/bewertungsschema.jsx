import { toast } from 'react-toastify';
import React, { useState, useEffect } from 'react';
import { schemeService } from '../services/schemeService.js';
import '../css/Bewertung.css';
import '../App.css';
import { errorHandler, compareRatingSchemes, getAndShowAllMsgs, formOK } from './utils/utils';
import ModalWindow from './utils/modalWindow.jsx';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Bewertungsschema = () => {

    const [schemes, setSchemes] = useState([]);
    const [showRatingSchemes, setShowRatingSchemes] = useState(false);
    const [stateOfModal, setStateOfModal] = useState({
        isOpen: false
    });
    const openModal = () => setStateOfModal({ isOpen: true });
    const closeModal = () => setStateOfModal({ isOpen: false });
    const [stateOfRatingSchemeModal, setStateOfRatingSchemeModal] = useState({
        isOpen: false
    });
    const openRatingSchemeModal = () => setStateOfRatingSchemeModal({ isOpen: true });
    const closeRatingSchemeModal = () => setStateOfRatingSchemeModal({ isOpen: false });
    const [idOfCurrentScheme, setIdOfCurrentScheme] = useState(null);
    const [idOfCurrentRatingScheme, setIdOfCurrentRatingScheme] = useState(null);
    const emptySchemeObj = {
        'name': '',
        'weight': 1,
        'ratings': []
    }
    const emptyRatingSchemeObj = {
        'label': '',
        'symbol': '',
        'value': 0
    }
    const [unfilteredSchemes, setUnfilteredSchemes] = useState([]);
    const [scheme, setScheme] = useState(emptySchemeObj);
    const [ratingScheme, setRatingScheme] = useState(emptyRatingSchemeObj);
    const flagSwitchAddUpdate = {
        ADD: 0,
        UPDATE: 1
    };
    const [flagAddUpdate, setFlagAddUpdate] = useState(flagSwitchAddUpdate.UPDATE);
    const [flagAddUpdateRatingScheme, setFlagAddUpdateRatingScheme] = useState(flagSwitchAddUpdate.ADD);
    const initialFormValidationInfo = {
        'name': {
            valid: false,
            msg: 'No input for name'
        }
    };
    const [formValidationInfo, setFormValidationInfo] = useState(initialFormValidationInfo);
    const initialFormValidationInfoRatingScheme = {
        'label': {
            valid: false,
            msg: 'No input for label'
        },
        'symbol': {
            valid: false,
            msg: 'No input for name'
        }
    };
    const [formValidationInfoRatingScheme, setFormValidationInforatingScheme] = useState(initialFormValidationInfoRatingScheme);

    useEffect(() => {
        init();
    }, []);

    const init = () => {
        schemeService.getAll().then(_schemes => {
            setSchemes(_schemes);
        }).catch((reason) => {
            errorHandler(reason, toast);
        })
    }

    const initRecordScheme = () => {
        setFlagAddUpdateRatingScheme(flagSwitchAddUpdate.ADD);
        setRatingScheme(emptyRatingSchemeObj);
        setIdOfCurrentRatingScheme(null);
    }

    useEffect(() => {
        checkWholeSchemeObj();
    }, [scheme]);

    useEffect(() => {
        checkWholeRatingSchemeObj();
    }, [ratingScheme]);

    const onChange = (event) => {
        let { name, value } = event.target;
        if (name === 'weight') {
            if (value < 0) {
                value = 0;
            } else if (!(/^\d+$/.test(value))) {
                return;
            }
        }
        setScheme({ ...scheme, [name]: value });
    }

    const onRatingSchemeChange = (event) => {
        let { name, value } = event.target;
        if (name === 'value') {
            if (value > 100) {
                value = 100;
            } else if (value < 0) {
                value = 0;
            } else if (!(/^\d+$/.test(value))) {
                return;
            }
        }
        setRatingScheme({ ...ratingScheme, [name]: value });
    }

    function checkInput(name, value) {
        let tempObj = {
            valid: false,
            msg: ''
        }
        // if (!value)
        //   return;
        switch (name) {
            case 'label':

                if (value.length === 0) {
                    tempObj = {
                        valid: false,
                        msg: 'label cannot be empty'
                    };
                } else {
                    tempObj = {
                        valid: true,
                        msg: ''
                    };
                }
                break;
            case 'name':

                if (value.length === 0) {
                    tempObj = {
                        valid: false,
                        msg: 'name cannot be empty'
                    };
                } else {
                    tempObj = {
                        valid: true,
                        msg: ''
                    };
                }
                break;
            case 'symbol':

                if (value.length === 0) {
                    tempObj = {
                        valid: false,
                        msg: 'symbol cannot be empty'
                    };
                } else {
                    tempObj = {
                        valid: true,
                        msg: ''
                    };
                }
                break;
            default:
                break;
        }
        return tempObj;
    }

    function checkWholeRatingSchemeObj() {
        let newFormValidationInfo = {};
        Object.keys(formValidationInfoRatingScheme).forEach((element) => {
            newFormValidationInfo[element] = checkInput(element, ratingScheme[element]);
        })
        setFormValidationInforatingScheme({ ...newFormValidationInfo });
    }

    function checkWholeSchemeObj() {
        let newFormValidationInfo = {};
        Object.keys(formValidationInfo).forEach((element) => {
            newFormValidationInfo[element] = checkInput(element, scheme[element]);
        })
        setFormValidationInfo({ ...newFormValidationInfo });
    }

    const onBtnSaveClick = () => {
        if (!formOK(formValidationInfo)) {
            getAndShowAllMsgs(formValidationInfo, toast);
            return;
        }

        switch (flagAddUpdate) {
            case flagSwitchAddUpdate.ADD:
                schemeService.create(scheme).then((_el) => {
                    init();
                    setIdOfCurrentScheme(_el._id);
                    setFlagAddUpdate(flagSwitchAddUpdate.UPDATE);
                    toast.success('scheme created');
                }).catch((reason) => {
                    errorHandler(reason, toast);
                })
                break;
            case flagSwitchAddUpdate.UPDATE:
                if (!idOfCurrentScheme) {
                    toast.error('error: probably no scheme selected to delete');
                    break;
                }
                schemeService.update(idOfCurrentScheme, scheme).then(() => {
                    init();
                    toast.success('scheme updated');
                }).catch((reason) => {
                    errorHandler(reason, toast);
                })
                break;
            default:
                break;
        }
    }

    const onBtnSaveRatingSchemeClick = () => {
        if (!formOK(formValidationInfoRatingScheme)) {
            getAndShowAllMsgs(formValidationInfoRatingScheme, toast);
            return;
        }
        if (!idOfCurrentScheme) {
            toast.error('error: probably no scheme selected to edit');
            return;
        }
        if (scheme.ratings.some((item) => item._id === idOfCurrentRatingScheme)) {
            scheme.ratings.splice(scheme.ratings.findIndex(element => element._id === idOfCurrentRatingScheme), 1);
        }
        scheme.ratings.push(ratingScheme)

        schemeService.update(idOfCurrentScheme, scheme).then(() => {
            initRecordScheme();
            toast.success('ratings of scheme updated');
        }).catch((reason) => {
            errorHandler(reason, toast);
        })
    }

    const onLiBtnClick = (_scheme) => {
        setFlagAddUpdate(flagSwitchAddUpdate.UPDATE);
        setIdOfCurrentScheme(_scheme._id);
        setScheme(_scheme);
    }

    const onRatingSchemeClick = (_rs) => {
        setFlagAddUpdateRatingScheme(flagSwitchAddUpdate.UPDATE);
        setIdOfCurrentRatingScheme(_rs._id);
        setRatingScheme(_rs);
    }

    const deleteCurrPupil = () => {
        closeModal();

        schemeService.delete(idOfCurrentScheme).then(() => {
            setIdOfCurrentScheme(null);
            init();
            toast.success('scheme deleted');
        }).catch((reason) => {
            errorHandler(reason, toast);
        })
    }

    const deleteCurrRatingScheme = () => {
        closeRatingSchemeModal();
        if (!idOfCurrentScheme) {
            toast.error('error: probably no scheme selected to delete');
            return;
        }

        scheme.ratings.splice(scheme.ratings.findIndex(element => element._id === idOfCurrentRatingScheme), 1);

        schemeService.update(idOfCurrentScheme, scheme).then(() => {
            init();
            toast.success('ratings of scheme updated');
        }).catch((reason) => {
            errorHandler(reason, toast);
        })
    }

    const onSearchFieldChange = (event) => {
        const { value } = event.target;
        let tempSchemes = unfilteredSchemes;
        if (unfilteredSchemes.length < schemes.length) {
            setUnfilteredSchemes(schemes);
            tempSchemes = schemes;
        }
        setSchemes(tempSchemes.filter((element) => {
            return element.name.toUpperCase().includes(value.toUpperCase());
        }).map((element) => {
            return element;
        }));
    }

    const openDeleteModal = () => {
        try {
            if (idOfCurrentScheme === null) {
                toast.error('no pupil selected')
            } else {
                openModal();
            }
        } catch (err) {
            errorHandler(err, toast);
        }
    }

    const getSchemeBezeichnung = () => {
        if (scheme.name)
            return scheme.name;
        return ' ';
    }

    const getRatingSchemeBezeichnung = () => {
        if (ratingScheme.label)
            return ratingScheme.label;
        return ' ';
    }

    const onBtnSchemeAddClick = () => {
        setFlagAddUpdate(flagSwitchAddUpdate.ADD);
        setScheme(emptySchemeObj);
        setIdOfCurrentScheme(null);
    }

    const openDeleteRatingSchemeModal = (elem) => {
        setIdOfCurrentRatingScheme(elem);
        openRatingSchemeModal();
    }

    const onBtnRatingSchemesClick = () => {
        if (idOfCurrentScheme !== null && idOfCurrentScheme !== "null" && flagAddUpdate === flagSwitchAddUpdate.UPDATE) {
            setShowRatingSchemes(true);
        } else {
            setShowRatingSchemes(false);
            toast.error('create scheme before adding ratingschemes')
        }
    }

    const onBtnClearFormClick = () => {
        setRatingScheme(emptyRatingSchemeObj);
        setFlagAddUpdateRatingScheme(flagSwitchAddUpdate.ADD);
        setIdOfCurrentRatingScheme(null);
    }

    return (
        <div>
            <h1>Ratingscheme</h1>
            <div className="grid-container">
                <div style={{ margin: '5px' }} className="grid-row-sm">
                    <label className="half header">Schemes</label>
                    <button type="button" className="bttnFilter bttn" >
                        <FontAwesomeIcon icon="filter" size="sm" />
                    </button>
                    <input
                        type='text'
                        autoComplete="off"
                        className="search inputFieldPeriod inputField"
                        //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                        id='inputCtrl_Search'
                        name='label'
                        placeholder='Search...'
                        //value={period.label}
                        onChange={onSearchFieldChange}
                    ></input>
                    <button type="button" className="bttnSearch bttn" disabled >
                        <FontAwesomeIcon icon="search" size="sm" />
                    </button>
                    <div className="block">
                        <ul className="list-group">
                            {
                                schemes.map((item) =>
                                    <li className="list-group-item d-flex justify-content-center align-items-center" style={{ border: '2px solid #023047', color: (idOfCurrentScheme === item._id) ? 'white' : '#023047', backgroundColor: (idOfCurrentScheme === item._id) ? '#023047' : 'white' }} key={item._id} onClick={() => onLiBtnClick(item)}>
                                        {item.name}
                                    </li>

                                )
                            }
                        </ul>
                    </div>
                    <button type="button" className="bttnAdd bttn" onClick={onBtnSchemeAddClick} >Add Scheme</button>
                </div>
                <div style={{ margin: '5px', visibility: (flagSwitchAddUpdate.UPDATE === flagAddUpdate && idOfCurrentScheme === null) ? 'hidden' : 'visible' }} className="grid-row-sm">
                    <label className="header">Scheme-Info</label>
                    <form style={{ width: '100%' }}>
                        <label className="fat" htmlFor={`inputCtrl_Lastname`}>Label:</label>
                        <input
                            type='text'
                            autoComplete="off"
                            className="inputFieldPeriod inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            id='inputCtrl_Lastname'
                            name='name'
                            placeholder='name'
                            value={scheme.name}
                            onChange={onChange}
                        >
                        </input><br />
                        <label className="fat" htmlFor={`inputCtrl_Lastname`}>Weight:</label>
                        <input
                            type='number'
                            autoComplete="off"
                            className="inputFieldPeriod inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            id='inputCtrl_Lastname'
                            name='weight'
                            min='1'
                            placeholder='weight'
                            value={scheme.weight}
                            onChange={onChange}
                        >
                        </input>
                        <br />
                        <button type="button" className="bttnUpAdd bttn" onClick={onBtnSaveClick}>{(flagAddUpdate === flagSwitchAddUpdate.ADD) ? 'Add' : 'Update'}</button>
                        <button type="button" className="bttnDelete bttn" onClick={openDeleteModal}>Delete</button>
                        <button type="button" className="bttnRecords bttn" style={{ visibility: (idOfCurrentScheme === null) ? 'hidden' : 'visible' }} onClick={onBtnRatingSchemesClick}>Ratings</button>

                    </form>
                </div>
                <div style={{ margin: '5px', visibility: (showRatingSchemes && idOfCurrentScheme) ? 'visible' : 'hidden' }} className="grid-row-sm">
                    <label className="header">Ratings</label>
                    <form style={{ width: '100%' }}>
                        <label className="fat" htmlFor={`inputCtrl_Lastname`}>Label:</label>
                        <input
                            type='text'
                            autoComplete="off"
                            className="inputFieldPeriod inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            id='inputCtrl_Lastname'
                            name='label'
                            placeholder='label'
                            value={ratingScheme.label}
                            onChange={onRatingSchemeChange}
                        >
                        </input>
                        <label className="fat" htmlFor={`inputCtrl_Lastname`}>Symbol:</label>
                        <input
                            type='text'
                            autoComplete="off"
                            className="inputFieldPeriod inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            id='inputCtrl_Lastname'
                            name='symbol'
                            placeholder='symbol'
                            value={ratingScheme.symbol}
                            onChange={onRatingSchemeChange}
                        >
                        </input>
                        <label className="fat" htmlFor={`inputCtrl_Lastname`}>Value:</label>
                        <input
                            type='number'
                            autoComplete="off"
                            className="inputFieldPeriod inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            id='inputCtrl_Lastname'
                            name='value'
                            min='1'
                            max='100'
                            placeholder='value'
                            value={ratingScheme.value}
                            onChange={onRatingSchemeChange}
                        >
                        </input>
                        <br />

                        <button type="button" className="bttnUpAdd bttn" onClick={onBtnSaveRatingSchemeClick}>{(flagAddUpdateRatingScheme === flagSwitchAddUpdate.ADD) ? 'Add' : 'Update'}</button>
                        <button type="button" style={{ backgroundColor: 'transparent', border: '2px solid red', color: 'red' }} className="bttnDelete bttn" onClick={onBtnClearFormClick}>Clear Form To Add New</button>

                    </form>
                    <br />
                    <label className="fat" >
                        Elements:
                    </label>
                    {
                        <div className="block">
                            <ul className="myUl">
                                {
                                    scheme.ratings.sort(compareRatingSchemes).map((elem) => {
                                        return (
                                            <li style={{ backgroundColor: 'transparent' }} key={elem._id} >
                                                <div>
                                                    <input
                                                        onClick={() => onRatingSchemeClick(elem)}
                                                        type='button'
                                                        style={{ color: '#023047', height: '40px', borderRadius: '20px 0px 0px 20px', border: '3px solid #023047', backgroundColor: (idOfCurrentRatingScheme === elem._id) ? '#FB8500' : 'transparent', width: '80%' }}
                                                        value={elem.label + " \t Symbol: " + elem.symbol + "\t Value: " + elem.value}
                                                    >
                                                    </input>
                                                    <button type="button" key={"btn" + elem._id} className="bttnUpAdd bttn" style={{ width: '10%', height: '40px', marginLeft: '0px', borderRadius: '0px 20px 20px 0px', backgroundColor: (idOfCurrentRatingScheme === elem._id) ? '#FB8500' : '#023047' }} onClick={() => openDeleteRatingSchemeModal(elem)}>
                                                        <FontAwesomeIcon icon="trash" size="sm" />
                                                    </button>
                                                </div>
                                            </li>
                                        );
                                    })
                                }
                            </ul>
                        </div>
                    }
                </div>
            </div>

            <ModalWindow
                stateOfModal={stateOfModal}
                modalTitle={'Confirm Delete'}
                modalText={'Do you really want to delete scheme \'' + getSchemeBezeichnung() + '\' ?'}
                btnOkText={'Delete'}
                closeModal={closeModal}
                methodOkConfirmClick={deleteCurrPupil}
            ></ModalWindow>
            <ModalWindow
                stateOfModal={stateOfRatingSchemeModal}
                modalTitle={'Confirm Delete'}
                modalText={'Do you really want to delete ratingscheme \'' + getRatingSchemeBezeichnung() + '\' ?'}
                btnOkText={'Delete'}
                closeModal={closeRatingSchemeModal}
                methodOkConfirmClick={deleteCurrRatingScheme}
            ></ModalWindow>
        </div >
    );
};

export default Bewertungsschema;