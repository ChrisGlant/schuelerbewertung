import React, { useState, useEffect } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { pupilService } from '../services/pupilService.js';
import ModalWindow from './utils/modalWindow.jsx';
import { errorHandler, compareRatingSchemes, parseDateIntoValidFormat } from './utils/utils';
import { recordService } from '../services/recordService.js';
import { toast } from 'react-toastify';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { cprService } from '../services/cprService.js';
import { schemeService } from '../services/schemeService.js';
import '../css/Bulk.css';
import '../App.css';

const BulkAddRecords = () => {
    const history = useHistory();
    let { courseId } = useParams();
    const [stateOfModalTags, setStateOfModalTags] = useState({
        isOpen: false
    });
    const openModalTags = () => setStateOfModalTags({ isOpen: true });
    const closeModalTags = () => setStateOfModalTags({ isOpen: false });
    const [stateOfModalTagsGeneral, setStateOfModalTagsGeneral] = useState({
        isOpen: false
    });
    const openModalTagsGeneral = () => setStateOfModalTagsGeneral({ isOpen: true });
    const closeModalTagsGeneral = () => setStateOfModalTagsGeneral({ isOpen: false });
    const [tagsOfCourse, setTagsOfCourse] = useState([]);
    const emptyGeneralInfoObj = {
        "courseId": null,
        "validOn": null,
        "weight": 0,
    }
    const [generalInfo, setGeneralInfo] = useState(emptyGeneralInfoObj);
    const [idOfSelectedScheme, setIdOfSelectedScheme] = useState(null);
    const [modifiedTags, setModifiedTags] = useState([]);
    const [newTag, setNewTag] = useState('');
    const [errObjs, setErrObjs] = useState([]);
    const [schemes, setSchemes] = useState([]);
    const [pupilObjs, setPupilObjs] = useState([]);
    const [pupilObjToModify, setPupilObjToModify] = useState(null);
    const [pupils/*, setPupils*/] = useState([]);
    const [signs, setSigns] = useState([]);
    const [stateOfModal, setStateOfModal] = useState({
        isOpen: false
    });
    const openModal = () => setStateOfModal({ isOpen: true });
    const closeModal = () => setStateOfModal({ isOpen: false });

    useEffect(() => {
        init();
    }, [])

    const init = () => {
        //Pupils als Object (->object.js) setzen
        if (courseId) {
            setGeneralInfo({ ...generalInfo, 'courseId': courseId, 'validOn': parseDateIntoValidFormat(new Date(new Date(Date.now()).toISOString())) });
            cprService.getPupilsById(courseId).then((_cprstuff) => {
                for (let _obj of _cprstuff) {
                    pupilService.getById(_obj.pupilId).then((_pupil) => {
                        pupils.push(_pupil);
                        let _arr = pupilObjs;
                        _arr.push({ 'pupilId': _pupil._id, 'ratingId': null, 'pupilNotPresent': false, 'comment': '' });
                        setPupilObjs([..._arr]);
                    }).catch((reason) => {
                        errorHandler(reason, toast);
                    })
                }
            }).catch((reason) => {
                errorHandler(reason, toast);
            })
        }
        schemeService.getAll().then((_schemes) => {
            setSchemes(_schemes);
        }).catch((reason) => {
            errorHandler(reason, toast);
        })
        setAllTagsOfCourse();
    }

    useEffect(() => {
        //setSignsOfScheme();
        let scheme = schemes.find((el) => el._id === idOfSelectedScheme);
        if (scheme) {
            setSigns([...scheme.ratings].sort(compareRatingSchemes));
            setGeneralInfo({ ...generalInfo, 'weight': scheme.weight });
        }
    }, [idOfSelectedScheme])

    const onChange = (event) => {
        let { name, value } = event.target;

        if (name === 'weight') {
            if (value > 100) {
                value = 100;
            } else if (value < 0) {
                value = 0;
            } else if (!(/^\d+$/.test(value))) {
                return;
            }
            setGeneralInfo({ ...generalInfo, 'weight': parseInt(value) })
        } else {
            setGeneralInfo({ ...generalInfo, [name]: value });
        }

    }

    const onSingleRecordChange = (event, _po) => {
        let { name, value } = event.target;
        let tempPupils = pupilObjs;
        if (name === 'pupilNotPresent') {
            let { checked } = event.target;
            tempPupils.find((_el) => _el.pupilId === _po.pupilId).pupilNotPresent = checked;
        } else {
            tempPupils.find((_el) => _el.pupilId === _po.pupilId)[name] = value;
        }
        setPupilObjs([...tempPupils]);
    }

    const onBtnConfirmClick = () => {
        let arrOfErrObjs = pupilObjs.filter((_el) => !_el.pupilNotPresent && !_el.ratingId)
        setErrObjs(arrOfErrObjs);
        if (arrOfErrObjs.length > 0) {
            toast.error('select symbols or mark student as \'not present\'');
            return;

        }

        let payload = { ...generalInfo, 'validOn': new Date(generalInfo.validOn).toISOString(), 'records': pupilObjs }
        recordService.bulkAdd(payload).then((_res) => {
            toast.success('records created');
            //init();
            history.push('/records');
        }).catch((reason) => {
            errorHandler(reason, toast);
        })
    }

    function onSignSelectChanged(event, _po) {
        const { value } = event.target;
        let rating = signs.find((el) => el._id === value);
        let tempPupils = pupilObjs;
        let _pupil = tempPupils.find((_el) => _el.pupilId === _po.pupilId);
        _pupil.ratingId = rating._id;
        //_pupil.rating.value = rating.value;
        setPupilObjs([...tempPupils]);
    }


    var stringToColour = function (str) {
        var hash = 0;
        for (let i = 0; i < str.length; i++) {
            hash = str.charCodeAt(i) + ((hash << 5) - hash);
        }
        var colour = '#';
        for (let i = 0; i < 3; i++) {
            var value = (hash >> (i * 8)) & 0xFF;
            colour += ('00' + value.toString(16)).substr(-2);
        }
        return colour;
    }

    function invertColor(hex) {
        if (hex.indexOf('#') === 0) {
            hex = hex.slice(1);
        }
        // convert 3-digit hex to 6-digits.
        if (hex.length === 3) {
            hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
        }
        if (hex.length !== 6) {
            throw new Error('Invalid HEX color.');
        }
        // invert color components
        var r = (255 - parseInt(hex.slice(0, 2), 16)).toString(16),
            g = (255 - parseInt(hex.slice(2, 4), 16)).toString(16),
            b = (255 - parseInt(hex.slice(4, 6), 16)).toString(16);
        // pad each with zeros and return
        return '#' + padZero(r) + padZero(g) + padZero(b);
    }

    function padZero(str, len) {
        len = len || 2;
        var zeros = new Array(len).join('0');
        return (zeros + str).slice(-len);
    }

    const closeModalAndApplyChanges = () => {
        closeModalTags();
        setTagsOfCourse([...new Set([...tagsOfCourse, ...modifiedTags])]);
        let temppo = pupilObjs;
        temppo.find((_el) => _el.pupilId === pupilObjToModify.pupilId).tags = modifiedTags;
        setPupilObjs([...temppo]);
    }

    const onChangeTag = (event) => {
        let { value } = event.target;
        setNewTag(value);
    }

    const onBtnModifyTagsClick = (_po) => {
        setNewTag('');
        setPupilObjToModify(_po);
        let _tags = pupilObjs.find((_el) => _el.pupilId === _po.pupilId).tags;
        if (_tags) {
            setModifiedTags([..._tags]);
        } else {
            setModifiedTags([]);
        }
        openModalTags();
    }

    const onBtnAddTagClick = () => {
        if (modifiedTags.includes(newTag)) {
            toast.error('tag already exists');
            return;
        }
        if (newTag === "") {
            toast.error('provide a tagname or select an existing tag');
            return;
        }
        //modifiedTags.push(newTag);
        setModifiedTags([...modifiedTags, ...[newTag]]);
        setNewTag('');
    }

    const onBtnCloseModalTagsClick = () => {
        closeModalTags();
        setModifiedTags([]);
    }

    const removeTag = (tagname) => {
        let temptags = modifiedTags;
        temptags.splice(modifiedTags.indexOf(tagname), 1);
        setModifiedTags([...temptags]);
    }

    const closeModalAndApplyChangesGeneral = () => {
        closeModalTagsGeneral();
        setTagsOfCourse([...new Set([...tagsOfCourse, ...modifiedTags])]);
        setGeneralInfo({ ...generalInfo, tags: [...modifiedTags] });
    }

    const onBtnModifyTagsClickGeneral = () => {
        setNewTag('');

        setModifiedTags([...(generalInfo.tags) ? generalInfo.tags : []]);
        openModalTagsGeneral();
    }

    const onBtnCloseModalTagsClickGeneral = () => {
        closeModalTagsGeneral();
        setModifiedTags([]);
    }

    function onTagSelectChanged(event) {
        const { value } = event.target;
        let temptags = modifiedTags;
        if (temptags.includes(value)) {
            toast.error('tag already exists');
            return;
        }
        if (value === "") {
            toast.error('provide a tagname or select an existing tag');
            return;
        }
        temptags.push(value);
        setModifiedTags([...temptags]);

        setNewTag('');
    }

    const setAllTagsOfCourse = () => {
        if (courseId)
            recordService.getAllOfCourse(courseId).then((_recsOfC) => {
                let mappedArr = _recsOfC.map((_el) => _el.tags);
                let temparr = [...tagsOfCourse];
                for (let i of mappedArr) {
                    temparr = temparr.concat(i);
                }
                setTagsOfCourse([...new Set(temparr)]);
            }).catch((reason) => {
                errorHandler(reason, toast);
            })
    }
    let i = 0;


    function onSchemeSelectChanged(event) {
        const { value } = event.target; //id
        setIdOfSelectedScheme(value);
    }


    return (
        <div>
            <h1>Bulk Add Records</h1>
            <form style={{ width: '95%', margin: '30px' }}>
                <label className="fat" htmlFor={`inputCtrl_date`}>Date:</label>
                <input
                    type='datetime-local'
                    autoComplete="off"
                    className="inputFieldPeriod inputField"
                    //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                    id='inputCtrl_date'
                    name='validOn'
                    placeholder='validOn'
                    value={generalInfo.validOn}
                    onChange={onChange}
                >
                </input>
                <br />
                <label className="fat" htmlFor={`inputCtrl_Status`}>Select Scheme:</label>
                <select id={`inputCtrl_Status`} name='schemaId' onChange={(event) => onSchemeSelectChanged(event)} >
                    <option value="88" disabled selected={(idOfSelectedScheme === null || idOfSelectedScheme === undefined || idOfSelectedScheme === 'null') ? true : false}>Select your scheme</option>
                    {
                        schemes.map((element) => {
                            return (
                                <option key={element._id} value={element._id} selected={(idOfSelectedScheme === element._id) ? true : false}>{element.name}</option>
                            );
                        })
                    }
                </select>
                <br></br>
                <label className="fat" htmlFor="weight">Weight:</label>
                <input type="number" id="weight" name="weight" className="inputFieldPeriod inputField"
                    min="1" value={generalInfo.weight} onChange={onChange}></input>
                <br></br>
                <button type="button" className="bttnTags bttn" onClick={onBtnModifyTagsClickGeneral}>Manage Tags</button>
            </form>
            <div className="block" style={{ marginRight: '20px', height: '500px' }}>
                <ul className="list-group">
                    {
                        pupilObjs.map((_po) => {
                            return (
                                <li className="list-group-item d-flex justify-content-between align-items-center" key={'list' + i++} style={{ marginLeft: '20px', marginBottom: '3px', border: "2px solid #023047", height: '50px', backgroundColor: 'white', color: '#023047' }}>
                                    <h6 className="fatty" style={{ minWidth: '15%', textAlign: 'left' }}>{pupils.find((_pupil) => _pupil._id === _po.pupilId).lastname + ' ' + pupils.find((_pupil) => _pupil._id === _po.pupilId).firstname}</h6>
                                    <div>
                                        <label htmlFor={`inputCtrl_Sign`} style={{ marginRight: '4px' }}>Symbol:</label>
                                        <select id={`inputCtrl_Sign`} name='sign' onChange={(event) => onSignSelectChanged(event, _po)} >
                                            <option value="" disabled selected={(_po.ratingId === null || _po.ratingId === "null") ? true : false}>Select your symbol</option>
                                            {
                                                signs.map((element) => {
                                                    return (
                                                        <option key={element._id} value={element._id} selected={(_po.ratingId !== null && _po.ratingId === element._id) ? true : false}>{element.label} {element.symbol} {element.value}</option>
                                                    );
                                                })
                                            }
                                        </select>
                                    </div>
                                    {/* <div>
                                        <label className="fat" htmlFor="value">Value</label>
                                        <input type="number" id="value" name="value" className="inputFieldPeriod inputField"
                                            min="1" value={(_po.rating) ? _po.rating.value : 1} onChange={(event) => onValueChange(event, _po)}></input>

                                    </div> */}
                                    <div
                                        style={{ marginTop: '10px' }}>
                                        <label htmlFor="pupilNotPresent">Pupil not Present</label>
                                        <input type="checkbox" id="pupilNotPresent" name="pupilNotPresent" style={{ marginLeft: '10px' }}
                                            checked={_po.pupilNotPresent} onChange={(event) => onSingleRecordChange(event, _po)}></input>
                                    </div>
                                    <div>
                                        <label htmlFor={`inputCtrl_Notes`} style={{ marginRight: '4px' }}>Comment: </label>
                                        <input
                                            type='text'
                                            autoComplete="off"
                                            className="inputFieldPeriod inputField"
                                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                                            id='inputCtrl_Notes'
                                            name='comment'
                                            style={{ marginTop: '5px' }}
                                            placeholder='comment'
                                            value={_po.comment}
                                            onChange={(event) => onSingleRecordChange(event, _po)}
                                        >
                                        </input>
                                    </div>
                                    <button type="button" className="smallT bttnTags bttn" onClick={() => onBtnModifyTagsClick(_po)}>Manage Tags</button>
                                    <FontAwesomeIcon style={{ visibility: (errObjs.some((_el) => _el.pupilId === _po.pupilId)) ? 'visible' : 'hidden' }} icon="exclamation-triangle" size="lg" color={"red"} />
                                </li>
                            );
                        })
                    }
                </ul>
            </div>
            <div style={{ margin: '20px' }} className="row">
                <div className="col">
                    <button type="button" className="bttnDelete bttn" style={{ width: '100%' }} onClick={openModal}>Cancel</button>
                </div>
                <div className="col">
                    <button style={{ margin: '0px', width: '100%' }} type="button" className="bttnUpAdd bttn" onClick={onBtnConfirmClick}>Confirm</button>
                </div>
            </div>
            <ModalWindow
                stateOfModal={stateOfModal}
                modalTitle={'Abort Bulk Add Records'}
                modalText={'Do you really want to abort this action?'}
                btnOkText={'OK'}
                closeModal={closeModal}
                methodOkConfirmClick={() => { closeModal(); history.push('/records'); }}
            ></ModalWindow>
            <ModalWindow
                stateOfModal={stateOfModalTags}
                modalTitle={'Manage Tags'}
                modalText={
                    <div>
                        <h6>Create New Tag:</h6>
                        <input style={{ borderRadius: '25px 0px 0px 25px', width: '70%', display: 'inline-block' }}
                            type='text'
                            autoComplete="off"
                            className="inline50 inputFieldPeriod inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            id='inputCtrl_Notes'
                            name='comment'
                            placeholder='comment'
                            //value={newTag}
                            onChange={onChangeTag}
                        >
                        </input>
                        <button type="button" style={{ borderRadius: '0px 25px 25px 0px', border: '2px solid #FB8500', width: '25%', display: 'inline-block' }} className="inline50 bttnUpAdd bttn" onClick={onBtnAddTagClick}>Add</button>

                        <br></br>
                        <br></br>
                        <h6>or Select Existing Tags:</h6>
                        <select onChange={onTagSelectChanged}>
                            <option value="" disabled selected={(newTag === "") ? true : false}>Select a tag</option>
                            {
                                tagsOfCourse.map((_tag) => {
                                    return (
                                        <option value={_tag}>
                                            {_tag}
                                        </option>
                                    );
                                })
                            }
                        </select>
                        <br></br>
                        <br></br>
                        <h6>Added tags:</h6>
                        <ul className="list-group list-group-horizontal">
                            {
                                (modifiedTags) ?
                                    modifiedTags.map((_el) => {
                                        return (
                                            <li style={{ color: invertColor(stringToColour(_el)), backgroundColor: stringToColour(_el) }} className="list-group-item d-flex justify-content-between align-items-center" onClick={() => removeTag(_el)} key={"btn" + _el}>
                                                {_el}
                                            </li>
                                        );
                                    })
                                    : ''
                            }
                        </ul>
                    </div>
                }
                btnOkText={'Apply'}
                closeModal={onBtnCloseModalTagsClick}
                methodOkConfirmClick={closeModalAndApplyChanges}
            ></ModalWindow>
            <ModalWindow
                stateOfModal={stateOfModalTagsGeneral}
                modalTitle={'Manage Tags'}
                modalText={
                    <div>
                        <h6>Create New Tag:</h6>
                        <input style={{ borderRadius: '25px 0px 0px 25px', width: '70%', display: 'inline-block' }}
                            type='text'
                            autoComplete="off"
                            className="inline50 inputFieldPeriod inputField"
                            //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                            id='inputCtrl_Notes'
                            name='comment'
                            placeholder='comment'
                            //value={newTag}
                            onChange={onChangeTag}
                        >
                        </input>
                        <button type="button" style={{ borderRadius: '0px 25px 25px 0px', border: '2px solid #FB8500', width: '25%', display: 'inline-block' }} className="inline50 bttnUpAdd bttn" onClick={onBtnAddTagClick}>Add</button>

                        <br></br>
                        <br></br>
                        <h6>or Select Existing Tags:</h6>
                        <select onChange={onTagSelectChanged}>
                            <option value="" disabled selected={(newTag === "") ? true : false}>Select a tag</option>
                            {
                                tagsOfCourse.map((_tag) => {
                                    return (
                                        <option value={_tag}>
                                            {_tag}
                                        </option>
                                    );
                                })
                            }
                        </select>
                        <br></br>
                        <br></br>
                        <h6>Added tags:</h6>
                        <ul className="list-group list-group-horizontal">
                            {
                                (modifiedTags) ?
                                    modifiedTags.map((_el) => {
                                        return (
                                            <li style={{ color: invertColor(stringToColour(_el)), backgroundColor: stringToColour(_el) }} className="list-group-item d-flex justify-content-between align-items-center" onClick={() => removeTag(_el)} key={"btn" + _el}>
                                                {_el}
                                            </li>
                                        );
                                    })
                                    : ''
                            }
                        </ul>
                    </div>
                }
                btnOkText={'Apply'}
                closeModal={onBtnCloseModalTagsClickGeneral}
                methodOkConfirmClick={closeModalAndApplyChangesGeneral}
            ></ModalWindow>
        </div>
    );
};

export default BulkAddRecords;