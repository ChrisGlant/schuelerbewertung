import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { pupilService } from '../services/pupilService.js';
import { errorHandler, getDateTimeString, parseDateIntoValidFormat } from './utils/utils';
import { recordService } from '../services/recordService.js';
import { toast } from 'react-toastify';
import { cprService } from '../services/cprService.js';
import { schemeService } from '../services/schemeService.js';
import { courseService } from '../services/courseService.js';
import { BsChevronDown } from "react-icons/bs"; //react-icon
import ModalWindow from './utils/modalWindow.jsx';
import Collapsible from 'react-collapsible';
import { Line } from 'react-chartjs-2';
import '../css/Bewertung.css';
import '../App.css';
import { Chart as ChartJS, CategoryScale, LinearScale, PointElement, LineElement, Title, Tooltip, Legend, } from 'chart.js';

ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
);

const options = {
    plugins: {
        title: {
            display: true,
            text: 'Grade-Graph'
        }
    },
    responsive: true,
    maintainAspectRatio: false,
    scales: {
        y: {
            max: 100,
            min: 0
        }
    }
}

const RecordsOfPupil = () => {
    //console.log(matchUrl);
    const [stateOfModal, setStateOfModal] = useState({
        isOpen: false
    });
    const openModal = () => setStateOfModal({ isOpen: true });
    const closeModal = () => setStateOfModal({ isOpen: false });
    let { pupilId } = useParams();
    const [pupil, setPupil] = useState({});
    const [records, setRecords] = useState([]);
    const [courseSchemeObjects, setCourseSchemeObjs] = useState([]);//[{course:{},schemes:[{}]}]
    const [gradeSuccs, setGradeSuccs] = useState([]);
    const [cprs, setCPRs] = useState([]);
    const [recordToShow, setRecordToShow] = useState({ rating: {} });
    const [data, setData] = useState({});
    const [labeling, setLabeling] = useState({});

    useEffect(() => {
        pupilService.getById(pupilId).then((_pupil) => {
            setPupil(_pupil);
        }).catch((reason) => {
            errorHandler(reason, toast);
        })
        recordService.getGradeSuccsOfPupil(pupilId).then((_gso) => { /*[{courseId, pupilId,grade}]*/
            setGradeSuccs(_gso);
        }).catch((reason) => {
            errorHandler(reason, toast);
        })
        // let _csos = courseSchemeObjects;
        let _csos = [];
        schemeService.getAll().then((_schemes) => {
            cprService.getCoursesById(pupilId).then((_cprs) => {
                //alle cprs von einem pupil = alle courses mit allen records
                setCPRs(_cprs);

                for (let _cpr in _cprs) {
                    //einzelne cprs = einzelne courses
                    _cpr = _cprs[_cpr];
                    courseService.getById(_cpr.courseId).then((_course) => {
                        let temparr = [];
                        for (let rid in _cpr.records) {
                            rid = _cpr.records[rid];

                            recordService.getById(rid).then((_record) => {
                                //record mit r.rating.symbol
                                if (_record.rating === null)
                                    return null;
                                let _recs = records;
                                _recs.push(_record);
                                setRecords([..._recs]);
                                let schemeOfRecord = _schemes.find((_el) => _el.ratings.some((_r) => _r._id === _record.rating._id));
                                if (temparr.some((_el) => _el.scheme._id === schemeOfRecord._id)) { //wenn schema bereits in arr
                                    if (temparr.find((_el) => _el.scheme._id === schemeOfRecord._id).ratings.some((_el) => _el.rating._id === _record.rating._id)) {//is rating in array?
                                        temparr.find((_el) => _el.scheme._id === schemeOfRecord._id).ratings.find((_el) => _el.rating._id === _record.rating._id).count++;
                                        return false;
                                    } else {
                                        temparr.find((_el) => _el.scheme._id === schemeOfRecord._id).ratings.push({ 'rating': _record.rating, 'count': 1 });
                                        return true;
                                    }
                                } else {
                                    temparr.push({ 'scheme': schemeOfRecord, 'ratings': [{ 'rating': _record.rating, 'count': 1 }] })
                                    return true;
                                }
                            }).then((update) => {
                                if (update !== null) {
                                    // let _csos = courseSchemeObjects;
                                    if (!_csos.some((_el) => _el.course._id === _course._id)) {
                                        _csos.push({ 'course': _course, 'schemes': temparr/*array of objects ([{scheme, [{symb,numb}]}])*/ });
                                        //setCourseSchemeObjects([..._csos]);
                                    } else {
                                        _csos.find((_el) => _el.course._id === _course._id).schemes = temparr;
                                        //setCourseSchemeObjects([..._csos]);
                                    }
                                    setCourseSchemeObjs([..._csos]);
                                }
                            }).catch((reason) => {
                                errorHandler(reason, toast);
                            })
                        }
                    }).catch((reason) => {
                        errorHandler(reason, toast);
                    })

                }
            }).catch((reason) => {
                errorHandler(reason, toast);
            })
        }).catch((reason) => {
            errorHandler(reason, toast);
        })
    }, []);

    useEffect(() => {
        courseSchemeObjects.forEach(_cso => {
            recordService.getGradeSuccHistoryOfPupilInClass(_cso.course._id, pupil._id).then((grades) => { /*[{courseId, pupilId,grade}]*/
                data[_cso.course._id] = []
                labeling[_cso.course._id] = []

                grades[0].grade.forEach(grd => {
                    data[_cso.course._id].push(grd.grade);
                    let date = new Date(grd.date.toString());
                    date = date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear();
                    labeling[_cso.course._id].push(date);
                })

                setData({ ...data });
                setLabeling({ ...labeling });
            }).catch((reason) => {
                errorHandler(reason, toast);
            })
        });
    }, [courseSchemeObjects]);


    const onRecordClick = (event) => {
        setRecordToShow({ ...event, 'validOn': parseDateIntoValidFormat(new Date(new Date(Date.now()).toISOString())) });
        openModal();
    }


    const getCourseLabel = (item) => {
        let _courseSchemeObj = courseSchemeObjects.find((_c) => _c.course._id === cprs.find((_cpr) => _cpr._id === item.cprId).courseId);
        if (!_courseSchemeObj)
            return '';
        return _courseSchemeObj.course.label;
    }

    var stringToColour = function (str) {
        var hash = 0;
        for (let i = 0; i < str.length; i++) {
            hash = str.charCodeAt(i) + ((hash << 5) - hash);
        }
        var colour = '#';
        for (let i = 0; i < 3; i++) {
            var value = (hash >> (i * 8)) & 0xFF;
            colour += ('00' + value.toString(16)).substr(-2);
        }
        return colour;
    }

    function invertColor(hex) {
        if (hex.indexOf('#') === 0) {
            hex = hex.slice(1);
        }
        // convert 3-digit hex to 6-digits.
        if (hex.length === 3) {
            hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
        }
        if (hex.length !== 6) {
            throw new Error('Invalid HEX color.');
        }
        // invert color components
        var r = (255 - parseInt(hex.slice(0, 2), 16)).toString(16),
            g = (255 - parseInt(hex.slice(2, 4), 16)).toString(16),
            b = (255 - parseInt(hex.slice(4, 6), 16)).toString(16);
        // pad each with zeros and return
        return '#' + padZero(r) + padZero(g) + padZero(b);
    }

    function padZero(str, len) {
        len = len || 2;
        var zeros = new Array(len).join('0');
        return (zeros + str).slice(-len);
    }

    return (
        <div>
            <h1>Records of {pupil.lastname + ' ' + pupil.firstname}</h1>
            <div className="block" style={{ marginLeft: '5px', marginRight: '5px', width: '99%', maxHeight: '800px', display: 'inline-block', verticalAlign: 'top' }}>
                <label className="header">Course Records</label>
                <ul className='list-group'>
                    {
                        courseSchemeObjects.map(_cso => {

                            return (
                                <Collapsible triggerTagName="div" key={'collapsible' + _cso.course._id} triggerStyle={{ border: '2px solid #023047', backgroundColor: '#023047', color: 'white', fontWeight: 'bold', textAlign: 'center', marginTop: '5px' }} /*trigger={[_cso.course.label, <div style={{ color: 'red' }}>{(gradeSuccs.find((_el) => _el.courseId === _cso.course._id)) ? (Math.round(gradeSuccs.find((_el) => _el.courseId === _cso.course._id).grade * 10) / 10) + '%' : ''}</div>]}*/ trigger={[_cso.course.label, <div style={{ color: 'red' }}>{Math.round(gradeSuccs.find((_el) => _el.courseId === _cso.course._id).grade * 10) / 10}%</div>, <BsChevronDown />]} transitionTime={200} >
                                <div style={{ height: "400px", width: '49%', display: 'inline-block', verticalAlign: 'top' }} key={'div2' + _cso.course._id}>
                                    <Line key={'line' + _cso.course._id} options={options} data={{
                                        label: 'Grades',
                                        labels: labeling[_cso.course._id],
                                        datasets: [
                                            {
                                                data: data[_cso.course._id],
                                                borderColor: '#5a03fc',
                                                label: _cso.course.label
                                            }
                                        ],
                                    }} />
                                </div>
                                <div key={'div1' + _cso.course._id} style={{ width: '49%', paddingTop: '15px', display: 'inline-block', alignContent: 'flex-start' }}>
                                    <ul className="list-group">
                                        {
                                            records.filter(i => { return getCourseLabel(i) === _cso.course.label }).map((item) =>
                                                <li className="list-group-item d-flex justify-content-between align-items-center" style={{ margin: '0px', border: '2px solid #023047', marginBottom: '1px', color: '#023047', backgroundColor: 'white' }} key={item._id} onClick={() => onRecordClick(item)}>
                                                    <div className="ms-4 me-auto">
                                                        <div className="fw-bold">at: {getDateTimeString(item.validOn)} </div>
                                                    </div>
                                                    <div className="ms-2 me-auto">
                                                        <div className="fw-bold">Symbol: {item.rating.symbol}</div>
                                                    </div>
                                                </li>
                                            )
                                        }
                                    </ul>
                                </div>
                            </Collapsible>
                            )
                        })
                    }
                </ul>
            </div>
            {/* <div style={{ width: '49%', display: 'inline-block', alignContent: 'flex-start' }}>
                <label className="header">All Records</label>
                <input
                    type='text'
                    autoComplete="off"
                    className="search inputFieldPeriod inputField"
                    //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                    id='inputCtrl_Search'
                    name='label'
                    placeholder='Search...'
                    //value={period.label}
                    onChange={onSearchFieldRecordsChange}
                ></input>
                <button type="button" className="bttnSearch bttn" disabled >
                    <FontAwesomeIcon icon="search" size="sm" />
                </button>
                <ul className="list-group">
                    {
                        records.map((item) =>
                            <li className="list-group-item d-flex justify-content-between align-items-center" style={{ border: '2px solid #023047', color: '#023047', backgroundColor: 'white' }} key={item._id} onClick={() => onRecordClick(item)}>
                                <div className="ms-4 me-auto" style={{ minWidth: '30%', textAlign: 'left' }}>
                                    <div className="fw-bold">at: {getDateTimeString(item.validOn)} </div>
                                </div>
                                <div className="ms-2 me-auto" style={{ minWidth: '40%', textAlign: 'center' }}>
                                    <div className="fw-bold">{getCourseLabel(item)}</div>
                                </div>
                                <div className="ms-2 me-auto" style={{ minWidth: '30%', textAlign: 'right' }}>
                                    <div className="fw-bold">Symbol: {item.rating.symbol}</div>
                                </div>
                            </li>
                        )
                    }
                </ul>
            </div> */}
            <ModalWindow
                stateOfModal={stateOfModal}
                modalTitle={'Record Info'}
                modalText={
                    <div>
                        <form style={{ width: '100%' }}>

                            <label className="fat" htmlFor={`inputCtrl_date`}>Date:</label>
                            <input
                                type='datetime-local'
                                autoComplete="off"
                                className="inputFieldPeriod inputField"
                                //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                                id='inputCtrl_date'
                                name='validOn'
                                placeholder='validOn'
                                value={recordToShow.validOn}
                                disabled
                            >
                            </input>
                            <br />

                            <label className="fat" htmlFor={`inputCtrl_Sign`}>Label:</label>
                            <input type="text" id="Label" name="Label" className="inputFieldPeriod inputField"
                                min="0" max="100" value={recordToShow.rating.label} disabled></input>
                            <label className="fat" htmlFor={`inputCtrl_Sign`}>Symbol:</label>
                            <input type="text" id="Symbol" name="Symbol" className="inputFieldPeriod inputField"
                                min="0" max="100" value={recordToShow.rating.symbol} disabled></input>

                            <label className="fat" htmlFor="weight">Weight</label>
                            <input type="number" id="weight" name="weight" className="inputFieldPeriod inputField"
                                min="0" max="100" value={recordToShow.weight} disabled></input>

                            <br />
                            <label className="fat" htmlFor="pupilNotPresent">Pupil not Present</label>
                            <input type="checkbox" id="pupilNotPresent" name="pupilNotPresent" style={{ marginLeft: '10px' }}
                                checked={recordToShow.pupilNotPresent} disabled></input>
                            <br></br>
                            <label className="fat" htmlFor={`inputCtrl_Notes`} >Comment: </label>
                            <input
                                type='text'
                                autoComplete="off"
                                className="inputFieldPeriod inputField"
                                //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                                id='inputCtrl_Notes'
                                name='comment'
                                placeholder='comment'
                                value={recordToShow.comment}
                                disabled
                            >
                            </input>
                            <br></br>
                            {(recordToShow.tags && recordToShow.tags.length !== 0) ? <label className="fat" htmlFor={`inputCtrl_Notes`} >Tags: </label> : ''}
                            <ul className="list-group list-group-horizontal">
                                {
                                    (recordToShow.tags) ?
                                        recordToShow.tags.map((_el) => {
                                            return (
                                                <li key={'rts' + _el.toString()} style={{ color: invertColor(stringToColour(_el)), backgroundColor: stringToColour(_el) }} className="list-group-item d-flex justify-content-between align-items-center" >
                                                    {_el}
                                                </li>
                                            );
                                        })
                                        : ''
                                }
                            </ul>
                        </form>
                    </div>
                }
                btnOkText={'OK'}
                //closeModal={closeModal}
                methodOkConfirmClick={closeModal}
            ></ModalWindow>
        </div>
    );
};

export default RecordsOfPupil;