import React, { useState } from 'react';
import { authService } from '../services/authenticationService.js';
import { userService } from '../services/userService.js';
import '../css/LogIn.css';
import '../App.css';
import { toast } from 'react-toastify';
import logo from '../data/logo.png';
import TextInputWithValidation from './utils/TextInputValidator';
import { errorHandler } from './utils/utils';
import { useHistory } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const LoginPage = () => {
    const history = useHistory();
    const [isShown, setIsShown] = useState(true);
    const [token, setToken] = useState(sessionStorage.getItem(process.env.REACT_APP_TOKEN_LOGIN_REGISTER));
    const [pwfg, setPWFG] = useState(false);
    const [email, setEmail] = useState(
        ''
    );
    const [userData, setUserData] = useState({
        'username': '@k.k',
        'password': 'pswrd'
    });
    let defaultFormUser = {
        'firstname': '',
        'lastname': '',
        'username': '@k.k',
        'password': 'pswrd',
        'email': '',
        'state': 'active'
    };

    const [passwords, setPasswords] = useState({
        'password1': '',
        'password2': '',
    });
    const [formUser, setFormUser] = useState(defaultFormUser);
    const [formValidationInfo, setFormValidationInfo] = useState(
        {
            'firstname': {
                valid: true,
                msg: ''
            },
            'lastname': {
                valid: true,
                msg: ''
            },
            'email': {
                valid: true,
                msg: ''
            },
            'password1': {
                valid: true,
                msg: ' '
            },
            'username': {
                valid: true,
                msg: ''
            },
            'password2': {
                valid: true,
                msg: ' '
            },
            'form': {
                valid: false,
                msg: 'all fields except username are mandatory!'
            }
        });

    const validateEmail = (email) => {
        return String(email)
            .toLowerCase()
            .match(
                /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            );
    };


    const onBtnBackToLoginClick = () => {
        sessionStorage.removeItem(process.env.REACT_APP_TOKEN_LOGIN_REGISTER);
        window.location.reload();
        setPWFG(false);
    }


    if (token === undefined || token === null) {
        //#region  login

        const onChange = (event) => {
            const { name, value } = event.target;
            setUserData({ ...userData, [name]: value });
        }

        const onSubmit = () => {
            try {
                if (userData.username === '' || userData.password === '') {
                    toast.error('Please enter username and password');
                } else {
                    authService.login(userData)
                        .then(() => {
                            history.push('/Home');
                            window.location.reload();
                        }).catch((reason) => {
                            errorHandler(reason, toast);
                        })
                }
            } catch (err) {
                errorHandler(err, toast);
            }
        }

        const onBtnRegister = () => {
            try {
                sessionStorage.setItem(process.env.REACT_APP_TOKEN_LOGIN_REGISTER, 1);
                window.location.reload();
                //history.push('/Register');
            } catch (err) {
                errorHandler(err, toast);
            }
        }

        const onChangePWFG = (event) => {
            const { value } = event.target;
            setEmail(value);
        }

        const onBtnSubmit = () => {
            if (!validateEmail(email)) {
                toast.error('invalid email format');
            } else {
                authService.startPasswordResetProcess({ email }).then(() => {
                    toast.success('success - check your mailbox!');
                }).catch((reason) => {
                    errorHandler(reason, toast);
                })
            }
        }

        const handleKeypress = e => {
            if (e.keyCode === 13) {
                onSubmit();
            }
        }

        const handleKeypressPasword = e => {
            if (e.keyCode === 13) {
                onBtnSubmit();
            }
        }

        if (!pwfg) {
            return (
                <div className="login">
                    <form className="loginForm">
                        <div className="form-group row">
                            <div className="logoEnv form-group col">
                                <img className="logo" alt="logo" src={logo}></img>
                            </div>
                            <div className="inline form-group col">
                                <h3 className="loginheader headers">LogIn</h3>
                                <input
                                    type='text'
                                    autoComplete="off"
                                    className="username inputField"
                                    //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                                    id='inputCtrl_username'
                                    name='username'
                                    placeholder='username'
                                    value={userData.username}
                                    onChange={onChange}
                                    onKeyDown={handleKeypress}
                                >
                                </input>
                                <br />
                                <input
                                    type={isShown ? 'password' : 'text'}
                                    autoComplete="off"
                                    className="password inputField"
                                    //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                                    id='inputCtrl_password'
                                    name='password'
                                    placeholder='Password'
                                    value={userData.password}
                                    onChange={onChange}
                                    onKeyDown={handleKeypress}>
                                </input>
                                <button type="button" className="bttnEye bttn"
                                    onMouseEnter={() => setIsShown(false)}
                                    onMouseLeave={() => setIsShown(true)}>
                                    <FontAwesomeIcon icon="eye" size="lg" />
                                </button>
                                <button type="button" className="bttnForgot" onClick={() => { setPWFG(true) }} >I forgot my password</button>
                                <br />
                                <button type="button" className="bttn bttnLogin" onClick={onSubmit} >LogIn</button>
                                <br />
                                <div style={{ display: "flex" }}>
                                    <button type="button" className="bttnRegister" onClick={onBtnRegister} >New around here?</button>
                                </div>
                            </div >
                        </div >
                    </form>

                </div >
            );
        } else {
            return (
                <div className="login">
                    <form className="loginForm">
                        <div className="form-group row">
                            <div className="logoEnv form-group col">
                                <img className="logo" alt="logo" src={logo}></img>
                            </div>
                            <div className="inline form-group col">
                                <h3 className="loginheader headers">Reset Password</h3>
                                <div className="form-group row">
                                    <input
                                        type='text'
                                        autoComplete="off"
                                        className="username inputField"
                                        //className={!formValidationInfo[key]?.valid ? "form-control is-invalid" : "form-control is-valid"}
                                        id='inputCtrl_username'
                                        name='email'
                                        placeholder='email'
                                        value={email}
                                        onChange={onChangePWFG}
                                        onKeyDown={handleKeypressPasword}
                                    >
                                    </input>
                                </div>
                                <br></br>
                                <div className="noMarginBottom form-group row">
                                    <button type="button" className="bttn bttnLogin" onClick={onBtnSubmit} >Submit</button>
                                </div>
                                <div className="form-group row">
                                    <button type="button" className="inline50 bttn bttnBack bttnBackForgot" onClick={onBtnBackToLoginClick}>Back to LogIn</button>
                                </div>
                            </div >
                        </div >
                    </form>

                </div >
            );
        }

        //#endregion login

    } else {
        //#region register

        const checkFirstname = (value) => {
            let msg = 'Firstname is okay';
            let valid = true;
            if (!value) {
                msg = 'provide a first name';
                valid = false;
            } else {
                if (value.length < 4) {
                    msg = 'first name is too short';
                    valid = false;
                } else if (value.length > 20) {
                    msg = 'first name is too long';
                    valid = false;
                }
            }
            return { valid, msg };

        }

        const checkLastname = (value) => {
            let msg = 'Lastname is okay';
            let valid = true;
            if (!value) {
                msg = 'provide a last name';
                valid = false;
            }
            return { valid, msg };
        }

        const checkPW = (value, nameOfField) => {
            setPasswords({ ...passwords, [nameOfField]: value });
            let msg = 'Password is okay';
            let valid = true;
            if (!value) {
                msg = 'provide a Password';
                valid = false;
            } else {
                if (value.length < 4) {
                    msg = 'Password is too short';
                    valid = false;
                } else if (value.length > 20) {
                    msg = 'Password is too long';
                    valid = false;
                } else if (nameOfField === 'password1' && value !== passwords.password2) {
                    msg = 'Passwords do not match';
                    valid = false;
                } else if (nameOfField === 'password2' && value !== passwords.password1) {
                    msg = 'Passwords do not match';
                    valid = false;
                } else {
                    return {
                        'password1': {
                            'valid': valid,
                            'msg': msg
                        },
                        'password2': {
                            'valid': valid,
                            'msg': msg
                        }
                    };
                }
            }
            return { valid, msg };
        }

        const handleKeypressRegister = e => {
            if (e.keyCode === 13) {
                onSubmitRegister();
            }
        }

        const checkEmail = (value) => {
            let msg = 'email is okay';
            let valid = true;
            if (!value) {
                msg = 'provide an email';
                valid = false;
            } else {
                if (!validateEmail(value)) {
                    msg = 'invalid email format';
                    valid = false;
                }
            }
            return { valid, msg };
        }

        const checkUsername = (value) => {
            let msg = 'username is okay';
            let valid = true;
            return { valid, msg };
        }

        const validateField = (name, value) => {

            let validationInfo;
            switch (name) {
                case 'firstname':
                    validationInfo = checkFirstname(value);
                    break;
                case 'lastname':
                    validationInfo = checkLastname(value);
                    break;
                case 'email':
                    validationInfo = checkEmail(value);
                    break;
                case 'username':
                    validationInfo = checkUsername(value);
                    break;
                case 'password1':
                case 'password2':
                    validationInfo = checkPW(value, name);
                    break;
                default:
                    break;
            }

            let newFormValidationInfo;

            if (!['password1', 'password2'].includes(name) || (!validationInfo.password1 && !validationInfo.password2)) {
                newFormValidationInfo = {
                    ...formValidationInfo,
                    [name]: validationInfo
                };
            } else {
                newFormValidationInfo = {
                    ...formValidationInfo,
                    'password1': validationInfo.password1,
                    'password2': validationInfo.password2
                };
                setFormUser({ ...formUser, 'password': value });
            }

            if (areSomeFieldsEmpty()) {
                newFormValidationInfo.form.valid = false;
                newFormValidationInfo.form.msg = 'all fields except username are mandatory!';
            } else if (isFormValidationObjectValid(newFormValidationInfo)) {
                newFormValidationInfo.form.valid = true;
                newFormValidationInfo.form.msg = '';
            } else {
                newFormValidationInfo.form.valid = false;
                newFormValidationInfo.form.msg = 'Some data is invalid';
            }
            setFormValidationInfo(newFormValidationInfo);
        }

        const isFormValidationObjectValid = (obj) => {
            let keys = Object.keys(obj);
            return keys.every((element) => (element === 'form' || obj[element].valid));
        }

        const areSomeFieldsEmpty = () => {
            let keys = Object.keys(formUser);
            return keys.some((element) => element !== 'username' && formUser[element] === '');
        }

        const onChangeRegister = async (event) => {
            const { name, value } = event.target;
            if (Object.keys(formUser).includes(name)) {
                let obj = { ...formUser, [name]: value };
                setFormUser(obj);
            }
            validateField(name, value);
        }

        const onSubmitRegister = () => {
            try {
                let tempEl = Object.keys(formValidationInfo).find((el) => !formValidationInfo[el].valid && el !== 'form');
                if (tempEl) {
                    toast.error(formValidationInfo[tempEl].msg);
                    return;
                }
                if (areSomeFieldsEmpty()) {
                    toast.error(formValidationInfo.form.msg);
                    return;
                }
                userService.create(formUser).then(() => {
                    sessionStorage.removeItem(process.env.REACT_APP_TOKEN_LOGIN_REGISTER);
                    //window.location.reload();
                    setToken(null);
                    toast.success('user registered');
                }).catch((reason) => {
                    errorHandler(reason, toast);
                })


            } catch (err) {
                errorHandler(err, toast);
            }
        }


        return (
            <div className="login">
                <form className="registerForm loginForm">
                    <div className="nomargin form-group row">
                        <div style={{ width: "50%" }} className="form-group col-6 logoEnv">
                            <img className="logo" alt="logo" src={logo}></img>
                        </div>
                        <div style={{ width: "50%" }} className="form-group col-6"
                            onKeyDown={handleKeypressRegister}>
                            <TextInputWithValidation
                                objectKey="firstname"
                                placeholder="Firstname"
                                formValidationInfo={formValidationInfo}
                                onChange={onChangeRegister}
                            ></TextInputWithValidation>
                            <TextInputWithValidation
                                objectKey="lastname"
                                placeholder="Lastname"
                                formValidationInfo={formValidationInfo}
                                onChange={onChangeRegister}
                            ></TextInputWithValidation>
                            <TextInputWithValidation
                                objectKey="username"
                                placeholder="Username"
                                formValidationInfo={formValidationInfo}
                                onChange={onChangeRegister}
                            ></TextInputWithValidation>
                            <TextInputWithValidation
                                objectKey="email"
                                placeholder="Email"
                                formValidationInfo={formValidationInfo}
                                onChange={onChangeRegister}
                            ></TextInputWithValidation>
                            <TextInputWithValidation
                                type="password"
                                objectKey="password1"
                                placeholder="Password"
                                formValidationInfo={formValidationInfo}
                                onChange={onChangeRegister}
                            ></TextInputWithValidation>
                            <TextInputWithValidation
                                type="password"
                                objectKey="password2"
                                placeholder="Password"
                                formValidationInfo={formValidationInfo}
                                onChange={onChangeRegister}
                            ></TextInputWithValidation>
                        </div>
                    </div>
                    <div className="nomargin form-group row">
                        <div className="form-group col-6">
                            <button type="button" className="bttn bttnBack" onClick={onBtnBackToLoginClick}>Back to LogIn</button>
                        </div>
                        <div className="form-group col-6">
                            <button type="button" className="bttnReg bttn" onClick={onSubmitRegister} /*disabled={(!formValidationInfo["form"]?.valid)}*/>Register</button>
                        </div>
                    </div>
                </form >

            </div >
        );

        //#endregion
    }

}

export default LoginPage;