import express from 'express';
const { json } = express;

import { requestLogger } from '../middleware/request-logger.js';
import { globalErrorHandler, defaultError } from '../errorHandling/error-handler.js';
import { healthChecker } from '../utils/health-checker.js';

import { cors } from '../middleware/cors-management.js';
import { log } from '../logging/app-logger.js';
import { apiV1Router } from '../routers/api-router.js';

const clientFolder = '_client/build';

export const configure = async (app) => {

  loadHealthCheck(app);
  app.use(json());
  app.use(cors());
  app.use(requestLogger);

  app.use('/api/v1', apiV1Router);

  app.use(express.static(clientFolder));
  log.success(`Serving static content from folder ${clientFolder}`);

  app.get('*', (req, res) => {
    res.sendFile('index.html', { root: clientFolder });
  });

  // must be last loaded function to collect all errors before
  app.use(globalErrorHandler);
};

export const configureInErrorMode = async (app) => {
  loadHealthCheck(app);
  app.use(defaultError);
};

function loadHealthCheck(app) {
  app.get('/api/v1/healthcheck', healthChecker);
}
