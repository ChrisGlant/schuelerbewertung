'use strict'
import chai from 'chai';
import chaiHttp from 'chai-http';
import app from '../server.js';

import { healthCheck } from './00-startup-hook.js';

const { expect } = chai;

// endpoints
const loginEndpoint = '/api/v1/login';
const userEndpoint = '/api/v1/users';
const periodEndpoint = '/api/v1/periods';
const courseEndpoint = '/api/v1/courses';
const lessonEndpointToTest = '/api/v1/lessons';

// data variables

let user = {
    username: "lessonUser",
    email: "lessonUser@gmail.com",
    password: "pswrd",
    lastname: "mock-data",
    firstname: "lesson"
}

let secUser = {
    username: "lessonUser2",
    email: "lessonUser2@gmail.com",
    password: "pswrd",
    lastname: "mock-data",
    firstname: "lesson"
}

let periodToCreate = {
    'label': 'Schuljahr 2021/22 lesson',
    'startdate': new Date('01.09.4050'),
    'enddate': new Date('01.07.4051'),
    'state': 'active'
};

let periodToCreateSecUser = {
    'label': 'Schuljahr 2022/23 lesson2',
    'startdate': new Date('01.09.4051'),
    'enddate': new Date('01.07.4052'),
    'state': 'active'
};

let courseToCreate = {
    "label": "lesson tests",
    "subjectLabel": "tests",
    "pupilGroupLabel": "Gruppe A (die besseren)",
    "notes": "test"
}

let courseToCreateSecUser = {
    "label": "lesson tests2",
    "subjectLabel": "tests",
    "pupilGroupLabel": "Gruppe A (die besseren)",
    "notes": "test"
}

let lessonToCreate = {
    'courseLabel': 'NVS 4 AHIF',
    'starttime': '10:30',
    'endtime': '11:30',
    'roomNr': '187',
    'weekday': 'monday',
    'colorCode': '#023047'
}

let lessonToCreateSecUser = {}

// other variables (keys, ids)

let neededKeysCourse = ['_id', 'label', 'subjectLabel', 'pupilGroupLabel', 'notes', 'periodId']
let neededKeysPeriod = ['_id', 'label', 'startdate', 'enddate', 'state']
let neededKeysLessons = ['_id', 'courseId', 'courseLabel', 'starttime', 'endtime', 'roomNr', 'weekday', 'colorCode']
let notFoundId = '61a8ea9bd4022b3534077c0d'

// setup

chai.use(chaiHttp);
chai.should();

// hook - done() fires, if healthcheck point answers with 200
before(() => {
    return healthCheck(app);
});

describe(`CRUD Tests for ${lessonEndpointToTest}`, () => {
    let testGroupName;
    let testNumber = 7000;

    testGroupName = 'Basic user stuff'
    describe(testGroupName, () => {
        it(`${testNumber++} ${testGroupName} - No Auth. header- 401 `, done => {
            chai
                .request(app)
                .get(lessonEndpointToTest)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(401);
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - Create first user - 201`, done => {
            chai
                .request(app)
                .post(userEndpoint)
                .send(user)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(201);
                    res.body.should.be.a('object');

                    res.body.should.have.property('_id').not.to.be.null;
                    res.body.should.have.property('username').not.to.be.null;
                    res.body.should.have.property('state').not.to.be.null;
                    res.body.should.have.property('firstname').not.to.be.null;
                    res.body.should.have.property('lastname').not.to.be.null;

                    user._id = res.body._id;
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - Login first user - 200`, (done) => {
            chai
                .request(app)
                .post(loginEndpoint)
                .send({ 'username': user['username'], 'password': user['password'] })
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(1);
                    res.body.should.have.property('token');
                    user.token = res.body.token;
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - Create second user - 201`, done => {
            chai
                .request(app)
                .post(userEndpoint)
                .send(secUser)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(201);
                    res.body.should.be.a('object');

                    res.body.should.have.property('_id').not.to.be.null;
                    res.body.should.have.property('username').not.to.be.null;
                    res.body.should.have.property('state').not.to.be.null;
                    res.body.should.have.property('firstname').not.to.be.null;
                    res.body.should.have.property('lastname').not.to.be.null;

                    secUser._id = res.body._id;
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - Login second user - 200`, (done) => {
            chai
                .request(app)
                .post(loginEndpoint)
                .send({ 'username': secUser['username'], 'password': secUser['password'] })
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(1);
                    res.body.should.have.property('token');
                    secUser.token = res.body.token;
                    done();
                });
        });
    })

    testGroupName = 'Create Mock-Data';
    describe(testGroupName, () => {
        describe('POST Mock-Data', () => {
            it(`${testNumber++} ${testGroupName} - create period firstUser - 201`, done => {
                chai
                    .request(app)
                    .post(periodEndpoint)
                    .set('Authorization', user.token)
                    .send(periodToCreate)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(201);
                        res.body.should.be.a('object');

                        neededKeysPeriod.forEach(key => {
                            res.body.should.have.property(key).to.not.be.null
                        })

                        periodToCreate._id = res.body._id;
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - create period secUser - 201`, done => {
                chai
                    .request(app)
                    .post(periodEndpoint)
                    .set('Authorization', secUser.token)
                    .send(periodToCreateSecUser)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(201);
                        res.body.should.be.a('object');

                        neededKeysPeriod.forEach(key => {
                            res.body.should.have.property(key).to.not.be.null
                        })

                        periodToCreateSecUser._id = res.body._id;
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - create course firstUser - 201`, done => {
                chai
                    .request(app)
                    .post(courseEndpoint)
                    .set('Authorization', user.token)
                    .send(courseToCreate)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(201);
                        res.body.should.be.a('object');

                        neededKeysCourse.forEach(key => {
                            res.body.should.have.property(key).to.not.be.null
                        })

                        courseToCreate._id = res.body._id;
                        lessonToCreate.courseId = courseToCreate._id;
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - create course secUser - 201`, done => {
                chai
                    .request(app)
                    .post(courseEndpoint)
                    .set('Authorization', secUser.token)
                    .send(courseToCreateSecUser)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(201);
                        res.body.should.be.a('object');

                        neededKeysCourse.forEach(key => {
                            res.body.should.have.property(key).to.not.be.null
                        })

                        courseToCreateSecUser._id = res.body._id;
                        lessonToCreateSecUser.courseId = courseToCreateSecUser._id;
                        done();
                    });
            });
        })

        describe('GET Mock-Data', () => {
            it(`${testNumber++} ${testGroupName} - GET created period firstuser - 200 `, done => {
                chai
                    .request(app)
                    .get(periodEndpoint + '/' + periodToCreate._id)
                    .set('Authorization', user.token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        res.body.should.be.a('object');

                        neededKeysPeriod.forEach(key => res.body.should.have.property(key).to.not.be.null)
                        expect(res.body._id).to.be.equal(periodToCreate._id);
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - GET created period secUser - 200 `, done => {
                chai
                    .request(app)
                    .get(periodEndpoint + '/' + periodToCreateSecUser._id)
                    .set('Authorization', secUser.token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        res.body.should.be.a('object');

                        neededKeysPeriod.forEach(key => res.body.should.have.property(key).to.not.be.null)
                        expect(res.body._id).to.be.equal(periodToCreateSecUser._id);
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - GET created course firstuser - 200 `, done => {
                chai
                    .request(app)
                    .get(courseEndpoint + '/' + courseToCreate._id)
                    .set('Authorization', user.token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        res.body.should.be.a('object');

                        neededKeysCourse.forEach(key => res.body.should.have.property(key).to.not.be.null)
                        expect(res.body._id).to.be.equal(courseToCreate._id);
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - GET created course secUser - 200 `, done => {
                chai
                    .request(app)
                    .get(courseEndpoint + '/' + courseToCreateSecUser._id)
                    .set('Authorization', secUser.token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        res.body.should.be.a('object');

                        neededKeysCourse.forEach(key => res.body.should.have.property(key).to.not.be.null)
                        expect(res.body._id).to.be.equal(courseToCreateSecUser._id);
                        done();
                    });
            });
        })
    })

    testGroupName = 'POST'
    describe(testGroupName, () => {
        describe('check basic test cases', () => {
            // create 
            it(`${testNumber++} ${testGroupName} - create lesson - 201`, done => {
                chai
                    .request(app)
                    .post(lessonEndpointToTest)
                    .set('Authorization', user.token)
                    .send(lessonToCreate)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(201);
                        res.body.should.be.a('object');

                        neededKeysLessons.forEach(key => {
                            res.body.should.have.property(key).to.not.be.null
                        })

                        lessonToCreate._id = res.body._id;
                        done();
                    });
            });
            // get created
            it(`${testNumber++} ${testGroupName} - GET created lesson - 200 `, done => {
                chai
                    .request(app)
                    .get(lessonEndpointToTest + '/' + lessonToCreate._id)
                    .set('Authorization', user.token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        res.body.should.be.a('object');

                        neededKeysLessons.forEach(key => res.body.should.have.property(key).to.not.be.null)
                        expect(res.body._id).to.be.equal(lessonToCreate._id);
                        done();
                    });
            });
            // create during other lesson 
            it(`${testNumber++} ${testGroupName} - create lesson during other lesson - 400`, done => {
                let dLesson = { ...lessonToCreate };
                delete dLesson._id;
                chai
                    .request(app)
                    .post(lessonEndpointToTest)
                    .set('Authorization', user.token)
                    .send(dLesson)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(400);
                        done();
                    });
            });
            // delet created
            it(`${testNumber++} ${testGroupName} - delete created lesson - 204`, done => {
                chai
                    .request(app)
                    .delete(lessonEndpointToTest + '/' + lessonToCreate._id)
                    .set('Authorization', user.token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(204);

                        lessonToCreate._id = undefined;
                        done();
                    });
            });

            /* --------------------- property tests --------------------- */

            /* --------------------- id --------------------- */
            describe('check property id ', () => {
                let wrongLessons = [];

                let lesson = { ...lessonToCreate };
                lesson._id = {};
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson._id = 12432434;
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson._id = 'JALKJ23kJ21';
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson._id = notFoundId;
                wrongLessons.push(lesson);

                wrongLessons.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - id - 400`, done => {
                        chai
                            .request(app)
                            .patch(lessonEndpointToTest + '/' + lessonToCreate._id)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- courseId -------------------------- */
            describe('check property courseId ', () => {
                let wrongLessons = [];

                let lesson = { ...lessonToCreate };
                lesson.courseId = undefined;
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.courseId = null;
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.courseId = 12344;
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.courseId = '1870';
                wrongLessons.push(lesson);


                wrongLessons.forEach((we, idx) => {
                    it(`${testNumber++} ${testGroupName} - courseId - 400`, done => {
                        chai
                            .request(app)
                            .post(lessonEndpointToTest)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });

                it(`${testNumber++} ${testGroupName} - courseId - 404`, done => {
                    chai
                        .request(app)
                        .post(lessonEndpointToTest)
                        .set('Authorization', user.token)
                        .send({ ...lessonToCreate, ...{ courseId: notFoundId } })
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(404);
                            done();
                        });
                });
            });

            /* --------------------------- courseLabel -------------------------- */
            describe('check property courseLabel ', () => {
                let wrongLessons = [];

                let lesson = { ...lessonToCreate };
                lesson.courseLabel = undefined;
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.courseLabel = null;
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.courseLabel = {};
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.courseLabel = [];
                wrongLessons.push(lesson);

                wrongLessons.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - courseLabel - 400`, done => {
                        chai
                            .request(app)
                            .post(lessonEndpointToTest)
                            .set('Authorization', user.token)
                            .send({ ...we, ...{ courseId: courseToCreate._id } })
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- starttime -------------------------- */
            describe('check property starttime ', () => {
                let wrongLessons = [];

                let lesson = { ...lessonToCreate };
                lesson.starttime = undefined;
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.starttime = null;
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.starttime = {}
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.starttime = []
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.starttime = '101:10'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.starttime = '10:101'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.starttime = '24:10'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.starttime = '23:61'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.starttime = '23:1'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.starttime = '2:11'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.starttime = 'hihihi'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.starttime = 1010
                wrongLessons.push(lesson);

                // past endtime
                lesson = { ...lessonToCreate };
                lesson.starttime = '11:31'
                wrongLessons.push(lesson);

                wrongLessons.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - starttime - 400`, done => {
                        chai
                            .request(app)
                            .post(lessonEndpointToTest)
                            .set('Authorization', user.token)
                            .send({ ...we, ...{ courseId: courseToCreate._id } })
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });


            /* --------------------------- endtime -------------------------- */
            describe('check property endtime ', () => {
                let wrongLessons = [];

                let lesson = { ...lessonToCreate };
                lesson.endtime = undefined;
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.endtime = null;
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.endtime = {}
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.endtime = []
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.endtime = '101:10'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.endtime = '10:101'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.endtime = '24:10'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.endtime = '23:61'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.endtime = '23:1'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.endtime = '2:11'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.endtime = 'hihihi'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.endtime = 1010
                wrongLessons.push(lesson);

                // before starttime
                lesson = { ...lessonToCreate };
                lesson.endtime = '10:29'
                wrongLessons.push(lesson);

                wrongLessons.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - endtime - 400`, done => {
                        chai
                            .request(app)
                            .post(lessonEndpointToTest)
                            .set('Authorization', user.token)
                            .send({ ...we, ...{ courseId: courseToCreate._id } })
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- roomNr -------------------------- */
            describe('check property roomNr ', () => {
                let wrongLessons = [];

                let lesson = { ...lessonToCreate };
                lesson.roomNr = {}
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.roomNr = []
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.roomNr = null
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.roomNr = undefined
                wrongLessons.push(lesson);

                wrongLessons.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - roomNr - 400`, done => {
                        chai
                            .request(app)
                            .post(lessonEndpointToTest)
                            .set('Authorization', user.token)
                            .send({ ...we, ...{ courseId: courseToCreate._id } })
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- weekday -------------------------- */
            describe('check property weekday ', () => {
                let wrongLessons = [];

                let lesson = { ...lessonToCreate };
                lesson.weekday = {}
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.weekday = []
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.weekday = undefined
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.weekday = null
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.weekday = 'dienstag'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.weekday = 'sonntag'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.weekday = 'Thursday'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.weekday = 'hihihi'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.weekday = 12344
                wrongLessons.push(lesson);

                wrongLessons.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - weekday - 400`, done => {
                        chai
                            .request(app)
                            .post(lessonEndpointToTest)
                            .set('Authorization', user.token)
                            .send({ ...we, ...{ courseId: courseToCreate._id } })
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- colorCode -------------------------- */
            describe('check property colorCode ', () => {
                describe('check wrong colorCodes', () => {
                    let wrongLessons = [];

                    let lesson = { ...lessonToCreate };
                    lesson.colorCode = {};
                    wrongLessons.push(lesson);

                    lesson = { ...lessonToCreate };
                    lesson.colorCode = [];
                    wrongLessons.push(lesson);

                    lesson = { ...lessonToCreate };
                    lesson.colorCode = 1;
                    wrongLessons.push(lesson);

                    lesson = { ...lessonToCreate };
                    lesson.colorCode = '1';
                    wrongLessons.push(lesson);

                    lesson = { ...lessonToCreate };
                    lesson.colorCode = '#1234567';
                    wrongLessons.push(lesson);

                    lesson = { ...lessonToCreate };
                    lesson.colorCode = '#1_ABLK4';
                    wrongLessons.push(lesson);

                    wrongLessons.forEach(we => {
                        it(`${testNumber++} ${testGroupName} - colorCode - 400`, done => {
                            chai
                                .request(app)
                                .post(lessonEndpointToTest)
                                .set('Authorization', user.token)
                                .send({ ...we, ...{ courseId: courseToCreate._id } })
                                .end((err, res) => {
                                    expect(err).to.be.null;
                                    res.should.have.status(400);
                                    done();
                                });
                        });
                    });
                });

                describe('check working colorcodes', () => {
                    let workingLessons = [];
                    let testLesson = {
                        'courseLabel': 'NVS 4 AHIF',
                        'starttime': '00:00',
                        'endtime': '01:00',
                        'roomNr': '187',
                        'weekday': 'friday',
                    }

                    let lesson = { ...testLesson };
                    lesson.colorCode = null;
                    workingLessons.push(lesson);

                    lesson = { ...testLesson };
                    lesson.colorCode = undefined;
                    lesson.starttime = '01:00';
                    lesson.endtime = '02:00';
                    workingLessons.push(lesson);

                    lesson = { ...testLesson };
                    delete lesson.colorCode;
                    lesson.starttime = '02:00';
                    lesson.endtime = '03:00';
                    workingLessons.push(lesson);

                    lesson = { ...testLesson };
                    lesson.colorCode = '#0BC';
                    lesson.starttime = '03:00';
                    lesson.endtime = '04:00';
                    workingLessons.push(lesson);

                    workingLessons.forEach(we => {
                        it(`${testNumber++} ${testGroupName} - colorCode - 201`, done => {
                            chai
                                .request(app)
                                .post(lessonEndpointToTest)
                                .set('Authorization', user.token)
                                .send({ ...we, ...{ courseId: courseToCreate._id } })
                                .end((err, res) => {
                                    expect(err).to.be.null;
                                    res.should.have.status(201);
                                    res.body.should.be.a('object');

                                    neededKeysLessons.forEach(key => {
                                        res.body.should.have.property(key).to.not.be.null
                                    });
                                    we._id = res.body._id;

                                    done();
                                });
                        });
                        it(`${testNumber++} ${testGroupName} - GET created lesson - 200 `, done => {
                            chai
                                .request(app)
                                .get(lessonEndpointToTest + '/' + we._id)
                                .set('Authorization', user.token)
                                .end((err, res) => {
                                    expect(err).to.be.null;
                                    res.should.have.status(200);
                                    res.body.should.be.a('object');

                                    neededKeysLessons.forEach(key => res.body.should.have.property(key).to.not.be.null)
                                    expect(res.body._id).to.be.equal(we._id);
                                    done();
                                });
                        });
                        // delete created
                        it(`${testNumber++} ${testGroupName} - delete created lesson - 204`, done => {
                            chai
                                .request(app)
                                .delete(lessonEndpointToTest + '/' + we._id)
                                .set('Authorization', user.token)
                                .end((err, res) => {
                                    expect(err).to.be.null;
                                    res.should.have.status(204);
                                    done();
                                });
                        });
                    });
                });
            });

            /* --------------------------- missing/not allowed properies-------------------------- */
            describe('missing/not allowed properies', () => {
                let wrongLessons = [];

                let lesson = { ...lessonToCreate };
                delete lesson.courseLabel
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                delete lesson.starttime
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                delete lesson.endtime
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                delete lesson.roomNr
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                delete lesson.weekday
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.notAllowed = 'hihihi'
                wrongLessons.push(lesson);

                wrongLessons.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - missing/not allowed properties - 400`, done => {
                        chai
                            .request(app)
                            .post(lessonEndpointToTest)
                            .set('Authorization', user.token)
                            .send({ ...we, ...{ courseId: courseToCreate._id } })
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
                it(`${testNumber++} ${testGroupName} - missing/not allowed properties - 400`, done => {
                    chai
                        .request(app)
                        .post(lessonEndpointToTest)
                        .set('Authorization', user.token)
                        .send({ ...lessonToCreate, ...{ courseId: undefined } })
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(400);
                            done();
                        });
                });
            });
        });
    });

    testGroupName = 'GET SINGLE'
    describe(testGroupName, () => {
        // create
        it(`${testNumber++} ${testGroupName} - create lesson - 201`, done => {
            chai
                .request(app)
                .post(lessonEndpointToTest)
                .set('Authorization', user.token)
                .send(lessonToCreate)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(201);
                    res.body.should.be.a('object');

                    neededKeysLessons.forEach(key => {
                        res.body.should.have.property(key).to.not.be.null
                    })

                    lessonToCreate._id = res.body._id;
                    done();
                });
        });
        // no auth. header
        it(`${testNumber++} ${testGroupName} - no auth. header - 401 `, done => {
            chai
                .request(app)
                .get(lessonEndpointToTest + '/' + lessonToCreate._id)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(401);
                    done();
                });
        });
        // get created
        it(`${testNumber++} ${testGroupName} - GET created lesson - 200 `, done => {
            chai
                .request(app)
                .get(lessonEndpointToTest + '/' + lessonToCreate._id)
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');

                    neededKeysLessons.forEach(key => res.body.should.have.property(key).to.not.be.null)
                    expect(res.body._id).to.be.equal(lessonToCreate._id);
                    done();
                });
        });
        // get not found
        it(`${testNumber++} ${testGroupName} - not found - 404 `, done => {
            chai
                .request(app)
                .get(lessonEndpointToTest + '/' + notFoundId)
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(404);
                    done();
                });
        });
        // get with other user
        it(`${testNumber++} ${testGroupName} - get with user 2 - 404 `, done => {
            chai
                .request(app)
                .get(lessonEndpointToTest + '/' + lessonToCreate._id)
                .set('Authorization', secUser.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(404);
                    done();
                });
        });
        // get with invalid object id
        it(`${testNumber++} ${testGroupName} - invalid ObjectId - 400`, done => {
            chai
                .request(app)
                .get(lessonEndpointToTest + '/1870')
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(400);
                    done();
                })
        });
    });

    testGroupName = 'GET ALL';
    describe(testGroupName, () => {
        // no auth. header
        it(`${testNumber++} ${testGroupName} - no auth. header - 401`, done => {
            chai
                .request(app)
                .get(lessonEndpointToTest)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(401);
                    done();
                });
        });
        // get all with first user
        it(`${testNumber++} ${testGroupName} - first user - 200`, done => {
            chai
                .request(app)
                .get(lessonEndpointToTest)
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;

                    res.should.have.status(200);
                    res.body.should.be.a('array');

                    expect(res.body.length).to.equal(1);

                    done();
                });
        });
        // get all with second user
        it(`${testNumber++} ${testGroupName} - get with user 2 - 200 `, done => {
            chai
                .request(app)
                .get(lessonEndpointToTest)
                .set('Authorization', secUser.token)
                .end((err, res) => {
                    expect(err).to.be.null;

                    res.should.have.status(200);
                    res.body.should.be.a('array');

                    expect(res.body.length).to.equal(0);
                    done();
                });
        });
    });

    testGroupName = 'PATCH';
    describe(testGroupName, () => {
        describe('basic test cases', () => {
            // no auth. header
            it(`${testNumber++} ${testGroupName} - No Auth. header - 401`, done => {
                chai
                    .request(app)
                    .patch(lessonEndpointToTest + '/' + lessonToCreate._id)
                    .send({ ...lessonToCreate })
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(401);
                        done();
                    });
            });
            // patch with invalid objectid
            it(`${testNumber++} ${testGroupName} - invalid ObjectId - 400`, done => {
                chai
                    .request(app)
                    .patch(lessonEndpointToTest + '/1870')
                    .send({ ...lessonToCreate })
                    .set('Authorization', user.token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(400);
                        done();
                    });
            });
            // patch with not found
            it(`${testNumber++} ${testGroupName} - not found - 404`, done => {
                let lesson = { ...lessonToCreate }
                delete lesson._id;
                chai
                    .request(app)
                    .patch(lessonEndpointToTest + '/' + notFoundId)
                    .set('Authorization', user.token)
                    .send(lesson)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(404);
                        done();
                    });
            });
            // patch with second user
            it(`${testNumber++} ${testGroupName} - with other user - 404`, done => {
                chai
                    .request(app)
                    .patch(lessonEndpointToTest + '/' + lessonToCreate._id)
                    .set('Authorization', secUser.token)
                    .send({ ...lessonToCreate })
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(404);
                        done();
                    });
            });
            // valid patch
            it(`${testNumber++} ${testGroupName} - valid patch - 200`, done => {
                chai
                    .request(app)
                    .patch(lessonEndpointToTest + '/' + lessonToCreate._id)
                    .set('Authorization', user.token)
                    .send({ courseLabel: 'test' })
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        done();
                    });
            });

            describe('patch lesson to be in other lesson', () => {
                let dLesson = { ...lessonToCreate };
                delete dLesson._id;
                dLesson.starttime = '12:30'
                dLesson.endtime = '13:30'
                // create during other lesson 
                it(`${testNumber++} ${testGroupName} - POST create lesson - 200`, done => {
                    chai
                        .request(app)
                        .post(lessonEndpointToTest)
                        .set('Authorization', user.token)
                        .send({ ...dLesson, ...{ courseId: courseToCreate._id } })
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(201);
                            dLesson._id = res.body._id
                            done();
                        });
                });
                // patch during other lesson
                it(`${testNumber++} ${testGroupName} - patch during other lesson - 400`, done => {
                    chai
                        .request(app)
                        .patch(lessonEndpointToTest + '/' + dLesson._id)
                        .set('Authorization', user.token)
                        .send({ starttime: '10:30' })
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(400);
                            done();
                        });
                });
            })
        });

        describe('property tests', () => {
            /* --------------------- id --------------------- */
            describe('check property id ', () => {
                let wrongLessons = [];

                let lesson = { ...lessonToCreate };
                lesson._id = {};
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson._id = 12432434;
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson._id = 'JALKJ23kJ21';
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson._id = notFoundId;
                wrongLessons.push(lesson);

                wrongLessons.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - id - 400`, done => {
                        chai
                            .request(app)
                            .patch(lessonEndpointToTest + '/' + lessonToCreate._id)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------- userId --------------------- */
            describe('check property id ', () => {
                let wrongLessons = [];

                let lesson = { ...lessonToCreate };
                lesson.userId = {};
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.userId = 12432434;
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.userId = 'JALKJ23kJ21';
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.userId = notFoundId;
                wrongLessons.push(lesson);

                wrongLessons.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - id - 400`, done => {
                        chai
                            .request(app)
                            .patch(lessonEndpointToTest + '/' + lessonToCreate._id)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });



            /* --------------------------- courseId -------------------------- */
            describe('check property courseId ', () => {
                let wrongLessons = [];

                let lesson = { ...lessonToCreate };
                lesson.courseId = null;
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.courseId = 12344;
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.courseId = '1870';
                wrongLessons.push(lesson);


                wrongLessons.forEach((we, idx) => {
                    it(`${testNumber++} ${testGroupName} - courseId - 400`, done => {
                        chai
                            .request(app)
                            .patch(lessonEndpointToTest + '/' + lessonToCreate._id)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });

                it(`${testNumber++} ${testGroupName} - courseId - 404`, done => {
                    chai
                        .request(app)
                        .patch(lessonEndpointToTest + '/' + lessonToCreate._id)
                        .set('Authorization', user.token)
                        .send({ ...lessonToCreate, ...{ courseId: notFoundId } })
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(404);
                            done();
                        });
                });
            });

            /* --------------------------- courseLabel -------------------------- */
            describe('check property courseLabel ', () => {
                let wrongLessons = [];

                let lesson = { ...lessonToCreate };
                lesson.courseLabel = null;
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.courseLabel = {};
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.courseLabel = [];
                wrongLessons.push(lesson);

                wrongLessons.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - courseLabel - 400`, done => {
                        chai
                            .request(app)
                            .patch(lessonEndpointToTest + '/' + lessonToCreate._id)
                            .set('Authorization', user.token)
                            .send({ ...we, ...{ courseId: courseToCreate._id } })
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- starttime -------------------------- */
            describe('check property starttime ', () => {
                let wrongLessons = [];

                let lesson = { ...lessonToCreate };
                lesson.starttime = null;
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.starttime = {}
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.starttime = []
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.starttime = '101:10'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.starttime = '10:101'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.starttime = '24:10'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.starttime = '23:61'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.starttime = '23:1'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.starttime = '2:11'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.starttime = 'hihihi'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.starttime = 1010
                wrongLessons.push(lesson);

                // past endtime
                lesson = { ...lessonToCreate };
                lesson.starttime = '11:31'
                wrongLessons.push(lesson);

                wrongLessons.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - starttime - 400`, done => {
                        chai
                            .request(app)
                            .patch(lessonEndpointToTest + '/' + lessonToCreate._id)
                            .set('Authorization', user.token)
                            .send({ ...we, ...{ courseId: courseToCreate._id } })
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });


            /* --------------------------- endtime -------------------------- */
            describe('check property endtime ', () => {
                let wrongLessons = [];

                let lesson = { ...lessonToCreate };
                lesson.endtime = null;
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.endtime = {}
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.endtime = []
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.endtime = '101:10'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.endtime = '10:101'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.endtime = '24:10'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.endtime = '23:61'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.endtime = '23:1'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.endtime = '2:11'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.endtime = 'hihihi'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.endtime = 1010
                wrongLessons.push(lesson);

                // before starttime
                lesson = { ...lessonToCreate };
                lesson.endtime = '10:29'
                wrongLessons.push(lesson);

                wrongLessons.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - endtime - 400`, done => {
                        chai
                            .request(app)
                            .patch(lessonEndpointToTest + '/' + lessonToCreate._id)
                            .set('Authorization', user.token)
                            .send({ ...we, ...{ courseId: courseToCreate._id } })
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- roomNr -------------------------- */
            describe('check property roomNr ', () => {
                let wrongLessons = [];

                let lesson = { ...lessonToCreate };
                lesson.roomNr = {}
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.roomNr = []
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.roomNr = null
                wrongLessons.push(lesson);

                wrongLessons.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - roomNr - 400`, done => {
                        chai
                            .request(app)
                            .patch(lessonEndpointToTest + '/' + lessonToCreate._id)
                            .set('Authorization', user.token)
                            .send({ ...we, ...{ courseId: courseToCreate._id } })
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- weekday -------------------------- */
            describe('check property weekday ', () => {
                let wrongLessons = [];

                let lesson = { ...lessonToCreate };
                lesson.weekday = {}
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.weekday = []
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.weekday = null
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.weekday = 'dienstag'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.weekday = 'sonntag'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.weekday = 'Thursday'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.weekday = 'hihihi'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson.weekday = 12344
                wrongLessons.push(lesson);

                wrongLessons.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - weekday - 400`, done => {
                        chai
                            .request(app)
                            .patch(lessonEndpointToTest + '/' + lessonToCreate._id)
                            .set('Authorization', user.token)
                            .send({ ...we, ...{ courseId: courseToCreate._id } })
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- colorCode -------------------------- */
            describe('check property colorCode ', () => {
                describe('check wrong colorCodes', () => {
                    let wrongLessons = [];

                    let lesson = { ...lessonToCreate };
                    lesson.colorCode = {};
                    wrongLessons.push(lesson);

                    lesson = { ...lessonToCreate };
                    lesson.colorCode = [];
                    wrongLessons.push(lesson);

                    lesson = { ...lessonToCreate };
                    lesson.colorCode = 1;
                    wrongLessons.push(lesson);

                    lesson = { ...lessonToCreate };
                    lesson.colorCode = '1';
                    wrongLessons.push(lesson);

                    lesson = { ...lessonToCreate };
                    lesson.colorCode = '#1234567';
                    wrongLessons.push(lesson);

                    lesson = { ...lessonToCreate };
                    lesson.colorCode = '#1_ABLK4';
                    wrongLessons.push(lesson);

                    wrongLessons.forEach(we => {
                        it(`${testNumber++} ${testGroupName} - colorCode - 400`, done => {
                            chai
                                .request(app)
                                .patch(lessonEndpointToTest + '/' + lessonToCreate._id)
                                .set('Authorization', user.token)
                                .send({ ...we, ...{ courseId: courseToCreate._id } })
                                .end((err, res) => {
                                    expect(err).to.be.null;
                                    res.should.have.status(400);
                                    done();
                                });
                        });
                    });
                });
                describe('check working colorCodes', () => {
                    let workingLessons = [];
                    let testLesson = {
                        'courseLabel': 'NVS 4 AHIF',
                        'starttime': '00:00',
                        'endtime': '01:00',
                        'roomNr': '187',
                        'weekday': 'friday',
                    }

                    let lesson = { ...testLesson };
                    lesson.colorCode = null;
                    workingLessons.push(lesson);

                    lesson = { ...testLesson };
                    lesson.colorCode = undefined;
                    workingLessons.push(lesson);

                    lesson = { ...testLesson };
                    delete lesson.colorCode;
                    workingLessons.push(lesson);

                    lesson = { ...testLesson };
                    lesson.colorCode = '#0BC';
                    workingLessons.push(lesson);

                    lesson = { ...testLesson };
                    lesson.colorCode = '#7693A1';
                    workingLessons.push(lesson);

                    workingLessons.forEach(we => {
                        it(`${testNumber++} ${testGroupName} - colorCode - 200`, done => {
                            chai
                                .request(app)
                                .patch(lessonEndpointToTest + '/' + lessonToCreate._id)
                                .set('Authorization', user.token)
                                .send({ ...we, ...{ courseId: courseToCreate._id } })
                                .end((err, res) => {
                                    expect(err).to.be.null;
                                    res.should.have.status(200);
                                    res.body.should.be.a('object');
                                    done();
                                });
                        });
                    });
                });
            });

            /* --------------------------- missing/not allowed properies-------------------------- */
            describe('missing/not allowed properties', () => {
                let wrongLessons = [];

                let lesson = { ...lessonToCreate };
                lesson.notAllowed = 'hihihi'
                wrongLessons.push(lesson);

                lesson = { ...lessonToCreate };
                lesson = {}
                wrongLessons.push(lesson);

                wrongLessons.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - missing/not allowed properies - 400`, done => {
                        chai
                            .request(app)
                            .patch(lessonEndpointToTest + '/' + lessonToCreate._id)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });
        });
    });

    testGroupName = 'DELETE';
    describe(testGroupName, () => {
        // no auth. header
        it(`${testNumber++} ${testGroupName} - No Auth. header - 401`, done => {
            chai
                .request(app)
                .delete(lessonEndpointToTest + '/' + lessonToCreate._id)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(401);
                    done();
                });
        });
        // delete with invalid objectid
        it(`${testNumber++} ${testGroupName} - invalid ObjectId - 400`, done => {
            chai
                .request(app)
                .delete(lessonEndpointToTest + '/1870')
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(400);
                    done();
                })
        });
        // delete with not found
        it(`${testNumber++} ${testGroupName} - not found - 404`, done => {
            chai
                .request(app)
                .delete(lessonEndpointToTest + '/' + notFoundId)
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(404);
                    done();
                })
        });
        // delete with other user
        it(`${testNumber++} ${testGroupName} - with other user - 404`, done => {
            chai
                .request(app)
                .delete(lessonEndpointToTest + '/' + lessonToCreate._id)
                .set('Authorization', secUser.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(404);
                    done();
                });
        });
        // successfull delete
        it(`${testNumber++} ${testGroupName} - successful delete - 204`, done => {
            chai
                .request(app)
                .delete(lessonEndpointToTest + '/' + lessonToCreate._id)
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(204);
                    done();
                })
        });
        // get deleted
        it(`${testNumber++} ${testGroupName} - get deleted item - 404`, done => {
            chai
                .request(app)
                .get(lessonEndpointToTest + '/' + lessonToCreate._id)
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(404);
                    done();
                });
        });
        // delete again
        it(`${testNumber++} ${testGroupName} - delete again - 204`, done => {
            chai
                .request(app)
                .delete(lessonEndpointToTest + '/' + lessonToCreate._id)
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(404);
                    done();
                })
        });
    });
});