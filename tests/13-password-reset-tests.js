'use strict'
import chai from 'chai';
import chaiHttp from 'chai-http';
import app from '../server.js';

import { healthCheck } from './00-startup-hook.js';

import User from '../models/user-model.js'
import { pswrdReset } from '../models/pswrdReset-model.js'

const { expect } = chai;

// endpoints
const loginEndpoint = '/api/v1/login';
const userEndpoint = '/api/v1/users';
const entityEndpointToTest = '/api/v1/password-reset';

// data variables

let user = {
    username: "passwordResetUser",
    password: "pswrd",
    lastname: "mock-data",
    firstname: "password-reset",
    email: "passwordReset@gmail.com"
}

let secUser = {
    username: "passwordResetUser2",
    password: "pswrd",
    lastname: "mock-data",
    firstname: "password-reset",
    email: "passwordReset1@gmail.com"
}

let thirdUser = {
    username: "passwordResetUser3",
    password: "pswrd",
    lastname: "mock-data",
    firstname: "password-reset",
    email: "passwordReset2@gmail.com"
}

let entityToCreate = { email: "passwordReset2@gmail.com" }
let entityToCreateSec = { new: 'new' }

// other variables (keys, ids)

let needKeysEntity = []
let notFoundId = '61a8ea9bd4022b3534077c0d'

// setup

let u = new User(thirdUser);
await u.save();
thirdUser._id = u._id;

let expiredReset = new pswrdReset({
    userId: notFoundId,
    recoveryToken: '4711000000471100000047110000004711000000',
    createdAt: '2020-12-25T20:04:00.516+00:00'
});
await expiredReset.save();

let invalidReset = new pswrdReset({
    userId: notFoundId,
    recoveryToken: '4711000000471100000147110000004711000001',
});
await invalidReset.save();

let validReset = new pswrdReset({
    userId: thirdUser._id,
    recoveryToken: '4711000000471100000247110000004711000002',
});
await validReset.save();
entityToCreateSec.recoverytoken = validReset.recoveryToken;

chai.use(chaiHttp);
chai.should();

// hook - done() fires, if healthcheck point answers with 200
before(() => {
    return healthCheck(app);
});

describe(`CRUD Tests for ${entityEndpointToTest}`, () => {
    let testGroupName;
    let testNumber = 13000;

    testGroupName = 'Basic user stuff'
    describe(testGroupName, () => {
        it(`${testNumber++} ${testGroupName} - Login third user - 200`, (done) => {
            chai
                .request(app)
                .post(loginEndpoint)
                .send({ 'username': thirdUser['username'], 'password': thirdUser['password'] })
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(1);
                    res.body.should.have.property('token');
                    thirdUser.token = res.body.token;
                    done();
                });
        });
    })

    testGroupName = 'PUT Password recovery';
    describe(testGroupName, () => {
        // PUT valid => 202
        it(`${testNumber++} ${testGroupName} - valid - 202`, done => {
            chai
                .request(app)
                .put(entityEndpointToTest)
                .send(entityToCreate)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(202);
                    done();
                });
        });

        // PUT not existing user email => 202
        it(`${testNumber++} ${testGroupName} - valid - 202`, done => {
            chai
                .request(app)
                .put(entityEndpointToTest)
                .send({ email: entityToCreate.email + 'notexisting' })
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(202);
                    done();
                });
        });

        // PUT invalid req.body => 400
        describe('check req.body', () => {
            let wrongEntities = [];

            let entity;
            entity = {};
            wrongEntities.push(entity);

            entity = undefined
            wrongEntities.push(entity);

            entity = [];
            wrongEntities.push(entity);

            entity = 'hihii';
            wrongEntities.push(entity);

            entity = { ...entityToCreate };
            entity.notAllowed = 'hihihi';
            wrongEntities.push(entity);

            wrongEntities.forEach(we => {
                it(`${testNumber++} ${testGroupName} - req.body - 400`, done => {
                    chai
                        .request(app)
                        .put(entityEndpointToTest)
                        .send(we)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(400);
                            done();
                        });
                });
            });
        });

        // PUT invalid email => 400
        describe('check property email', () => {
            let wrongEntities = [];

            let entity = { ...entityToCreate };
            entity.email = {};
            wrongEntities.push(entity);

            entity = { ...entityToCreate };
            entity.email = [];
            wrongEntities.push(entity);

            entity = { ...entityToCreate };
            entity.email = undefined;
            wrongEntities.push(entity);

            entity = { ...entityToCreate };
            entity.email = null;
            wrongEntities.push(entity);

            entity = { ...entityToCreate };
            entity.email = 12345;
            wrongEntities.push(entity);

            entity = { ...entityToCreate };
            entity.email = true;
            wrongEntities.push(entity);

            wrongEntities.forEach(we => {
                it(`${testNumber++} ${testGroupName} - email - 400`, done => {
                    chai
                        .request(app)
                        .put(entityEndpointToTest)
                        .send(we)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(400);
                            done();
                        });
                });
            });
        });
    });

    testGroupName = 'POST Password recovery';
    describe(testGroupName, () => {
        // POST expired => 400
        it(`${testNumber++} ${testGroupName} - expired - 400`, done => {
            chai
                .request(app)
                .post(entityEndpointToTest)
                .send({ new: 'new', recoverytoken: expiredReset.recoveryToken })
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(400);
                    done();
                });
        });

        // POST valid but not existing user => 204
        it(`${testNumber++} ${testGroupName} - valid but not existing user - 204`, done => {
            chai
                .request(app)
                .post(entityEndpointToTest)
                .send({ new: 'new', recoverytoken: invalidReset.recoveryToken })
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(204);
                    done();
                });
        });

        // POST not existing => 204
        it(`${testNumber++} ${testGroupName} - valid but not existing user - 204`, done => {
            chai
                .request(app)
                .post(entityEndpointToTest)
                .send({ new: 'new', recoverytoken: '0000000000000000000000000000000000000000' })
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(204);
                    done();
                });
        });

        // POST valid  => 204
        it(`${testNumber++} ${testGroupName} - valid - 204`, done => {
            chai
                .request(app)
                .post(entityEndpointToTest)
                .send({ new: 'new', recoverytoken: validReset.recoveryToken })
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(204);
                    done();
                });
        });

        // POST invalid req.body => 400
        describe('check req.body', () => {
            let wrongEntities = [];

            let entity;
            entity = {};
            wrongEntities.push(entity);

            entity = undefined
            wrongEntities.push(entity);

            entity = [];
            wrongEntities.push(entity);

            entity = 'hihii';
            wrongEntities.push(entity);

            entity = { ...entityToCreate };
            entity.notAllowed = 'hihihi';
            wrongEntities.push(entity);

            wrongEntities.forEach(we => {
                it(`${testNumber++} ${testGroupName} - req.body - 400`, done => {
                    chai
                        .request(app)
                        .post(entityEndpointToTest)
                        .send(we)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(400);
                            done();
                        });
                });
            });
        });
        // POST invalid new password => 400
        describe('check recoverytoken', () => {
            let wrongEntities = [];

            let entity = { ...entityToCreateSec };
            entity.new = {};
            wrongEntities.push(entity);

            entity = { ...entityToCreateSec };
            entity.new = [];
            wrongEntities.push(entity);

            entity = { ...entityToCreateSec };
            entity.new = undefined;
            wrongEntities.push(entity);

            entity = { ...entityToCreateSec };
            entity.new = null;
            wrongEntities.push(entity);

            entity = { ...entityToCreateSec };
            entity.new = true;
            wrongEntities.push(entity);

            wrongEntities.forEach(we => {
                it(`${testNumber++} ${testGroupName} - new - 400`, done => {
                    chai
                        .request(app)
                        .post(entityEndpointToTest)
                        .send(we)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(400);
                            done();
                        });
                });
            });
        });
        // POST invalid recoveryToken => 400
        describe('check property recoveryToken', () => {
            let wrongEntities = [];

            let entity = { ...entityToCreateSec };
            entity.recoverytoken = {};
            wrongEntities.push(entity);

            entity = { ...entityToCreateSec };
            entity.recoverytoken = [];
            wrongEntities.push(entity);

            entity = { ...entityToCreateSec };
            entity.recoverytoken = undefined;
            wrongEntities.push(entity);

            entity = { ...entityToCreateSec };
            entity.recoverytoken = null;
            wrongEntities.push(entity);

            entity = { ...entityToCreateSec };
            entity.recoverytoken = 12345;
            wrongEntities.push(entity);

            entity = { ...entityToCreateSec };
            entity.recoverytoken = true;
            wrongEntities.push(entity);

            entity = { ...entityToCreateSec };
            entity.recoverytoken = 123451234512345123451;
            wrongEntities.push(entity);

            entity = { ...entityToCreateSec };
            entity.recoverytoken = 1234512345123451234;
            wrongEntities.push(entity);

            wrongEntities.forEach(we => {
                it(`${testNumber++} ${testGroupName} - recoveryToken- 400`, done => {
                    chai
                        .request(app)
                        .post(entityEndpointToTest)
                        .send(we)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(400);
                            done();
                        });
                });
            });
        });
    });
});