import chai from 'chai';
import chaiHttp from 'chai-http';
import app from '../server.js';

import { healthCheck } from './00-startup-hook.js';

const { expect } = chai;

const loginEndpoint = '/api/v1/login';
const userEndpoint = '/api/v1/users';
const periodsEndpoint = '/api/v1/periods';
const coursesEndpoinToTest = '/api/v1/courses';

let user = {
    username: "courseUser",
    email: "courseUser@gmail.com",
    password: "pswrd",
    lastname: "mock-data",
    firstname: "course"
}

let secUser = {
    username: "courseUser2",
    email: "courseUser2@gmail.com",
    password: "pswrd",
    lastname: "mock-data",
    firstname: "course"
}

let courseToCreate = {
    "label": "NVS 4AHIF",
    "subjectLabel": "NVS",
    "pupilGroupLabel": "Gruppe A (die besseren)",
    "notes": "test"
}

let periodToCreate = {
    'label': 'Schuljahr 2021/22 Course',
    'startdate': new Date('01.09.2050'),
    'enddate': new Date('01.07.2051'),
    'state': 'active'
};

let periodToCreateSecUser = {
    'label': 'Schuljahr 2022/23 Course2',
    'startdate': new Date('01.09.2051'),
    'enddate': new Date('01.07.2052'),
    'state': 'active'
};

let neededKeys = ['_id', 'label', 'subjectLabel', 'pupilGroupLabel', 'notes', 'periodId']
let invalidId = '61a8ea9bd4022b3534077c0d'

chai.use(chaiHttp);
chai.should();

// hook - done() fires, if healthcheck point answers with 200
before(() => {
    return healthCheck(app);
});

describe(`CRUD Tests for ${coursesEndpoinToTest}`, () => {
    let testGroupName;
    let testNumber = 4000;

    testGroupName = "Basic user stuff"
    describe(testGroupName, () => {
        it(`${testNumber++} ${testGroupName} - No Auth. header- 401 `, done => {
            chai
                .request(app)
                .get(coursesEndpoinToTest)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(401);
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - Create first user - 201`, done => {
            chai
                .request(app)
                .post(userEndpoint)
                .send(user)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(201);
                    res.body.should.be.a('object');

                    res.body.should.have.property('_id').not.to.be.null;
                    res.body.should.have.property('username').not.to.be.null;
                    res.body.should.have.property('state').not.to.be.null;
                    res.body.should.have.property('firstname').not.to.be.null;
                    res.body.should.have.property('lastname').not.to.be.null;

                    user._id = res.body._id;
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - Login first user - 200`, (done) => {
            chai
                .request(app)
                .post(loginEndpoint)
                .send({ "username": user["username"], "password": user["password"] })
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(1);
                    res.body.should.have.property('token');
                    user.token = res.body.token;
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - Create second user - 201`, done => {
            chai
                .request(app)
                .post(userEndpoint)
                .send(secUser)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(201);
                    res.body.should.be.a('object');

                    res.body.should.have.property('_id').not.to.be.null;
                    res.body.should.have.property('username').not.to.be.null;
                    res.body.should.have.property('state').not.to.be.null;
                    res.body.should.have.property('firstname').not.to.be.null;
                    res.body.should.have.property('lastname').not.to.be.null;

                    secUser._id = res.body._id;
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - Login second user - 200`, (done) => {
            chai
                .request(app)
                .post(loginEndpoint)
                .send({ "username": secUser["username"], "password": secUser["password"] })
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(1);
                    res.body.should.have.property('token');
                    secUser.token = res.body.token;
                    done();
                });
        });
    })

    testGroupName = 'POST'
    describe(testGroupName, () => {
        describe('check basic test cases', () => {
            it(`${testNumber++} ${testGroupName} - Create course without period - 400`, done => {
                chai
                    .request(app)
                    .post(coursesEndpoinToTest)
                    .set('Authorization', user.token)
                    .send(courseToCreate)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(400);
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - create period - 201`, done => {
                chai
                    .request(app)
                    .post(periodsEndpoint)
                    .send(periodToCreate)
                    .set('Authorization', user.token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(201);
                        res.body.should.be.a('object');
                        Object.keys(res.body).length.should.eql(5);

                        periodToCreate._id = res.body._id;
                        courseToCreate.periodId = periodToCreate._id;
                        done();
                    });
            });

            // in order to perform get all on second user active period is needed
            it(`${testNumber++} ${testGroupName} - create period for second user - 201`, done => {
                chai
                    .request(app)
                    .post(periodsEndpoint)
                    .send(periodToCreateSecUser)
                    .set('Authorization', secUser.token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(201);
                        res.body.should.be.a('object');
                        Object.keys(res.body).length.should.eql(5);

                        periodToCreateSecUser._id = res.body._id;
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - Create course - 201`, done => {
                chai
                    .request(app)
                    .post(coursesEndpoinToTest)
                    .set('Authorization', user.token)
                    .send(courseToCreate)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(201);
                        res.body.should.be.a('object');

                        neededKeys.forEach(key => {
                            res.body.should.have.property(key).to.not.be.null
                        })

                        courseToCreate._id = res.body._id;
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - GET created course - 200 `, done => {
                chai
                    .request(app)
                    .get(coursesEndpoinToTest + '/' + courseToCreate._id)
                    .set('Authorization', user.token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        res.body.should.be.a('object');

                        neededKeys.forEach(key => res.body.should.have.property(key).to.not.be.null)
                        expect(res.body._id).to.be.equal(courseToCreate._id);
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - Delete created course - 204`, done => {
                chai
                    .request(app)
                    .delete(coursesEndpoinToTest + '/' + courseToCreate._id)
                    .set('Authorization', user.token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(204);

                        courseToCreate._id = undefined;
                        done();
                    });
            });

            /* --------------------- property tests --------------------- */

            /* --------------------------- id -------------------------- */
            describe('check property id ', () => {
                let wrongCourses = [];

                let course = { ...courseToCreate };
                course._id = {};
                wrongCourses.push(course);

                course = { ...courseToCreate };
                course._id = 12432434;
                wrongCourses.push(course);

                course = { ...courseToCreate };
                course._id = 'JALKJ23kJ21';
                wrongCourses.push(course);

                course = { ...courseToCreate };
                course._id = invalidId;
                wrongCourses.push(course);

                wrongCourses.forEach(wrs => {
                    it(`${testNumber++} ${testGroupName} - id - 400`, done => {
                        chai
                            .request(app)
                            .post(coursesEndpoinToTest)
                            .set('Authorization', user.token)
                            .send(wrs)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- label -------------------------- */
            describe('check property label ', () => {
                let wrongCourses = [];

                let course = { ...courseToCreate };
                course.label = {};
                wrongCourses.push(course);

                course = { ...courseToCreate };
                course.label = undefined;
                wrongCourses.push(course);

                course = { ...courseToCreate };
                course.label = null;
                wrongCourses.push(course);

                wrongCourses.forEach(wrs => {
                    it(`${testNumber++} ${testGroupName} - label - 400`, done => {
                        chai
                            .request(app)
                            .post(coursesEndpoinToTest)
                            .set('Authorization', user.token)
                            .send(wrs)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- subjectLabel -------------------------- */
            describe('check property subjectLabel ', () => {
                let wrongCourses = [];

                let course = { ...courseToCreate };
                course.subjectLabel = {};
                wrongCourses.push(course);

                course = { ...courseToCreate };
                course.subjectLabel = [];
                wrongCourses.push(course);

                wrongCourses.forEach(wrs => {
                    it(`${testNumber++} ${testGroupName} - subjectLabel - 400`, done => {
                        chai
                            .request(app)
                            .post(coursesEndpoinToTest)
                            .set('Authorization', user.token)
                            .send(wrs)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- pubilGroupLabel -------------------------- */
            describe('check property pubilGroupLabel ', () => {
                let wrongCourses = [];

                let course = { ...courseToCreate };
                course.pupilGroupLabel = {};
                wrongCourses.push(course);

                course = { ...courseToCreate };
                course.pupilGroupLabel = [];
                wrongCourses.push(course);

                wrongCourses.forEach(wrs => {
                    it(`${testNumber++} ${testGroupName} - pubilGroupLabel - 400`, done => {
                        chai
                            .request(app)
                            .post(coursesEndpoinToTest)
                            .set('Authorization', user.token)
                            .send(wrs)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- notes -------------------------- */
            describe('check property notes ', () => {
                let wrongCourses = [];

                let course = { ...courseToCreate };
                course.notes = {};
                wrongCourses.push(course);

                wrongCourses.forEach(wrs => {
                    it(`${testNumber++} ${testGroupName} - notes - 400`, done => {
                        chai
                            .request(app)
                            .post(coursesEndpoinToTest)
                            .set('Authorization', user.token)
                            .send(wrs)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });


            /* --------------------------- user id -------------------------- */
            describe('check property user id ', () => {
                let wrongCourses = [];

                let course = { ...courseToCreate };
                course.userId = {};
                wrongCourses.push(course);

                course = { ...courseToCreate };
                course.userId = 12344;
                wrongCourses.push(course);

                course = { ...courseToCreate };
                course.userId = '12344';
                wrongCourses.push(course);

                course = { ...courseToCreate };
                course.userId = invalidId;
                wrongCourses.push(course);

                wrongCourses.forEach(wrs => {
                    it(`${testNumber++} ${testGroupName} - user id - 400`, done => {
                        chai
                            .request(app)
                            .post(coursesEndpoinToTest)
                            .set('Authorization', user.token)
                            .send(wrs)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- period id -------------------------- */
            describe('check property period id ', () => {
                let wrongCourses = [];

                let course = { ...courseToCreate };
                course.periodId = {};
                wrongCourses.push(course);

                course = { ...courseToCreate };
                course.periodId = 12344;
                wrongCourses.push(course);

                course = { ...courseToCreate };
                course.periodId = '12344';
                wrongCourses.push(course);

                course = { ...courseToCreate };
                course.periodId = invalidId + 'askdj';
                wrongCourses.push(course);

                wrongCourses.forEach(wrs => {
                    it(`${testNumber++} ${testGroupName} - period id - 400`, done => {
                        chai
                            .request(app)
                            .post(coursesEndpoinToTest)
                            .set('Authorization', user.token)
                            .send(wrs)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });


            /* --------------------------- missing/not allowed properies-------------------------- */
            describe('missing/not allowed properies', () => {
                let wrongCourses = [];

                let course = { ...courseToCreate };
                delete course.label;
                wrongCourses.push(course);

                course = { ...courseToCreate };
                course.notAllowed = 'hihihi'
                wrongCourses.push(course);

                course = { ...courseToCreate };
                course.pupils = [];
                wrongCourses.push(course);

                wrongCourses.forEach(wrs => {
                    it(`${testNumber++} ${testGroupName} - missing/not allowed properies - 400`, done => {
                        chai
                            .request(app)
                            .post(coursesEndpoinToTest)
                            .set('Authorization', user.token)
                            .send(wrs)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });
        });
    });

    testGroupName = 'GET SINGLE'
    describe(testGroupName, () => {
        it(`${testNumber++} ${testGroupName} - Create course - 201`, done => {
            chai
                .request(app)
                .post(coursesEndpoinToTest)
                .set('Authorization', user.token)
                .send(courseToCreate)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(201);
                    res.body.should.be.a('object');

                    neededKeys.forEach(key => res.body.should.have.property(key).to.not.be.null)

                    courseToCreate._id = res.body._id;
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - No Auth. header - 401 `, done => {
            chai
                .request(app)
                .get(coursesEndpoinToTest + '/' + courseToCreate._id)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(401);
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - GET created course - 200 `, done => {
            chai
                .request(app)
                .get(coursesEndpoinToTest + '/' + courseToCreate._id)
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');

                    neededKeys.forEach(key => res.body.should.have.property(key).to.not.be.null)

                    expect(res.body._id).to.be.equal(courseToCreate._id);
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - not found - 404 `, done => {
            chai
                .request(app)
                .get(coursesEndpoinToTest + '/' + invalidId)
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(404);
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - get with user 2 - 404 `, done => {
            chai
                .request(app)
                .get(coursesEndpoinToTest + '/' + courseToCreate._id)
                .set('Authorization', secUser.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(404);
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - invalid ObjectId - 400`, done => {
            chai
                .request(app)
                .get(coursesEndpoinToTest + '/1870')
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(400);
                    done();
                })
        });
    });

    testGroupName = 'GET ALL';
    describe(testGroupName, () => {
        it(`${testNumber++} ${testGroupName} - No Auth. header - 401`, done => {
            chai
                .request(app)
                .get(coursesEndpoinToTest)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(401);
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - first user - 200`, done => {
            chai
                .request(app)
                .get(coursesEndpoinToTest)
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;

                    res.should.have.status(200);
                    res.body.should.be.a('array');

                    expect(res.body.length).to.equal(1);

                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - get with user 2 - 200 `, done => {
            chai
                .request(app)
                .get(coursesEndpoinToTest)
                .set('Authorization', secUser.token)
                .end((err, res) => {
                    expect(err).to.be.null;

                    res.should.have.status(200);
                    res.body.should.be.a('array');

                    expect(res.body.length).to.equal(0);
                    done();
                });
        });
    });

    testGroupName = 'PATCH';
    describe(testGroupName, () => {
        describe('basic test cases', () => {
            it(`${testNumber++} ${testGroupName} - No Auth. header - 401`, done => {
                chai
                    .request(app)
                    .patch(coursesEndpoinToTest + '/' + courseToCreate._id)
                    .send({ ...courseToCreate })
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(401);
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - invalid ObjectId - 400`, done => {
                chai
                    .request(app)
                    .patch(coursesEndpoinToTest + '/1870')
                    .send({ ...courseToCreate })
                    .set('Authorization', user.token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(400);
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - not found - 404`, done => {
                let course = { ...courseToCreate };
                delete course._id;
                chai
                    .request(app)
                    .patch(coursesEndpoinToTest + '/' + invalidId)
                    .set('Authorization', user.token)
                    .send(course)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(404);
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - course with other user - 404`, done => {
                chai
                    .request(app)
                    .patch(coursesEndpoinToTest + '/' + courseToCreate._id)
                    .set('Authorization', secUser.token)
                    .send({ ...courseToCreate })
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(404);
                        done();
                    });
            });
        });

        describe('property tests', () => {
            describe('check property id ', () => {
                let wrongCourses = [];

                let course = { ...courseToCreate };
                course._id = {};
                wrongCourses.push(course);

                course = { ...courseToCreate };
                course._id = 12432434;
                wrongCourses.push(course);

                course = { ...courseToCreate };
                course._id = 'JALKJ23kJ21';
                wrongCourses.push(course);

                course = { ...courseToCreate };
                course._id = invalidId;
                wrongCourses.push(course);

                wrongCourses.forEach(wrs => {
                    it(`${testNumber++} ${testGroupName} - id - 400`, done => {
                        chai
                            .request(app)
                            .patch(coursesEndpoinToTest + '/' + courseToCreate._id)
                            .set('Authorization', user.token)
                            .send(wrs)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- label -------------------------- */
            describe('check property label ', () => {
                let wrongCourses = [];

                let course = { ...courseToCreate };
                delete course._id;
                course.label = {};
                wrongCourses.push(course);

                course = { ...courseToCreate };
                delete course._id;
                course.label = null;
                wrongCourses.push(course);

                wrongCourses.forEach(wrs => {
                    it(`${testNumber++} ${testGroupName} - label - 400`, done => {
                        chai
                            .request(app)
                            .patch(coursesEndpoinToTest + '/' + courseToCreate._id)
                            .set('Authorization', user.token)
                            .send(wrs)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- subjectLabel -------------------------- */
            describe('check property subjectLabel ', () => {
                let wrongCourses = [];

                let course = { ...courseToCreate };
                delete course._id;
                course.subjectLabel = {};
                wrongCourses.push(course);

                wrongCourses.forEach(wrs => {
                    it(`${testNumber++} ${testGroupName} - subjectLabel - 400`, done => {
                        chai
                            .request(app)
                            .patch(coursesEndpoinToTest + '/' + courseToCreate._id)
                            .set('Authorization', user.token)
                            .send(wrs)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- pubilGroupLabel -------------------------- */
            describe('check property pubilGroupLabel ', () => {
                let wrongCourses = [];

                let course = { ...courseToCreate };
                delete course._id;
                course.pupilGroupLabel = {};
                wrongCourses.push(course);

                wrongCourses.forEach(wrs => {
                    it(`${testNumber++} ${testGroupName} - pubilGroupLabel - 400`, done => {
                        chai
                            .request(app)
                            .patch(coursesEndpoinToTest + '/' + courseToCreate._id)
                            .set('Authorization', user.token)
                            .send(wrs)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- notes -------------------------- */
            describe('check property notes ', () => {
                let wrongCourses = [];

                let course = { ...courseToCreate };
                delete course._id;
                course.notes = {};
                wrongCourses.push(course);

                wrongCourses.forEach(wrs => {
                    it(`${testNumber++} ${testGroupName} - notes - 400`, done => {
                        chai
                            .request(app)
                            .patch(coursesEndpoinToTest + '/' + courseToCreate._id)
                            .set('Authorization', user.token)
                            .send(wrs)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });


            /* --------------------------- user id -------------------------- */
            describe('check property user id ', () => {
                let wrongCourses = [];

                let course = { ...courseToCreate };
                delete course._id;
                course.userId = {};
                wrongCourses.push(course);

                course = { ...courseToCreate };
                delete course._id;
                course.userId = 12344;
                wrongCourses.push(course);

                course = { ...courseToCreate };
                delete course._id;
                course.userId = '12344';
                wrongCourses.push(course);

                course = { ...courseToCreate };
                delete course._id;
                course.userId = invalidId;
                wrongCourses.push(course);

                wrongCourses.forEach(wrs => {
                    it(`${testNumber++} ${testGroupName} - user id - 400`, done => {
                        chai
                            .request(app)
                            .patch(coursesEndpoinToTest + '/' + courseToCreate._id)
                            .set('Authorization', user.token)
                            .send(wrs)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });


            /* --------------------------- missing/not allowed properies-------------------------- */
            describe('missing/not allowed properies', () => {
                let wrongCourses = [];

                let course = { ...courseToCreate };
                course.notAllowed = 'hihihi'
                wrongCourses.push(course);

                wrongCourses.forEach(wrs => {
                    it(`${testNumber++} ${testGroupName} - missing/not allowed properies - 400`, done => {
                        chai
                            .request(app)
                            .patch(coursesEndpoinToTest + '/' + courseToCreate._id)
                            .set('Authorization', user.token)
                            .send(wrs)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });
        });
    });

    testGroupName = 'DELETE';
    describe(testGroupName, () => {
        it(`${testNumber++} ${testGroupName} - No Auth. header - 401`, done => {
            chai
                .request(app)
                .delete(coursesEndpoinToTest + '/' + courseToCreate._id)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(401);
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - invalid ObjectId - 400`, done => {
            chai
                .request(app)
                .delete(coursesEndpoinToTest + '/1870')
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(400);
                    done();
                })
        });

        it(`${testNumber++} ${testGroupName} - not found - 404`, done => {
            chai
                .request(app)
                .delete(coursesEndpoinToTest + '/' + invalidId)
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(404);
                    done();
                })
        });

        it(`${testNumber++} ${testGroupName} - course of other user - 404`, done => {
            chai
                .request(app)
                .delete(coursesEndpoinToTest + '/' + courseToCreate._id)
                .set('Authorization', secUser.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(404);
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - successful delete - 204`, done => {
            chai
                .request(app)
                .delete(coursesEndpoinToTest + '/' + courseToCreate._id)
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(204);
                    done();
                })
        });

        it(`${testNumber++} ${testGroupName} - get deleted item - 404`, done => {
            chai
                .request(app)
                .get(coursesEndpoinToTest + '/' + courseToCreate._id)
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(404);
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - delete again - 204`, done => {
            chai
                .request(app)
                .delete(coursesEndpoinToTest + '/' + courseToCreate._id)
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(404);
                    done();
                })
        });
    });
});