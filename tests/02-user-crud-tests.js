import chai from 'chai';
import chaiHttp from 'chai-http';
import app from '../server.js';

import { healthCheck } from './00-startup-hook.js';
import './01-authenthication.js';

const { expect } = chai;

chai.use(chaiHttp);
chai.should();

const loginEndpoint = '/api/v1/login';
const entityEndpoint = '/api/v1/users';
let authInfo;

const foundUser = {
    _id: "5fce46cc305c2d245480a74e",
    username: "foundUser",
    email: "foundUser@gmail.com",
    password: "pswrd",
    lastname: "user-mock-data",
    firstname: "found"
}
const updateUser = foundUser;
const deleteUser = {
    _id: "5fce46cc305c2d245480a74f",
    username: "deleteUser",
    email: "deleteUser@gmail.com",
    password: "pswrd",
    lastname: "user-mock-data",
    firstname: "delete"
};
const userToCreate = {
    username: "userCrudTests",
    email: "userCrudTests1@gmail.com",
    password: "pswrdCrud",
    lastname: "user",
    firstname: "crud-tests"
}

const masterUserId = "5fce46cc305c2d245480a74d";
const notFoundId = '5ace46cc305c2d245480a74d';
const toDeleteId = '5fce46cc305c2d245480a74f';
const updateId = foundUser._id;
const foundId = updateId;
let createId;

// hook - done() fires, if healthcheck point answers with 200
before(() => {
    return healthCheck(app);
});

describe(`CRUD Tests for ${entityEndpoint}`, () => {
    let testGroupName = 'GET SINGLE';
    let testNumber = 2000;
    describe(testGroupName, () => {
        it(`${testNumber++} ${testGroupName} - Login found User - 200`, (done) => {
            chai
                .request(app)
                .post(loginEndpoint)
                .send({ "username": foundUser["username"], "password": foundUser["password"] })
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(1);
                    res.body.should.have.property('token');
                    authInfo = res.body.token;
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - 200`, done => {
            chai
                .request(app)
                .get(entityEndpoint + '/' + foundId)
                .set('Authorization', authInfo)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(6);

                    res.body.should.have.property('_id');
                    expect(res.body._id).to.equal(foundId);

                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - No Auth. header - 401`, done => {
            chai
                .request(app)
                .get(entityEndpoint + '/' + foundId)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(401);
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - invalid ObjectId - 400`, done => {
            chai
                .request(app)
                .get(entityEndpoint + '/abc')
                .set('Authorization', authInfo)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(400);
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - foundUser gets masterUser - 401`, done => {
            chai
                .request(app)
                .get(entityEndpoint + '/' + masterUserId)
                .set('Authorization', authInfo)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(401);
                    done();
                });
        });
    });

    testGroupName = 'DELETE';
    describe(testGroupName, () => {
        it(`${testNumber++} ${testGroupName} - Login delete User - 200`, (done) => {
            chai
                .request(app)
                .post(loginEndpoint)
                .send({ "username": deleteUser["username"], "password": deleteUser["password"] })
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(1);
                    res.body.should.have.property('token');
                    authInfo = res.body.token;
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - No Auth. header - 401`, done => {
            chai
                .request(app)
                .delete(entityEndpoint + '/' + toDeleteId)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(401);
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - invalid ObjectId - 400`, done => {
            chai
                .request(app)
                .delete(entityEndpoint + '/abc')
                .set('Authorization', authInfo)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(400);
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - foundUsere deletes masteruser - 401`, done => {
            chai
                .request(app)
                .delete(entityEndpoint + '/' + masterUserId)
                .set('Authorization', authInfo)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(401);
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - 204`, done => {
            chai
                .request(app)
                .delete(entityEndpoint + '/' + toDeleteId)
                .set('Authorization', authInfo)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(204);
                    done();
                });
        });


        it(`${testNumber++} ${testGroupName} - check delete - 401`, done => {
            chai
                .request(app)
                .get(entityEndpoint + '/' + toDeleteId)
                .set('Authorization', authInfo)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(401);
                    done();
                });
        });
    });

    testGroupName = 'POST';
    describe(testGroupName, () => {
        describe('check basic test cases ', () => {
            it(`${testNumber++} ${testGroupName} - Create user - 201`, done => {
                chai
                    .request(app)
                    .post(entityEndpoint)
                    .send(userToCreate)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(201);
                        res.body.should.be.a('object');

                        res.body.should.have.property('_id').not.to.be.null;
                        res.body.should.have.property('username').not.to.be.null;
                        res.body.should.have.property('state').not.to.be.null;
                        res.body.should.have.property('firstname').not.to.be.null;
                        res.body.should.have.property('lastname').not.to.be.null;

                        createId = res.body._id;
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - Login created User - 200`, (done) => {
                chai
                    .request(app)
                    .post(loginEndpoint)
                    .send({ "username": userToCreate["username"], "password": userToCreate["password"] })
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        Object.keys(res.body).length.should.eql(1);
                        res.body.should.have.property('token');
                        authInfo = res.body.token;
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - GET created user - 200 `, done => {
                chai
                    .request(app)
                    .get(entityEndpoint + '/' + createId)
                    .set('Authorization', authInfo)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        res.body.should.be.a('object');

                        res.body.should.have.property('_id').not.to.be.null;
                        res.body.should.have.property('username').not.to.be.null;
                        res.body.should.have.property('state').not.to.be.null;
                        res.body.should.have.property('firstname').not.to.be.null;
                        res.body.should.have.property('lastname').not.to.be.null;

                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - Delete created User - 204`, done => {
                chai
                    .request(app)
                    .delete(entityEndpoint + '/' + createId)
                    .set('Authorization', authInfo)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(204);
                        done();
                    });
            });

        });

        /* --------------------- property tests --------------------- */

        /* --------------------------- username -------------------------- */
        describe('check property username ', () => {
            let wrongUsers = [];

            let u = { ...userToCreate };
            u.username = {};
            wrongUsers.push(u);

            u = { ...userToCreate };
            u.username = "masterUser";
            wrongUsers.push(u);

            wrongUsers.forEach(wrongUser => {
                it(`${testNumber++} ${testGroupName} - Username - 400`, done => {
                    chai
                        .request(app)
                        .post(entityEndpoint)
                        .send(wrongUser)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(400);
                            done();
                        });
                });
            });
        });

        /* ----------------------------- email -------------------------- */
        describe('check property email ', () => {
            let wrongUsers = [];
            let u = { ...userToCreate };
            u.email = "";
            wrongUsers.push(u);

            u = { ...userToCreate };
            u.email = [];
            wrongUsers.push(u);

            u = { ...userToCreate };
            u.email = {};
            wrongUsers.push(u);

            u = { ...userToCreate };
            u.email = 12456;
            wrongUsers.push(u);

            u = { ...userToCreate };
            u.email = '@gmail.com';
            wrongUsers.push(u);

            u = { ...userToCreate };
            u.email = 'test@gmail.';
            wrongUsers.push(u);

            u = { ...userToCreate };
            u.email = 'test@.com';
            wrongUsers.push(u);

            u = { ...userToCreate };
            u.email = '@gmail.';
            wrongUsers.push(u);

            u = { ...userToCreate };
            u.email = 'test@.';
            wrongUsers.push(u);

            u = { ...userToCreate };
            u.email = '@.com';
            wrongUsers.push(u);

            u = { ...userToCreate };
            u.email = '@.';
            wrongUsers.push(u);

            u = { ...userToCreate };
            u.email = 'test';
            wrongUsers.push(u);

            wrongUsers.forEach(wrongUser => {
                it(`${testNumber++} ${testGroupName} - email - 400`, done => {
                    chai
                        .request(app)
                        .post(entityEndpoint)
                        .send(wrongUser)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(400);
                            done();
                        });
                });
            });
        });

        /* --------------------------- state -------------------------- */
        describe('check property state ', () => {
            let wrongUsers = [];
            let u = { ...userToCreate };
            u.state = "Franz";
            wrongUsers.push(u);

            u = { ...userToCreate };
            u.state = {};
            wrongUsers.push(u);

            u = { ...userToCreate };
            u.state = "";
            wrongUsers.push(u);

            wrongUsers.forEach(wrongUser => {
                it(`${testNumber++} ${testGroupName} - State - 400`, done => {
                    chai
                        .request(app)
                        .post(entityEndpoint)
                        .send(wrongUser)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(400);
                            done();
                        });
                });
            });
        });

        /* --------------------------- firstname -------------------------- */
        describe('check property firstname ', () => {
            let wrongUsers = [];
            let u = { ...userToCreate };
            u.firstname = "";
            wrongUsers.push(u);

            u = { ...userToCreate };
            u.lastname = {};
            wrongUsers.push(u);

            wrongUsers.forEach(wrongUser => {
                it(`${testNumber++} ${testGroupName} - Firstname - 400`, done => {
                    chai
                        .request(app)
                        .post(entityEndpoint)
                        .send(wrongUser)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(400);
                            done();
                        });
                });
            });
        });

        /* --------------------------- lastname -------------------------- */
        describe('check property lastname ', () => {
            let wrongUsers = [];
            let u = { ...userToCreate };
            u.lastname = "";
            wrongUsers.push(u);

            u = { ...userToCreate };
            u.lastname = {};
            wrongUsers.push(u);

            wrongUsers.forEach(wrongUser => {
                it(`${testNumber++} ${testGroupName} - Lastname - 400`, done => {
                    chai
                        .request(app)
                        .post(entityEndpoint)
                        .send(wrongUser)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(400);
                            done();
                        });
                });
            });
        });


        /* --------------------- missing / wrong props ------------------- */
        describe('check missing / wrong props ', () => {
            let wrongUsers = [];
            let u = { ...userToCreate };
            delete u.lastname;
            wrongUsers.push(u);

            u = { ...userToCreate };
            delete u.firstname;
            wrongUsers.push(u);

            u = { ...userToCreate };
            delete u.email;
            wrongUsers.push(u);

            u = { ...userToCreate };
            delete u.password;
            wrongUsers.push(u);

            u = { ...userToCreate };
            u['wrongprop'] = 'hihi;'
            wrongUsers.push(u);


            wrongUsers.forEach(wrongUser => {
                it(`${testNumber++} ${testGroupName} - missing prop(s) - 400`, done => {
                    chai
                        .request(app)
                        .post(entityEndpoint)
                        .send(wrongUser)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(400);
                            done();
                        });
                });
            });
        });

        /* --------------------- successful creates ----------------------- */
        describe('check successful creates ', () => {
            it(`${testNumber++} ${testGroupName} - Create user with crazy name - 201`, done => {
                let u = { ...userToCreate };
                u.username = "  @  ";
                u.email = "test1@gmail.com";
                chai
                    .request(app)
                    .post(entityEndpoint)
                    .send(u)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(201);
                        res.body.should.be.a('object');

                        res.body.should.have.property('_id').not.to.be.null;
                        res.body.should.have.property('username').not.to.be.null;
                        res.body.should.have.property('state').not.to.be.null;
                        res.body.should.have.property('firstname').not.to.be.null;
                        res.body.should.have.property('lastname').not.to.be.null;

                        done();
                    });
            });
        });

        /* --------------------- special fails  ----------------------- */
        describe('check special fails ', () => {
            it(`${testNumber++} ${testGroupName} - Create user duplicate 1 - 201`, done => {
                chai
                    .request(app)
                    .post(entityEndpoint)
                    .send(userToCreate)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(201);
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - Create user duplicate 2 - 400`, done => {
                chai
                    .request(app)
                    .post(entityEndpoint)
                    .send(userToCreate)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(400);
                        done();
                    });
            });


            it(`${testNumber++} ${testGroupName} - Create user with id - 400`, done => {
                let u = { ...userToCreate };
                u._id = "5ece87cc87843db743cea878";
                u.username = "id@problem";
                chai
                    .request(app)
                    .post(entityEndpoint)
                    .send(u)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(400);
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - Create user lastname problem1 - 400`, done => {
                let u = { ...userToCreate };
                u.username = "lastname@Problem";
                u.lastname = "";
                chai
                    .request(app)
                    .post(entityEndpoint)
                    .send(u)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(400);
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - Create user lastname problem2 - 400`, done => {
                let u = { ...userToCreate };
                u.username = "lastname@Problem";
                u.lastname = [];
                chai
                    .request(app)
                    .post(entityEndpoint)
                    .send(u)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(400);
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - Create user firstname problem - 400`, done => {
                let u = { ...userToCreate };
                u.username = "firstname@Problem";
                u.firstname = [];
                chai
                    .request(app)
                    .post(entityEndpoint)
                    .send(u)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(400);
                        done();
                    });
            });
        });
    });

    testGroupName = 'PATCH';
    describe(testGroupName, () => {
        describe('basic test cases ', () => {
            it(`${testNumber++} ${testGroupName} - No Auth. header - 401`, done => {
                chai
                    .request(app)
                    .patch(entityEndpoint)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(401);
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - Login update User - 200`, (done) => {
                chai
                    .request(app)
                    .post(loginEndpoint)
                    .send({ "username": updateUser["username"], "password": updateUser["password"] })
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        Object.keys(res.body).length.should.eql(1);
                        res.body.should.have.property('token');
                        authInfo = res.body.token;
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - invalid ObjectId - 400`, done => {
                chai
                    .request(app)
                    .patch(entityEndpoint + '/abc')
                    .set('Authorization', authInfo)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(400);
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - 401`, done => {
                let payload = { 'firstname': 'Susi' };
                chai
                    .request(app)
                    .patch(entityEndpoint + '/' + notFoundId)
                    .set('Authorization', authInfo)
                    .send(payload)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(401);
                        done();
                    });
            });
        });

        /* --------------------- update username ------------------- */
        describe('property username ', () => {
            let wrongUsers = [];
            let u = {};
            u.username = {};
            wrongUsers.push(u);

            u = {};
            u.username = 'online';
            u['wrongprop'] = 'hihi;'
            wrongUsers.push(u);

            u = {};
            u.username = 'online';
            u._id = '5fce46ccbcf604e0928f6659';
            wrongUsers.push(u);

            wrongUsers.forEach(wrongUser => {
                it(`${testNumber++} ${testGroupName} - username - 400`, done => {
                    chai
                        .request(app)
                        .patch(entityEndpoint + '/' + updateId)
                        .send(wrongUser)
                        .set('Authorization', authInfo)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(400);
                            done();
                        });
                });
            });
        });


        /* ----------------------------- update email -------------------------- */
        describe('check property email ', () => {
            let wrongUsers = [];
            let u = {};
            u.email = "";
            wrongUsers.push(u);

            u = {};
            u.email = [];
            wrongUsers.push(u);

            u = {};
            u.email = {};
            wrongUsers.push(u);

            u = {};
            u.email = 12456;
            wrongUsers.push(u);

            u = {};
            u.email = '@gmail.com';
            wrongUsers.push(u);

            u = {};
            u.email = 'test@gmail.';
            wrongUsers.push(u);

            u = {};
            u.email = 'test@.com';
            wrongUsers.push(u);

            u = {};
            u.email = '@gmail.';
            wrongUsers.push(u);

            u = {};
            u.email = 'test@.';
            wrongUsers.push(u);

            u = {};
            u.email = '@.com';
            wrongUsers.push(u);

            u = {};
            u.email = '@.';
            wrongUsers.push(u);

            u = {};
            u.email = 'test';
            wrongUsers.push(u);

            wrongUsers.forEach(wrongUser => {
                it(`${testNumber++} ${testGroupName} - email - 400`, done => {
                    chai
                        .request(app)
                        .patch(entityEndpoint + '/' + updateId)
                        .set('Authorization', authInfo)
                        .send(wrongUser)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(400);
                            done();
                        });
                });
            });
        });

        /* --------------------- update password ------------------- */
        describe('property password ', () => {
            let wrongUsers = [];
            let u = {};
            u.password = {};
            wrongUsers.push(u);

            u = {};
            u.password = 'online';
            u['wrongprop'] = 'hihi;'
            wrongUsers.push(u);

            u = {};
            u.password = 'online';
            u._id = '5fce46ccbcf604e0928f6659';
            wrongUsers.push(u);

            u = {};
            u.password = '';
            wrongUsers.push(u);

            wrongUsers.forEach(wrongUser => {
                it(`${testNumber++} ${testGroupName} - password - 400`, done => {
                    chai
                        .request(app)
                        .patch(entityEndpoint + '/' + updateId)
                        .send(wrongUser)
                        .set('Authorization', authInfo)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(400);
                            done();
                        });
                });
            });
        });

        /* --------------------- update state ------------------- */
        describe('property state ', () => {
            let wrongUsers = [];
            let u = {};
            u.state = {};
            wrongUsers.push(u);

            u = {};
            u.state = 'nix gut';
            wrongUsers.push(u);

            u = {};
            u.state = 'active';
            u['wrongprop'] = 'hihi;'
            wrongUsers.push(u);

            u = {};
            u.state = 'active';
            u._id = '5fce46ccbcf604e0928f6659';
            wrongUsers.push(u);

            wrongUsers.forEach(wrongUser => {
                it(`${testNumber++} ${testGroupName} - state - 400`, done => {
                    chai
                        .request(app)
                        .patch(entityEndpoint + '/' + updateId)
                        .send(wrongUser)
                        .set('Authorization', authInfo)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(400);
                            done();
                        });
                });
            });
        });

        /* --------------------- update firstname ------------------- */
        describe('property firstname ', () => {
            let wrongUsers = [];
            let u = {};
            u.firstname = {};
            wrongUsers.push(u);

            u = {};
            u.firstname = 'online';
            u['wrongprop'] = 'hihi;'
            wrongUsers.push(u);

            u = {};
            u.firstname = 'online';
            u._id = '5fce46ccbcf604e0928f6659';
            wrongUsers.push(u);

            wrongUsers.forEach(wrongUser => {
                it(`${testNumber++} ${testGroupName} - firstname - 400`, done => {
                    chai
                        .request(app)
                        .patch(entityEndpoint + '/' + updateId)
                        .send(wrongUser)
                        .set('Authorization', authInfo)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(400);
                            done();
                        });
                });
            });
        });

        /* --------------------- update lastname ------------------- */
        describe('property lastname ', () => {
            let wrongUsers = [];
            let u = {};
            u.lastname = {};
            wrongUsers.push(u);

            u = {};
            u.lastname = 'online';
            u['wrongprop'] = 'hihi;'
            wrongUsers.push(u);

            u = {};
            u.lastname = 'online';
            u._id = '5fce46ccbcf604e0928f6659';
            wrongUsers.push(u);

            u = {};
            u.lastname = '';
            u.state = 'online';
            wrongUsers.push(u);

            wrongUsers.forEach(wrongUser => {
                it(`${testNumber++} ${testGroupName} - lastname - 400`, done => {
                    chai
                        .request(app)
                        .patch(entityEndpoint + '/' + updateId)
                        .send(wrongUser)
                        .set('Authorization', authInfo)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(400);
                            done();
                        });
                });
            });
        });


        /* ------------------- working updates ------------------- */
        describe('working updates ', () => {
            let correctUpdatUsers = [];
            let u = {};
            u.username = 'updated@chat.com';
            correctUpdatUsers.push(u);

            u = {};
            u.password = 'password';
            correctUpdatUsers.push(u);

            u = {};
            u.firstname = 'update';
            u.lastname = 'update';
            correctUpdatUsers.push(u);

            u = {};
            u.state = 'inactive';
            correctUpdatUsers.push(u);

            u = {};
            u._id = updateId;
            correctUpdatUsers.push(u);

            u = {};
            u._id = '5fce46ccf4f27b02c72f5489';
            u.username = 'Franklin@chat.app';
            u.password = 'changed';

            correctUpdatUsers.forEach(correctUser => {
                it(`${testNumber++} ${testGroupName} - correct update - 200`, done => {
                    chai
                        .request(app)
                        .patch(entityEndpoint + '/' + updateId)
                        .send(correctUser)
                        .set('Authorization', authInfo)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(200);

                            res.body.should.be.a('object');

                            Object.keys(res.body).length.should.eql(6);

                            res.body.should.have.property('_id');
                            expect(res.body._id).to.equal(updateId);
                            done();
                        });
                });
            });

            it(`${testNumber++} ${testGroupName} - GET and check updates resource - 200`, done => {
                chai
                    .request(app)
                    .get(entityEndpoint + '/' + updateId)
                    .set('Authorization', authInfo)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        res.body.should.be.a('object');

                        Object.keys(res.body).length.should.eql(6);

                        res.body.should.have.property('_id');
                        expect(res.body._id).to.equal(updateId);
                        expect(res.body.firstname).to.equal('update');
                        expect(res.body.lastname).to.equal('update');
                        expect(res.body.state).to.equal('inactive');
                        expect(res.body.username).to.equal('updated@chat.com');

                        done();
                    });
            });
        });

        describe('special fails', () => {
            let failingUsers = [];
            let u = {};
            failingUsers.push(u);

            u = {};
            u.username = "masterUser";
            failingUsers.push(u);

            failingUsers.forEach(failingUser => {
                it(`${testNumber++} ${testGroupName} - failing special update - 400`, done => {
                    chai
                        .request(app)
                        .patch(entityEndpoint + '/' + updateId)
                        .send(failingUser)
                        .set('Authorization', authInfo)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(400);
                            done();
                        });
                });
            });
        });
    });
});
