import chai from 'chai';
import chaiHttp from 'chai-http';
import app from '../server.js';

import Period from '../models/period-model.js';
import readJson from '../utils/readJson.js';
import { healthCheck } from './00-startup-hook.js';

import User from '../models/user-model.js';

const { expect } = chai;

const periodTestDataFile = './tests/period-mockdata.json';
const userTestDataFile = './tests/user-mockdata.json';

const loginEndpoint = '/api/v1/login';
const entityEndpoint = '/api/v1/periods';

const periodStates = {
    active: 'active',
    inactive: 'inactive',
    archived: 'archived'
};

const periodPropertyCount = 5;

let notFoundId = '607f11dbc03e786bada6d87c';
let createdId;

let periodToCreate = {
    'label': 'Schuljahr 2021/22',
    'startdate': new Date('01.09.2021'),
    'enddate': new Date('01.07.2022'),
    'state': 'active'
};

let updatedPeriod = {
    'label': 'updated'
};

let testNr = 3000;
let testGroupName;

let loginUsers = [];
let generatedPeriods = [];

chai.use(chaiHttp);
chai.should();

let periods = await readJson(periodTestDataFile);
periods.forEach(async p => {
    let period = new Period(p);
    generatedPeriods.push(period);
    await period.save();
});

let users = await readJson(userTestDataFile);
users.forEach(async u => {
    let user = new User(u);
    loginUsers.push({
        _id: u._id,
        credentials: {
            username: u.username,
            password: u.password
        }
    });
    await user.save();
});

before(() => {
    return healthCheck(app);
});

describe(`CRUD Tests for ${entityEndpoint}`, () => {
    testGroupName = 'LOGIN USERS';
    describe(testGroupName, async () => {
        loginUsers.forEach((u, idx) => {
            it(`${testNr++} LOGIN user ${idx + 1}`, done => {
                chai
                    .request(app)
                    .post(loginEndpoint)
                    .send(u.credentials)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        res.body.should.have.property('token');
                        u.token = res.body['token'];
                        done();
                    });
            });
        });
    });

    testGroupName = 'GET ALL';
    describe(testGroupName, () => {
        it(`${testNr++} ${testGroupName} - No Auth. header - 401`, done => {
            chai
                .request(app)
                .get(entityEndpoint)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(401);
                    done();
                });
        });

        loginUsers.forEach((u, idx) => {
            it(`${testNr++} ${testGroupName} - user ${idx + 1} - 200`, done => {
                chai
                    .request(app)
                    .get(entityEndpoint)
                    .set('Authorization', u.token)
                    .end((err, res) => {
                        expect(err).to.be.null;

                        res.should.have.status(200);
                        res.body.should.be.a('array');

                        expect(res.body.length).to.equal(3);
                        expect(res.body.every(period => { period.userId === u._id }));

                        done();
                    });
            });
        });
    });

    testGroupName = 'GET SINGLE';
    describe(testGroupName, () => {
        it(`${testNr++} ${testGroupName} - No Auth. header - 401`, done => {
            chai
                .request(app)
                .get(entityEndpoint + '/' + generatedPeriods[0]._id)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(401);
                    done();
                })
        });

        it(`${testNr++} ${testGroupName} - 200`, done => {
            chai
                .request(app)
                .get(entityEndpoint + '/' + generatedPeriods[0]._id)
                .set('Authorization', loginUsers[0].token)
                .end((err, res) => {
                    expect(err).to.be.null;

                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(periodPropertyCount);

                    res.body.should.have.property('_id');

                    expect(res.body.userId === loginUsers[0]._id);

                    done();
                });
        });

        it(`${testNr++} ${testGroupName} - not found - 404`, done => {
            chai
                .request(app)
                .get(entityEndpoint + '/' + notFoundId)
                .set('Authorization', loginUsers[0].token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(404);
                    done();
                })
        });

        it(`${testNr++} ${testGroupName} - get with user 2 - 404`, done => {
            chai
                .request(app)
                .get(entityEndpoint + '/' + generatedPeriods[0]._id)
                .send(periodToCreate)
                .set('Authorization', loginUsers[1].token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(404);
                    done();
                });
        });

        it(`${testNr++} ${testGroupName} - invalid ObjectId - 400`, done => {
            chai
                .request(app)
                .get(entityEndpoint + '/1870')
                .set('Authorization', loginUsers[0].token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(400);
                    done();
                })
        });
    });

    testGroupName = 'POST';
    describe(testGroupName, () => {
        describe('basic test cases', () => {
            it(`${testNr++} ${testGroupName} - No Auth. header - 401`, done => {
                chai
                    .request(app)
                    .post(entityEndpoint)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(401);
                        done();
                    });
            });

            it(`${testNr++} ${testGroupName} - CREATE Period User 1 - 201`, done => {
                chai
                    .request(app)
                    .post(entityEndpoint)
                    .send(periodToCreate)
                    .set('Authorization', loginUsers[0].token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(201);
                        res.body.should.be.a('object');
                        Object.keys(res.body).length.should.eql(periodPropertyCount);

                        expect(res.body.state).to.eql(periodStates.active);
                        createdId = res.body._id;
                        done();
                    });
            });

            it(`${testNr++} ${testGroupName} - only one active period - 200`, done => {
                chai
                    .request(app)
                    .get(entityEndpoint)
                    .set('Authorization', loginUsers[0].token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        expect(res.body.filter((period) => period.state == periodStates.active).length).to.eql(1);

                        done();
                    })
            });

            it(`${testNr++} ${testGroupName} - GET created period User 1 - 200`, done => {
                chai
                    .request(app)
                    .get(entityEndpoint + '/' + createdId)
                    .set('Authorization', loginUsers[0].token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);

                        done();
                    });
            });

            it(`${testNr++} ${testGroupName} - GET created period User 2 - 404`, done => {
                chai
                    .request(app)
                    .get(entityEndpoint + '/' + createdId)
                    .send(periodToCreate)
                    .set('Authorization', loginUsers[1].token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(404);
                        done();
                    });
            });
        });

        describe('property tests', () => {
            describe('property userId', () => {
                let wrongPeriod = { ...periodToCreate };
                wrongPeriod.userId = notFoundId;

                it(`${testNr++} ${testGroupName} - insert period with userId - 400`, done => {
                    chai
                        .request(app)
                        .post(entityEndpoint)
                        .send(wrongPeriod)
                        .set('Authorization', loginUsers[0].token)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(400);
                            done();
                        });
                });
            });

            describe('property label', () => {
                let wrongPeriods = [];
                let period = { ...periodToCreate };
                period.label = {};
                wrongPeriods.push(period);

                period = { ...periodToCreate };
                period.label = [];
                wrongPeriods.push(period);

                wrongPeriods.forEach((p) => {
                    it(`${testNr++} ${testGroupName} - wrong label - 400`, done => {
                        chai
                            .request(app)
                            .post(entityEndpoint)
                            .send(p)
                            .set('Authorization', loginUsers[0].token)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            describe('date properties', () => {
                let wrongPeriods = [];
                let period = { ...periodToCreate };
                period.startdate = new Date('02.02.2021');
                period.enddate = new Date('01.01.2021');
                wrongPeriods.push(period);

                period = { ...periodToCreate };
                period.startdate = 'abc';
                wrongPeriods.push(period);

                period = { ...periodToCreate };
                period.enddate = 'abc';
                wrongPeriods.push(period);

                period = { ...periodToCreate };
                period.startdate = '000';
                wrongPeriods.push(period);

                period = { ...periodToCreate };
                period.enddate = '000';
                wrongPeriods.push(period);

                period = { ...periodToCreate };
                period.startdate = {};
                wrongPeriods.push(period);

                period = { ...periodToCreate };
                period.enddate = {};
                wrongPeriods.push(period);

                wrongPeriods.forEach((p, idx) => {
                    p.label = `period ${testNr}:${idx}`;
                    it(`${testNr++} ${testGroupName} - wrong dates - 400`, done => {
                        chai
                            .request(app)
                            .post(entityEndpoint)
                            .send(p)
                            .set('Authorization', loginUsers[0].token)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            describe('property state', () => {
                let wrongPeriods = [];
                let period = { ...periodToCreate };
                period.state = true;
                wrongPeriods.push(period);

                period = { ...periodToCreate };
                period.state = 'abc';
                wrongPeriods.push(period);

                period = { ...periodToCreate };
                period.state = 187;
                wrongPeriods.push(period);

                period = { ...periodToCreate };
                period.state = {};
                wrongPeriods.push(period);

                period = { ...periodToCreate };
                period.state = [];
                wrongPeriods.push(period);

                wrongPeriods.forEach(p => {
                    it(`${testNr++} ${testGroupName} - wrong state property - 400`, done => {
                        chai
                            .request(app)
                            .post(entityEndpoint)
                            .send(p)
                            .set('Authorization', loginUsers[0].token)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            describe('missing/not allowed properties', () => {
                let wrongPeriods = [];
                let period = { ...periodToCreate };
                delete period.label;
                wrongPeriods.push(period);

                period = { ...periodToCreate };
                delete period.startdate;
                wrongPeriods.push(period);

                period = { ...periodToCreate };
                delete period.enddate;
                wrongPeriods.push(period);

                period = { ...periodToCreate };
                delete period.state;
                wrongPeriods.push(period);

                period = { ...periodToCreate };
                period['notvalid'] = '187gang';
                wrongPeriods.push(period);

                wrongPeriods.forEach(p => {
                    it(`${testNr++} ${testGroupName} - missing/wrong prop(s) - 400`, done => {
                        chai
                            .request(app)
                            .post(entityEndpoint)
                            .set('Authorization', loginUsers[0].token)
                            .send(p)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });
        });

        describe('special cases', () => {
            it(`${testNr++} ${testGroupName} - create period with id - 400`, done => {
                let p = { ...periodToCreate };
                p.label = `period ${testGroupName}:${testNr}`;
                p._id = notFoundId;
                chai
                    .request(app)
                    .post(entityEndpoint)
                    .set('Authorization', loginUsers[0].token)
                    .send(p)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(400);
                        done();
                    });
            });

            it(`${testNr++} ${testGroupName} - create duplicate period - 400`, done => {
                chai
                    .request(app)
                    .post(entityEndpoint)
                    .set('Authorization', loginUsers[0].token)
                    .send(periodToCreate)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(400);
                        done();
                    });
            });

            it(`${testNr++} ${testGroupName} - create period with user 2 - 200`, done => {
                chai
                    .request(app)
                    .post(entityEndpoint)
                    .set('Authorization', loginUsers[1].token)
                    .send(periodToCreate)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(201);
                        res.body.should.be.a('object');
                        Object.keys(res.body).length.should.eql(periodPropertyCount);

                        expect(res.body.state).to.eql(periodStates.active);
                        done();
                    });
            });
        });
    });

    testGroupName = 'PATCH';
    describe(testGroupName, () => {
        describe('basic test cases', () => {
            it(`${testNr++} ${testGroupName} - No Auth. header - 401`, done => {
                chai
                    .request(app)
                    .patch(entityEndpoint + '/' + createdId)
                    .send(updatedPeriod)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(401);
                        done();
                    });
            });

            it(`${testNr++} ${testGroupName} - invalid ObjectId - 400`, done => {
                chai
                    .request(app)
                    .patch(entityEndpoint + '/1870')
                    .send(updatedPeriod)
                    .set('Authorization', loginUsers[0].token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(400);
                        done();
                    });
            });

            it(`${testNr++} ${testGroupName} - not found - 404`, done => {
                chai
                    .request(app)
                    .patch(entityEndpoint + '/' + notFoundId)
                    .set('Authorization', loginUsers[0].token)
                    .send(updatedPeriod)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(404);
                        done();
                    });
            });

            it(`${testNr++} ${testGroupName} - period of other user - 404`, done => {
                chai
                    .request(app)
                    .patch(entityEndpoint + '/' + createdId)
                    .set('Authorization', loginUsers[1].token)
                    .send(updatedPeriod)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(404);
                        done();
                    });
            });
        });

        describe('property tests', () => {
            describe('property userId', () => {
                let wrongPeriods = [];
                let period = {};
                period.userId = notFoundId;
                wrongPeriods.push(period);

                wrongPeriods.forEach(p => {
                    it(`${testNr++} ${testGroupName} - update userId - 400`, done => {
                        chai
                            .request(app)
                            .patch(entityEndpoint + '/' + createdId)
                            .set('Authorization', loginUsers[0].token)
                            .send(p)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            describe('property label', () => {
                let wrongPeriods = [];
                let period = {};
                period.label = {};
                wrongPeriods.push(period);

                period = {};
                period.label = [];
                wrongPeriods.push(period);

                wrongPeriods.forEach(p => {
                    it(`${testNr++} ${testGroupName} - wrong label - 400`, done => {
                        chai
                            .request(app)
                            .patch(entityEndpoint + '/' + createdId)
                            .set('Authorization', loginUsers[0].token)
                            .send(p)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            describe('date properties', () => {
                let wrongPeriods = [];

                let period = {};
                period.startdate = {};
                wrongPeriods.push(period);

                period = {};
                period.enddate = {};
                wrongPeriods.push(period);

                period = {};
                period.startdate = 'abc';
                wrongPeriods.push(period);

                period = {};
                period.enddate = 'abc';
                wrongPeriods.push(period);

                period = {};
                period.startdate = 187;
                wrongPeriods.push(period);

                period = {};
                period.endate = 187;
                wrongPeriods.push(period);

                period = {};
                period.startdate = {};
                wrongPeriods.push(period);

                period = {};
                period.enddate = {};
                wrongPeriods.push(period);

                period = {};
                period.startdate = '04.05.2020';
                wrongPeriods.push(period);

                period = {};
                period.enddate = '02.03.2022';
                wrongPeriods.push(period);

                wrongPeriods.forEach(p => {
                    it(`${testNr++} ${testGroupName} - wrong dates - 400`, done => {
                        chai
                            .request(app)
                            .patch(entityEndpoint + '/' + createdId)
                            .set('Authorization', loginUsers[0].token)
                            .send(p)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            describe('property state', () => {
                let wrongPeriods = [];
                let period = {};
                period.state = true;
                wrongPeriods.push(period);

                period = {};
                period.state = 187;
                wrongPeriods.push(period);

                period = {};
                period.state = 'abc';
                wrongPeriods.push(period);

                wrongPeriods.forEach(p => {
                    it(`${testNr++} ${testGroupName} - wrong state property - 400`, done => {
                        chai
                            .request(app)
                            .patch(entityEndpoint + '/' + createdId)
                            .send(p)
                            .set('Authorization', loginUsers[0].token)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });
        });

        describe('special failing updates', () => {
            let wrongPeriodUpdates = [];
            let period = {};
            wrongPeriodUpdates.push(period);

            period = {};
            period.label = 'Schuljahr 2018/19';
            wrongPeriodUpdates.push(period);

            period = {};
            period._id = notFoundId;
            wrongPeriodUpdates.push(period);

            wrongPeriodUpdates.forEach(p => {
                it(`${testNr++} ${testGroupName} - special failing updates - 400`, done => {
                    chai
                        .request(app)
                        .patch(entityEndpoint + '/' + createdId)
                        .set('Authorization', loginUsers[0].token)
                        .send(p)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(400);
                            done();
                        });
                });
            });
        });

        describe('working updates', () => {
            let expectedObj = { ...periodToCreate }
            let correctPeriodUpdates = [];
            let period = {};
            period.label = 'updatedLabel';
            correctPeriodUpdates.push(period);

            period = {};
            period.startdate = '2020.09.10';
            correctPeriodUpdates.push(period);

            period = {};
            period.enddate = '2021.07.08';
            correctPeriodUpdates.push(period);

            period = {};
            period.startdate = '2020-09-12';
            correctPeriodUpdates.push(period);

            period = {};
            period.enddate = '2021-07-10';
            correctPeriodUpdates.push(period);

            period = {};
            period.startdate = new Date('01.09.2020');
            correctPeriodUpdates.push(period);

            period = {};
            period.enddate = new Date('01.07.2021');
            correctPeriodUpdates.push(period);

            period = {};
            period.state = periodStates.archived;
            correctPeriodUpdates.push(period);

            correctPeriodUpdates.forEach(p => {
                it(`${testNr++} ${testGroupName} - correct update - 200`, done => {
                    expectedObj = {
                        ...expectedObj,
                        ...p
                    };
                    chai
                        .request(app)
                        .patch(entityEndpoint + '/' + createdId)
                        .set('Authorization', loginUsers[0].token)
                        .send(p)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(200);

                            res.body.should.be.a('object');
                            Object.keys(res.body).length.should.eql(periodPropertyCount);

                            Object.keys(expectedObj).forEach(key => {
                                if (key == 'startdate' || key == 'enddate') {
                                    expect(new Date(res.body[key]).getTime() == new Date(expectedObj[key]).getTime()).to.be.true;
                                } else {
                                    expect(res.body[key] == expectedObj[key]).to.be.true;
                                }
                            });

                            expect(res.body._id).to.equal(createdId);
                            done();
                        });
                });
            });

            it(`${testNr} ${testGroupName} - check updates - 200`, done => {
                chai
                    .request(app)
                    .get(entityEndpoint + '/' + createdId)
                    .set('Authorization', loginUsers[0].token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        delete res.body['_id'];
                        res.body.should.be.a('object');

                        Object.keys(expectedObj).forEach(key => {
                            if (key == 'startdate' || key == 'enddate') {
                                expect(new Date(res.body[key]).getTime() == expectedObj[key].getTime()).to.be.true;
                            } else {
                                expect(res.body[key] == expectedObj[key]).to.be.true;
                            }
                        });
                        done();
                    });
            });
        });

        describe('special working updates', () => {
            describe('activate period', () => {
                let periodUpdate = {
                    'state': periodStates.active
                }

                it(`${testNr} ${testGroupName} - activate period - 200`, done => {
                    chai
                        .request(app)
                        .patch(entityEndpoint + '/' + generatedPeriods[0]._id)
                        .set('Authorization', loginUsers[0].token)
                        .send(periodUpdate)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(200);
                            res.body.should.be.a('object');

                            expect(res.body['state'] === periodStates.active);

                            done();
                        });
                });

                it(`${testNr++} ${testGroupName} - only one active period, actived one is active - 200`, done => {
                    chai
                        .request(app)
                        .get(entityEndpoint)
                        .set('Authorization', loginUsers[0].token)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(200);

                            expect(res.body.filter((period) => period.state == periodStates.active).length).to.eql(1);
                            expect(res.body.find((period) => period._id == generatedPeriods[0]._id).state === periodStates.active);

                            done();
                        })
                });

                it(`${testNr} ${testGroupName} - activate created period - 200`, done => {
                    chai
                        .request(app)
                        .patch(entityEndpoint + '/' + createdId)
                        .set('Authorization', loginUsers[0].token)
                        .send(periodUpdate)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(200);
                            res.body.should.be.a('object');

                            expect(res.body['state'] === periodStates.active);

                            done();
                        });
                });

                it(`${testNr++} ${testGroupName} - only one active period, actived one is active - 200`, done => {
                    chai
                        .request(app)
                        .get(entityEndpoint)
                        .set('Authorization', loginUsers[0].token)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(200);

                            expect(res.body.filter((period) => period.state == periodStates.active).length).to.eql(1);
                            expect(res.body.find((period) => period._id == createdId).state === periodStates.active);

                            done();
                        });
                });
            });

            describe('deactivate period', () => {
                let periodUpdate = {
                    'state': periodStates.inactive
                }

                it(`${testNr++} ${testGroupName} - deactivate period - 200`, done => {
                    chai
                        .request(app)
                        .patch(entityEndpoint + '/' + createdId)
                        .set('Authorization', loginUsers[0].token)
                        .send(periodUpdate)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(200);

                            res.body.should.be.a('object');
                            Object.keys(res.body).length.should.eql(periodPropertyCount);

                            res.body.should.have.property('_id');
                            expect(res.body._id).to.equal(createdId);
                            expect(res.body.state).to.equal(periodStates.inactive);
                            done();
                        });
                });

                it(`${testNr++} ${testGroupName} - no active period - 200`, done => {
                    chai
                        .request(app)
                        .get(entityEndpoint)
                        .set('Authorization', loginUsers[0].token)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(200);

                            res.body.should.be.a('array');
                            expect(res.body.every(p => p.state === periodStates.inactive)).to.be.true;
                            done();
                        });
                });
            });
        });
    });

    testGroupName = 'DELETE';
    describe(testGroupName, () => {
        it(`${testNr++} ${testGroupName} - No Auth. header - 401`, done => {
            chai
                .request(app)
                .delete(entityEndpoint + '/' + createdId)
                .send(updatedPeriod)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(401);
                    done();
                });
        });

        it(`${testNr++} ${testGroupName} - invalid ObjectId - 400`, done => {
            chai
                .request(app)
                .delete(entityEndpoint + '/1870')
                .set('Authorization', loginUsers[0].token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(400);
                    done();
                })
        });

        it(`${testNr++} ${testGroupName} - not found - 404`, done => {
            chai
                .request(app)
                .delete(entityEndpoint + '/' + notFoundId)
                .set('Authorization', loginUsers[0].token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(404);
                    done();
                })
        });

        it(`${testNr++} ${testGroupName} - period of other user - 404`, done => {
            chai
                .request(app)
                .delete(entityEndpoint + '/' + createdId)
                .set('Authorization', loginUsers[1].token)
                .send(updatedPeriod)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(404);
                    done();
                });
        });

        it(`${testNr++} ${testGroupName} - successful delete - 204`, done => {
            chai
                .request(app)
                .delete(entityEndpoint + '/' + createdId)
                .set('Authorization', loginUsers[0].token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(204);
                    done();
                })
        });

        it(`${testNr++} ${testGroupName} - get deleted item - 404`, done => {
            chai
                .request(app)
                .get(entityEndpoint + '/' + createdId)
                .set('Authorization', loginUsers[0].token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(404);
                    done();
                });
        });
    });
});