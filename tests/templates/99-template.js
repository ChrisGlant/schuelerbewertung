'use strict'
import chai from 'chai';
import chaiHttp from 'chai-http';
import app from '../server.js';

import { healthCheck } from './00-startup-hook.js';

const { expect } = chai;

// endpoints
const loginEndpoint = '/api/v1/login';
const userEndpoint = '/api/v1/users';
const entityEndpointToTest = '/api/v1/endpoint';

// data variables

let user = {
    username: "entityUser",
    password: "pswrd",
    lastname: "mock-data",
    firstname: "entity",
    email: "mock@gmail.com"
}

let secUser = {
    username: "entityUser2",
    password: "pswrd",
    lastname: "mock-data",
    firstname: "entity",
    email: "mock2@gmail.com"
}

let entityToCreate = {}

// other variables (keys, ids)

let needKeysEntity = []
let notFoundId = '61a8ea9bd4022b3534077c0d'

// setup

chai.use(chaiHttp);
chai.should();

// hook - done() fires, if healthcheck point answers with 200
before(() => {
    return healthCheck(app);
});

describe(`CRUD Tests for ${entityEndpointToTest}`, () => {
    let testGroupName;
    let testNumber = 0;

    testGroupName = 'Basic user stuff'
    describe(testGroupName, () => {
        it(`${testNumber++} ${testGroupName} - No Auth. header- 401 `, done => {
            chai
                .request(app)
                .get(entityEndpointToTest)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(401);
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - Create first user - 201`, done => {
            chai
                .request(app)
                .post(userEndpoint)
                .send(user)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(201);
                    res.body.should.be.a('object');

                    res.body.should.have.property('_id').not.to.be.null;
                    res.body.should.have.property('username').not.to.be.null;
                    res.body.should.have.property('state').not.to.be.null;
                    res.body.should.have.property('firstname').not.to.be.null;
                    res.body.should.have.property('lastname').not.to.be.null;

                    user._id = res.body._id;
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - Login first user - 200`, (done) => {
            chai
                .request(app)
                .post(loginEndpoint)
                .send({ 'username': user['username'], 'password': user['password'] })
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(1);
                    res.body.should.have.property('token');
                    user.token = res.body.token;
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - Create second user - 201`, done => {
            chai
                .request(app)
                .post(userEndpoint)
                .send(secUser)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(201);
                    res.body.should.be.a('object');

                    res.body.should.have.property('_id').not.to.be.null;
                    res.body.should.have.property('username').not.to.be.null;
                    res.body.should.have.property('state').not.to.be.null;
                    res.body.should.have.property('firstname').not.to.be.null;
                    res.body.should.have.property('lastname').not.to.be.null;

                    secUser._id = res.body._id;
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - Login second user - 200`, (done) => {
            chai
                .request(app)
                .post(loginEndpoint)
                .send({ 'username': secUser['username'], 'password': secUser['password'] })
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(1);
                    res.body.should.have.property('token');
                    secUser.token = res.body.token;
                    done();
                });
        });
    })

    testGroupName = 'POST'
    describe(testGroupName, () => {
        describe('check basic test cases', () => {
            // create
            // get created
            // delete created

            /* --------------------- property tests --------------------- */
            
            /* --------------------- id --------------------- */
            describe('check property id ', () => {
                let wrongEntities = [];

                let entity = { ...entityToCreate };
                entity._id = {};
                wrongEntities.push(entity);

                entity = { ...entityToCreate };
                entity._id = 12432434;
                wrongEntities.push(entity);

                entity = { ...entityToCreate };
                entity._id = 'JALKJ23kJ21';
                wrongEntities.push(entity);

                entity = { ...entityToCreate };
                entity._id = notFoundId;
                wrongEntities.push(entity);

                wrongEntities.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - id - 400`, done => {
                        chai
                            .request(app)
                            .patch(entityEndpointToTest + '/' + entityToCreate._id)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- property -------------------------- */
            describe('check property property ', () => {
                let wrongEntites = [];

                let entity = { ...entityToCreate };
                // modify property
                wrongEntites.push(entity);

                wrongEntites.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - property - 400`, done => {
                        chai
                            .request(app)
                            .post(entityEndpointToTest)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });  

            /* --------------------------- missing/not allowed properies-------------------------- */
            describe('missing/not allowed properies', () => {
                let wrongEntities = [];

                let entity = { ...entityToCreate };
                // delete/add propertie
                wrongEntities.push(entity);

                wrongEntities.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - missing/not allowed properies - 400`, done => {
                        chai
                            .request(app)
                            .post(entityEndpointToTest)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });
        });
    });

    testGroupName = 'GET SINGLE'
    describe(testGroupName, () => {
        // create
        // no auth. header
        // get created
        // get not found
        // get with other user
        // get with invalid object id
    });

    testGroupName = 'GET ALL';
    describe(testGroupName, () => {
        // no auth. header
        // get all with first user
        // get all with second user
    });

    testGroupName = 'PATCH';
    describe(testGroupName, () => {
        describe('basic test cases', () => {
            // no auth. header
            // patch with invalid objectid
            // patch with not found
            // patch with second user
        });

        describe('property tests', () => {
            /* --------------------- id --------------------- */
            describe('check property id ', () => {
                let wrongEntities = [];

                let entity = { ...entityToCreate };
                entity._id = {};
                wrongEntities.push(entity);

                entity = { ...entityToCreate };
                entity._id = 12432434;
                wrongEntities.push(entity);

                entity = { ...entityToCreate };
                entity._id = 'JALKJ23kJ21';
                wrongEntities.push(entity);

                entity = { ...entityToCreate };
                entity._id = notFoundId;
                wrongEntities.push(entity);

                wrongEntities.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - id - 400`, done => {
                        chai
                            .request(app)
                            .patch(entityEndpointToTest + '/' + entityToCreate._id)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- property -------------------------- */
            describe('check property label ', () => {
                let wrongEntites = [];

                let entity = { ...entityToCreate };
                // modify property
                wrongEntites.push(entity);

                wrongEntites.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - property - 400`, done => {
                        chai
                            .request(app)
                            .patch(entityEndpointToTest + '/' + entityToCreate._id)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- missing/not allowed properies-------------------------- */
            describe('missing/not allowed properies', () => {
                let wrongEntities = [];

                let entity = { ...entityToCreate };
                entity.notAllowed = 'hihihi'
                wrongEntities.push(entity);

                wrongEntities.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - missing/not allowed properies - 400`, done => {
                        chai
                            .request(app)
                            .patch(entityEndpointToTest + '/' + entityToCreate._id)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });
        });
    });

    testGroupName = 'DELETE';
    describe(testGroupName, () => {
        // no auth. header
        // delete with invalid objectid
        // delete with not found
        // delete with other user
        // successfull delete
        // get deleted
        // delete again
    });
});