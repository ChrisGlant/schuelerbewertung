import chai from 'chai';
import chaiHttp from 'chai-http';
import app from '../server.js';

import { healthCheck } from './00-startup-hook.js';
import './01-authenthication.js';

import { Course } from '../models/course-model.js';
import Pupil from '../models/pupil-model.js';
import Period from '../models/period-model.js';
import cprConnection from '../models/cprConnection-model.js'
const selectionFields = '_id courseId pupilId records';

const { expect } = chai;

chai.use(chaiHttp);
chai.should();

const loginEndpoint = '/api/v1/login';
const cprEndpointToTest = '/api/v1/cprconnections';
const courseEndpoint = '/api/v1/courses';
const pupilEndpoint = '/api/v1/pupils';

const masterUser = {
    _id: "5fce46cc305c2d245480a74d",
    username: "masterUser",
    email: "masterUser@gmail.com",
    password: "pswrdMaster",
    lastname: "lastname",
    firstname: "firstname"
}

let periodToCreate = {
    'label': 'Schuljahr 2021/22',
    'startdate': new Date('01.09.2021'),
    'enddate': new Date('01.07.2022'),
    'state': 'active',
    userId: masterUser._id
};

let course1ToCreate = {
    label: "4ahif",
    subjectLabel: "NVS",
    pupilGroupLabel: "loosergruppn",
    notes: "de kennen nix",
    userId: "5fce46cc305c2d245480a74d"
}

let course2ToCreate = {
    label: "3ahif",
    subjectLabel: "DBI",
    pupilGroupLabel: "adkfals",
    notes: "",
    userId: "5fce46cc305c2d245480a74d"
}

let course3ToCreate = {
    label: "3aahif",
    subjectLabel: "DBI",
    pupilGroupLabel: "adkfals",
    notes: "",
    userId: "5fce46cc305c2d245480a74d"
}

let pupil1ToCreate = {
    userId: "5fce46cc305c2d245480a74d",
    firstname: 'Glant',
    lastname: 'Gabi',
    mail: 'Glant@Gabi.com'
}

let pupil2ToCreate = {
    userId: "5fce46cc305c2d245480a74d",
    firstname: 'Markus',
    lastname: 'Ruehl',
    mail: 'Markus@Ruehl.com'
}

let courseKeyCount = 6;
let neededKeysCPR = ['_id', 'courseId', 'pupilId', 'records']
let validCPRIds = []
let notFoundId = '61a8ea9bd4022b3534077c0d'

let pd = new Period(periodToCreate);
await pd.save();
periodToCreate._id = pd._id;

course1ToCreate.periodId = periodToCreate._id;
let c = new Course(course1ToCreate);
await c.save();
course1ToCreate._id = c._id.toString();

course2ToCreate.periodId = periodToCreate._id;
c = new Course(course2ToCreate);
await c.save();
course2ToCreate._id = c._id.toString();

course3ToCreate.periodId = periodToCreate._id;
c = new Course(course3ToCreate);
await c.save();
course3ToCreate._id = c._id.toString();

let p = new Pupil(pupil1ToCreate);
await p.save();
pupil1ToCreate._id = p._id;

p = new Pupil(pupil2ToCreate);
await p.save();
pupil2ToCreate._id = p._id;

delete course1ToCreate.userId;
delete course2ToCreate.userId;

// hook - done() fires, if healthcheck point answers with 200
before(() => {
    return healthCheck(app);
});

describe(`Entity Tests for ${cprEndpointToTest}`, () => {
    let testGroupName = 'LOGIN';
    let testNumber = 6000;

    describe(testGroupName, () => {
        it(`${testNumber++} ${testGroupName} - get auth data - 200`, (done) => {
            chai
                .request(app)
                .post(loginEndpoint)
                .send({ username: masterUser.username, password: masterUser.password })
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.have.property('token');
                    masterUser.token = res.body['token'];
                    done();
                });
        });
    });

    testGroupName = 'GET MOCK DATA';
    describe(testGroupName, () => {
        it(`${testNumber++} ${testGroupName} - get course1 - 200`, (done) => {
            chai
                .request(app)
                .get(`${courseEndpoint}/${course1ToCreate._id}`)
                .set('Authorization', masterUser.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(courseKeyCount);
                    res.body._id.should.eql(course1ToCreate._id);
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - get course2 - 200`, (done) => {
            chai
                .request(app)
                .get(`${courseEndpoint}/${course2ToCreate._id}`)
                .set('Authorization', masterUser.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(courseKeyCount);
                    res.body._id.should.eql(course2ToCreate._id);
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - get pupil1 - 200`, (done) => {
            chai
                .request(app)
                .get(`${pupilEndpoint}/${pupil1ToCreate._id}`)
                .set('Authorization', masterUser.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(7);
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - get pupil2 - 200`, (done) => {
            chai
                .request(app)
                .get(`${pupilEndpoint}/${pupil2ToCreate._id}`)
                .set('Authorization', masterUser.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(7);
                    done();
                });
        });
    })

    testGroupName = 'ADD/REMOVE PUPIL1 TO/FROM COURSE1';
    describe(testGroupName, () => {
        it(`${testNumber++} ${testGroupName} - update course1 - add pupil1 - 200`, (done) => {
            course1ToCreate.pupils = [pupil1ToCreate._id];
            chai
                .request(app)
                .patch(`${courseEndpoint}/${course1ToCreate._id}`)
                .set('Authorization', masterUser.token)
                .send(course1ToCreate)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(6);

                    cprConnection.find({}, selectionFields)
                        .then((results) => {
                            if (!Array.isArray(results))
                                throw ` there is no data`

                            if (results.length != 1)
                                throw `invalid length array`

                            if (results[0].courseId.toString() != course1ToCreate._id || results[0].pupilId.toString() != pupil1ToCreate._id)
                                throw `invalid data`
                        })
                        .catch(err =>
                            console.error('*************************\n' + '*************************\n' + ' -> ' + err + '\n*************************\n' + '*************************\n')
                        )

                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - update course1 - remove pupil1 - 200`, (done) => {
            course1ToCreate.pupils = [];
            chai
                .request(app)
                .patch(`${courseEndpoint}/${course1ToCreate._id}`)
                .set('Authorization', masterUser.token)
                .send(course1ToCreate)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(courseKeyCount);

                    cprConnection.find({}, selectionFields)
                        .then((results) => {
                            if (!Array.isArray(results))
                                throw `there is no data`

                            if (results.length != 0)
                                throw `invalid length array`

                        })
                        .catch(err =>
                            console.error('*************************\n' + '*************************\n' + ' -> ' + err + '\n*************************\n' + '*************************\n')
                        )

                    done();
                });
        });
    });

    testGroupName = 'ADD/REMOVE PUPILS TO/FROM COURSES';
    describe(testGroupName, () => {
        it(`${testNumber++} ${testGroupName} - update course1 - add pupil1 - 200`, (done) => {
            course1ToCreate.pupils = [pupil1ToCreate._id];
            chai
                .request(app)
                .patch(`${courseEndpoint}/${course1ToCreate._id}`)
                .set('Authorization', masterUser.token)
                .send(course1ToCreate)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(courseKeyCount);

                    cprConnection.find({}, selectionFields)
                        .then((results) => {
                            if (!Array.isArray(results))
                                throw ` there is no data`

                            if (results.length != 1)
                                throw `invalid length array`

                            if (results[0].courseId.toString() != course1ToCreate._id || results[0].pupilId.toString() != pupil1ToCreate._id)
                                throw `invalid data`
                        })
                        .catch(err =>
                            console.error('*************************\n' + '*************************\n' + ' -> ' + err + '\n*************************\n' + '*************************\n')
                        )

                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - update course1 - add pupil2 - 200`, (done) => {
            course1ToCreate.pupils = [pupil1ToCreate._id, pupil2ToCreate._id];
            chai
                .request(app)
                .patch(`${courseEndpoint}/${course1ToCreate._id}`)
                .set('Authorization', masterUser.token)
                .send(course1ToCreate)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(courseKeyCount);

                    cprConnection.find({}, selectionFields)
                        .then((results) => {
                            if (!Array.isArray(results))
                                throw ` there is no data`

                            if (results.length != 2)
                                throw `invalid length array`

                            if (results[0].courseId.toString() != course1ToCreate._id || results[0].pupilId.toString() != pupil1ToCreate._id)
                                throw `invalid data`

                            if (results[1].courseId.toString() != course1ToCreate._id || results[1].pupilId.toString() != pupil2ToCreate._id)
                                throw `invalid data`
                        })
                        .catch(err =>
                            console.error('*************************\n' + '*************************\n' + ' -> ' + err + '\n*************************\n' + '*************************\n')
                        )

                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - update course1 - remove pupil1 - 200`, (done) => {
            course1ToCreate.pupils = [pupil2ToCreate._id];
            chai
                .request(app)
                .patch(`${courseEndpoint}/${course1ToCreate._id}`)
                .set('Authorization', masterUser.token)
                .send(course1ToCreate)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(courseKeyCount);

                    cprConnection.find({}, selectionFields)
                        .then((results) => {
                            if (!Array.isArray(results))
                                throw `there is no data`

                            if (results.length != 1)
                                throw `invalid length array`

                            if (results[0].courseId.toString() != course1ToCreate._id || results[0].pupilId.toString() != pupil2ToCreate._id)
                                throw `invalid data`

                        })
                        .catch(err =>
                            console.error('*************************\n' + '*************************\n' + ' -> ' + err + '\n*************************\n' + '*************************\n')
                        )

                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - update course1 - add pupil2 while already in course - 200`, (done) => {
            course1ToCreate.pupils = [pupil2ToCreate._id];
            chai
                .request(app)
                .patch(`${courseEndpoint}/${course1ToCreate._id}`)
                .set('Authorization', masterUser.token)
                .send(course1ToCreate)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');

                    cprConnection.find({}, selectionFields)
                        .then((results) => {
                            if (!Array.isArray(results))
                                throw ` there is no data`

                            if (results.length != 1)
                                throw `invalid length array`

                            if (results[0].courseId.toString() != course1ToCreate._id || results[0].pupilId.toString() != pupil2ToCreate._id)
                                throw `invalid data`
                        })
                        .catch(err =>
                            console.error('*************************\n' + '*************************\n' + ' -> ' + err + '\n*************************\n' + '*************************\n')
                        )

                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - update course1 - add pupil1 - 200`, (done) => {
            course1ToCreate.pupils = [pupil1ToCreate._id, pupil2ToCreate._id];
            chai
                .request(app)
                .patch(`${courseEndpoint}/${course1ToCreate._id}`)
                .set('Authorization', masterUser.token)
                .send(course1ToCreate)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(courseKeyCount);

                    cprConnection.find({}, selectionFields)
                        .then((results) => {
                            if (!Array.isArray(results))
                                throw ` there is no data`

                            if (results.length != 2)
                                throw `invalid length array`

                            if (results[0].courseId.toString() != course1ToCreate._id || results[0].pupilId.toString() != pupil2ToCreate._id)
                                throw `invalid data`

                            if (results[1].courseId.toString() != course1ToCreate._id || results[1].pupilId.toString() != pupil1ToCreate._id)
                                throw `invalid data`
                        })
                        .catch(err =>
                            console.error('*************************\n' + '*************************\n' + ' -> ' + err + '\n*************************\n' + '*************************\n')
                        )

                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - update course2 - add pupil1 - 200`, (done) => {
            course2ToCreate.pupils = [pupil1ToCreate._id];
            chai
                .request(app)
                .patch(`${courseEndpoint}/${course2ToCreate._id}`)
                .set('Authorization', masterUser.token)
                .send(course2ToCreate)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(courseKeyCount);

                    cprConnection.find({}, selectionFields)
                        .then((results) => {
                            if (!Array.isArray(results))
                                throw ` there is no data`

                            if (results.length != 3)
                                throw `invalid length array`

                            if (results[2].courseId.toString() != course2ToCreate._id || results[2].pupilId.toString() != pupil1ToCreate._id)
                                throw `invalid data`
                        })
                        .catch(err =>
                            console.error('*************************\n' + '*************************\n' + ' -> ' + err + '\n*************************\n' + '*************************\n')
                        )

                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - update course2 - add pupil2 - 200`, (done) => {
            course2ToCreate.pupils = [pupil1ToCreate._id, pupil2ToCreate._id];
            chai
                .request(app)
                .patch(`${courseEndpoint}/${course2ToCreate._id}`)
                .set('Authorization', masterUser.token)
                .send(course2ToCreate)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(courseKeyCount);

                    cprConnection.find({}, selectionFields)
                        .then((results) => {
                            if (!Array.isArray(results))
                                throw ` there is no data`

                            if (results.length != 4)
                                throw `invalid length array`

                            if (results[2].courseId.toString() != course2ToCreate._id || results[2].pupilId.toString() != pupil1ToCreate._id)
                                throw `invalid data`

                            if (results[3].courseId.toString() != course2ToCreate._id || results[3].pupilId.toString() != pupil2ToCreate._id)
                                throw `invalid data`
                        })
                        .catch(err =>
                            console.error('*************************\n' + '*************************\n' + ' -> ' + err + '\n*************************\n' + '*************************\n')
                        )

                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - update course2 - remove pupil1 - 200`, (done) => {
            course2ToCreate.pupils = [pupil2ToCreate._id];
            chai
                .request(app)
                .patch(`${courseEndpoint}/${course2ToCreate._id}`)
                .set('Authorization', masterUser.token)
                .send(course2ToCreate)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(courseKeyCount);

                    cprConnection.find({}, selectionFields)
                        .then((results) => {
                            if (!Array.isArray(results))
                                throw `there is no data`

                            if (results.length != 3)
                                throw `invalid length array`

                            if (results[2].courseId.toString() != course2ToCreate._id || results[2].pupilId.toString() != pupil2ToCreate._id)
                                throw `invalid data`

                        })
                        .catch(err =>
                            console.error('*************************\n' + '*************************\n' + ' -> ' + err + '\n*************************\n' + '*************************\n')
                        )

                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - update course2 - add pupil2 while already in course2 - 200`, (done) => {
            course2ToCreate.pupils = [pupil2ToCreate._id];
            chai
                .request(app)
                .patch(`${courseEndpoint}/${course2ToCreate._id}`)
                .set('Authorization', masterUser.token)
                .send(course2ToCreate)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');

                    cprConnection.find({}, selectionFields)
                        .then((results) => {
                            if (!Array.isArray(results))
                                throw ` there is no data`

                            if (results.length != 3)
                                throw `invalid length array`

                            if (results[2].courseId.toString() != course2ToCreate._id || results[2].pupilId.toString() != pupil2ToCreate._id)
                                throw `invalid data`
                        })
                        .catch(err =>
                            console.error('*************************\n' + '*************************\n' + ' -> ' + err + '\n*************************\n' + '*************************\n')
                        )

                    done();
                });
        });
    });

    testGroupName = 'Query tests';
    describe(testGroupName, () => {
        describe('invalid querys', () => {
            let invalidQuerys = []

            let query = ''
            invalidQuerys.push(query)

            query = 'hihihi=notAllowed'
            invalidQuerys.push(query)

            query = 'courseId=notAllowed&courseId=hihi'
            invalidQuerys.push(query)

            invalidQuerys.forEach(query => {
                it(`${testNumber++} ${testGroupName} - invalid querys - 400`, (done) => {
                    chai
                        .request(app)
                        .get(cprEndpointToTest + '?' + query)
                        .set('Authorization', masterUser.token)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(400);
                            done();
                        });
                });
            })

            let notFoundQuerys = []

            query = 'courseId=' + notFoundId
            notFoundQuerys.push(query)

            query = 'pupilId=' + notFoundId
            notFoundQuerys.push(query)

            query = 'pupilId=' + notFoundId + '&courseId=' + notFoundId
            notFoundQuerys.push(query)

            notFoundQuerys.forEach(query => {
                it(`${testNumber++} ${testGroupName} - not found querys - 404`, (done) => {
                    chai
                        .request(app)
                        .get(cprEndpointToTest + '?' + query)
                        .set('Authorization', masterUser.token)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(404);
                            done();
                        });
                });
            })
        })

        describe('valid querys', () => {
            let validQuerys = []

            let query = 'courseId=' + course1ToCreate._id
            validQuerys.push(query)

            query = 'pupilId=' + pupil1ToCreate._id
            validQuerys.push(query)

            query = 'pupilId=' + pupil1ToCreate._id + '&courseId=' + course1ToCreate._id
            validQuerys.push(query)

            validQuerys.forEach(query => {
                it(`${testNumber++} ${testGroupName} - valid querys - 200`, (done) => {
                    chai
                        .request(app)
                        .get(cprEndpointToTest + '?' + query)
                        .set('Authorization', masterUser.token)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(200);
                            validCPRIds.push(res.body[0]._id)
                            done();
                        });
                });
            })
        })

        testGroupName = 'GET Single'
        describe(testGroupName, () => {
            it(`${testNumber++} ${testGroupName} - get cpr - 400`, (done) => {
                chai
                    .request(app)
                    .get(cprEndpointToTest + '/1870')
                    .set('Authorization', masterUser.token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(400);
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - get cpr - 404`, (done) => {
                chai
                    .request(app)
                    .get(cprEndpointToTest + '/' + notFoundId)
                    .set('Authorization', masterUser.token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(404);
                        done();
                    });
            });

            for (let counter = 0; counter < 3; counter++) {
                it(`${testNumber++} ${testGroupName} - get cpr - 200`, (done) => {
                    chai
                        .request(app)
                        .get(cprEndpointToTest + '/' + validCPRIds[counter])
                        .set('Authorization', masterUser.token)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(200);
                            neededKeysCPR.forEach(key => res.body.should.have.property(key).to.not.be.null)
                            expect(res.body._id).to.equal(validCPRIds[counter])
                            done();
                        });
                });
            }
        })
    })
});
