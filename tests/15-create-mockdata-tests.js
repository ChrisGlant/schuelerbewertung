'use strict'
import chai from 'chai';
import chaiHttp from 'chai-http';
import app from '../server.js';
import { healthCheck } from './00-startup-hook.js';

const { expect } = chai;

const creationEndpoint = '/api/v1/mockdata';

chai.use(chaiHttp);
chai.should();

before(() => {
    return healthCheck(app);
});

describe(`Tests for mockdata-creation ${creationEndpoint}`, () => {
    let testNumber = 0;

    it(`${testNumber++} - create mockdata - 204`, done => {
        chai
            .request(app)
            .post(creationEndpoint)
            .end((err, res) => {
                expect(err).to.be.null;
                res.should.have.status(204);
                done();
            });
    });
});