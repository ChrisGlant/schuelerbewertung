'use strict'
import chai from 'chai';
import chaiHttp from 'chai-http';
import { Course } from '../models/course-model.js';
import app from '../server.js';

import { healthCheck } from './00-startup-hook.js';

const { expect } = chai;

const apiPrefix = '/api/v1';
const loginEndpoint = `${apiPrefix}/login`;
const userEndpoint = `${apiPrefix}/users`;
const pupilEndpoint = `${apiPrefix}/pupils`;
const courseEndpoint = `${apiPrefix}/courses`;
const periodEndpoint = `${apiPrefix}/periods`;
const cprConnectionEndpoint = `${apiPrefix}/cprConnections`;
const pupilImportEndpoint = `${apiPrefix}/import/pupils`;
const pupilImportConfirmEndpoint = `${apiPrefix}/import/pupils/confirm`;

let user = {
    username: 'importPupilUser1',
    password: 'pswrd',
    lastname: 'mock-data',
    firstname: 'import-pupil',
    email: 'import-pupil1@gmail.com'
}

let secUser = {
    username: 'importPupilUser2',
    password: 'pswrd',
    lastname: 'mock-data',
    firstname: 'import-pupil',
    email: 'import-pupil2@gmail.com'
}

let periodsToCreate = [
    {
        'label': 'importtest_SJ 2021/22',
        'startdate': '2021.09.13',
        'enddate': '2022.07.07',
        'state': 'active'
    }
]

let existingPupils = [
    {
        'mail': 'importtest.new@user.com',
        'firstname': 'NewUser',
        'lastname': 'importtest',
        'state': 'active'
    }
];

let existingCourses = [
    {
        'label': 'importtest_5AHIF SYP',
        'subjectLabel': 'SYP importtest'
    }
];

let testImportPupil = existingPupils[0];

let workingImport = [
    {
        'mail': 'importtest_raphael@artl.com',
        'firstname': 'Raphael',
        'lastname': 'Artl',
        'state': 'active',
        'courses': [
            'importtest_5AHIF NVS',
            'importtest_5AHIF SYP'
        ]
    },
    {
        'mail': 'importtest_glant@chris.com',
        'firstname': 'Christoph',
        'lastname': 'Glantschnig',
        'state': 'active',
        'notes': 'Adlerwarte Landskron',
        'courses': [
            'importtest_5AHIF DBI'
        ]
    },
    {
        'mail': 'importtest_leon@hueber.com',
        'firstname': 'Leon',
        'lastname': 'Hueber'
    },
    {
        ...existingPupils[0],
        'firstname': 'update'
    }
]

let neededPupilKeys = ['_id', 'state', 'firstname', 'lastname', 'mail', 'notes'];
let neededCourseKeys = ['_id', 'label', 'subjectLabel', 'pupilGroupLabel', 'periodId'];

chai.use(chaiHttp);
chai.should();

// hook - done() fires, if healthcheck point answers with 200
before(() => {
    return healthCheck(app);
});

describe(`Tests for importing pupils (${pupilImportEndpoint})`, () => {
    let testGroupName;
    let testNumber = 14000;

    testGroupName = 'Basic user stuff'
    describe(testGroupName, () => {
        it(`${testNumber++} ${testGroupName} - No Auth. header 1 - 401 `, done => {
            chai
                .request(app)
                .get(pupilImportEndpoint)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(401);
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - No Auth. header 2 - 401 `, done => {
            chai
                .request(app)
                .get(pupilImportConfirmEndpoint)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(401);
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - Create first user - 201`, done => {
            chai
                .request(app)
                .post(userEndpoint)
                .send(user)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(201);
                    res.body.should.be.a('object');

                    res.body.should.have.property('_id').not.to.be.null;
                    res.body.should.have.property('username').not.to.be.null;
                    res.body.should.have.property('state').not.to.be.null;
                    res.body.should.have.property('firstname').not.to.be.null;
                    res.body.should.have.property('lastname').not.to.be.null;

                    user._id = res.body._id;
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - Login first user - 200`, done => {
            chai
                .request(app)
                .post(loginEndpoint)
                .send({ 'username': user['username'], 'password': user['password'] })
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(1);
                    res.body.should.have.property('token');
                    user.token = res.body.token;
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - Create second user - 201`, done => {
            chai
                .request(app)
                .post(userEndpoint)
                .send(secUser)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(201);
                    res.body.should.be.a('object');

                    res.body.should.have.property('_id').not.to.be.null;
                    res.body.should.have.property('username').not.to.be.null;
                    res.body.should.have.property('state').not.to.be.null;
                    res.body.should.have.property('firstname').not.to.be.null;
                    res.body.should.have.property('lastname').not.to.be.null;

                    secUser._id = res.body._id;
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - Login second user - 200`, done => {
            chai
                .request(app)
                .post(loginEndpoint)
                .send({ 'username': secUser['username'], 'password': secUser['password'] })
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(1);
                    res.body.should.have.property('token');
                    secUser.token = res.body.token;
                    done();
                });
        });
    });

    testGroupName = 'create mockdata';
    describe(testGroupName, () => {
        describe('create periods', () => {
            periodsToCreate.forEach(p => {
                it(`${testNumber++} ${testGroupName} - create mockdata period`, done => {
                    chai
                        .request(app)
                        .post(periodEndpoint)
                        .set('Authorization', user.token)
                        .send(p)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(201);
                            res.body.should.be.a('object');
                            res.body.should.have.property('_id').to.not.be.null;

                            p._id = res.body._id;
                            done();
                        });
                });
                it(`${testNumber++} ${testGroupName} - get created mockdata period - 200 `, done => {
                    chai
                        .request(app)
                        .get(periodEndpoint + '/' + p._id)
                        .set('Authorization', user.token)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(200);
                            res.body.should.be.a('object');

                            res.body.should.have.property('_id').to.not.be.null;
                            expect(res.body._id).to.be.equal(p._id);
                            done();
                        });
                });
            });
        });
        describe('create pupils', () => {
            existingPupils.forEach(p => {
                it(`${testNumber++} ${testGroupName} - create mockdata pupil`, done => {
                    chai
                        .request(app)
                        .post(pupilEndpoint)
                        .set('Authorization', user.token)
                        .send(p)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(201);
                            res.body.should.be.a('object');

                            neededPupilKeys.forEach(key => {
                                res.body.should.have.property(key).to.be.not.null;
                            });

                            p._id = res.body._id;
                            done();
                        });
                });
                it(`${testNumber++} ${testGroupName} - get created mockdata pupil - 200 `, done => {
                    chai
                        .request(app)
                        .get(pupilEndpoint + '/' + p._id)
                        .set('Authorization', user.token)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(200);
                            res.body.should.be.a('object');

                            neededPupilKeys.forEach(key => res.body.should.have.property(key).to.not.be.null)
                            expect(res.body._id).to.be.equal(p._id);
                            done();
                        });
                });
            });
        });
        describe('create courses', () => {
            existingCourses.forEach(c => {
                it(`${testNumber++} ${testGroupName} - create mockdata course`, done => {
                    chai
                        .request(app)
                        .post(courseEndpoint)
                        .set('Authorization', user.token)
                        .send(c)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(201);
                            res.body.should.be.a('object');

                            neededCourseKeys.forEach(key => {
                                res.body.should.have.property(key).to.be.not.null;
                            });

                            c._id = res.body._id;
                            done();
                        });
                });
                it(`${testNumber++} ${testGroupName} - get created mockdata course - 200 `, done => {
                    chai
                        .request(app)
                        .get(courseEndpoint + '/' + c._id)
                        .set('Authorization', user.token)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(200);
                            res.body.should.be.a('object');

                            neededCourseKeys.forEach(key => res.body.should.have.property(key).to.not.be.null)
                            expect(res.body._id).to.be.equal(c._id);
                            done();
                        });
                });
            });
        });
        describe('assign pupil to course', () => {
            it(`${testNumber++} ${testGroupName} - create mockdata course`, done => {
                let course = { pupils: [existingPupils[0]._id] };
                chai
                    .request(app)
                    .patch(courseEndpoint + '/' + existingCourses[0]._id)
                    .set('Authorization', user.token)
                    .send(course)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        done();
                    });
            });
        });
    });

    testGroupName = 'get actions';
    describe(testGroupName, () => {
        describe('get actions with wrong payload', () => {
            let wrongPayloads = [];
            wrongPayloads.push({ 'test': '123' });
            wrongPayloads.push([]);
            wrongPayloads.push([1, 2, 3, 4]);
            wrongPayloads.push('test');
            wrongPayloads.push([{ ...workingImport[1], courses: 'test' }]);

            wrongPayloads.forEach(w => {
                it(`${testNumber++} ${testGroupName} - wrong import payload - 400`, done => {
                    chai
                        .request(app)
                        .put(pupilImportEndpoint)
                        .set('Authorization', user.token)
                        .send(w)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(400);
                            done();
                        });
                });
            });
        });

        describe('get actions with wrong/not allowed/missing properties', () => {
            let wrongImports = [];
            let wrongCombinedImports = [];

            let wrongImport = { ...testImportPupil };
            wrongImport.mail = 'test123.com';
            let importObj = { obj: wrongImport, wrongFields: ['mail'], expectedState: 'actions-necessary' }
            wrongImports.push([importObj]);
            wrongCombinedImports.push(importObj)

            wrongImport = { ...testImportPupil };
            delete wrongImport.mail;
            importObj = { obj: wrongImport, wrongFields: ['mail'], expectedState: 'actions-necessary' };
            wrongImports.push([importObj]);
            wrongCombinedImports.push(importObj);

            wrongImport = { ...testImportPupil };
            delete wrongImport.firstname;
            importObj = { obj: wrongImport, wrongFields: ['firstname'], expectedState: 'actions-necessary' };
            wrongImports.push([importObj]);
            wrongCombinedImports.push(importObj);

            wrongImport = { ...testImportPupil };
            delete wrongImport.lastname;
            importObj = { obj: wrongImport, wrongFields: ['lastname'], expectedState: 'actions-necessary' };
            wrongImports.push([importObj]);
            wrongCombinedImports.push(importObj);

            wrongImport = { ...testImportPupil };
            delete wrongImport.lastname;
            delete wrongImport.firstname;
            importObj = { obj: wrongImport, wrongFields: ['lastname', 'firstname'], expectedState: 'actions-necessary' };
            wrongImports.push([importObj]);
            wrongCombinedImports.push(importObj);

            wrongImport = { ...testImportPupil };
            wrongImport.notAllowed = 'iAmNotAllowedHere';
            importObj = { obj: wrongImport, wrongFields: ['notAllowed'], expectedState: 'actions-necessary' };
            wrongImports.push([importObj]);
            wrongCombinedImports.push(importObj);

            wrongImports.push(wrongCombinedImports);

            wrongImports.forEach(w => {
                it(`${testNumber++} ${testGroupName} - wrong import properties - 200`, done => {
                    chai
                        .request(app)
                        .put(pupilImportEndpoint)
                        .set('Authorization', user.token)
                        .send(w.map(e => e.obj))
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(200);

                            res.body.should.be.an('array');
                            expect(res.body.length).to.equal(w.length);

                            res.body.forEach((b, idx) => {
                                b.should.be.an('object');
                                expect(b.state).to.equal(w[idx].expectedState);

                                b.actions.should.be.an('object');
                                expect(Object.keys(b.actions).length).to.equal(1);

                                b.actions.error.should.be.an('object');
                                expect(b.actions.update).to.be.undefined;
                                expect(b.actions.coursesToCreate).to.be.undefined;
                                expect(Object.keys(b.actions.error).length).to.equal(w[idx].wrongFields.length);
                                w[idx].wrongFields.forEach(er => {
                                    let included = Object.keys(b.actions.error).includes(er);
                                    expect(included).to.equal(true);
                                });
                            });

                            done();
                        });
                });
            });
        });

        describe('get actions with properties to update', () => {
            let importsToUpdate = [];
            let combinedImportsToUpdate = [];

            let importToUpdate = { ...existingPupils[0] };
            importToUpdate.firstname = 'updated';
            let importObj = { obj: importToUpdate, updateFields: ['firstname'], expectedState: 'actions-necessary' }
            importsToUpdate.push([importObj]);
            combinedImportsToUpdate.push(importObj);

            importToUpdate = { ...existingPupils[0] };
            importToUpdate.lastname = 'updated';
            importObj = { obj: importToUpdate, updateFields: ['lastname'], expectedState: 'actions-necessary' }
            importsToUpdate.push([importObj]);
            combinedImportsToUpdate.push(importObj);

            importToUpdate = { ...existingPupils[0] };
            importToUpdate.firstname = 'updated';
            importToUpdate.lastname = 'updated';
            importObj = { obj: importToUpdate, updateFields: ['firstname', 'lastname'], expectedState: 'actions-necessary' }
            importsToUpdate.push([importObj]);
            combinedImportsToUpdate.push(importObj);

            importsToUpdate.push(combinedImportsToUpdate);

            importsToUpdate.forEach(u => {
                it(`${testNumber++} ${testGroupName} - import properties to update - 200`, done => {
                    chai
                        .request(app)
                        .put(pupilImportEndpoint)
                        .set('Authorization', user.token)
                        .send(u.map(e => e.obj))
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(200);

                            res.body.should.be.an('array');
                            expect(res.body.length).to.equal(u.length);

                            res.body.forEach((b, idx) => {
                                b.should.be.an('object');
                                expect(b.state).to.equal(u[idx].expectedState);

                                b.actions.should.be.an('object');
                                expect(Object.keys(b.actions).length).to.equal(1);

                                b.actions.update.should.be.an('array');
                                expect(b.actions.coursesToCreate).to.be.undefined;
                                expect(b.actions.error).to.be.undefined;
                                expect(Object.keys(b.actions.update).length).to.equal(u[idx].updateFields.length);
                                u[idx].updateFields.forEach(er => {
                                    let included = b.actions.update.includes(er);
                                    expect(included).to.equal(true);
                                });
                            });

                            done();
                        });
                });
            });
        });

        describe('get actions with courses to create', () => {
            let importsWithCourses = [];
            let combinedImportsWithCourse = [];

            let importWithCourse = { ...testImportPupil };
            importWithCourse.courses = ['testcourse1'];
            let importObj = { obj: importWithCourse, coursesToCreate: ['testcourse1'], expectedState: 'actions-necessary' };
            importsWithCourses.push([importObj]);
            combinedImportsWithCourse.push(importObj);

            importWithCourse = { ...testImportPupil };
            importWithCourse.courses = ['testcourse1', 'testcourse2'];
            importObj = { obj: importWithCourse, coursesToCreate: ['testcourse1', 'testcourse2'], expectedState: 'actions-necessary' };
            importsWithCourses.push([importObj]);
            combinedImportsWithCourse.push(importObj);

            importWithCourse = { ...testImportPupil };
            importWithCourse.courses = ['testcourse1', 'testcourse2', 'importtest_5AHIF SYP'];
            importObj = { obj: importWithCourse, coursesToCreate: ['testcourse1', 'testcourse2'], expectedState: 'actions-necessary' };
            importsWithCourses.push([importObj]);
            combinedImportsWithCourse.push(importObj);

            importsWithCourses.push(combinedImportsWithCourse);

            importsWithCourses.forEach(c => {
                it(`${testNumber++} ${testGroupName} - import with courses - 200`, done => {
                    chai
                        .request(app)
                        .put(pupilImportEndpoint)
                        .set('Authorization', user.token)
                        .send(c.map(e => e.obj))
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(200);

                            res.body.should.be.an('array');
                            expect(res.body.length).to.equal(c.length);

                            res.body.forEach((b, idx) => {
                                b.should.be.an('object');
                                expect(b.state).to.equal(c[idx].expectedState);

                                b.actions.should.be.an('object');
                                expect(Object.keys(b.actions).length).to.equal(1);

                                b.actions.coursesToCreate.should.be.an('array');
                                expect(b.actions.error).to.be.undefined;
                                expect(b.update).to.be.undefined;
                                expect(Object.keys(b.actions.coursesToCreate).length).to.equal(c[idx].coursesToCreate.length);
                                c[idx].coursesToCreate.forEach(er => {
                                    let included = b.actions.coursesToCreate.includes(er);
                                    expect(included).to.equal(true);
                                });
                            });

                            done();
                        });
                });
            });
        });

        describe('get actions without necessary actions', () => {
            let importsWithoutActions = [];
            let combinedImportsWithoutActions = [];

            let importWithoutAction = { ...testImportPupil };
            let importObj = { obj: importWithoutAction, expectedState: 'to-import' };
            importsWithoutActions.push([importObj]);
            combinedImportsWithoutActions.push(importObj);

            importWithoutAction = { ...testImportPupil };
            importWithoutAction.courses = ['importtest_5AHIF SYP'];
            importObj = { obj: importWithoutAction, expectedState: 'to-import' };
            importsWithoutActions.push([importObj]);
            combinedImportsWithoutActions.push(importObj);

            importsWithoutActions.push(combinedImportsWithoutActions);

            importsWithoutActions.forEach(a => {
                it(`${testNumber++} ${testGroupName} - import with courses - 200`, done => {
                    chai
                        .request(app)
                        .put(pupilImportEndpoint)
                        .set('Authorization', user.token)
                        .send(a.map(e => e.obj))
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(200);

                            res.body.should.be.an('array');
                            expect(res.body.length).to.equal(a.length);

                            res.body.forEach((b, idx) => {
                                b.should.be.an('object');
                                expect(b.state).to.equal(a[idx].expectedState);
                                expect(b.actions).to.be.undefined;
                            });

                            done();
                        });
                });
            });
        });
    });

    testGroupName = 'confirm import';
    describe(testGroupName, () => {
        describe('wrong properties', () => {
            let wrongImports = [];
            let wrongCombinedImports = [];

            let wrongImport = { ...testImportPupil };
            wrongImport.mail = 'test123.com';
            let importObj = { obj: wrongImport, wrongFields: ['mail'], expectedState: 'actions-necessary' }
            wrongImports.push([importObj]);
            wrongCombinedImports.push(importObj)

            wrongImport = { ...testImportPupil };
            delete wrongImport.mail;
            importObj = { obj: wrongImport, wrongFields: ['mail'], expectedState: 'actions-necessary' };
            wrongImports.push([importObj]);
            wrongCombinedImports.push(importObj);

            wrongImport = { ...testImportPupil };
            delete wrongImport.firstname;
            importObj = { obj: wrongImport, wrongFields: ['firstname'], expectedState: 'actions-necessary' };
            wrongImports.push([importObj]);
            wrongCombinedImports.push(importObj);

            wrongImport = { ...testImportPupil };
            delete wrongImport.lastname;
            importObj = { obj: wrongImport, wrongFields: ['lastname'], expectedState: 'actions-necessary' };
            wrongImports.push([importObj]);
            wrongCombinedImports.push(importObj);

            wrongImport = { ...testImportPupil };
            delete wrongImport.lastname;
            delete wrongImport.firstname;
            importObj = { obj: wrongImport, wrongFields: ['lastname', 'firstname'], expectedState: 'actions-necessary' };
            wrongImports.push([importObj]);
            wrongCombinedImports.push(importObj);

            wrongImport = { ...testImportPupil };
            wrongImport.notAllowed = 'iAmNotAllowedHere';
            importObj = { obj: wrongImport, wrongFields: ['notAllowed'], expectedState: 'actions-necessary' };
            wrongImports.push([importObj]);
            wrongCombinedImports.push(importObj);

            wrongImports.push(wrongCombinedImports);

            wrongImports.forEach(w => {
                it(`${testNumber++} ${testGroupName} - wrong import confirm payload - 400`, done => {
                    chai
                        .request(app)
                        .post(pupilImportConfirmEndpoint)
                        .set('Authorization', user.token)
                        .send(w)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(400);
                            done();
                        });
                });
            });
        });
        describe('successful import', () => {
            it(`${testNumber++} ${testGroupName} - working import - 201`, done => {
                chai
                    .request(app)
                    .post(pupilImportConfirmEndpoint)
                    .set('Authorization', user.token)
                    .send(workingImport)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(201);

                        res.body.should.be.an('array');
                        expect(res.body.length).to.equal(workingImport.length);

                        res.body.forEach(b => {
                            neededPupilKeys.forEach(key => b.should.have.property(key).to.not.be.null);
                            let pupil = workingImport.find(obj => obj.mail == b.mail);
                            pupil._id = b._id;
                        });

                        done();
                    });
            });

            workingImport.forEach(wi => {
                it(`${testNumber++} ${testGroupName} - check imported pupil - 200`, done => {
                    chai
                        .request(app)
                        .get(pupilEndpoint + `/${wi._id}`)
                        .set('Authorization', user.token)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(200);

                            res.body.should.be.an('object');
                            Object.keys(res.body).forEach(key => {
                                if (wi[key])
                                    expect(res.body[key]).to.equal(wi[key]);
                            });

                            done();
                        });
                });

                it(`${testNumber++} ${testGroupName} - check cprconnections - 200`, done => {
                    chai
                        .request(app)
                        .get(cprConnectionEndpoint + `?pupilId=${wi._id}`)
                        .set('Authorization', user.token)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(200);

                            res.body.should.be.an.an('array');

                            let foundCourseIds = res.body.map(cpr => cpr.courseId);

                            Course.find().where('_id').in(foundCourseIds).exec((dbErr, courses) => {
                                expect(dbErr).to.be.null;
                                let courseNames = courses.map(c => c.label);
                                if (wi.courses) {
                                    wi.courses.forEach(c => {
                                        expect(courseNames.includes(c)).to.equal(true);
                                    });
                                }
                            });

                            done();
                        });
                });
            });
        });
    });
});