'use strict'
import chai from 'chai';
import chaiHttp from 'chai-http';
import app from '../server.js';

import { healthCheck } from './00-startup-hook.js';

const { expect } = chai;

// endpoints
const loginEndpoint = '/api/v1/login';
const userEndpoint = '/api/v1/users';
const periodEndpoint = '/api/v1/periods';
const courseEndpoint = '/api/v1/courses';
const pupilEndpoint = '/api/v1/pupils';
const ratingschemeEndpoint = '/api/v1/ratingschemes';
const cprEndpoint = '/api/v1/cprconnections';
const recordEndpointToTest = '/api/v1/records';
const bulkAddEndpoint = '/api/v1/records/bulkadd';
const gradeEndpoint = '/api/v1/records/grade';

// data variables

let user = {
    username: "ratingUser",
    email: "ratingUser@gmail.com",
    password: "pswrd",
    lastname: "mock-data",
    firstname: "rating"
}

let secUser = {
    username: "ratingUser2",
    email: "ratingUser2@gmail.com",
    password: "pswrd",
    lastname: "mock-data",
    firstname: "rating"
}

let courseToCreateFirstUser = {
    "label": "course ratings-crud-test",
    "subjectLabel": "NVS",
    "pupilGroupLabel": "Gruppe A (die besseren)",
    "notes": "test"
}

let courseToCreateSecUser = {
    "label": "course ratings-crud-test-sec-user",
    "subjectLabel": "NVS",
    "pupilGroupLabel": "Gruppe A (die besseren)",
    "notes": "test"
}

let periodToCreate = {
    'label': 'Schuljahr 2021/22 ratings',
    'startdate': new Date('01.09.3050'),
    'enddate': new Date('01.07.3051'),
    'state': 'active'
};

let periodToCreateSecUser = {
    'label': 'Schuljahr 2022/23 ratings-sec-user',
    'startdate': new Date('01.09.3051'),
    'enddate': new Date('01.07.3052'),
    'state': 'active'
};

let pupilsToCreateFirstUser = [
    {
        "state": "active",
        "firstname": "recordstestpupil01",
        "lastname": "records",
        "mail": "records.firstpupil@gmx.at",
        "notes": ""
    },
    {
        "state": "active",
        "firstname": "recordstestpupil02",
        "lastname": "records",
        "mail": "records.secondpupil@gmx.at",
        "notes": ""
    },
]

let pupilToCreateSecUser = {
    "state": "active",
    "firstname": "secuser",
    "lastname": "ratings",
    "mail": "ratings.secuser@gmx.at",
    "notes": ""
}

let ratingschemeToCreate = {
    "name": "ratings-test-mockdata",
    "ratings": [
        {
            "label": "Sehr Gut",
            "symbol": "1",
            "value": 100
        },
        {
            "label": "Negativ",
            "symbol": "5",
            "value": 50
        }
    ],
    weight: 60
}

let ratingschemeToCreateSecUser = {
    "name": "ratings-test-mockdata",
    "ratings": [
        {
            "label": "Sehr Gut",
            "symbol": "1",
            "value": 91
        },
        {
            "label": "Negativ",
            "symbol": "5",
            "value": 50
        }
    ],
    weight: 60
}

let cprIdsFirstUser = [];
let cprIdSecUser;

let recordToCreate = {
    weight: 50,
    validOn: '3051.01.01',
    pupilNotPresent: false,
    comment: 'this is so stupid to test',
    tags: ['test', 'servus']
}

let recordForGradeSuggPos = {
    weight: 50,
    validOn: '3051.01.01',
    pupilNotPresent: false,
    comment: 'this is so stupid to test',
    tags: ['test', 'servus']
}

let recordForGradeSuggNeg = {
    weight: 50,
    validOn: '3051.01.01',
    pupilNotPresent: false,
    comment: 'this is so stupid to test',
    tags: ['test', 'servus']
}


let recordToCreateSecUser = {
    weight: 50,
    validOn: '3052.01.01',
    pupilNotPresent: false,
    comment: 'this is so stupid to test'
}

let bulkAddTemplate = {
    courseId: null,
    validOn: '3053.01.01',
    weight: 50,
    tags: ['bulkAddTest', 'desbedarfs'],
    records: [
        {
            pupilId: null,
            ratingId: null,
            tags: ['additional'],
            comment: 'kommentar1'
        },
        {
            pupilId: null,
            ratingId: null,
            comment: 'kommentar2'
        }
    ]
};


// other variables (keys, ids)

let neededKeysRecord = ['_id', 'cprId', 'rating', 'validOn', 'pupilNotPresent', 'comment', 'tags']
let neededKeysPupil = ['_id', 'state', 'firstname', 'lastname', 'mail', 'notes'];
let neededKeysCourse = ['_id', 'label', 'subjectLabel', 'pupilGroupLabel', 'notes', 'periodId']
let neededKeysPeriod = ['_id', 'label', 'startdate', 'enddate', 'state']
let neededKeysRatingscheme = ['_id', 'name', 'ratings']
let neededKeysCPR = ['_id', 'courseId', 'pupilId', 'records']
let neededKeysGradeSugg = ['courseId', 'pupilId']

let notFoundId = '61a8ea9bd4022b3534077c0d'

let expectedGradeSecPupil = 75

// setup

chai.use(chaiHttp);
chai.should();

// hook - done() fires, if healthcheck point answers with 200
before(() => {
    return healthCheck(app);
});

describe(`CRUD Tests for ${recordEndpointToTest}`, () => {
    let testGroupName;
    let testNumber = 11000;

    testGroupName = 'Basic user stuff'
    describe(testGroupName, () => {
        it(`${testNumber++} ${testGroupName} - No Auth. header- 401 `, done => {
            chai
                .request(app)
                .get(recordEndpointToTest)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(401);
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - Create first user - 201`, done => {
            chai
                .request(app)
                .post(userEndpoint)
                .send(user)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(201);
                    res.body.should.be.a('object');

                    res.body.should.have.property('_id').not.to.be.null;
                    res.body.should.have.property('username').not.to.be.null;
                    res.body.should.have.property('state').not.to.be.null;
                    res.body.should.have.property('firstname').not.to.be.null;
                    res.body.should.have.property('lastname').not.to.be.null;

                    user._id = res.body._id;
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - Login first user - 200`, (done) => {
            chai
                .request(app)
                .post('/api/v1/login')
                .send({ 'username': user['username'], 'password': user['password'] })
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(1);
                    res.body.should.have.property('token');
                    user.token = res.body.token;
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - Create second user - 201`, done => {
            chai
                .request(app)
                .post('/api/v1/users')
                .send(secUser)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(201);
                    res.body.should.be.a('object');

                    res.body.should.have.property('_id').not.to.be.null;
                    res.body.should.have.property('username').not.to.be.null;
                    res.body.should.have.property('state').not.to.be.null;
                    res.body.should.have.property('firstname').not.to.be.null;
                    res.body.should.have.property('lastname').not.to.be.null;

                    secUser._id = res.body._id;
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - Login second user - 200`, (done) => {
            chai
                .request(app)
                .post('/api/v1/login')
                .send({ 'username': secUser['username'], 'password': secUser['password'] })
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(1);
                    res.body.should.have.property('token');
                    secUser.token = res.body.token;
                    done();
                });
        });
    })

    testGroupName = 'Create Mock-Data';
    describe(testGroupName, () => {
        describe('POST Mock-Data', () => {
            it(`${testNumber++} ${testGroupName} - create period firstUser - 201`, done => {
                chai
                    .request(app)
                    .post(periodEndpoint)
                    .set('Authorization', user.token)
                    .send(periodToCreate)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(201);
                        res.body.should.be.a('object');

                        neededKeysPeriod.forEach(key => {
                            res.body.should.have.property(key).to.not.be.null
                        })

                        periodToCreate._id = res.body._id;
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - create period secUser - 201`, done => {
                chai
                    .request(app)
                    .post(periodEndpoint)
                    .set('Authorization', secUser.token)
                    .send(periodToCreateSecUser)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(201);
                        res.body.should.be.a('object');

                        neededKeysPeriod.forEach(key => {
                            res.body.should.have.property(key).to.not.be.null
                        })

                        periodToCreateSecUser._id = res.body._id;
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - create courses firstUser - 201`, done => {
                chai
                    .request(app)
                    .post(courseEndpoint)
                    .set('Authorization', user.token)
                    .send(courseToCreateFirstUser)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(201);
                        res.body.should.be.a('object');

                        neededKeysCourse.forEach(key => {
                            res.body.should.have.property(key).to.not.be.null
                        })

                        courseToCreateFirstUser._id = res.body._id;
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - create course secUser - 201`, done => {
                chai
                    .request(app)
                    .post(courseEndpoint)
                    .set('Authorization', secUser.token)
                    .send(courseToCreateSecUser)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(201);
                        res.body.should.be.a('object');

                        neededKeysCourse.forEach(key => {
                            res.body.should.have.property(key).to.not.be.null
                        })

                        courseToCreateSecUser._id = res.body._id;
                        done();
                    });
            });

            pupilsToCreateFirstUser.forEach(p => {
                it(`${testNumber++} ${testGroupName} - create pupils firstUser - 201`, done => {
                    chai
                        .request(app)
                        .post(pupilEndpoint)
                        .set('Authorization', user.token)
                        .send(p)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(201);
                            res.body.should.be.a('object');

                            neededKeysPupil.forEach(key => {
                                res.body.should.have.property(key).to.not.be.null
                            });

                            p._id = res.body._id;
                            done();
                        });
                });
            });

            it(`${testNumber++} ${testGroupName} - create pupil secUser - 201`, done => {
                chai
                    .request(app)
                    .post(pupilEndpoint)
                    .set('Authorization', secUser.token)
                    .send(pupilToCreateSecUser)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(201);
                        res.body.should.be.a('object');

                        neededKeysPupil.forEach(key => {
                            res.body.should.have.property(key).to.not.be.null
                        })

                        pupilToCreateSecUser._id = res.body._id;
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - create ratingscheme firstUser - 201`, done => {
                chai
                    .request(app)
                    .post(ratingschemeEndpoint)
                    .set('Authorization', user.token)
                    .send(ratingschemeToCreate)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(201);
                        res.body.should.be.a('object');

                        neededKeysRatingscheme.forEach(key => {
                            res.body.should.have.property(key).to.not.be.null
                        })

                        ratingschemeToCreate = res.body;
                        recordToCreate.rating = ratingschemeToCreate;

                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - create ratingscheme secUser - 201`, done => {
                chai
                    .request(app)
                    .post(ratingschemeEndpoint)
                    .set('Authorization', secUser.token)
                    .send(ratingschemeToCreateSecUser)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(201);
                        res.body.should.be.a('object');

                        neededKeysRatingscheme.forEach(key => {
                            res.body.should.have.property(key).to.not.be.null
                        })

                        ratingschemeToCreateSecUser = res.body;
                        recordToCreateSecUser.rating = ratingschemeToCreateSecUser;

                        done();
                    });
            });
        })

        describe('PATCH Mock-Data', () => {
            it(`${testNumber++} ${testGroupName} - add pupils to course firstUser - 200`, done => {
                chai
                    .request(app)
                    .patch(courseEndpoint + '/' + courseToCreateFirstUser._id)
                    .set('Authorization', user.token)
                    .send({ pupils: pupilsToCreateFirstUser.map(p => p._id) })
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        res.body.should.be.a('object');

                        neededKeysCourse.forEach(key => {
                            res.body.should.have.property(key).to.not.be.null
                        })
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - add pupil to course secUser - 200`, done => {
                chai
                    .request(app)
                    .patch(courseEndpoint + '/' + courseToCreateSecUser._id)
                    .set('Authorization', secUser.token)
                    .send({ pupils: [pupilToCreateSecUser._id] })
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        res.body.should.be.a('object');

                        neededKeysCourse.forEach(key => {
                            res.body.should.have.property(key).to.not.be.null
                        })
                        done();
                    });
            });
        })

        describe('GET Mock-Data', () => {
            it(`${testNumber++} ${testGroupName} - GET created period firstuser - 200 `, done => {
                chai
                    .request(app)
                    .get(periodEndpoint + '/' + periodToCreate._id)
                    .set('Authorization', user.token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        res.body.should.be.a('object');

                        neededKeysPeriod.forEach(key => res.body.should.have.property(key).to.not.be.null)
                        expect(res.body._id).to.be.equal(periodToCreate._id);
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - GET created period secUser - 200 `, done => {
                chai
                    .request(app)
                    .get(periodEndpoint + '/' + periodToCreateSecUser._id)
                    .set('Authorization', secUser.token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        res.body.should.be.a('object');

                        neededKeysPeriod.forEach(key => res.body.should.have.property(key).to.not.be.null)
                        expect(res.body._id).to.be.equal(periodToCreateSecUser._id);
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - GET created course firstuser - 200 `, done => {
                chai
                    .request(app)
                    .get(courseEndpoint + '/' + courseToCreateFirstUser._id)
                    .set('Authorization', user.token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        res.body.should.be.a('object');

                        neededKeysCourse.forEach(key => res.body.should.have.property(key).to.not.be.null)
                        expect(res.body._id).to.be.equal(courseToCreateFirstUser._id);
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - GET created course secUser - 200 `, done => {
                chai
                    .request(app)
                    .get(courseEndpoint + '/' + courseToCreateSecUser._id)
                    .set('Authorization', secUser.token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        res.body.should.be.a('object');

                        neededKeysCourse.forEach(key => res.body.should.have.property(key).to.not.be.null)
                        expect(res.body._id).to.be.equal(courseToCreateSecUser._id);
                        done();
                    });
            });

            pupilsToCreateFirstUser.forEach(p => {
                it(`${testNumber++} ${testGroupName} - GET created pupil firstUser - 200 `, done => {
                    chai
                        .request(app)
                        .get(pupilEndpoint + '/' + p._id)
                        .set('Authorization', user.token)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(200);
                            res.body.should.be.a('object');

                            neededKeysPupil.forEach(key => res.body.should.have.property(key).to.not.be.null)
                            expect(res.body._id).to.be.equal(p._id);
                            done();
                        });
                });
            });

            it(`${testNumber++} ${testGroupName} - GET created pupil secUser - 200 `, done => {
                chai
                    .request(app)
                    .get(pupilEndpoint + '/' + pupilToCreateSecUser._id)
                    .set('Authorization', secUser.token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        res.body.should.be.a('object');

                        neededKeysPupil.forEach(key => res.body.should.have.property(key).to.not.be.null)
                        expect(res.body._id).to.be.equal(pupilToCreateSecUser._id);
                        done();
                    });
            });


            it(`${testNumber++} ${testGroupName} - GET created ratingscheme firstUser - 200 `, done => {
                chai
                    .request(app)
                    .get(ratingschemeEndpoint + '/' + ratingschemeToCreate._id)
                    .set('Authorization', user.token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        res.body.should.be.a('object');

                        neededKeysRatingscheme.forEach(key => res.body.should.have.property(key).to.not.be.null)
                        expect(res.body._id).to.be.equal(ratingschemeToCreate._id);
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - GET created ratingscheme secUser - 200 `, done => {
                chai
                    .request(app)
                    .get(ratingschemeEndpoint + '/' + ratingschemeToCreateSecUser._id)
                    .set('Authorization', secUser.token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        res.body.should.be.a('object');

                        neededKeysRatingscheme.forEach(key => res.body.should.have.property(key).to.not.be.null)
                        expect(res.body._id).to.be.equal(ratingschemeToCreateSecUser._id);
                        done();
                    });
            });

            pupilsToCreateFirstUser.forEach((p, idx) => {
                it(`${testNumber++} ${testGroupName} - GET cpr-connection firstUser - 200 `, done => {
                    chai
                        .request(app)
                        .get(cprEndpoint + '?' + getCPRQuery(p._id))
                        .set('Authorization', user.token)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(200);
                            res.body.should.be.a('array');
                            res.body.length.should.equal(1);

                            for (let cpr of res.body) {
                                neededKeysCPR.forEach(key => cpr.should.have.property(key).to.not.be.null)
                            }
                            res.body[0].courseId.should.equal(courseToCreateFirstUser._id)
                            res.body[0].pupilId.should.equal(p._id)
                            cprIdsFirstUser[idx] = res.body[0]._id;
                            recordToCreate.cprId = cprIdsFirstUser[0];
                            recordForGradeSuggPos.cprId = cprIdsFirstUser[idx];
                            recordForGradeSuggNeg.cprId = cprIdsFirstUser[idx];
                            recordToCreate.rating = ratingschemeToCreate.ratings[0];
                            recordForGradeSuggPos.rating = ratingschemeToCreate.ratings[0];
                            recordForGradeSuggNeg.rating = ratingschemeToCreate.ratings[1];
                            done();
                        });
                });
            });

            it(`${testNumber++} ${testGroupName} - GET cpr-connection secUser - 200 `, done => {
                chai
                    .request(app)
                    .get(cprEndpoint + '?' + getCPRQuery(pupilToCreateSecUser._id))
                    .set('Authorization', secUser.token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.equal(1);

                        for (let cpr of res.body) {
                            neededKeysCPR.forEach(key => cpr.should.have.property(key).to.not.be.null)
                        }

                        res.body[0].courseId.should.equal(courseToCreateSecUser._id)
                        res.body[0].pupilId.should.equal(pupilToCreateSecUser._id)
                        cprIdSecUser = res.body[0]._id;
                        recordToCreateSecUser.cprId = cprIdSecUser
                        recordToCreateSecUser.rating = ratingschemeToCreateSecUser.ratings[0];

                        done();
                    });
            });
        })
    })

    testGroupName = 'POST'
    describe(testGroupName, () => {
        describe('check basic test cases', () => {
            // create
            it(`${testNumber++} ${testGroupName} - create record - 201`, done => {
                chai
                    .request(app)
                    .post(recordEndpointToTest)
                    .set('Authorization', user.token)
                    .send(recordToCreate)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(201);
                        res.body.should.be.a('object');

                        neededKeysRecord.forEach(key => {
                            res.body.should.have.property(key).to.not.be.null
                        })

                        recordToCreate._id = res.body._id;
                        done();
                    });
            });
            // get created
            it(`${testNumber++} ${testGroupName} - GET created record - 200 `, done => {
                chai
                    .request(app)
                    .get(recordEndpointToTest + '/' + recordToCreate._id)
                    .set('Authorization', user.token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        res.body.should.be.a('object');

                        neededKeysRecord.forEach(key => res.body.should.have.property(key).to.not.be.null)
                        expect(res.body._id).to.be.equal(recordToCreate._id);
                        done();
                    });
            });
            // delete created
            it(`${testNumber++} ${testGroupName} - delete created record - 204`, done => {
                chai
                    .request(app)
                    .delete(recordEndpointToTest + '/' + recordToCreate._id)
                    .set('Authorization', user.token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(204);

                        recordToCreate._id = undefined;
                        done();
                    });
            });
            // create
            it(`${testNumber++} ${testGroupName} - create record secUser - 201`, done => {
                chai
                    .request(app)
                    .post(recordEndpointToTest)
                    .set('Authorization', secUser.token)
                    .send(recordToCreateSecUser)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(201);
                        res.body.should.be.a('object');

                        neededKeysRecord.forEach(key => {
                            res.body.should.have.property(key).to.not.be.null
                        })

                        recordToCreateSecUser._id = res.body._id;
                        done();
                    });
            });
            // get created
            it(`${testNumber++} ${testGroupName} - GET created record secUser - 200 `, done => {
                chai
                    .request(app)
                    .get(recordEndpointToTest + '/' + recordToCreateSecUser._id)
                    .set('Authorization', secUser.token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        res.body.should.be.a('object');

                        neededKeysRecord.forEach(key => res.body.should.have.property(key).to.not.be.null)
                        expect(res.body._id).to.be.equal(recordToCreateSecUser._id);
                        done();
                    });
            });
            // delete created
            it(`${testNumber++} ${testGroupName} - delete created record secUser - 204`, done => {
                chai
                    .request(app)
                    .delete(recordEndpointToTest + '/' + recordToCreateSecUser._id)
                    .set('Authorization', secUser.token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(204);

                        recordToCreate._id = undefined;
                        done();
                    });
            });


            /* --------------------- property tests --------------------- */

            /* --------------------- id --------------------- */
            describe('check property id ', () => {
                let wrongRatings = [];

                let rating = { ...recordToCreate };
                rating._id = {};
                wrongRatings.push(rating);

                rating = { ...recordToCreate };
                rating._id = 12432434;
                wrongRatings.push(rating);

                rating = { ...recordToCreate };
                rating._id = 'JALKJ23kJ21';
                wrongRatings.push(rating);

                rating = { ...recordToCreate };
                rating._id = notFoundId;
                wrongRatings.push(rating);

                wrongRatings.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - id - 400`, done => {
                        chai
                            .request(app)
                            .patch(recordEndpointToTest + '/' + recordToCreate._id)
                            .set('Authorization', user.token)
                            .send({ ...we, ...{ 'cprId': cprIdsFirstUser[0], 'rating': ratingschemeToCreate.ratings[0] } })
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- cprId -------------------------- */
            describe('check property cprId ', () => {
                let wrongRecords = [];

                let record = { ...recordToCreate };
                record.cprId = '1870'
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.cprId = 12344
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.cprId = undefined;
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.cprId = null;
                wrongRecords.push(record);

                wrongRecords.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - cprId - 400`, done => {
                        chai
                            .request(app)
                            .post(recordEndpointToTest)
                            .set('Authorization', user.token)
                            .send({ ...we, ...{ 'rating': ratingschemeToCreate.ratings[0] } })
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- rating -------------------------- */
            describe('check property rating ', () => {
                let wrongRecords = [];

                let record = { ...recordToCreate };
                record.rating = {}
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.rating = []
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.rating = '1870'
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.rating = 12344
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.rating = undefined
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.rating = null
                wrongRecords.push(record);

                wrongRecords.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - rating - 400`, done => {
                        chai
                            .request(app)
                            .post(recordEndpointToTest)
                            .set('Authorization', user.token)
                            .send({ ...we, ...{ 'cprId': cprIdsFirstUser[0] } })
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- weight -------------------------- */
            describe('check property weight ', () => {
                let wrongRecords = [];

                let record = { ...recordToCreate };
                record.weight = undefined
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.weight = null
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.weight = {}
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.weight = []
                wrongRecords.push(record);

                wrongRecords.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - weight - 400`, done => {
                        chai
                            .request(app)
                            .post(recordEndpointToTest)
                            .set('Authorization', user.token)
                            .send({ ...we, ...{ 'cprId': cprIdsFirstUser[0], 'rating': ratingschemeToCreate.ratings[0] } })
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });


            /* --------------------------- validOn -------------------------- */
            describe('check property validOn ', () => {
                let wrongRecords = [];

                let record = { ...recordToCreate };
                record.validOn = true;
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.validOn = undefined;
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.validOn = null;
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.validOn = 12344;
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.validOn = '1870';
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.validOn = '300.01.01';
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.validOn = '30000.01.01';
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.validOn = '3000.111.01';
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.validOn = '3000.01.101';
                wrongRecords.push(record);

                wrongRecords.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - validOn - 400`, done => {
                        chai
                            .request(app)
                            .post(recordEndpointToTest)
                            .set('Authorization', user.token)
                            .send({ ...we, ...{ 'cprId': cprIdsFirstUser[0], 'rating': ratingschemeToCreate.ratings[0] } })
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });


            /* --------------------------- pupilNotPresent -------------------------- */
            describe('check property pupilNotPresent ', () => {
                let wrongRecords = [];

                let record = { ...recordToCreate };
                record.pupilNotPresent = 'true';
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.pupilNotPresent = 12344;
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.pupilNotPresent = '1870';
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.pupilNotPresent = [];
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.pupilNotPresent = {};
                wrongRecords.push(record);

                wrongRecords.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - pupilNotPresent - 400`, done => {
                        chai
                            .request(app)
                            .post(recordEndpointToTest)
                            .set('Authorization', user.token)
                            .send({ ...we, ...{ 'cprId': cprIdsFirstUser[0], 'rating': ratingschemeToCreate.ratings[0] } })
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- tags -------------------------- */
            describe('check property tags ', () => {
                let wrongRecords = [];

                let record = { ...recordToCreate };
                record.tags = 'true';
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.tags = false;
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.tags = 12344;
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.tags = '1870';
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.tags = {};
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.tags = ['test', {}];
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.tags = ['test', []];
                wrongRecords.push(record);

                wrongRecords.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - tags - 400`, done => {
                        chai
                            .request(app)
                            .post(recordEndpointToTest)
                            .set('Authorization', user.token)
                            .send({ ...we, ...{ 'cprId': cprIdsFirstUser[0], 'rating': ratingschemeToCreate.ratings[0] } })
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- missing/not allowed properies-------------------------- */
            describe('missing/not allowed properties', () => {
                let wrongRecords = [];

                let record = { ...recordToCreate };
                delete record.weight;
                wrongRecords.push(record);

                record = { ...recordToCreate };
                delete record.validOn;
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.notAllowed = 'hihihi';
                wrongRecords.push(record);

                wrongRecords.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - missing/not allowed properies - 400`, done => {
                        chai
                            .request(app)
                            .post(recordEndpointToTest)
                            .set('Authorization', user.token)
                            .send({ ...we, ...{ 'cprId': cprIdsFirstUser[0], 'rating': ratingschemeToCreate.ratings[0] } })
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });
        });

        describe('check bulkadd', () => {
            describe('check properties', () => {
                describe('check property courseId', () => {
                    let wrongBulkAdds = [];

                    let wrongBulkAdd = { ...bulkAddTemplate };
                    wrongBulkAdd.courseId = true;
                    wrongBulkAdds.push(wrongBulkAdd);

                    wrongBulkAdd = { ...bulkAddTemplate };
                    delete wrongBulkAdd.courseId;
                    wrongBulkAdds.push(wrongBulkAdd);

                    wrongBulkAdd = { ...bulkAddTemplate };
                    wrongBulkAdd.courseId = '123';
                    wrongBulkAdds.push(wrongBulkAdd);

                    wrongBulkAdd = { ...bulkAddTemplate };
                    wrongBulkAdd.courseId = 123;
                    wrongBulkAdds.push(wrongBulkAdd);

                    wrongBulkAdd = { ...bulkAddTemplate };
                    wrongBulkAdd.courseId = undefined;
                    wrongBulkAdds.push(wrongBulkAdd);

                    wrongBulkAdd = { ...bulkAddTemplate };
                    wrongBulkAdd.courseId = null;
                    wrongBulkAdds.push(wrongBulkAdd);

                    wrongBulkAdds.forEach(wb => {
                        it(`${testNumber++} ${testGroupName} - check courseid - 400`, done => {
                            let spreadObjectToAdjustKeys = {
                                records: pupilsToCreateFirstUser.map((p, idx) => {
                                    return {
                                        ...bulkAddTemplate.records[idx],
                                        pupilId: p._id,
                                        ratingId: ratingschemeToCreate.ratings[idx]._id
                                    }
                                })
                            };
                            let objToSend = { ...wb, ...spreadObjectToAdjustKeys }

                            chai
                                .request(app)
                                .post(bulkAddEndpoint)
                                .set('Authorization', user.token)
                                .send(objToSend)
                                .end((err, res) => {
                                    expect(err).to.be.null;
                                    res.should.have.status(400);
                                    done();
                                });
                        });
                    });
                });

                describe('check property validOn', () => {
                    let wrongBulkAdds = [];

                    let wrongBulkAdd = { ...bulkAddTemplate };
                    wrongBulkAdd.validOn = true;
                    wrongBulkAdds.push(wrongBulkAdd);

                    wrongBulkAdd = { ...bulkAddTemplate };
                    wrongBulkAdd.validOn = undefined;
                    wrongBulkAdds.push(wrongBulkAdd);

                    wrongBulkAdd = { ...bulkAddTemplate };
                    wrongBulkAdd.validOn = null;
                    wrongBulkAdds.push(wrongBulkAdd);

                    wrongBulkAdd = { ...bulkAddTemplate };
                    wrongBulkAdd.validOn = 1234;
                    wrongBulkAdds.push(wrongBulkAdd);

                    wrongBulkAdd = { ...bulkAddTemplate };
                    wrongBulkAdd.validOn = '1870';
                    wrongBulkAdds.push(wrongBulkAdd);

                    wrongBulkAdd = { ...bulkAddTemplate };
                    wrongBulkAdd.validOn = '300.01.01';
                    wrongBulkAdds.push(wrongBulkAdd);

                    wrongBulkAdd = { ...bulkAddTemplate };
                    wrongBulkAdd.validOn = '30000.01.01';
                    wrongBulkAdds.push(wrongBulkAdd);

                    wrongBulkAdd = { ...bulkAddTemplate };
                    wrongBulkAdd.validOn = '3000.111.01';
                    wrongBulkAdds.push(wrongBulkAdd);

                    wrongBulkAdd = { ...bulkAddTemplate };
                    wrongBulkAdd.validOn = '3000.11.101';
                    wrongBulkAdds.push(wrongBulkAdd);

                    wrongBulkAdds.forEach(wb => {
                        it(`${testNumber++} ${testGroupName} - check validOn - 400`, done => {
                            let spreadObjectToAdjustKeys = {
                                courseId: courseToCreateFirstUser._id,
                                records: pupilsToCreateFirstUser.map((p, idx) => {
                                    return {
                                        ...bulkAddTemplate.records[idx],
                                        pupilId: p._id,
                                        ratingId: ratingschemeToCreate.ratings[idx]._id
                                    }
                                })
                            };
                            let objToSend = { ...wb, ...spreadObjectToAdjustKeys }

                            chai
                                .request(app)
                                .post(bulkAddEndpoint)
                                .set('Authorization', user.token)
                                .send(objToSend)
                                .end((err, res) => {
                                    expect(err).to.be.null;
                                    res.should.have.status(400);
                                    done();
                                });
                        });
                    });
                });

                describe('check property weight', () => {
                    let wrongBulkAdds = [];

                    let wrongBulkAdd = { ...bulkAddTemplate };
                    wrongBulkAdd.weight = undefined;
                    wrongBulkAdds.push(wrongBulkAdd);

                    wrongBulkAdd = { ...bulkAddTemplate };
                    wrongBulkAdd.weight = null;
                    wrongBulkAdds.push(wrongBulkAdd);

                    wrongBulkAdd = { ...bulkAddTemplate };
                    wrongBulkAdd.weight = {};
                    wrongBulkAdds.push(wrongBulkAdd);

                    wrongBulkAdd = { ...bulkAddTemplate };
                    wrongBulkAdd.weight = [];
                    wrongBulkAdds.push(wrongBulkAdd);

                    wrongBulkAdd = { ...bulkAddTemplate };
                    wrongBulkAdd.weight = 'test';
                    wrongBulkAdds.push(wrongBulkAdd);

                    wrongBulkAdds.forEach(wb => {
                        it(`${testNumber++} ${testGroupName} - check weight - 400`, done => {
                            let spreadObjectToAdjustKeys = {
                                courseId: courseToCreateFirstUser._id,
                                records: pupilsToCreateFirstUser.map((p, idx) => {
                                    return {
                                        ...bulkAddTemplate.records[idx],
                                        pupilId: p._id,
                                        ratingId: ratingschemeToCreate.ratings[idx]._id
                                    }
                                })
                            };
                            let objToSend = { ...wb, ...spreadObjectToAdjustKeys }

                            chai
                                .request(app)
                                .post(bulkAddEndpoint)
                                .set('Authorization', user.token)
                                .send(objToSend)
                                .end((err, res) => {
                                    expect(err).to.be.null;
                                    res.should.have.status(400);
                                    done();
                                });
                        });
                    });
                });

                describe('check property tags', () => {
                    let wrongBulkAdds = [];

                    let wrongBulkAdd = { ...bulkAddTemplate };
                    wrongBulkAdd.tags = 'true';
                    wrongBulkAdds.push(wrongBulkAdd);

                    wrongBulkAdd = { ...bulkAddTemplate };
                    wrongBulkAdd.tags = false;
                    wrongBulkAdds.push(wrongBulkAdd);

                    wrongBulkAdd = { ...bulkAddTemplate };
                    wrongBulkAdd.tags = 1234;
                    wrongBulkAdds.push(wrongBulkAdd);

                    wrongBulkAdd = { ...bulkAddTemplate };
                    wrongBulkAdd.tags = '1870';
                    wrongBulkAdds.push(wrongBulkAdd);

                    wrongBulkAdd = { ...bulkAddTemplate };
                    wrongBulkAdd.tags = {};
                    wrongBulkAdds.push(wrongBulkAdd);

                    wrongBulkAdd = { ...bulkAddTemplate };
                    wrongBulkAdd.tags = ['test', []];
                    wrongBulkAdds.push(wrongBulkAdd);

                    wrongBulkAdd = { ...bulkAddTemplate };
                    wrongBulkAdd.tags = ['test', {}];
                    wrongBulkAdds.push(wrongBulkAdd);

                    wrongBulkAdds.forEach(wb => {
                        it(`${testNumber++} ${testGroupName} - check tags - 400`, done => {
                            let spreadObjectToAdjustKeys = {
                                courseId: courseToCreateFirstUser._id,
                                records: pupilsToCreateFirstUser.map((p, idx) => {
                                    return {
                                        ...bulkAddTemplate.records[idx],
                                        pupilId: p._id,
                                        ratingId: ratingschemeToCreate.ratings[idx]._id
                                    }
                                })
                            };
                            let objToSend = { ...wb, ...spreadObjectToAdjustKeys }

                            chai
                                .request(app)
                                .post(bulkAddEndpoint)
                                .set('Authorization', user.token)
                                .send(objToSend)
                                .end((err, res) => {
                                    expect(err).to.be.null;
                                    res.should.have.status(400);
                                    done();
                                });
                        });
                    });
                });

                describe('check property records', () => {
                    let wrongBulkAdds = [];

                    let wrongBulkAdd = { ...bulkAddTemplate };
                    wrongBulkAdd.records = 1234;
                    wrongBulkAdds.push(wrongBulkAdd);

                    wrongBulkAdd = { ...bulkAddTemplate };
                    wrongBulkAdd.records = false;
                    wrongBulkAdds.push(wrongBulkAdd);

                    wrongBulkAdd = { ...bulkAddTemplate };
                    wrongBulkAdd.records = false;
                    wrongBulkAdds.push(wrongBulkAdd);

                    wrongBulkAdd = { ...bulkAddTemplate };
                    wrongBulkAdd.records = {};
                    wrongBulkAdds.push(wrongBulkAdd);

                    wrongBulkAdds.forEach(wb => {
                        it(`${testNumber++} ${testGroupName} - check records property - 400`, done => {
                            let spreadObjectToAdjustKeys = {
                                courseId: courseToCreateFirstUser._id
                            };
                            let objToSend = { ...wb, ...spreadObjectToAdjustKeys }

                            chai
                                .request(app)
                                .post(bulkAddEndpoint)
                                .set('Authorization', user.token)
                                .send(objToSend)
                                .end((err, res) => {
                                    expect(err).to.be.null;
                                    res.should.have.status(400);
                                    done();
                                });
                        });
                    });
                });

                describe('missing/not allowed properties', () => {
                    let wrongBulkAdds = [];

                    let wrongBulkAdd = { ...bulkAddTemplate };
                    wrongBulkAdd.notallowed = 'hihihi';
                    wrongBulkAdds.push(wrongBulkAdd);

                    wrongBulkAdd = { ...bulkAddTemplate };
                    delete wrongBulkAdd.validOn;
                    wrongBulkAdds.push(wrongBulkAdd);

                    wrongBulkAdd = { ...bulkAddTemplate };
                    delete wrongBulkAdd.weight;
                    wrongBulkAdds.push(wrongBulkAdd);

                    wrongBulkAdds.forEach(wb => {
                        it(`${testNumber++} ${testGroupName} - check validOn - 400`, done => {
                            let spreadObjectToAdjustKeys = {
                                courseId: courseToCreateFirstUser._id,
                                records: pupilsToCreateFirstUser.map((p, idx) => {
                                    return {
                                        ...bulkAddTemplate.records[idx],
                                        pupilId: p._id,
                                        ratingId: ratingschemeToCreate.ratings[idx]._id
                                    }
                                })
                            };
                            let objToSend = { ...wb, ...spreadObjectToAdjustKeys }

                            chai
                                .request(app)
                                .post(bulkAddEndpoint)
                                .set('Authorization', user.token)
                                .send(objToSend)
                                .end((err, res) => {
                                    expect(err).to.be.null;
                                    res.should.have.status(400);
                                    done();
                                });
                        });
                    });
                });

                describe('check records', () => {
                    describe('check property pupilId', () => {
                        let wrongBulkAdds = [];

                        let wrongBulkAdd = { ...bulkAddTemplate.records[0] };
                        wrongBulkAdd.pupilId = true;
                        wrongBulkAdds.push(wrongBulkAdd);

                        wrongBulkAdd = { ...bulkAddTemplate.records[0] };
                        delete wrongBulkAdd.pupilId;
                        wrongBulkAdds.push(wrongBulkAdd);

                        wrongBulkAdd = { ...bulkAddTemplate.records[0] };
                        wrongBulkAdd.pupilId = '123';
                        wrongBulkAdds.push(wrongBulkAdd);

                        wrongBulkAdd = { ...bulkAddTemplate.records[0] };
                        wrongBulkAdd.pupilId = 123;
                        wrongBulkAdds.push(wrongBulkAdd);

                        wrongBulkAdd = { ...bulkAddTemplate.records[0] };
                        wrongBulkAdd.pupilId = undefined;
                        wrongBulkAdds.push(wrongBulkAdd);

                        wrongBulkAdd = { ...bulkAddTemplate.records[0] };
                        wrongBulkAdd.pupilId = null;
                        wrongBulkAdds.push(wrongBulkAdd);

                        wrongBulkAdds.forEach(wb => {
                            it(`${testNumber++} ${testGroupName} - check pupilId - 400`, done => {
                                let spreadObjectToAdjustKeys = {
                                    courseId: courseToCreateFirstUser._id,
                                    records: [
                                        {
                                            ...wb,
                                            ratingId: ratingschemeToCreate.ratings[0]._id
                                        },
                                        {
                                            ...bulkAddTemplate.records[1],
                                            pupilId: pupilsToCreateFirstUser[1]._id,
                                            ratingId: ratingschemeToCreate.ratings[1]._id
                                        }
                                    ]
                                };
                                let objToSend = { ...bulkAddTemplate, ...spreadObjectToAdjustKeys }

                                chai
                                    .request(app)
                                    .post(bulkAddEndpoint)
                                    .set('Authorization', user.token)
                                    .send(objToSend)
                                    .end((err, res) => {
                                        expect(err).to.be.null;
                                        res.should.have.status(400);
                                        done();
                                    });
                            });
                        });
                    });

                    describe('check property ratingId', () => {
                        let wrongBulkAdds = [];

                        let wrongBulkAdd = { ...bulkAddTemplate.records[0] };
                        wrongBulkAdd.ratingId = true;
                        wrongBulkAdds.push(wrongBulkAdd);

                        wrongBulkAdd = { ...bulkAddTemplate.records[0] };
                        delete wrongBulkAdd.ratingId;
                        wrongBulkAdds.push(wrongBulkAdd);

                        wrongBulkAdd = { ...bulkAddTemplate.records[0] };
                        wrongBulkAdd.ratingId = '123';
                        wrongBulkAdds.push(wrongBulkAdd);

                        wrongBulkAdd = { ...bulkAddTemplate.records[0] };
                        wrongBulkAdd.ratingId = 123;
                        wrongBulkAdds.push(wrongBulkAdd);

                        wrongBulkAdd = { ...bulkAddTemplate.records[0] };
                        wrongBulkAdd.ratingId = undefined;
                        wrongBulkAdds.push(wrongBulkAdd);

                        wrongBulkAdd = { ...bulkAddTemplate.records[0] };
                        wrongBulkAdd.ratingId = null;
                        wrongBulkAdds.push(wrongBulkAdd);

                        wrongBulkAdds.forEach(wb => {
                            it(`${testNumber++} ${testGroupName} - check pupilId - 400`, done => {
                                let spreadObjectToAdjustKeys = {
                                    courseId: courseToCreateFirstUser._id,
                                    records: [
                                        {
                                            ...wb,
                                            pupilId: pupilsToCreateFirstUser[0]._id
                                        },
                                        {
                                            ...bulkAddTemplate.records[1],
                                            pupilId: pupilsToCreateFirstUser[1]._id,
                                            ratingId: ratingschemeToCreate.ratings[1]._id
                                        }
                                    ]
                                };
                                let objToSend = { ...bulkAddTemplate, ...spreadObjectToAdjustKeys }

                                chai
                                    .request(app)
                                    .post(bulkAddEndpoint)
                                    .set('Authorization', user.token)
                                    .send(objToSend)
                                    .end((err, res) => {
                                        expect(err).to.be.null;
                                        res.should.have.status(400);
                                        done();
                                    });
                            });
                        });
                    });

                    describe('check property tags', () => {
                        let wrongBulkAdds = [];

                        let wrongBulkAdd = { ...bulkAddTemplate.records[0] };
                        wrongBulkAdd.tags = 'true';
                        wrongBulkAdds.push(wrongBulkAdd);

                        wrongBulkAdd = { ...bulkAddTemplate.records[0] };
                        wrongBulkAdd.tags = false;
                        wrongBulkAdds.push(wrongBulkAdd);

                        wrongBulkAdd = { ...bulkAddTemplate.records[0] };
                        wrongBulkAdd.tags = 1234;
                        wrongBulkAdds.push(wrongBulkAdd);

                        wrongBulkAdd = { ...bulkAddTemplate.records[0] };
                        wrongBulkAdd.tags = '1870';
                        wrongBulkAdds.push(wrongBulkAdd);

                        wrongBulkAdd = { ...bulkAddTemplate.records[0] };
                        wrongBulkAdd.tags = {};
                        wrongBulkAdds.push(wrongBulkAdd);

                        wrongBulkAdd = { ...bulkAddTemplate.records[0] };
                        wrongBulkAdd.tags = ['test', []];
                        wrongBulkAdds.push(wrongBulkAdd);

                        wrongBulkAdd = { ...bulkAddTemplate.records[0] };
                        wrongBulkAdd.tags = ['test', {}];
                        wrongBulkAdds.push(wrongBulkAdd);

                        wrongBulkAdds.forEach(wb => {
                            it(`${testNumber++} ${testGroupName} - check pupilId - 400`, done => {
                                let spreadObjectToAdjustKeys = {
                                    courseId: courseToCreateFirstUser._id,
                                    records: [
                                        {
                                            ...wb,
                                            pupilId: pupilsToCreateFirstUser[0]._id,
                                            ratingId: ratingschemeToCreate.ratings[0]._id
                                        },
                                        {
                                            ...bulkAddTemplate.records[1],
                                            pupilId: pupilsToCreateFirstUser[1]._id,
                                            ratingId: ratingschemeToCreate.ratings[1]._id
                                        }
                                    ]
                                };
                                let objToSend = { ...bulkAddTemplate, ...spreadObjectToAdjustKeys }

                                chai
                                    .request(app)
                                    .post(bulkAddEndpoint)
                                    .set('Authorization', user.token)
                                    .send(objToSend)
                                    .end((err, res) => {
                                        expect(err).to.be.null;
                                        res.should.have.status(400);
                                        done();
                                    });
                            });
                        });
                    });

                    describe('check property pupilNotPresent', () => {
                        let wrongBulkAdds = [];

                        let wrongBulkAdd = { ...bulkAddTemplate.records[0] };
                        wrongBulkAdd.pupilNotPresent = 'test';
                        wrongBulkAdds.push(wrongBulkAdd);

                        wrongBulkAdd = { ...bulkAddTemplate.records[0] };
                        wrongBulkAdd.pupilNotPresent = 1234;
                        wrongBulkAdds.push(wrongBulkAdd);

                        wrongBulkAdd = { ...bulkAddTemplate.records[0] };
                        wrongBulkAdd.pupilNotPresent = {};
                        wrongBulkAdds.push(wrongBulkAdd);

                        wrongBulkAdd = { ...bulkAddTemplate.records[0] };
                        wrongBulkAdd.pupilNotPresent = [];
                        wrongBulkAdds.push(wrongBulkAdd);

                        wrongBulkAdds.forEach(wb => {
                            it(`${testNumber++} ${testGroupName} - check pupilId - 400`, done => {
                                let spreadObjectToAdjustKeys = {
                                    courseId: courseToCreateFirstUser._id,
                                    records: [
                                        {
                                            ...wb,
                                            pupilId: pupilsToCreateFirstUser[0]._id
                                        },
                                        {
                                            ...bulkAddTemplate.records[1],
                                            pupilId: pupilsToCreateFirstUser[1]._id,
                                            ratingId: ratingschemeToCreate.ratings[1]._id
                                        }
                                    ]
                                };
                                let objToSend = { ...bulkAddTemplate, ...spreadObjectToAdjustKeys }

                                chai
                                    .request(app)
                                    .post(bulkAddEndpoint)
                                    .set('Authorization', user.token)
                                    .send(objToSend)
                                    .end((err, res) => {
                                        expect(err).to.be.null;
                                        res.should.have.status(400);
                                        done();
                                    });
                            });
                        });
                    });

                    describe('missing/not allowed properties', () => {
                        let wrongBulkAdds = [];

                        let wrongBulkAdd = { ...bulkAddTemplate.records[0] };
                        wrongBulkAdd.notallowed = 'hihihi';
                        wrongBulkAdds.push(wrongBulkAdd);

                        wrongBulkAdds.forEach(wb => {
                            it(`${testNumber++} ${testGroupName} - check pupilId - 400`, done => {
                                let spreadObjectToAdjustKeys = {
                                    courseId: courseToCreateFirstUser._id,
                                    records: [
                                        {
                                            ...wb,
                                            pupilId: pupilsToCreateFirstUser[0]._id,
                                            ratingId: ratingschemeToCreate.ratings[0]._id
                                        },
                                        {
                                            ...bulkAddTemplate.records[1],
                                            pupilId: pupilsToCreateFirstUser[1]._id,
                                            ratingId: ratingschemeToCreate.ratings[1]._id
                                        }
                                    ]
                                };
                                let objToSend = { ...bulkAddTemplate, ...spreadObjectToAdjustKeys }

                                chai
                                    .request(app)
                                    .post(bulkAddEndpoint)
                                    .set('Authorization', user.token)
                                    .send(objToSend)
                                    .end((err, res) => {
                                        expect(err).to.be.null;
                                        res.should.have.status(400);
                                        done();
                                    });
                            });
                        });
                    });
                });
            });

            describe('check pupilnotpresent but rating given', () => {
                it(`${testNumber++} ${testGroupName} - check pupilId - 400`, done => {
                    let spreadObjectToAdjustKeys = {
                        courseId: courseToCreateFirstUser._id,
                        records: [
                            {
                                pupilId: pupilsToCreateFirstUser[0]._id,
                                pupilNotPresent: true,
                                ratingId: ratingschemeToCreate.ratings[0]._id
                            },
                            {
                                ...bulkAddTemplate.records[1],
                                pupilId: pupilsToCreateFirstUser[1]._id,
                                ratingId: ratingschemeToCreate.ratings[1]._id
                            }
                        ]
                    };
                    let objToSend = { ...bulkAddTemplate, ...spreadObjectToAdjustKeys }

                    chai
                        .request(app)
                        .post(bulkAddEndpoint)
                        .set('Authorization', user.token)
                        .send(objToSend)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(400);
                            done();
                        });
                });
            });

            describe('successful bulkadd', () => {
                let successfulBulkadds = [];

                let successfulBulkAdd = { ...bulkAddTemplate };
                successfulBulkadds.push(successfulBulkAdd);

                successfulBulkAdd = { ...bulkAddTemplate };
                delete successfulBulkAdd.tags;
                successfulBulkadds.push(successfulBulkAdd);

                successfulBulkadds.forEach(ba => {
                    it(`${testNumber++} ${testGroupName} - working bulkadd`, done => {
                        let spreadObjectToAdjustKeys = {
                            courseId: courseToCreateFirstUser._id,
                            records: pupilsToCreateFirstUser.map((p, idx) => {
                                return {
                                    ...bulkAddTemplate.records[idx],
                                    pupilId: p._id,
                                    ratingId: ratingschemeToCreate.ratings[idx]._id
                                }
                            })
                        };
                        let objToSend = { ...ba, ...spreadObjectToAdjustKeys }

                        chai
                            .request(app)
                            .post(bulkAddEndpoint)
                            .set('Authorization', user.token)
                            .send(objToSend)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(201);

                                res.body.should.be.an('array');
                                expect(res.body.length).to.equal(objToSend.records.length);

                                res.body.forEach((b, idx) => {
                                    neededKeysRecord.forEach(key => b.should.have.property(key).to.not.be.null);
                                    b.rating._id.should.equal(objToSend.records[idx].ratingId);
                                    ba.records[idx]._id = b._id;
                                });

                                done();
                            });
                    });

                    ba.records.forEach(r => {
                        it(`${testNumber++} ${testGroupName} - GET created record - 200 `, done => {
                            chai
                                .request(app)
                                .get(recordEndpointToTest + '/' + r._id)
                                .set('Authorization', user.token)
                                .end((err, res) => {
                                    expect(err).to.be.null;
                                    res.should.have.status(200);
                                    res.body.should.be.a('object');

                                    neededKeysRecord.forEach(key => res.body.should.have.property(key).to.not.be.null)
                                    expect(res.body._id).to.be.equal(r._id);
                                    done();
                                });
                        });

                        it(`${testNumber++} ${testGroupName} - delete created record - 204`, done => {
                            chai
                                .request(app)
                                .delete(recordEndpointToTest + '/' + r._id)
                                .set('Authorization', user.token)
                                .end((err, res) => {
                                    expect(err).to.be.null;
                                    res.should.have.status(204);

                                    r._id = undefined;
                                    done();
                                });
                        });
                    });
                });
            });
        });
    });

    testGroupName = 'GET SINGLE'
    describe(testGroupName, () => {
        // create
        it(`${testNumber++} ${testGroupName} - create record - 201`, done => {
            chai
                .request(app)
                .post(recordEndpointToTest)
                .set('Authorization', user.token)
                .send(recordToCreate)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(201);
                    res.body.should.be.a('object');

                    neededKeysRecord.forEach(key => {
                        res.body.should.have.property(key).to.not.be.null
                    })

                    recordToCreate._id = res.body._id;
                    done();
                });
        });
        // no auth. header
        it(`${testNumber++} ${testGroupName} - no auth. header - 401 `, done => {
            chai
                .request(app)
                .get(recordEndpointToTest + '/' + recordToCreate._id)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(401);
                    done();
                });
        });
        // get created
        it(`${testNumber++} ${testGroupName} - GET created record - 200 `, done => {
            chai
                .request(app)
                .get(recordEndpointToTest + '/' + recordToCreate._id)
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');

                    neededKeysRecord.forEach(key => res.body.should.have.property(key).to.not.be.null)
                    expect(res.body._id).to.be.equal(recordToCreate._id);
                    done();
                });
        });
        // get not found
        it(`${testNumber++} ${testGroupName} - not found - 404 `, done => {
            chai
                .request(app)
                .get(recordEndpointToTest + '/' + notFoundId)
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(404);
                    done();
                });
        });
        // get with other user
        it(`${testNumber++} ${testGroupName} - get with user 2 - 404 `, done => {
            chai
                .request(app)
                .get(recordEndpointToTest + '/' + recordToCreate._id)
                .set('Authorization', secUser.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(404);
                    done();
                });
        })
        // get with invalid object id
        it(`${testNumber++} ${testGroupName} - invalid ObjectId - 400`, done => {
            chai
                .request(app)
                .get(recordEndpointToTest + '/1870')
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(400);
                    done();
                })
        });
    });

    testGroupName = 'GET ALL';
    describe(testGroupName, () => {
        it(`${testNumber++} ${testGroupName} - get records firstUser - ofCpr - 200`, done => {
            chai
                .request(app)
                .get(recordEndpointToTest + '?cprId=' + cprIdsFirstUser[0])
                .set('Authorization', user.token)
                .send(recordToCreate)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.equal(1)

                    res.body.forEach(r => neededKeysRecord.forEach(key => {
                        r.should.have.property(key).to.not.be.null
                    }))
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - get records secUser - ofCpr - 200`, done => {
            chai
                .request(app)
                .get(recordEndpointToTest + '?cprId=' + cprIdSecUser)
                .set('Authorization', user.token)
                .send(recordToCreate)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.equal(0)

                    res.body.forEach(r => neededKeysRecord.forEach(key => {
                        r.should.have.property(key).to.not.be.null
                    }))
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - get records firstUser - ofPupil - 200`, done => {
            chai
                .request(app)
                .get(recordEndpointToTest + '?pupilId=' + pupilsToCreateFirstUser[0]._id)
                .set('Authorization', user.token)
                .send(recordToCreate)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.equal(1)

                    res.body.forEach(r => neededKeysRecord.forEach(key => {
                        r.should.have.property(key).to.not.be.null
                    }))
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - get records secUser - ofPupil - 200`, done => {
            chai
                .request(app)
                .get(recordEndpointToTest + '?pupilId=' + pupilToCreateSecUser._id)
                .set('Authorization', user.token)
                .send(recordToCreate)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.equal(0)

                    res.body.forEach(r => neededKeysRecord.forEach(key => {
                        r.should.have.property(key).to.not.be.null
                    }))
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - get records firstUser - ofCourse - 200`, done => {
            chai
                .request(app)
                .get(recordEndpointToTest + '?courseId=' + courseToCreateFirstUser._id)
                .set('Authorization', user.token)
                .send(recordToCreate)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.equal(1)

                    res.body.forEach(r => neededKeysRecord.forEach(key => {
                        r.should.have.property(key).to.not.be.null
                    }))
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - get records secUser - ofCourse - 200`, done => {
            chai
                .request(app)
                .get(recordEndpointToTest + '?courseId=' + courseToCreateSecUser._id)
                .set('Authorization', user.token)
                .send(recordToCreate)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.equal(0)

                    res.body.forEach(r => neededKeysRecord.forEach(key => {
                        r.should.have.property(key).to.not.be.null
                    }))
                    done();
                });
        });

        describe('query check', () => {
            let invalidQuerys = []

            let query = {}
            invalidQuerys.push(query)

            query = []
            invalidQuerys.push(query)

            query = 'hihihi'
            invalidQuerys.push(query)

            query = 'userId=' + notFoundId
            invalidQuerys.push(query)

            query = 'courseId=' + notFoundId + 'userId=' + notFoundId
            invalidQuerys.push(query)

            query = 'courseId=[' + notFoundId + ',' + notFoundId + ']'
            invalidQuerys.push(query)

            invalidQuerys.forEach(query => {
                it(`${testNumber++} ${testGroupName} - invalid query - 400`, done => {
                    chai
                        .request(app)
                        .get(recordEndpointToTest + '?' + query)
                        .set('Authorization', user.token)
                        .send(recordToCreate)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(400);
                            done();
                        });
                });
            })
        })
    })

    testGroupName = 'PATCH';
    describe(testGroupName, () => {
        describe('basic test cases', () => {
            // no auth. header
            it(`${testNumber++} ${testGroupName} - No Auth. header - 401`, done => {
                chai
                    .request(app)
                    .patch(recordEndpointToTest + '/' + recordToCreate._id)
                    .send({ ...recordToCreate })
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(401);
                        done();
                    });
            });
            // patch with invalid objectid
            it(`${testNumber++} ${testGroupName} - invalid ObjectId - 400`, done => {
                chai
                    .request(app)
                    .patch(recordEndpointToTest + '/1870')
                    .send({ ...pupilsToCreateFirstUser[0] })
                    .set('Authorization', user.token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(400);
                        done();
                    });
            });
            // patch with not found
            it(`${testNumber++} ${testGroupName} - not found - 404`, done => {
                let course = { ...pupilsToCreateFirstUser[0] };
                delete course._id;
                chai
                    .request(app)
                    .patch(recordEndpointToTest + '/' + notFoundId)
                    .set('Authorization', user.token)
                    .send(course)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(404);
                        done();
                    });
            });
            // patch with second user
            it(`${testNumber++} ${testGroupName} - with other user - 404`, done => {
                chai
                    .request(app)
                    .patch(recordEndpointToTest + '/' + recordToCreate._id)
                    .set('Authorization', secUser.token)
                    .send({ ...recordToCreate })
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(404);
                        done();
                    });
            });
            // valid patch
            it(`${testNumber++} ${testGroupName} - vaid patch - 200`, done => {
                chai
                    .request(app)
                    .patch(recordEndpointToTest + '/' + recordToCreate._id)
                    .set('Authorization', user.token)
                    .send({ tags: null })
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        done();
                    });
            });
            // valid patch
            it(`${testNumber++} ${testGroupName} - vaid patch - 200`, done => {
                chai
                    .request(app)
                    .patch(recordEndpointToTest + '/' + recordToCreate._id)
                    .set('Authorization', user.token)
                    .send({ tags: [] })
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        done();
                    });
            });
        });

        describe('property tests', () => {
            /* --------------------- id --------------------- */
            describe('check property id ', () => {
                let wrongRatings = [];

                let rating = { ...recordToCreate };
                rating._id = {};
                wrongRatings.push(rating);

                rating = { ...recordToCreate };
                rating._id = 12432434;
                wrongRatings.push(rating);

                rating = { ...recordToCreate };
                rating._id = 'JALKJ23kJ21';
                wrongRatings.push(rating);

                rating = { ...recordToCreate };
                rating._id = notFoundId;
                wrongRatings.push(rating);

                wrongRatings.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - id - 400`, done => {
                        chai
                            .request(app)
                            .patch(recordEndpointToTest + '/' + recordToCreate._id)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------- userId --------------------- */
            describe('check property userId ', () => {
                let wrongRatings = [];

                let rating = { ...recordToCreate };
                rating.userId = {};
                wrongRatings.push(rating);

                rating = { ...recordToCreate };
                rating.userId = 12432434;
                wrongRatings.push(rating);

                rating = { ...recordToCreate };
                rating.userId = 'JALKJ23kJ21';
                wrongRatings.push(rating);

                rating = { ...recordToCreate };
                rating.userId = notFoundId;
                wrongRatings.push(rating);

                wrongRatings.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - userId - 400`, done => {
                        chai
                            .request(app)
                            .patch(recordEndpointToTest + '/' + recordToCreate._id)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- cprId -------------------------- */
            describe('check property cprId ', () => {
                let wrongRecords = [];

                let record = { ...recordToCreate };
                record.cprId = '1870'
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.cprId = 12344
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.cprId = null;
                wrongRecords.push(record);

                wrongRecords.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - cprId - 400`, done => {
                        chai
                            .request(app)
                            .patch(recordEndpointToTest + '/' + recordToCreate._id)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- rating -------------------------- */
            describe('check property rating ', () => {
                let wrongRecords = [];

                let record = { ...recordToCreate };
                record.rating = {}
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.rating = []
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.rating = '1870'
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.rating = 12344
                wrongRecords.push(record);

                wrongRecords.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - rating - 400`, done => {
                        chai
                            .request(app)
                            .patch(recordEndpointToTest + '/' + recordToCreate._id)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- weight -------------------------- */
            describe('check property weight ', () => {
                let wrongRecords = [];

                let record = { ...recordToCreate };
                record.weight = {}
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.weight = []
                wrongRecords.push(record);

                wrongRecords.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - weight - 400`, done => {
                        chai
                            .request(app)
                            .patch(recordEndpointToTest + '/' + recordToCreate._id)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });


            /* --------------------------- validOn -------------------------- */
            describe('check property validOn ', () => {
                let wrongRecords = [];

                let record = { ...recordToCreate };
                record.validOn = true;
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.validOn = null;
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.validOn = 12344;
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.validOn = '1870';
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.validOn = '300.01.01';
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.validOn = '30000.01.01';
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.validOn = '3000.111.01';
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.validOn = '3000.01.101';
                wrongRecords.push(record);

                wrongRecords.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - validOn - 400`, done => {
                        chai
                            .request(app)
                            .patch(recordEndpointToTest + '/' + recordToCreate._id)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });


            /* --------------------------- pupilNotPresent -------------------------- */
            describe('check property pupilNotPresent ', () => {
                let wrongRecords = [];

                let record = { ...recordToCreate };
                record.pupilNotPresent = 12344;
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.pupilNotPresent = '1870';
                wrongRecords.push(record);

                wrongRecords.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - pupilNotPresent - 400`, done => {
                        chai
                            .request(app)
                            .patch(recordEndpointToTest + '/' + recordToCreate._id)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- tags -------------------------- */
            describe('check property tags ', () => {
                let wrongRecords = [];

                let record = { ...recordToCreate };
                record.tags = 'true';
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.tags = false;
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.tags = 12344;
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.tags = '1870';
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.tags = {};
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.tags = ['test', {}];
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record.tags = ['test', []];
                wrongRecords.push(record);
                wrongRecords.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - tags - 400`, done => {
                        chai
                            .request(app)
                            .patch(recordEndpointToTest + '/' + recordToCreate._id)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- missing/not allowed properies-------------------------- */
            describe('missing/not allowed properies', () => {
                let wrongRecords = [];

                let record = { ...recordToCreate };
                record.notAllowed = 'hihihi';
                wrongRecords.push(record);

                record = { ...recordToCreate };
                record = {}
                wrongRecords.push(record);

                wrongRecords.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - missing/not allowed properies - 400`, done => {
                        chai
                            .request(app)
                            .patch(recordEndpointToTest + '/' + recordToCreate._id)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });
        });
    });

    testGroupName = 'DELETE';
    describe(testGroupName, () => {
        // no auth. header
        it(`${testNumber++} ${testGroupName} - No Auth. header - 401`, done => {
            chai
                .request(app)
                .delete(recordEndpointToTest + '/' + recordToCreate._id)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(401);
                    done();
                });
        });
        // delete with invalid objectid
        it(`${testNumber++} ${testGroupName} - invalid ObjectId - 400`, done => {
            chai
                .request(app)
                .delete(recordEndpointToTest + '/1870')
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(400);
                    done();
                })
        });
        // delete with not found
        it(`${testNumber++} ${testGroupName} - not found - 404`, done => {
            chai
                .request(app)
                .delete(recordEndpointToTest + '/' + notFoundId)
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(404);
                    done();
                })
        });
        // delete with other user
        it(`${testNumber++} ${testGroupName} - with other user - 404`, done => {
            chai
                .request(app)
                .delete(recordEndpointToTest + '/' + recordToCreate._id)
                .set('Authorization', secUser.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(404);
                    done();
                });
        });
        // successfull delete
        it(`${testNumber++} ${testGroupName} - successful delete - 204`, done => {
            chai
                .request(app)
                .delete(recordEndpointToTest + '/' + recordToCreate._id)
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(204);
                    done();
                })
        });
        // get deleted
        it(`${testNumber++} ${testGroupName} - get deleted item - 404`, done => {
            chai
                .request(app)
                .get(recordEndpointToTest + '/' + recordToCreate._id)
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(404);
                    done();
                });
        });
        // delete again
        it(`${testNumber++} ${testGroupName} - successful delete - 204`, done => {
            chai
                .request(app)
                .delete(recordEndpointToTest + '/' + recordToCreate._id)
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(404);
                    done();
                })
        });
    });

    testGroupName = 'GRADE SUGG';
    describe(testGroupName, () => {
        describe('create records', () => {
            it(`${testNumber++} ${testGroupName} - create record pos sec pupil first user - 201`, done => {
                chai
                    .request(app)
                    .post(recordEndpointToTest)
                    .set('Authorization', user.token)
                    .send(recordForGradeSuggPos)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(201);
                        res.body.should.be.a('object');

                        neededKeysRecord.forEach(key => {
                            res.body.should.have.property(key).to.not.be.null
                        })

                        recordForGradeSuggPos._id = res.body._id;
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - create record neg sec pupil first user - 201`, done => {
                chai
                    .request(app)
                    .post(recordEndpointToTest)
                    .set('Authorization', user.token)
                    .send(recordForGradeSuggNeg)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(201);
                        res.body.should.be.a('object');

                        neededKeysRecord.forEach(key => {
                            res.body.should.have.property(key).to.not.be.null
                        })

                        recordForGradeSuggNeg._id = res.body._id;
                        done();
                    });
            });
        })

        describe('GET Grade Sugg', () => {
            it(`${testNumber++} ${testGroupName} - get grade sugg 0 records - 200`, done => {
                chai
                    .request(app)
                    .get(gradeEndpoint + '?' + getCPRQuery(pupilsToCreateFirstUser[0]._id))
                    .set('Authorization', user.token)
                    .send()
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.equal(1)

                        neededKeysGradeSugg.forEach(key => {
                            res.body.forEach(g => g.should.have.property(key).to.not.be.null)
                        })

                        res.body[0].should.have.property('notEnoughData')
                        res.body[0].notEnoughData.should.be.equal(true)

                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - get grade sugg 2 records - 200`, done => {
                chai
                    .request(app)
                    .get(gradeEndpoint + '?' + getCPRQuery(pupilsToCreateFirstUser[1]._id))
                    .set('Authorization', user.token)
                    .send()
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.equal(1)

                        neededKeysGradeSugg.forEach(key => {
                            res.body.forEach(g => g.should.have.property(key).to.not.be.null)
                        })

                        res.body[0].should.have.property('grade')
                        res.body[0].grade.should.be.equal(expectedGradeSecPupil)

                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - get grade sugg 2 records multiple=true - 200`, done => {
                chai
                    .request(app)
                    .get(gradeEndpoint + '?' + getCPRQuery(pupilsToCreateFirstUser[1]._id) + "&multiple=true")
                    .set('Authorization', user.token)
                    .send()
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.equal(1)

                        neededKeysGradeSugg.forEach(key => {
                            res.body.forEach(g => g.should.have.property(key).to.not.be.null)
                        })

                        res.body[0].should.have.property('grade')
                        res.body[0].grade.should.be.a('array');
                        res.body[0].grade.length.should.equal(2);

                        res.body[0].grade[0].should.be.a('object');
                        res.body[0].grade[0].should.have.property('grade')
                        res.body[0].grade[0].should.have.property('date')
                        res.body[0].grade[0].grade.should.equal(100);
                        res.body[0].grade[1].should.be.a('object');
                        res.body[0].grade[1].should.have.property('grade')
                        res.body[0].grade[1].should.have.property('date')
                        res.body[0].grade[1].grade.should.equal(75);

                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - get grade with course query - 200`, done => {
                chai
                    .request(app)
                    .get(gradeEndpoint + '?courseId=' + courseToCreateFirstUser._id)
                    .set('Authorization', user.token)
                    .send()
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.equal(2)

                        neededKeysGradeSugg.forEach(key => {
                            res.body.forEach(g => g.should.have.property(key).to.not.be.null)
                        })

                        res.body[0].should.have.property('notEnoughData')
                        res.body[0].notEnoughData.should.be.equal(true)

                        res.body[1].should.have.property('grade')
                        res.body[1].grade.should.be.equal(expectedGradeSecPupil)

                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - delete 50 record - 204`, done => {
                chai
                    .request(app)
                    .delete(recordEndpointToTest + '/' + recordForGradeSuggNeg._id)
                    .set('Authorization', user.token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(204);
                        done();
                    })
            });

            it(`${testNumber++} ${testGroupName} - get grade sugg 1 records - 200`, done => {
                chai
                    .request(app)
                    .get(gradeEndpoint + '?' + getCPRQuery(pupilsToCreateFirstUser[1]._id))
                    .set('Authorization', user.token)
                    .send()
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.equal(1)

                        neededKeysGradeSugg.forEach(key => {
                            res.body.forEach(g => g.should.have.property(key).to.not.be.null)
                        })

                        res.body[0].should.have.property('grade')
                        res.body[0].grade.should.be.equal(recordForGradeSuggPos.rating.value)

                        done();
                    });
            });

            describe('query check', () => {
                let invalidQuerys = []

                let query = {}
                invalidQuerys.push(query)

                query = []
                invalidQuerys.push(query)

                query = 'hihihi'
                invalidQuerys.push(query)

                query = 'userId=' + notFoundId
                invalidQuerys.push(query)

                query = 'courseId=' + notFoundId + 'userId=' + notFoundId
                invalidQuerys.push(query)

                query = 'courseId=[' + notFoundId + ',' + notFoundId + ']'
                invalidQuerys.push(query)

                query = 'courseId=1213'
                invalidQuerys.push(query)

                query = ''
                invalidQuerys.push(query)

                invalidQuerys.forEach(query => {
                    it(`${testNumber++} ${testGroupName} - invalid query - 400`, done => {
                        chai
                            .request(app)
                            .get(gradeEndpoint + '?' + query)
                            .set('Authorization', user.token)
                            .send(recordToCreate)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                })
            })
        })
    })
});

function getCPRQuery(pupilId, courseId) {
    return (pupilId != undefined) ? `pupilId=${pupilId}` : '' + (pupilId == undefined || courseId == undefined) ? '' : '&' + (courseId != undefined) ? `courseId=${courseId}` : ''
}