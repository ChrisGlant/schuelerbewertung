import chai from 'chai';
import chaiHttp from 'chai-http';
import app from '../server.js';

import { healthCheck } from './00-startup-hook.js';

const { expect } = chai;

const entityEndpoint = '/api/v1/ratingschemes';

let rsUser = {
    username: "ratingSchemeUser",
    email: "ratingSchemeUser@gmail.com",
    password: "pswrd",
    lastname: "ratingscheme-mock-data",
    firstname: "record"
}

let secRsUser = {
    username: "ratingSchemeUser2",
    email: "ratingSchemeUser2@gmail.com",
    password: "pswrd",
    lastname: "ratingscheme-mock-data",
    firstname: "record"
}

let rsToCreate = {
    "name": "Schularbeit",
    "ratings": [
        {
            "label": "Sehr Gut",
            "symbol": "1",
            "value": 91
        }
    ],
    weight: 60
}
let rsId, ratingId;



chai.use(chaiHttp);
chai.should();

// hook - done() fires, if healthcheck point answers with 200
before(() => {
    return healthCheck(app);
});

describe(`CRUD Tests for ${entityEndpoint}`, () => {
    let testGroupName;
    let testNumber = 10000;

    testGroupName = "Basic user stuff"
    describe(testGroupName, () => {
        it(`${testNumber++} ${testGroupName} - No Auth. header- 401 `, done => {
            chai
                .request(app)
                .get(entityEndpoint + '/' + rsId)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(401);
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - Create first user - 201`, done => {
            chai
                .request(app)
                .post('/api/v1/users')
                .send(rsUser)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(201);
                    res.body.should.be.a('object');

                    res.body.should.have.property('_id').not.to.be.null;
                    res.body.should.have.property('username').not.to.be.null;
                    res.body.should.have.property('state').not.to.be.null;
                    res.body.should.have.property('firstname').not.to.be.null;
                    res.body.should.have.property('lastname').not.to.be.null;

                    rsUser._id = res.body._id;
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - Login first user - 200`, (done) => {
            chai
                .request(app)
                .post('/api/v1/login')
                .send({ "username": rsUser["username"], "password": rsUser["password"] })
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(1);
                    res.body.should.have.property('token');
                    rsUser.token = res.body.token;
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - Create second user - 201`, done => {
            chai
                .request(app)
                .post('/api/v1/users')
                .send(secRsUser)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(201);
                    res.body.should.be.a('object');

                    res.body.should.have.property('_id').not.to.be.null;
                    res.body.should.have.property('username').not.to.be.null;
                    res.body.should.have.property('state').not.to.be.null;
                    res.body.should.have.property('firstname').not.to.be.null;
                    res.body.should.have.property('lastname').not.to.be.null;

                    secRsUser._id = res.body._id;
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - Login second user - 200`, (done) => {
            chai
                .request(app)
                .post('/api/v1/login')
                .send({ "username": secRsUser["username"], "password": secRsUser["password"] })
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(1);
                    res.body.should.have.property('token');
                    secRsUser.token = res.body.token;
                    done();
                });
        });
    })

    testGroupName = 'POST'
    describe(testGroupName, () => {
        describe('check basic test cases', () => {
            it(`${testNumber++} ${testGroupName} - Create ratingscheme - 201`, done => {
                chai
                    .request(app)
                    .post(entityEndpoint)
                    .set('Authorization', rsUser.token)
                    .send(rsToCreate)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(201);
                        res.body.should.be.a('object');

                        res.body.should.have.property('_id').not.to.be.null;
                        res.body.should.have.property('name').not.to.be.null;
                        res.body.should.have.property('ratings').not.to.be.null;
                        res.body.should.have.property('weight').not.to.be.null;
                        expect(res.body.ratings).to.be.a('array');
                        expect(res.body.ratings.length).to.be.equal(1);
                        res.body.ratings[0].should.have.property('_id');
                        res.body.ratings[0].should.have.property('label');
                        res.body.ratings[0].should.have.property('symbol');
                        res.body.ratings[0].should.have.property('value');

                        rsId = res.body._id;
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - GET created ratingscheme - 200 `, done => {
                chai
                    .request(app)
                    .get(entityEndpoint + '/' + rsId)
                    .set('Authorization', rsUser.token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        res.body.should.be.a('object');

                        res.body.should.have.property('_id').not.to.be.null;
                        res.body.should.have.property('name').not.to.be.null;
                        res.body.should.have.property('ratings').not.to.be.null;
                        res.body.should.have.property('weight').not.to.be.null;
                        expect(res.body.ratings).to.be.a('array');
                        expect(res.body.ratings.length).to.be.equal(1);
                        res.body.ratings[0].should.have.property('_id');
                        res.body.ratings[0].should.have.property('label');
                        res.body.ratings[0].should.have.property('symbol');
                        res.body.ratings[0].should.have.property('value');

                        expect(res.body._id).to.be.equal(rsId);
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - Delete created ratingscheme - 204`, done => {
                chai
                    .request(app)
                    .delete(entityEndpoint + '/' + rsId)
                    .set('Authorization', rsUser.token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(204);

                        rsId = undefined;
                        done();
                    });
            });

            /* --------------------- property tests --------------------- */

            /* --------------------------- id -------------------------- */
            describe('check property id ', () => {
                let wrongRs = [];

                let rs = { ...rsToCreate };
                rs._id = {};
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs._id = 12432434;
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs._id = 'JALKJ23kJ21';
                wrongRs.push(rs);

                wrongRs.forEach(wrs => {
                    it(`${testNumber++} ${testGroupName} - id - 400`, done => {
                        chai
                            .request(app)
                            .post(entityEndpoint)
                            .set('Authorization', rsUser.token)
                            .send(wrs)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- name -------------------------- */
            describe('check property name ', () => {
                let wrongRs = [];

                let rs = { ...rsToCreate };
                rs.name = {};
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs.name = undefined;
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs.name = null;
                wrongRs.push(rs);

                wrongRs.forEach(wrs => {
                    it(`${testNumber++} ${testGroupName} - name - 400`, done => {
                        chai
                            .request(app)
                            .post(entityEndpoint)
                            .set('Authorization', rsUser.token)
                            .send(wrs)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });
            
            /* --------------------------- weight -------------------------- */
            describe('check property weight ', () => {
                let wrongRs = [];

                let rs = { ...rsToCreate };
                rs.weight = {};
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs.weight = undefined;
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs.weight = null;
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs.weight = 'thisisnotanumber';
                wrongRs.push(rs);

                wrongRs.forEach(wrs => {
                    it(`${testNumber++} ${testGroupName} - weight - 400`, done => {
                        chai
                            .request(app)
                            .post(entityEndpoint)
                            .set('Authorization', rsUser.token)
                            .send(wrs)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- ratings overall -------------------------- */
            describe('check property ratings overall ', () => {
                let wrongRs = [];

                let rs = { ...rsToCreate };
                rs.ratings = {};
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs.ratings = 'böser string';
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs.ratings = 12324;
                wrongRs.push(rs);

                wrongRs.forEach(wrs => {
                    it(`${testNumber++} ${testGroupName} - ratings overall - 400`, done => {
                        chai
                            .request(app)
                            .post(entityEndpoint)
                            .set('Authorization', rsUser.token)
                            .send(wrs)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- ratings id -------------------------- */
            describe('check property ratings id ', () => {
                let wrongRs = [];

                let rs = { ...rsToCreate };
                rs.ratings = [{
                    "label": "Sehr Gut",
                    "symbol": "1",
                    "value": 91
                }]
                rs.ratings[0]._id = {};
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs.ratings = [{
                    "label": "Sehr Gut",
                    "symbol": "1",
                    "value": 91
                }]
                rs.ratings[0]._id = [];
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs.ratings = [{
                    "label": "Sehr Gut",
                    "symbol": "1",
                    "value": 91
                }]
                rs.ratings[0]._id = 'böser string';
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs.ratings = [{
                    "label": "Sehr Gut",
                    "symbol": "1",
                    "value": 91
                }]
                rs.ratings[0]._id = 12344;
                wrongRs.push(rs);

                wrongRs.forEach(wrs => {
                    it(`${testNumber++} ${testGroupName} - ratings id - 400`, done => {
                        chai
                            .request(app)
                            .post(entityEndpoint)
                            .set('Authorization', rsUser.token)
                            .send(wrs)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- ratings label -------------------------- */
            describe('check property ratings label ', () => {
                let wrongRs = [];

                let rs = { ...rsToCreate };
                rs.ratings = [{
                    "label": "Sehr Gut",
                    "symbol": "1",
                    "value": 91
                }]
                rs.ratings[0].label = {};
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs.ratings = [{
                    "label": "Sehr Gut",
                    "symbol": "1",
                    "value": 91
                }]
                rs.ratings[0].label = null;
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs.ratings = [{
                    "label": "Sehr Gut",
                    "symbol": "1",
                    "value": 91
                }]
                rs.ratings[0].label = undefined;
                wrongRs.push(rs);

                wrongRs.forEach(wrs => {
                    it(`${testNumber++} ${testGroupName} - ratings label - 400`, done => {
                        chai
                            .request(app)
                            .post(entityEndpoint)
                            .set('Authorization', rsUser.token)
                            .send(wrs)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- ratings symbol -------------------------- */
            describe('check property ratings label ', () => {
                let wrongRs = [];

                let rs = { ...rsToCreate };
                rs.ratings = [{
                    "label": "Sehr Gut",
                    "symbol": "1",
                    "value": 91
                }]
                rs.ratings[0].symbol = {};
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs.ratings = [{
                    "label": "Sehr Gut",
                    "symbol": "1",
                    "value": 91
                }]
                rs.ratings[0].symbol = null;
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs.ratings = [{
                    "label": "Sehr Gut",
                    "symbol": "1",
                    "value": 91
                }]
                rs.ratings[0].symbol = undefined;
                wrongRs.push(rs);

                wrongRs.forEach(wrs => {
                    it(`${testNumber++} ${testGroupName} - ratings label - 400`, done => {
                        chai
                            .request(app)
                            .post(entityEndpoint)
                            .set('Authorization', rsUser.token)
                            .send(wrs)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });

                /* --------------------------- ratings value -------------------------- */
                describe('check property ratings value ', () => {
                    let wrongRs = [];

                    let rs = { ...rsToCreate };
                    rs.ratings = [{
                        "label": "Sehr Gut",
                        "symbol": "1",
                        "value": 91
                    }]
                    rs.ratings[0].value = {};
                    wrongRs.push(rs);

                    rs = { ...rsToCreate };
                    rs.ratings = [{
                        "label": "Sehr Gut",
                        "symbol": "1",
                        "value": 91
                    }]
                    rs.ratings[0].value = [];
                    wrongRs.push(rs);

                    rs = { ...rsToCreate };
                    rs.ratings = [{
                        "label": "Sehr Gut",
                        "symbol": "1",
                        "value": 91
                    }]
                    rs.ratings[0].value = "heall";
                    wrongRs.push(rs);

                    rs = { ...rsToCreate };
                    rs.ratings = [{
                        "label": "Sehr Gut",
                        "symbol": "1",
                        "value": 91
                    }]
                    rs.ratings[0].value = -1;
                    wrongRs.push(rs);

                    rs = { ...rsToCreate };
                    rs.ratings = [{
                        "label": "Sehr Gut",
                        "symbol": "1",
                        "value": 91
                    }]
                    rs.ratings[0].value = 101;
                    wrongRs.push(rs);


                    wrongRs.forEach(wrs => {
                        it(`${testNumber++} ${testGroupName} - ratings value - 400`, done => {
                            chai
                                .request(app)
                                .post(entityEndpoint)
                                .set('Authorization', rsUser.token)
                                .send(wrs)
                                .end((err, res) => {
                                    expect(err).to.be.null;
                                    res.should.have.status(400);
                                    done();
                                });
                        });
                    });
                });

                /* --------------------------- missing/not allowed properies-------------------------- */
                describe('missing/not allowed properies', () => {
                    let wrongRs = [];

                    rs = { ...rsToCreate };
                    delete rs.name;
                    wrongRs.push(rs);

                    rs = { ...rsToCreate };
                    rs.ratings = [{
                        "label": "Sehr Gut",
                        "symbol": "1",
                        "value": 91
                    }]
                    delete rs.ratings[0].symbol;
                    wrongRs.push(rs);

                    rs = { ...rsToCreate };
                    rs.ratings = [{
                        "label": "Sehr Gut",
                        "symbol": "1",
                        "value": 91
                    }]
                    delete rs.ratings[0].label;
                    wrongRs.push(rs);

                    rs = { ...rsToCreate };
                    rs.hihhi = 'not allowed'
                    wrongRs.push(rs);

                    rs = { ...rsToCreate };
                    rs.ratings = [{
                        "label": "Sehr Gut",
                        "symbol": "1",
                        "value": 91
                    }]
                    rs.ratings[0].hihih = 'not allowed'
                    wrongRs.push(rs);

                    rs = { ...rsToCreate };
                    rs.ratings = [{
                        "label": "Sehr Gut",
                        "symbol": "1",
                        "value": 91
                    }]
                    rs.ratings[0]._id = 121;
                    wrongRs.push(rs);

                    wrongRs.forEach(wrs => {
                        it(`${testNumber++} ${testGroupName} - missing/not allowed properies - 400`, done => {
                            chai
                                .request(app)
                                .post(entityEndpoint)
                                .set('Authorization', rsUser.token)
                                .send(wrs)
                                .end((err, res) => {
                                    expect(err).to.be.null;
                                    res.should.have.status(400);
                                    done();
                                });
                        });
                    });
                });
            });
        });
    });

    testGroupName = 'GET SINGLE'
    describe(testGroupName, () => {
        it(`${testNumber++} ${testGroupName} - Create ratingscheme - 201`, done => {
            chai
                .request(app)
                .post(entityEndpoint)
                .set('Authorization', rsUser.token)
                .send(rsToCreate)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(201);
                    res.body.should.be.a('object');

                    res.body.should.have.property('_id').not.to.be.null;
                    res.body.should.have.property('name').not.to.be.null;
                    res.body.should.have.property('ratings').not.to.be.null;
                    res.body.should.have.property('weight').not.to.be.null;
                    expect(res.body.ratings).to.be.a('array');
                    expect(res.body.ratings.length).to.be.equal(1);
                    res.body.ratings[0].should.have.property('_id');
                    res.body.ratings[0].should.have.property('label');
                    res.body.ratings[0].should.have.property('symbol');
                    res.body.ratings[0].should.have.property('value');

                    ratingId = res.body.ratings[0]._id
                    rsId = res.body._id;
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - No Auth. header- 401 `, done => {
            chai
                .request(app)
                .get(entityEndpoint + '/' + rsId)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(401);
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - GET created ratingscheme - 200 `, done => {
            chai
                .request(app)
                .get(entityEndpoint + '/' + rsId)
                .set('Authorization', rsUser.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');

                    res.body.should.have.property('_id').not.to.be.null;
                    res.body.should.have.property('name').not.to.be.null;
                    res.body.should.have.property('ratings').not.to.be.null;                    
                    res.body.should.have.property('weight').not.to.be.null;
                    expect(res.body.ratings).to.be.a('array');
                    expect(res.body.ratings.length).to.be.equal(1);
                    res.body.ratings[0].should.have.property('_id');
                    res.body.ratings[0].should.have.property('label');
                    res.body.ratings[0].should.have.property('symbol');
                    res.body.ratings[0].should.have.property('value');

                    expect(res.body._id).to.be.equal(rsId);
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - not found - 404 `, done => {
            chai
                .request(app)
                .get(entityEndpoint + '/' + '607f11dbc03e786bada6d87c')
                .set('Authorization', rsUser.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(404);
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - get with user 2 - 404 `, done => {
            chai
                .request(app)
                .get(entityEndpoint + '/' + rsId)
                .set('Authorization', secRsUser.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(404);
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - invalid ObjectId - 400`, done => {
            chai
                .request(app)
                .get(entityEndpoint + '/1870')
                .set('Authorization', rsUser.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(400);
                    done();
                })
        });
    });

    testGroupName = 'GET ALL';
    describe(testGroupName, () => {
        it(`${testNumber++} ${testGroupName} - No Auth. header - 401`, done => {
            chai
                .request(app)
                .get(entityEndpoint)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(401);
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - first user - 200`, done => {
            chai
                .request(app)
                .get(entityEndpoint)
                .set('Authorization', rsUser.token)
                .end((err, res) => {
                    expect(err).to.be.null;

                    res.should.have.status(200);
                    res.body.should.be.a('array');

                    expect(res.body.length).to.equal(1);

                    done();
                });
        });
    });

    testGroupName = 'PATCH';
    describe(testGroupName, () => {
        describe('basic test cases', () => {
            it(`${testNumber++} ${testGroupName} - No Auth. header - 401`, done => {
                chai
                    .request(app)
                    .patch(entityEndpoint + '/' + rsId)
                    .send({ ...rsToCreate })
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(401);
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - invalid ObjectId - 400`, done => {
                chai
                    .request(app)
                    .patch(entityEndpoint + '/1870')
                    .send({ ...rsToCreate })
                    .set('Authorization', rsUser.token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(400);
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - not found - 404`, done => {
                chai
                    .request(app)
                    .patch(entityEndpoint + '/' + '607f11dbc03e786bada6d87c')
                    .set('Authorization', rsUser.token)
                    .send({ ...rsToCreate })
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(404);
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - ratingscheme of other user - 404`, done => {
                chai
                    .request(app)
                    .patch(entityEndpoint + '/' + rsId)
                    .set('Authorization', secRsUser.token)
                    .send({ ...rsToCreate })
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(404);
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - ratingscheme of other user - 200`, done => {
                chai
                    .request(app)
                    .patch(entityEndpoint + '/' + rsId)
                    .set('Authorization', rsUser.token)
                    .send({ name: 'test' })
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        done();
                    });
            });


            it(`${testNumber++} ${testGroupName} - update with same ratings id - 200`, done => {
                chai
                    .request(app)
                    .patch(entityEndpoint + '/' + rsId)
                    .set('Authorization', rsUser.token)
                    .send({
                        ...rsToCreate, ...{
                            ratings: [{
                                "_id": ratingId,
                                "label": "Sehr Gut",
                                "symbol": "1",
                                "value": 91
                            }]
                        }
                    })
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        done();
                    });
            });
        });

        describe('property tests', () => {
            describe('check property _id', () => {
                let wrongRs = [];

                let rs = { ...rsToCreate };
                rs._id = {};
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs._id = 12432434;
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs._id = 'JALKJ23kJ21';
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs._id = '607f11dbc03e786bada6d87c';
                wrongRs.push(rs);

                wrongRs.forEach(wrs => {
                    it(`${testNumber++} ${testGroupName} - update _id - 400`, done => {
                        chai
                            .request(app)
                            .patch(entityEndpoint + '/' + rsId)
                            .set('Authorization', rsUser.token)
                            .send(wrs)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            describe('check property userId', () => {
                let wrongRs = [];

                let rs = { ...rsToCreate };
                rs.userId = {};
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs.userId = 12432434;
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs.userId = 'JALKJ23kJ21';
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs.userId = '607f11dbc03e786bada6d87c';
                wrongRs.push(rs);

                wrongRs.forEach(wrs => {
                    it(`${testNumber++} ${testGroupName} - update userId - 400`, done => {
                        chai
                            .request(app)
                            .patch(entityEndpoint + '/' + rsId)
                            .set('Authorization', rsUser.token)
                            .send(wrs)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            describe('check property name', () => {
                let wrongRs = [];

                let rs = { ...rsToCreate };
                rs.name = {};
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs.name = null;
                wrongRs.push(rs);

                wrongRs.forEach(wrs => {
                    it(`${testNumber++} ${testGroupName} - update name - 400`, done => {
                        chai
                            .request(app)
                            .patch(entityEndpoint + '/' + rsId)
                            .set('Authorization', rsUser.token)
                            .send(wrs)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });
            
            /* --------------------------- weight -------------------------- */
            describe('check property weight ', () => {
                let wrongRs = [];

                let rs = { ...rsToCreate };
                rs.weight = {};
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs.weight = null;
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs.weight = 'thisisnotanumber';
                wrongRs.push(rs);

                wrongRs.forEach(wrs => {
                    it(`${testNumber++} ${testGroupName} - update weight - 400`, done => {
                        chai
                            .request(app)
                            .patch(entityEndpoint + '/' + rsId)
                            .set('Authorization', rsUser.token)
                            .send(wrs)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            describe('check property ratings overall', () => {
                let wrongRs = [];

                let rs = { ...rsToCreate };
                rs.ratings = {};
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs.ratings = 'böser string';
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs.ratings = 12324;
                wrongRs.push(rs);

                wrongRs.forEach(wrs => {
                    it(`${testNumber++} ${testGroupName} - update ratings overall - 400`, done => {
                        chai
                            .request(app)
                            .patch(entityEndpoint + '/' + rsId)
                            .set('Authorization', rsUser.token)
                            .send(wrs)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            describe('check property ratings id', () => {
                let wrongRs = [];

                let rs = { ...rsToCreate };
                rs.ratings = [{
                    "label": "Sehr Gut",
                    "symbol": "1",
                    "value": 91
                }]
                rs.ratings[0]._id = {};
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs.ratings = [{
                    "label": "Sehr Gut",
                    "symbol": "1",
                    "value": 91
                }]
                rs.ratings[0]._id = 'böser string';
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs.ratings = [{
                    "label": "Sehr Gut",
                    "symbol": "1",
                    "value": 91
                }]
                rs.ratings[0]._id = 99;
                wrongRs.push(rs);

                wrongRs.forEach(wrs => {
                    it(`${testNumber++} ${testGroupName} - update ratings id - 400`, done => {
                        chai
                            .request(app)
                            .patch(entityEndpoint + '/' + rsId)
                            .set('Authorization', rsUser.token)
                            .send(wrs)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            describe('check property ratings label', () => {
                let wrongRs = [];

                let rs = { ...rsToCreate };
                rs.ratings = [{
                    "label": "Sehr Gut",
                    "symbol": "1",
                    "value": 91
                }]
                rs.ratings[0].label = {};
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs.ratings = [{
                    "label": "Sehr Gut",
                    "symbol": "1",
                    "value": 91
                }]
                rs.ratings[0].label = null;
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs.ratings = [{
                    "label": "Sehr Gut",
                    "symbol": "1",
                    "value": 91
                }]
                rs.ratings[0].label = undefined;
                wrongRs.push(rs);

                wrongRs.forEach(wrs => {
                    it(`${testNumber++} ${testGroupName} - update ratings id - 400`, done => {
                        chai
                            .request(app)
                            .patch(entityEndpoint + '/' + rsId)
                            .set('Authorization', rsUser.token)
                            .send(wrs)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });
            describe('check property ratings symbol', () => {
                let wrongRs = [];

                let rs = { ...rsToCreate };
                rs.ratings = [{
                    "label": "Sehr Gut",
                    "symbol": "1",
                    "value": 91
                }]
                rs.ratings[0].symbol = {};
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs.ratings = [{
                    "label": "Sehr Gut",
                    "symbol": "1",
                    "value": 91
                }]
                rs.ratings[0].symbol = null;
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs.ratings = [{
                    "label": "Sehr Gut",
                    "symbol": "1",
                    "value": 91
                }]
                rs.ratings[0].symbol = undefined;
                wrongRs.push(rs);

                wrongRs.forEach(wrs => {
                    it(`${testNumber++} ${testGroupName} - update ratings id - 400`, done => {
                        chai
                            .request(app)
                            .patch(entityEndpoint + '/' + rsId)
                            .set('Authorization', rsUser.token)
                            .send(wrs)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- ratings value -------------------------- */
            describe('check property ratings value ', () => {
                let wrongRs = [];

                let rs = { ...rsToCreate };
                rs.ratings = [{
                    "label": "Sehr Gut",
                    "symbol": "1",
                    "value": 91
                }]
                rs.ratings[0].value = {};
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs.ratings = [{
                    "label": "Sehr Gut",
                    "symbol": "1",
                    "value": 91
                }]
                rs.ratings[0].value = [];
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs.ratings = [{
                    "label": "Sehr Gut",
                    "symbol": "1",
                    "value": 91
                }]
                rs.ratings[0].value = "heall";
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs.ratings = [{
                    "label": "Sehr Gut",
                    "symbol": "1",
                    "value": 91
                }]
                rs.ratings[0].value = -1;
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs.ratings = [{
                    "label": "Sehr Gut",
                    "symbol": "1",
                    "value": 91
                }]
                rs.ratings[0].value = 101;
                wrongRs.push(rs);


                wrongRs.forEach(wrs => {
                    it(`${testNumber++} ${testGroupName} - update ratings id - 400`, done => {
                        chai
                            .request(app)
                            .patch(entityEndpoint + '/' + rsId)
                            .set('Authorization', rsUser.token)
                            .send(wrs)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- missing/not allowed properies-------------------------- */
            describe('missing/not allowed properies', () => {
                let wrongRs = [];

                let rs = { ...rsToCreate };
                rs = {}
                wrongRs.push(rs);

                rs = { ...rsToCreate };
                rs.notAllowed = 'hihihi'
                wrongRs.push(rs);

                wrongRs.forEach(wrs => {
                    it(`${testNumber++} ${testGroupName} - missing/not allowed properies - 400`, done => {
                        chai
                            .request(app)
                            .patch(entityEndpoint + '/' + rsId)
                            .set('Authorization', rsUser.token)
                            .send(wrs)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });
        });
    });

    testGroupName = 'DELETE';
    describe(testGroupName, () => {
        it(`${testNumber++} ${testGroupName} - No Auth. header - 401`, done => {
            chai
                .request(app)
                .delete(entityEndpoint + '/' + rsId)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(401);
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - invalid ObjectId - 400`, done => {
            chai
                .request(app)
                .delete(entityEndpoint + '/1870')
                .set('Authorization', rsUser.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(400);
                    done();
                })
        });

        it(`${testNumber++} ${testGroupName} - not found - 404`, done => {
            chai
                .request(app)
                .delete(entityEndpoint + '/' + '607f11dbc03e786bada6d87c')
                .set('Authorization', rsUser.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(404);
                    done();
                })
        });

        it(`${testNumber++} ${testGroupName} - ratingscheme of other user - 404`, done => {
            chai
                .request(app)
                .delete(entityEndpoint + '/' + rsId)
                .set('Authorization', secRsUser.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(404);
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - successful delete - 204`, done => {
            chai
                .request(app)
                .delete(entityEndpoint + '/' + rsId)
                .set('Authorization', rsUser.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(204);
                    done();
                })
        });

        it(`${testNumber++} ${testGroupName} - get deleted item - 404`, done => {
            chai
                .request(app)
                .get(entityEndpoint + '/' + rsId)
                .set('Authorization', rsUser.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(404);
                    done();
                });
        });
    });
});