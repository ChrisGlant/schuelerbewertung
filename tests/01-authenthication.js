import chai from 'chai';
import chaiHttp from 'chai-http';
import app from '../server.js';
import mongoose from 'mongoose';

import { healthCheck } from './00-startup-hook.js';
import readJson from '../utils/readJson.js';
import User from '../models/user-model.js';

const ObjectId = mongoose.Types.ObjectId;
const { expect } = chai;

const apiVersionUrl = '/api/v1';
const userTestdataFile = './tests/user-mock-data.json'
const authKey = 'authorization';

const masterUserId = "5fce46cc305c2d245480a74d";
const masterUserCredentials = {
    "username": "masterUser",
    "password": "pswrdMaster",
}
let masterUserToken;

const newUser = {
    "username": "validUser",
    "email": "validUser@gmail.com",
    "password": "pswrdValid",
    "lastname": "lastname",
    "firstname": "firstname"
}
let newUserToken;

let notExistingUserToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c3IiOiI2MWNiMGUxM2MxYWFkYWI0Njg5Y2E2NzQiLCJpYXQiOjE2NDA2OTczNzI0NjIsImV4cCI6MTY0MDY5NzQxNTY2Mn0.ZDcBJGpgtUXEyDyAOMxfKjPAH6AEHCnJQYJNkmn6sMc'

chai.use(chaiHttp);
chai.should();

// hook - done() fires, if healthcheck point answers with 200
before(() => {
    return healthCheck(app);
});

describe(`Authentication Tests for: ${apiVersionUrl}`, () => {
    let testNumber = 1000;
    let testGroupName = "Create Testdata User";
    it(`${testNumber++} ${testGroupName} -  ${apiVersionUrl} with file "${userTestdataFile}"`, async () => {
        let allUsers = await readJson(userTestdataFile);

        let creationJobs = allUsers.map((u) => {
            let user = new User(u);
            //console.log(`testuser created: ${u.username}`);
            return user.save();
        });

        return Promise.all(creationJobs);
    });

    testGroupName = "POST Login"
    describe(testGroupName, () => {
        describe('Check Credentials', () => {
            let wrongCredentials = [];

            let credentials = { ...masterUserCredentials };
            credentials = undefined;
            wrongCredentials.push(credentials);

            credentials = { ...masterUserCredentials };
            credentials = null;
            wrongCredentials.push(credentials);

            credentials = { ...masterUserCredentials };
            credentials = "pswrdMaster";
            wrongCredentials.push(credentials);

            credentials = { ...masterUserCredentials };
            delete credentials.username;
            delete credentials.password;
            wrongCredentials.push(credentials);

            credentials = { ...masterUserCredentials };
            delete credentials.username;
            wrongCredentials.push(credentials);

            credentials = { ...masterUserCredentials };
            delete credentials.password;
            wrongCredentials.push(credentials);

            credentials = { ...masterUserCredentials };
            credentials.invalidProperty = "this should cause an error";
            wrongCredentials.push(credentials);

            credentials = { ...masterUserCredentials };
            credentials.password = "invalid";
            wrongCredentials.push(credentials);

            wrongCredentials.forEach(wc => {
                it(`${testNumber++} ${testGroupName} - Invalid Credentials - 401`, (done) => {
                    chai
                        .request(app)
                        .post(`${apiVersionUrl}/login`)
                        .send(wc)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(401);
                            done();
                        });
                });
            });
        })


        describe("Check Token", () => {
            let wrongTokens = [];
            let token;

            token = null;
            wrongTokens.push(token);

            token = "";
            wrongTokens.push(token);

            token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";
            wrongTokens.push(token);

            token = {};
            wrongTokens.push(token);

            token = { token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c" };
            wrongTokens.push(token);

            token = notExistingUserToken;
            wrongTokens.push(token);


            wrongTokens.forEach(wt => {
                it(`${testNumber++} ${testGroupName} - Invalid Token - 401`, (done) => {
                    chai
                        .request(app)
                        .get(`${apiVersionUrl}/whoIAm`)
                        .set(authKey, wt)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(401);
                            done();
                        });
                });
            })
        });

        describe("Successfull Login", () => {
            it(`${testNumber++} ${testGroupName} - Login - 200`, (done) => {
                chai
                    .request(app)
                    .post(`${apiVersionUrl}/login`)
                    .send(masterUserCredentials)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        Object.keys(res.body).length.should.eql(1);
                        res.body.should.have.property('token');
                        masterUserToken = res.body.token;
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - whoIAm - 200`, (done) => {
                chai
                    .request(app)
                    .get(`${apiVersionUrl}/whoIAm`)
                    .set(authKey, masterUserToken)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        Object.keys(res.body).length.should.eql(7);
                        res.body.should.have.property('username');
                        expect(res.body.username).to.equal(masterUserCredentials.username);
                        done();
                    });
            });

            it(`${testNumber++} ${testGroupName} - Login already logged in - 200`, (done) => {
                chai
                    .request(app)
                    .post(`${apiVersionUrl}/login`)
                    .send(masterUserCredentials)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        Object.keys(res.body).length.should.eql(1);
                        res.body.should.have.property('token');
                        masterUserToken = res.body.token;
                        done();
                    });
            });
        });
    });

    testGroupName = "new User - all routes"
    describe(testGroupName, () => {
        it(`${testNumber++} ${testGroupName} - Register User - 201`, (done) => {
            chai
                .request(app)
                .post(`${apiVersionUrl}/users`)
                .send(newUser)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(6);

                    res.body.should.have.property('_id');
                    res.body.should.have.property('username');
                    res.body.should.have.property('email');
                    res.body.should.have.property('firstname');
                    res.body.should.have.property('lastname');
                    res.body.should.have.property('state');

                    expect(ObjectId.isValid(res.body._id)).to.equal(true);
                    newUser._id = res.body._id;
                    expect(res.body.state).to.equal('inactive');

                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - Login - 200`, (done) => {
            chai
                .request(app)
                .post(`${apiVersionUrl}/login`)
                .send({ "username": newUser["username"], "password": newUser["password"] })
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(1);
                    res.body.should.have.property('token');
                    newUserToken = res.body.token;
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - whoIAm - 200`, (done) => {
            chai
                .request(app)
                .get(`${apiVersionUrl}/whoIAm`)
                .set(authKey, newUserToken)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(7);
                    res.body.should.have.property('username');
                    expect(res.body.username).to.equal(newUser.username);
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - Logout - 200`, (done) => {
            chai
                .request(app)
                .post(`${apiVersionUrl}/logout`)
                .set(authKey, newUserToken)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - Invalid token (logout) - 401`, (done) => {
            chai
                .request(app)
                .get(`${apiVersionUrl}/whoIAm`)
                .set(authKey, newUserToken)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(401);
                    done();
                });
        });
        
        it(`${testNumber++} ${testGroupName} - Login with email - 200`, (done) => {
            chai
                .request(app)
                .post(`${apiVersionUrl}/login`)
                .send({ "username": newUser["email"], "password": newUser["password"] })
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(1);
                    res.body.should.have.property('token');
                    newUserToken = res.body.token;
                    done();
                });
        });
    })
});