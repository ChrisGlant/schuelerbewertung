'use strict'
import chai from 'chai';
import chaiHttp from 'chai-http';
import app from '../server.js';

import { healthCheck } from './00-startup-hook.js';

const { expect } = chai;

// endpoints
const loginEndpoint = '/api/v1/login';
const userEndpoint = '/api/v1/users';
const pupilEndpointToTest = '/api/v1/pupils';

// data variables

let user = {
    username: "pupilUser",
    email: "pupilUser@gmail.com",
    password: "pswrd",
    lastname: "mock-data",
    firstname: "pupil"
}

let secUser = {
    username: "pupilUser2",
    email: "pupilUser2@gmail.com",
    password: "pswrd",
    lastname: "mock-data",
    firstname: "pupil"
}

let pupilToCreate = {
    "state": "active",
    "firstname": "Nurbert",
    "lastname": "Raissl",
    "mail": "riasslbauer.nurbert@gmx.at",
    "notes": "string"
}

// other variables (keys, ids)

let neededKeys = ['_id', 'state', 'firstname', 'lastname', 'mail', 'notes'];
let notFoundId = '61a8ea9bd4022b3534077c0d'

// setup

chai.use(chaiHttp);
chai.should();

// hook - done() fires, if healthcheck point answers with 200
before(() => {
    return healthCheck(app);
});

describe(`CRUD Tests for ${pupilEndpointToTest}`, () => {
    let testGroupName;
    let testNumber = 5000;

    testGroupName = 'Basic user stuff'
    describe(testGroupName, () => {
        it(`${testNumber++} ${testGroupName} - No Auth. header- 401 `, done => {
            chai
                .request(app)
                .get(pupilEndpointToTest)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(401);
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - Create first user - 201`, done => {
            chai
                .request(app)
                .post(userEndpoint)
                .send(user)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(201);
                    res.body.should.be.a('object');

                    res.body.should.have.property('_id').not.to.be.null;
                    res.body.should.have.property('username').not.to.be.null;
                    res.body.should.have.property('state').not.to.be.null;
                    res.body.should.have.property('firstname').not.to.be.null;
                    res.body.should.have.property('lastname').not.to.be.null;

                    user._id = res.body._id;
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - Login first user - 200`, (done) => {
            chai
                .request(app)
                .post(loginEndpoint)
                .send({ 'username': user['username'], 'password': user['password'] })
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(1);
                    res.body.should.have.property('token');
                    user.token = res.body.token;
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - Create second user - 201`, done => {
            chai
                .request(app)
                .post(userEndpoint)
                .send(secUser)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(201);
                    res.body.should.be.a('object');

                    res.body.should.have.property('_id').not.to.be.null;
                    res.body.should.have.property('username').not.to.be.null;
                    res.body.should.have.property('state').not.to.be.null;
                    res.body.should.have.property('firstname').not.to.be.null;
                    res.body.should.have.property('lastname').not.to.be.null;

                    secUser._id = res.body._id;
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - Login second user - 200`, (done) => {
            chai
                .request(app)
                .post(loginEndpoint)
                .send({ 'username': secUser['username'], 'password': secUser['password'] })
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(1);
                    res.body.should.have.property('token');
                    secUser.token = res.body.token;
                    done();
                });
        });
    })

    testGroupName = 'POST'
    describe(testGroupName, () => {
        describe('check basic test cases', () => {
            // create
            it(`${testNumber++} ${testGroupName} - create pupil - 201`, done => {
                chai
                    .request(app)
                    .post(pupilEndpointToTest)
                    .set('Authorization', user.token)
                    .send(pupilToCreate)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(201);
                        res.body.should.be.a('object');

                        neededKeys.forEach(key => {
                            res.body.should.have.property(key).to.not.be.null
                        })

                        pupilToCreate._id = res.body._id;
                        done();
                    });
            });
            // get created
            it(`${testNumber++} ${testGroupName} - GET created pupil - 200 `, done => {
                chai
                    .request(app)
                    .get(pupilEndpointToTest + '/' + pupilToCreate._id)
                    .set('Authorization', user.token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        res.body.should.be.a('object');

                        neededKeys.forEach(key => res.body.should.have.property(key).to.not.be.null)
                        expect(res.body._id).to.be.equal(pupilToCreate._id);
                        done();
                    });
            });
            // delet created
            it(`${testNumber++} ${testGroupName} - delete created pupil - 204`, done => {
                chai
                    .request(app)
                    .delete(pupilEndpointToTest + '/' + pupilToCreate._id)
                    .set('Authorization', user.token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(204);

                        pupilToCreate._id = undefined;
                        done();
                    });
            });

            /* --------------------- property tests --------------------- */

            /* --------------------- id --------------------- */
            describe('check property id ', () => {
                let wrongPupils = [];

                let pupil = { ...pupilToCreate };
                pupil._id = {};
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil._id = 12432434;
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil._id = 'JALKJ23kJ21';
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil._id = notFoundId;
                wrongPupils.push(pupil);

                wrongPupils.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - id - 400`, done => {
                        chai
                            .request(app)
                            .patch(pupilEndpointToTest + '/' + pupilToCreate._id)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- state -------------------------- */
            describe('check property state ', () => {
                let wrongPupils = [];

                let pupil = { ...pupilToCreate };
                pupil.state = 'test'
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.state = 12344
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.state = {}
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.state = []
                wrongPupils.push(pupil);

                wrongPupils.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - state - 400`, done => {
                        chai
                            .request(app)
                            .post(pupilEndpointToTest)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- notes -------------------------- */
            describe('check property notes ', () => {
                let wrongPupils = [];

                let pupil = { ...pupilToCreate };
                pupil.notes = {}
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.notes = []
                wrongPupils.push(pupil);

                wrongPupils.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - notes - 400`, done => {
                        chai
                            .request(app)
                            .post(pupilEndpointToTest)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- firstname -------------------------- */
            describe('check property firstname ', () => {
                let wrongPupils = [];

                let pupil = { ...pupilToCreate };
                pupil.firstname = {}
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.firstname = []
                wrongPupils.push(pupil);

                wrongPupils.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - firstname - 400`, done => {
                        chai
                            .request(app)
                            .post(pupilEndpointToTest)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- lastname -------------------------- */
            describe('check property lastname ', () => {
                let wrongPupils = [];

                let pupil = { ...pupilToCreate };
                pupil.lastname = {}
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.lastname = []
                wrongPupils.push(pupil);

                wrongPupils.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - lastname - 400`, done => {
                        chai
                            .request(app)
                            .post(pupilEndpointToTest)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- mail -------------------------- */
            describe('check property mail ', () => {
                let wrongPupils = [];

                let pupil = { ...pupilToCreate };
                pupil.mail = 12344;
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.mail = {};
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.mail = [];
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.mail = '@gmail.com';
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.mail = 'test@gmail.';
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.mail = 'test@.com';
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.mail = '@gmail.';
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.mail = 'test@.';
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.mail = '@.com';
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.mail = '@.';
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.mail = 'test';
                wrongPupils.push(pupil);

                wrongPupils.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - mail - 400`, done => {
                        chai
                            .request(app)
                            .post(pupilEndpointToTest)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- userId -------------------------- */
            describe('check property userId ', () => {
                let wrongPupils = [];

                let pupil = { ...pupilToCreate };
                pupil.userId = notFoundId
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.userId = '1870'
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.userId = 12344;
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.userId = [];
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.userId = {};
                wrongPupils.push(pupil);

                wrongPupils.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - userId - 400`, done => {
                        chai
                            .request(app)
                            .post(pupilEndpointToTest)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- missing/not allowed properies-------------------------- */
            describe('missing/not allowed properies', () => {
                let wrongPupils = [];

                let pupil = { ...pupilToCreate };
                delete pupil.firstname;
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                delete pupil.firstname;
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                delete pupil.lastname;
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                delete pupil.mail;
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.notAllowed = 'hihihi'
                wrongPupils.push(pupil);

                wrongPupils.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - missing/not allowed properies - 400`, done => {
                        chai
                            .request(app)
                            .post(pupilEndpointToTest)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });
        });
    });

    testGroupName = 'GET SINGLE'
    describe(testGroupName, () => {
        // create
        it(`${testNumber++} ${testGroupName} - create pupil - 201`, done => {
            chai
                .request(app)
                .post(pupilEndpointToTest)
                .set('Authorization', user.token)
                .send(pupilToCreate)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(201);
                    res.body.should.be.a('object');

                    neededKeys.forEach(key => {
                        res.body.should.have.property(key).to.not.be.null
                    })

                    pupilToCreate._id = res.body._id;
                    done();
                });
        });
        // no auth. header
        it(`${testNumber++} ${testGroupName} - no auth. header - 401 `, done => {
            chai
                .request(app)
                .get(pupilEndpointToTest + '/' + pupilToCreate._id)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(401);
                    done();
                });
        });
        // get created
        it(`${testNumber++} ${testGroupName} - GET created pupil - 200 `, done => {
            chai
                .request(app)
                .get(pupilEndpointToTest + '/' + pupilToCreate._id)
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');

                    neededKeys.forEach(key => res.body.should.have.property(key).to.not.be.null)
                    expect(res.body._id).to.be.equal(pupilToCreate._id);
                    done();
                });
        });
        // get not found
        it(`${testNumber++} ${testGroupName} - not found - 404 `, done => {
            chai
                .request(app)
                .get(pupilEndpointToTest + '/' + notFoundId)
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(404);
                    done();
                });
        });
        // get with other user
        it(`${testNumber++} ${testGroupName} - get with user 2 - 404 `, done => {
            chai
                .request(app)
                .get(pupilEndpointToTest + '/' + pupilToCreate._id)
                .set('Authorization', secUser.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(404);
                    done();
                });
        });
        // get with invalid object id  
        it(`${testNumber++} ${testGroupName} - invalid ObjectId - 400`, done => {
            chai
                .request(app)
                .get(pupilEndpointToTest + '/1870')
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(400);
                    done();
                })
        });
    });

    testGroupName = 'GET ALL';
    describe(testGroupName, () => {
        // no auth. header
        it(`${testNumber++} ${testGroupName} - no auth. header - 401`, done => {
            chai
                .request(app)
                .get(pupilEndpointToTest)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(401);
                    done();
                });
        });
        // get all with first user
        it(`${testNumber++} ${testGroupName} - first user - 200`, done => {
            chai
                .request(app)
                .get(pupilEndpointToTest)
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;

                    res.should.have.status(200);
                    res.body.should.be.a('array');

                    expect(res.body.length).to.equal(1);

                    done();
                });
        });
        // get all with second user
        it(`${testNumber++} ${testGroupName} - get with user 2 - 200 `, done => {
            chai
                .request(app)
                .get(pupilEndpointToTest)
                .set('Authorization', secUser.token)
                .end((err, res) => {
                    expect(err).to.be.null;

                    res.should.have.status(200);
                    res.body.should.be.a('array');

                    expect(res.body.length).to.equal(0);
                    done();
                });
        });
    });

    testGroupName = 'PATCH';
    describe(testGroupName, () => {
        describe('basic test cases', () => {
            // no auth. header
            it(`${testNumber++} ${testGroupName} - No Auth. header - 401`, done => {
                chai
                    .request(app)
                    .patch(pupilEndpointToTest + '/' + pupilToCreate._id)
                    .send({ ...pupilToCreate })
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(401);
                        done();
                    });
            });
            // patch with invalid objectid
            it(`${testNumber++} ${testGroupName} - invalid ObjectId - 400`, done => {
                chai
                    .request(app)
                    .patch(pupilEndpointToTest + '/1870')
                    .send({ ...pupilToCreate })
                    .set('Authorization', user.token)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(400);
                        done();
                    });
            });
            // patch with not found
            it(`${testNumber++} ${testGroupName} - not found - 404`, done => {
                let pupil = { ...pupilToCreate };
                delete pupil._id;
                chai
                    .request(app)
                    .patch(pupilEndpointToTest + '/' + notFoundId)
                    .set('Authorization', user.token)
                    .send(pupil)
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(404);
                        done();
                    });
            });
            // patch with second user
            it(`${testNumber++} ${testGroupName} - with other user - 404`, done => {
                chai
                    .request(app)
                    .patch(pupilEndpointToTest + '/' + pupilToCreate._id)
                    .set('Authorization', secUser.token)
                    .send({ ...pupilToCreate })
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(404);
                        done();
                    });
            });
            // valid patch
            it(`${testNumber++} ${testGroupName} - valid patch - 200`, done => {
                chai
                    .request(app)
                    .patch(pupilEndpointToTest + '/' + pupilToCreate._id)
                    .set('Authorization', user.token)
                    .send({ firstname: 'test' })
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);
                        done();
                    });
            });
        });

        describe('property tests', () => {
            /* --------------------- id --------------------- */
            describe('check property id ', () => {
                let wrongPupils = [];

                let pupil = { ...pupilToCreate };
                pupil._id = {};
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil._id = 12432434;
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil._id = 'JALKJ23kJ21';
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil._id = notFoundId;
                wrongPupils.push(pupil);

                wrongPupils.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - id - 400`, done => {
                        chai
                            .request(app)
                            .patch(pupilEndpointToTest + '/' + pupilToCreate._id)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- state -------------------------- */
            describe('check property state ', () => {
                let wrongPupils = [];

                let pupil = { ...pupilToCreate };
                pupil.state = 'test'
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.state = 12344
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.state = {}
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.state = []
                wrongPupils.push(pupil);

                wrongPupils.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - state - 400`, done => {
                        chai
                            .request(app)
                            .patch(pupilEndpointToTest + '/' + pupilToCreate._id)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- notes -------------------------- */
            describe('check property notes ', () => {
                let wrongPupils = [];

                let pupil = { ...pupilToCreate };
                pupil.notes = {}
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.notes = []
                wrongPupils.push(pupil);

                wrongPupils.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - notes - 400`, done => {
                        chai
                            .request(app)
                            .patch(pupilEndpointToTest + '/' + pupilToCreate._id)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- firstname -------------------------- */
            describe('check property firstname ', () => {
                let wrongPupils = [];

                let pupil = { ...pupilToCreate };
                pupil.firstname = {}
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.firstname = []
                wrongPupils.push(pupil);

                wrongPupils.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - firstname - 400`, done => {
                        chai
                            .request(app)
                            .patch(pupilEndpointToTest + '/' + pupilToCreate._id)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- lastname -------------------------- */
            describe('check property lastname ', () => {
                let wrongPupils = [];

                let pupil = { ...pupilToCreate };
                pupil.lastname = {}
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.lastname = []
                wrongPupils.push(pupil);

                wrongPupils.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - lastname - 400`, done => {
                        chai
                            .request(app)
                            .patch(pupilEndpointToTest + '/' + pupilToCreate._id)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- mail -------------------------- */
            describe('check property mail ', () => {
                let wrongPupils = [];

                let pupil = { ...pupilToCreate };
                pupil.mail = 12344;
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.mail = {};
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.mail = [];
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.mail = '@gmail.com';
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.mail = 'test@gmail.';
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.mail = 'test@.com';
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.mail = '@gmail.';
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.mail = 'test@.';
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.mail = '@.com';
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.mail = '@.';
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.mail = 'test';
                wrongPupils.push(pupil);

                wrongPupils.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - mail - 400`, done => {
                        chai
                            .request(app)
                            .patch(pupilEndpointToTest + '/' + pupilToCreate._id)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- userId -------------------------- */
            describe('check property userId ', () => {
                let wrongPupils = [];

                let pupil = { ...pupilToCreate };
                pupil.userId = notFoundId
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.userId = '1870'
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.userId = 12344;
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.userId = [];
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil.userId = {};
                wrongPupils.push(pupil);

                wrongPupils.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - userId - 400`, done => {
                        chai
                            .request(app)
                            .patch(pupilEndpointToTest + '/' + pupilToCreate._id)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });

            /* --------------------------- missing/not allowed properies-------------------------- */
            describe('missing/not allowed properies', () => {
                let wrongPupils = [];

                let pupil = { ...pupilToCreate };
                pupil.notAllowed = 'hihihi'
                wrongPupils.push(pupil);

                pupil = { ...pupilToCreate };
                pupil = {}
                wrongPupils.push(pupil);

                wrongPupils.forEach(we => {
                    it(`${testNumber++} ${testGroupName} - missing/not allowed properies - 400`, done => {
                        chai
                            .request(app)
                            .patch(pupilEndpointToTest + '/' + pupilToCreate._id)
                            .set('Authorization', user.token)
                            .send(we)
                            .end((err, res) => {
                                expect(err).to.be.null;
                                res.should.have.status(400);
                                done();
                            });
                    });
                });
            });
        });
    });

    testGroupName = 'DELETE';
    describe(testGroupName, () => {
        // no auth. header
        it(`${testNumber++} ${testGroupName} - No Auth. header - 401`, done => {
            chai
                .request(app)
                .delete(pupilEndpointToTest + '/' + pupilToCreate._id)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(401);
                    done();
                });
        });
        // delete with invalid objectid
        it(`${testNumber++} ${testGroupName} - invalid ObjectId - 400`, done => {
            chai
                .request(app)
                .delete(pupilEndpointToTest + '/1870')
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(400);
                    done();
                })
        });
        // delete with not found
        it(`${testNumber++} ${testGroupName} - not found - 404`, done => {
            chai
                .request(app)
                .delete(pupilEndpointToTest + '/' + notFoundId)
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(404);
                    done();
                })
        });
        // delete with other user
        it(`${testNumber++} ${testGroupName} - with other user - 404`, done => {
            chai
                .request(app)
                .delete(pupilEndpointToTest + '/' + pupilToCreate._id)
                .set('Authorization', secUser.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(404);
                    done();
                });
        });
        // successfull delete
        it(`${testNumber++} ${testGroupName} - successful delete - 204`, done => {
            chai
                .request(app)
                .delete(pupilEndpointToTest + '/' + pupilToCreate._id)
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(204);
                    done();
                })
        });
        // get deleted
        it(`${testNumber++} ${testGroupName} - get deleted item - 404`, done => {
            chai
                .request(app)
                .get(pupilEndpointToTest + '/' + pupilToCreate._id)
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(404);
                    done();
                });
        });
        // delete again
        it(`${testNumber++} ${testGroupName} - delete again - 204`, done => {
            chai
                .request(app)
                .delete(pupilEndpointToTest + '/' + pupilToCreate._id)
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(404);
                    done();
                })
        });
    });
});