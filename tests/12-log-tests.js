'use strict'
import chai from 'chai';
import chaiHttp from 'chai-http';
import app from '../server.js';

import { healthCheck } from './00-startup-hook.js';

const { expect } = chai;

// endpoints
const loginEndpoint = '/api/v1/login';
const userEndpoint = '/api/v1/users';
const periodEndpoint = '/api/v1/periods'
const logentriesEndpointToTest = '/api/v1/logentries';

// data variables

let user = {
    username: "logUser",
    email: "logUser@gmail.com",
    password: "pswrd",
    lastname: "mock-data",
    firstname: "log"
}

let secUser = {
    username: "logUser2",
    email: "logUser2@gmail.com",
    password: "pswrd",
    lastname: "mock-data",
    firstname: "log"
}

let periodToCreate = {
    'label': 'Schuljahr 2021121/22',
    'startdate': new Date('01.09.10021'),
    'enddate': new Date('01.07.10022'),
    'state': 'active'
};

let periodToCreateSecUser = {
    'label': 'Schuljahr 2021121/22',
    'startdate': new Date('01.09.10021'),
    'enddate': new Date('01.07.10022'),
    'state': 'active'
};

// other variables (keys, ids)

let neededKeysPeriod = ['_id', 'label', 'startdate', 'enddate', 'state']
let logCounter = 0, logCounterSecUser = 0;
const logLength = 12;
const maxLogLength = 10;

// setup

chai.use(chaiHttp);
chai.should();

// hook - done() fires, if healthcheck point answers with 200
before(() => {
    return healthCheck(app);
});

describe(`CRUD Tests for ${logentriesEndpointToTest}`, () => {
    let testGroupName;
    let testNumber = 12000;

    testGroupName = 'Basic user stuff'
    describe(testGroupName, () => {
        it(`${testNumber++} ${testGroupName} - No Auth. header- 401 `, done => {
            chai
                .request(app)
                .get(logentriesEndpointToTest)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(401);
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - Create first user - 201`, done => {
            chai
                .request(app)
                .post(userEndpoint)
                .send(user)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(201);
                    res.body.should.be.a('object');

                    res.body.should.have.property('_id').not.to.be.null;
                    res.body.should.have.property('username').not.to.be.null;
                    res.body.should.have.property('state').not.to.be.null;
                    res.body.should.have.property('firstname').not.to.be.null;
                    res.body.should.have.property('lastname').not.to.be.null;

                    user._id = res.body._id;
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - Login first user - 200`, (done) => {
            chai
                .request(app)
                .post(loginEndpoint)
                .send({ 'username': user['username'], 'password': user['password'] })
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(1);
                    res.body.should.have.property('token');
                    user.token = res.body.token;
                    logCounter++;
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - Create second user - 201`, done => {
            chai
                .request(app)
                .post(userEndpoint)
                .send(secUser)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(201);
                    res.body.should.be.a('object');

                    res.body.should.have.property('_id').not.to.be.null;
                    res.body.should.have.property('username').not.to.be.null;
                    res.body.should.have.property('state').not.to.be.null;
                    res.body.should.have.property('firstname').not.to.be.null;
                    res.body.should.have.property('lastname').not.to.be.null;

                    secUser._id = res.body._id;
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - Login second user - 200`, (done) => {
            chai
                .request(app)
                .post(loginEndpoint)
                .send({ 'username': secUser['username'], 'password': secUser['password'] })
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    Object.keys(res.body).length.should.eql(1);
                    res.body.should.have.property('token');
                    secUser.token = res.body.token;
                    logCounterSecUser++;
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - POST period first user - 201`, done => {
            chai
                .request(app)
                .post(periodEndpoint)
                .send(periodToCreate)
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(201);
                    res.body.should.be.a('object');

                    neededKeysPeriod.forEach(key => {
                        res.body.should.have.property(key).to.not.be.null
                    })

                    periodToCreate._id = res.body._id;
                    logCounter++;
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - POST period second user - 201`, done => {
            chai
                .request(app)
                .post(periodEndpoint)
                .send(periodToCreateSecUser)
                .set('Authorization', secUser.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(201);
                    res.body.should.be.a('object');

                    neededKeysPeriod.forEach(key => {
                        res.body.should.have.property(key).to.not.be.null
                    })

                    periodToCreateSecUser._id = res.body._id;
                    logCounterSecUser++;
                    done();
                });
        });
    })

    testGroupName = 'LOGGING';
    describe(testGroupName, () => {
        it(`${testNumber++} ${testGroupName} - GET logs first user`, done => {
            chai
                .request(app)
                .get(logentriesEndpointToTest)
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);

                    expect(res.body.length).to.equal(logCounter);
                    expect(res.body[0].logType).to.equal('login')
                    expect(res.body[1].logType).to.equal('create')

                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - GET logs sec user`, done => {
            chai
                .request(app)
                .get(logentriesEndpointToTest)
                .set('Authorization', secUser.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);

                    expect(res.body.length).to.equal(logCounterSecUser)
                    expect(res.body[0].logType).to.equal('login')
                    expect(res.body[1].logType).to.equal('create')

                    done();
                });
        });

        for (let counter = 0; counter < logLength; counter++) {
            it(`${testNumber++} ${testGroupName} - PATCH period first user`, done => {
                chai
                    .request(app)
                    .patch(periodEndpoint + '/' + periodToCreate._id)
                    .set('Authorization', user.token)
                    .send({ label: 'test1' })
                    .end((err, res) => {
                        expect(err).to.be.null;
                        res.should.have.status(200);

                        neededKeysPeriod.forEach(key => {
                            res.body.should.have.property(key).to.not.be.null
                        })

                        logCounter++;
                        done();
                    });
            });
        }

        it(`${testNumber++} ${testGroupName} - PATCH period sec user`, done => {
            chai
                .request(app)
                .patch(periodEndpoint + '/' + periodToCreateSecUser._id)
                .set('Authorization', secUser.token)
                .send({ label: 'test1' })
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);

                    neededKeysPeriod.forEach(key => {
                        res.body.should.have.property(key).to.not.be.null
                    })

                    logCounterSecUser++;
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - GET logs first user`, done => {
            chai
                .request(app)
                .get(logentriesEndpointToTest)
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);

                    expect(res.body.length).to.equal(logCounter - (logLength - maxLogLength));
                    expect(res.body[0].logType).to.equal('login')
                    expect(res.body[1].logType).to.equal('create')
                    expect(res.body[2].logType).to.equal('update')

                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - GET logs sec user`, done => {
            chai
                .request(app)
                .get(logentriesEndpointToTest)
                .set('Authorization', secUser.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);

                    expect(res.body.length).to.equal(logCounterSecUser)
                    expect(res.body[0].logType).to.equal('login')
                    expect(res.body[1].logType).to.equal('create')
                    expect(res.body[2].logType).to.equal('update')

                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - DELETE period first user`, done => {
            chai
                .request(app)
                .delete(periodEndpoint + '/' + periodToCreate._id)
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(204);
                    logCounter++;
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - DELETE period sec user`, done => {
            chai
                .request(app)
                .delete(periodEndpoint + '/' + periodToCreateSecUser._id)
                .set('Authorization', secUser.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(204);
                    logCounterSecUser++;
                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - GET logs first user`, done => {
            chai
                .request(app)
                .get(logentriesEndpointToTest)
                .set('Authorization', user.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);

                    expect(res.body.length).to.equal(logCounter - (logLength - maxLogLength))
                    expect(res.body[0].logType).to.equal('login')
                    expect(res.body[1].logType).to.equal('create')
                    expect(res.body[2].logType).to.equal('update')
                    expect(res.body[2 + maxLogLength].logType).to.equal('delete')

                    done();
                });
        });

        it(`${testNumber++} ${testGroupName} - GET logs sec user`, done => {
            chai
                .request(app)
                .get(logentriesEndpointToTest)
                .set('Authorization', secUser.token)
                .end((err, res) => {
                    expect(err).to.be.null;
                    res.should.have.status(200);

                    expect(res.body.length).to.equal(logCounterSecUser)
                    expect(res.body[0].logType).to.equal('login')
                    expect(res.body[1].logType).to.equal('create')
                    expect(res.body[2].logType).to.equal('update')
                    expect(res.body[3].logType).to.equal('delete')

                    done();
                });
        });

        describe('each logtype', () => {
            let logTypes = ['login', 'create', 'update', 'delete']

            logTypes.forEach(logType => {
                it(`${testNumber++} ${testGroupName} - GET logs ${logType} first user`, done => {
                    chai
                        .request(app)
                        .get(logentriesEndpointToTest + '?' + logType)
                        .set('Authorization', user.token)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(200);

                            expect(res.body.length).to.equal((logType == 'update') ? maxLogLength : 1)
                            expect(res.body[0].logType).to.equal(logType)

                            done();
                        });
                });

                it(`${testNumber++} ${testGroupName} - GET logs ${logType} sec user`, done => {
                    chai
                        .request(app)
                        .get(logentriesEndpointToTest + '?' + logType)
                        .set('Authorization', secUser.token)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(200);

                            expect(res.body.length).to.equal(1)
                            expect(res.body[0].logType).to.equal(logType)

                            done();
                        });
                });
            })
        })

        describe('invalid query strings', () => {
            let invalidQuerys = []

            let query = 'notallowed=hihi'
            invalidQuerys.push(query)

            query = 'notallowed=hihi&create'
            invalidQuerys.push(query)

            query = 'create=notAllowed'
            invalidQuerys.push(query)

            invalidQuerys.forEach(query => {
                it(`${testNumber++} ${testGroupName} - invalid query - 400`, done => {
                    chai
                        .request(app)
                        .get(logentriesEndpointToTest + '?' + query)
                        .set('Authorization', secUser.token)
                        .end((err, res) => {
                            expect(err).to.be.null;
                            res.should.have.status(400);
                            done();
                        });
                });
            })
        })
    });
});