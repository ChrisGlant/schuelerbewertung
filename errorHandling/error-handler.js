import mongoose from 'mongoose';
import AppError from './AppError.js';
import { log } from '../logging/app-logger.js';

export const defaultError = (err, req, res, next) => {
  res
    .status(500)
    .send('Problems at start up - contact the admin or use the healthcheck ');
};

export const globalErrorHandler = (err, req, res, next) => {
  let errorHandled = handleAppSpecificError(err, req, res, next);

  if (!errorHandled) {
    log.error(err);
    res.status(500).send(getErrorObj('Something broke!', 500));
    errorHandled = true;
    return;
  }
};

function handleAppSpecificError(err, req, res, next) {
  let errorHandled = false;

  if (!errorHandled && err instanceof AppError) {
    res.status(err.statusCode).send(getErrorObj(err));
    errorHandled = true;
  }

  if (!errorHandled && err instanceof mongoose.Error.ValidationError) {
    res.status(400).send(getErrorObj(err));
    errorHandled = true;
  }

  if (!errorHandled && err.name == 'MongoError' && err.code == 11000) {
    let errMsg = 'This is a duplicate Key Error';
    let key = Object.keys(err.keyValue)[0];
    let value = key ? err.keyValue[key] : '';

    errMsg = `Object with "${key}" "${value}" already exists.`;
    res.status(400).send(getErrorObj(errMsg));
    errorHandled = true;
  }

  if (!errorHandled && (err.name == 'JsonWebTokenError' || err.name == 'TokenExpiredError' || err.name == 'NotBeforeError')) {
    res.status(401).send(getErrorObj('Contact Adminstrator - the exception goes from here bis klagenfurt - howdy partner - ' + err.message, 401));
    errorHandled = true;
  }

  return errorHandled;
}

function getErrorObj(err, statusCode = 400) {
  let object = {};
  object.statusCode = err.statusCode || statusCode;
  object.message = err.msg || err.message || err;
  return object;
}

export const handleAsyncError = (callback) => {
  return (req, res, next) => {
    callback(req, res, next).catch((err) => next(err));
  }
}
