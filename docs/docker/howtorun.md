# How to Run
## Docker
### Build Image

	docker build . -t {image_name}

### Pull Image
	docker pull htldevzone.azurecr.io/5ahif/team-a/backend:main

### Run Image
Command must be executed with sample env in same directory

	docker run --name gradeservice-teamA -it --env-file ./.dockerenv -p 8080:8080 {image_identifier}
http://localhost:8080/

## Create Mockdata
POST http://localhost:8080/api/v1/mockdata (no auth required)  
Mockdata is created based on the mockdata files in misc/mock-data
### Testuser Credentials
	username: @k.k  
	password: pswrd
## env file
#### HOST
Host of Application
#### PORT
Port of Application (both Frontend and Backend)
#### MONGO_CONNECTION_STRING
Connection string for database, has to start with mongodb://
#### FRONTEND_URL
CORS Request URL
#### TOKEN_SECRET
JWT Token Secret
#### EXPIRATION_SECONDS
Seconds for expiration of Recovery Token
#### NODEMAILER_SERVICE
Nodemailer Service Provider
#### NODEMAILER_USER
Password Recovery Account E-Mail
#### NODEMAILER_PASS
Google Access Token 


