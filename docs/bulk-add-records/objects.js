let bulkAddRequest = {
    "courseId": "objectid",
    "validOn": "2020.01.01",
    "weight": 50,
    "tags": [
        "Schularbeit November"
    ],
    "records": [
        {
            "pupilId": "objectid",
            "ratingId": "objectid",
            "tags": [
                "negativ"
            ],
            "comment": "knapp"
        },
        {
            "pupilId": "objectid",
            "pupilNotPresent": true,
            "comment": "krank"
        }
    ]

}