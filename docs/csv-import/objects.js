let firstCall = [
    {
        "mail": "raphael@artl.com",
        "firstname": "Raphael",
        "lastname": "Artl",
        "state": "active",
        "courses": [
            "5AHIF SYP",
            "5AHIF NVS"
        ]
    },
    {
        "mail": "glant@chris.com",
        "firstname": "Christoph",
        "lastname": "Glantschnig",
        "state": "active",
        "notes": "Adlerwarte Landskron",
        "courses": [
            "5AHIF DBI"
        ]
    },
    //will be aborted by the user:
    {
        "mail": "leon@hueber.com",
        "firstname": "Leon",
        "lastname": "Hueber"
    }
];

let firstResponse = [
    {
        "state": "to-import",
        "object": {
            "mail": "raphael@artl.com",
            "firstname": "Raphael",
            "lastname": "Artl",
            "state": "active",
            "courses": [
                "5AHIF SYP",
                "5AHIF NVS"
            ]
        }
    },
    {
        "state": "action-required",
        "actions": {
            //pupil alread existing, fields to update:
            "update": [
                "notes"
            ],
            // given course not existing and to create:
            "coursesToCreate": [
                "5AHIF DBI"
            ]
        },
        "object": {
            "mail": "glant@chris.com",
            "firstname": "Christoph",
            "lastname": "Glantschnig",
            "state": "active",
            "notes": "Adlerwarte Landskron",
            "courses": [
                "5AHIF NVS",
                "5AHIF DBI"
            ]
        }
    },
    {
        "state": "to-import",
        "object": {
            "mail": "leon@hueber.com",
            "firstname": "Leon",
            "lastname": "Hueber"
        }
    }
];

let secondCall = [
    {
        "mail": "raphael@artl.com",
        "firstname": "Raphael",
        "lastname": "Artl",
        "state": "active",
        "courses": [
            "5AHIF NVS"
        ]
    },
    // confirmed that notes should be updated and course "5AHIF DBI" should be created
    {
        "mail": "glant@chris.com",
        "firstname": "Christoph",
        "lastname": "Glantschnig",
        "state": "active",
        "notes": "Adlerwarte Landskron",
        "courses": [
            "5AHIF NVS",
            "5AHIF DBI"
        ]
    }
    // leon hueber missing because user aborted import
];

let secondResponse = [
    {
        "state": "active",
        "_id": "61ccce3f6ee3405e645d3e38",
        "firstname": "Raphael",
        "lastname": "Artl",
        "mail": "raphael@artl.com",
        "userId": "61ccce166ee3405e645d3e2a"
    },
    {
        "state": "active",
        "notes": "Adlerwarte Landskron",
        "_id": "61ccce7b6ee3405e645d3e64",
        "mail": "glant@chris.com",
        "firstname": "Christoph",
        "lastname": "Glantschnig"
    },
    {
        "state": "active",
        "notes": "",
        "_id": "61ccce7b6ee3405e645d3e6c",
        "mail": "leon@hueber.com",
        "firstname": "Leon",
        "lastname": "Hueber"
    }
];

let possibleActionsAndStates = {
    "state": [
        "to-import",
        "actions-necessary",
        "imported",
        "imported-with-actions"
    ],
    "actions": {
        "update": [
            "fields that need to be updated"
        ],
        "coursesToCreate": [
            "courses that need to be created"
        ],
        "error": {
            "error_field": "error-msg"
        }
    }
}